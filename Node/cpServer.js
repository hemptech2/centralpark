/************************************************************ 
  This script is utilized by CP index.html
  It returns all the order results from the database.

  Update:
  =======
  Server crashing due to read/write errors.
  This file only write to the database via the POST method.
  The webpage gets its data from another source file.
 ************************************************************/

var http = require("http");
var MariaDB = require("mysql2");
//var CPSelect;
Tmpvar = "";
var connection;
//CPSendback = "";
 
function Setup() {

     connection = MariaDB.createConnection({

     host: "localhost",
     user: "root",
     password: "CPword1",
     database: "CP"
  });
  connection.connect();
  //console.log("Connection to Database made");

}


function onRequest(request, response){

   if (request.method == "GET") {

      Setup();
      //console.log(request.url);
      var tmp1 = request.url.split("/?");
      var CPSelect = parseInt(tmp1[1]);
//      console.log(CPSelect);

      var DB_Str = "";

      switch (CPSelect) {
        case 1:

           DB_Str = "SELECT * FROM order1";
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); } 
              var qryChk = rows;
          
              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                global.CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});                   
                connection.destroy();
                response.end(CPSendback);             
              }
              else {
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                //response.write(global.CPSendback, function(){
                   //response.end();
                //});                   
                connection.destroy();
                response.end(CPSendback);
              }

           }); 
           break;

        case 2: 
                 
           DB_Str = "SELECT uid, window, btype FROM order1 " +
                    "WHERE bcomplete = 'NO' AND btype != 'none' LIMIT 16";
   
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); }
           //throw error;

              var CPSendback = "";
              var qryChk = rows;
              
              //if(rows[0] == undefined){
              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                             
              }

              else {
                  
                for(var i=0; i<rows.length; i++) {              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].btype;                                           
                }
           
                //console.log(global.CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                  
              }        
            }); 
            break;

        case 3:        

           DB_Str = "SELECT uid, window, wtype FROM order1 " +
                    "WHERE wcomplete = 'NO' AND wtype != 'none' LIMIT 3";
   
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); }
           //throw error;

              var CPSendback = "";
              var qryChk = rows;
              
              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                //response.write(CPSendback, function (){
                   //response.end();
                //});         
                connection.destroy();
                response.end(CPSendback);                
              }

              else {
                  
                for(var i=0; i<rows.length; i++) {              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].wtype;                                           
                }
           
                //console.log(global.CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function (){
                   //response.end();
                //});                     
                connection.destroy();
                response.end(CPSendback);                
              }        
            }); 
            break;

        case 4:        

           //global.CPSendback = "";
           DB_Str = "SELECT uid, window, ntype FROM order1 " +
                    "WHERE ncomplete = 'NO' AND ntype != 'none' LIMIT 3";
   
           connection.query(DB_Str, function (error, rows){
           
              if(error) { console.error(error); }
           //throw error;
              
              var CPSendback="";
              var qryChk = rows;

              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                global.CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                                             
              }

              else {
                  
                for(var i=0; i<rows.length; i++) {              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].ntype;                                           
                }
           
                //console.log(global.CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function (){
                   //response.end();                
                //});
                connection.destroy();
                response.end(CPSendback);                
              }        
            }); 
            break;

        case 5:        
           
           DB_Str = "SELECT uid, window, ftype FROM order1 " +
                    "WHERE fcomplete = 'NO' AND ftype != 'none' LIMIT 8";
   
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); }
           //throw error;

           var CPSendback = "";
           var qryChk = rows;

              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                             
              }

              else {
                  
                for(var i=0; i<rows.length; i++) {              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].ftype;                                           
                }
           
                //console.log(global.CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function(){
                   //response.end();
                //});
                connection.destroy();
                response.end(CPSendback);                  
              }        
            }); 
            break;

        case 6:        

           DB_Str = "SELECT uid, window, dtype FROM order1 " +
                    "WHERE dcomplete = 'NO' AND dtype != 'none' LIMIT 8";
   
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); }
           //throw error;

              var CPSendback = "";
              var qryChk = rows;
                         
              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                             
              }

              else {
                  
                for(var i=0; i<rows.length; i++) {              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].dtype;                                           
                }
           
                //console.log(CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                  
              }        
            }); 
            break;

        case 7:        
           var Ord1 = 'NO';

           /*DB_Str = "SELECT uid, window, samecar, cartype, mOrdCnt FROM order1 " +
                    "WHERE ordcomplete = 'NO' LIMIT 1";*/
           DB_Str = "SELECT uid, window, btype, wtype, ntype, ftype, " +
                    "dtype, samecar, phone, cartype, mOrdCnt FROM order1 " +
                    "WHERE ordcomplete = 'NO' LIMIT 1";  

   
           connection.query(DB_Str, function (error, rows){

              if(error) { console.error(error); }
           //throw error;

              var CPSendback = "";
              var qryChk = rows;              
/*
              if(!rows[0].uid) {
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                response.write(CPSendback);
                connection.destroy();
                response.end();             
              }
*/
              if (qryChk == undefined) { 
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});
                CPSendback = "0&";
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                
              }
              else {
                  
                for(var i=0; i<rows.length; i++) { 
/*              
                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].samecar + "&" + rows[i].cartype +
                                 "&" + rows[i].mOrdCnt;*/

                   CPSendback += "$" + rows[i].uid + "&" + rows[i].window + 
                                 "&" + rows[i].btype + "&" + rows[i].wtype +    
                                 "&" + rows[i].ntype + "&" + rows[i].ftype +
                                 "&" + rows[i].dtype + "&" + rows[i].samecar + 
                                 "&" + rows[i].phone + "&" + rows[i].cartype + 
                                 "&" + rows[i].mOrdCnt;                     
                }
           
                //console.log(CPSendback);
                response.writeHead(200, {"Content-Type": "text/html", "Access-Control-Allow-Origin" : "*"});      
                //response.write(CPSendback, function(){
                   //response.end();
                //});   
                connection.destroy();
                response.end(CPSendback);                  
              }        
            }); 
            break;       

        default:
            break;    
      }
      
   }   

   else {
   // POST Method
     
     var CP_Post_Data = "";
     Setup(); 

     request.on("data", function (data) { CP_Post_Data += data; });
     request.on("end", function () {
       response.writeHead(200, {"Content-Type": "text/plain", "Access-Control-Allow-Origin" : "*"});
       response.end("\n");
 
       var CP_Split = CP_Post_Data.split("&");

       var DB_Str="";
       //console.log(CP_Split);

       if(CP_Split[0] == 1) {
          /* preloading all the object that's going to be used in the database */       

               DB_Str = "INSERT INTO order1 (window, btype, wtype, " +
                        "ntype, ftype, dtype, samecar, phone, " +
                        "cartype, multiorder, mOrdCnt, price, SepBag, otime) " +
                        "VALUE (?,?,?,?,?,?,?,?,?,?,?,?,?,now())";  

//             DB_Str = "INSERT INTO order1 (window,btype,wtype,ntype,ftype,dtype,otime) VALUE (?,?,?,?,?,?,now())";

                                       //Window       Burger     Wings       Nuggest      Fries      Drinks
             connection.query(DB_Str, [CP_Split[1],CP_Split[2],CP_Split[3],CP_Split[4],CP_Split[5],CP_Split[6],CP_Split[7],CP_Split[8],CP_Split[9],CP_Split[10],CP_Split[11],CP_Split[12],CP_Split[13]], function (error, rows){
 
                if(error) { console.error(error); }                  
         
             });
             connection.end();
       }

       else {
                        
          /* preloading all the object that's going to be used in the database */       
             DB_Str = "UPDATE order1 SET " +CP_Split[1]+"='YES' WHERE uid="+CP_Split[2];
                                      
             connection.query(DB_Str, function (error, rows){

                if(error) { console.error(error); }                  
         
             });

             connection.end();
      }

    }); 

   }// End else (request.method) 
}   // End of onRequest()


http.createServer(onRequest).listen(8500, "0.0.0.0");
console.log("CP Server is now running");