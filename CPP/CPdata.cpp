
/*******************************************************
 * This program is used with the cporder.com server
 * It takes some of the load off the cpServer.js file
 * to keep it from crashing.
 * *****************************************************/
 
#include <mysql/mysql.h>  // MySQL/MariaDB
#include <iostream>       // cout, etc
#include <cstdio>         // sprintf()
#include <ctime>          // Rand and Time
#include <sstream>        // String functions
#include <fstream>        // File I/O
#include <cstring>        // strtok()
#include <stdio.h>
#include <stdlib.h>       // system()
#include <time.h>
#include <vector>
#include <algorithm>      // find() for vectors

using namespace std;


// The delay function comes 
// from Stackoverflow.com

void delay(time_t delay)
{
  time_t t0, t1; 
  time(&t0);
  do
  {
	time(&t1);
  }
  while((t1-t0) < delay);
}
 
/* ==================================
      The purpose of this object
      is to hold all the data 
      loaded from the database(),
      and then write it to all the
      .json files
   ================================== */    

class JSONData{
	
	private:
	   int rowNum;
	   int curCnt;
	   int uid;
	   unsigned int lastID;
	   unsigned int UID;
	   int fState;
	   int mOrdCnt;
	   const char* win;
	   const char* btype;
	   const char* SepBag;
	   const char* fName;
       
       char strJS[1000]; 	   
	   vector<string> data;
	   vector<string> data2;
	   
	   fstream newFile; 
	   
   public:

// Constructor	   
   JSONData(){}
   
// Destructor   
   ~JSONData(){
	   
      CloseFile();	   
   }
   
// Setting default values   
   void setup()
   {
	  rowNum = 0;
	  curCnt = 0;
	  uid = 0;
	  lastID = -1;
	  fState = 0;
	  mOrdCnt = 0;
	  win = "none";
	  btype = "none";
	  SepBag = "none";
	  
	  for(int i=0; i<3; i++){
		 data.push_back("");  
	  }

	  for(int i=0; i<11; i++){
		 data2.push_back("");  
	  }	  
   }

/******************************************
 * File section *
 * ************** 
 * Everything that has to do with 
 * file manipulation:
 * ------------------------------------
 *  Opening, Loading, Writing, Closing
 * ------------------------------------
 ******************************************/

   void OpenFile()
   {
      newFile.open(fName, ios::out | ios::in | ios::trunc);	  
   }
   
   void CloseFile()
   {
	  newFile.close();
   }
   
   void WriteOpenBracket()
   {
      if(newFile.is_open())
      {	 
	     newFile<<"[";  
      }	
      else {cout<<"Unable to open file"<<endl;}		 
   }
   
   void WriteCloseBracket()
   {
      if(newFile.is_open())
      {	 
	     newFile<<"]";  
      }	
      else {cout<<"Unable to open file"<<endl;}		 
   }   
  
// returning the JSON filename string
// which is selected by the fState value.   
   const char* Get_FName(int x)
   {
	  switch(x){
		 case 0:
		   fName = "../JSON/SndWh.json";
		   break;
		 case 1:  
		   fName = "../JSON/Fries.json";
		   break;
         case 2:
		   fName = "../JSON/Drinks.json";
		   break;
         case 3:
		   fName = "../JSON/Nuggets.json";
		   break;         		                 		   
		 case 4:
		   fName = "../JSON/Wings.json";
		   break;
		 default:
		   fName = "../JSON/OrdCmpl.json";	   
		   break;		 
	  }
	  return fName;
   }
   
   void WriteReceiptData()
   {
	  //sprintf(strJS, "../PNG/tck%d.png", uid);
	  vector<string> rdata, rdata2, rdata3;
	
	  rdata.push_back("");
	  rdata2.push_back("");
	
      rdata[0] = data2[10];
   
      fstream newFile1;
    
      istringstream Str1(rdata[0]);
      string token;
   
   
      int rcnt = 0;
      while(getline(Str1, token, '@')){
	     if(rcnt == 0)
            rdata[rcnt] = token;
         else
            rdata.push_back(token);
         
         rcnt++;
      }
   
      int rElm = 0;
   
      for(unsigned int i=1; i<rdata.size(); i++){
        istringstream Str4(rdata[i]);
        while(getline(Str4, token, '%')){
	       if(rElm == 0)
              rdata2[0] = token;     
           else
		      rdata2.push_back(token); 
	   
	       rElm++;  
        }
      }

      newFile1.open("../JSON/receipt.txt", ios::out | ios::in | ios::trunc);
      if(newFile1.is_open())
      {
	     for(unsigned int i=0; i<rdata2.size(); i++)
	     {
		     if(i == 0)
		     {
              //cout<<data2[i]<<endl;   
	            newFile1<<"Central Park\n";
	            newFile1<<"============\n";
	            
	            sprintf(strJS, "%s   Tck%d", Get_wnd(), Get_uid());
	            newFile1<<strJS;
	            newFile1<<"\n\n";	            
	            
	            //sprintf(strJS, "%s", data2[i]);
	            newFile1<<rdata2[i];
	            newFile1<<"\n";
	         }
	         else
	         {
	            //sprintf(strJS, '%s', data2[i]);
	            newFile1<<rdata2[i];
	            newFile1<<"\n";		 
		     } 
	     }
	     newFile1.close();

	     //sprintf(UID, "%d", uid);
	  
	     //Creating receipt png file
	     sprintf(strJS, "convert -background white -fill black -pointsize 24 label:'@../JSON/receipt.txt'  ../PNG/tck%d.png", uid);
	     system(strJS);
	  
	     //Creating QR code png file
	     //sprintf(strJS, "qrencode -o  qrcode.png -s 10 tck%s.png", UID);
	     sprintf(strJS, "qrencode -o  ../QRCode/qrTck%d.png -s 10 192.168.1.100/cporder.com/receiptCanvas.html", uid);
	     system(strJS);
	     //system("clear");
	  
      }	
      else {cout<<"Unable to open receipt file"<<endl;}	  
	  
   }   
            
// Writing all the stored data to each JSON file   
   void WriteJsonData()
   {	  
	  if(rowNum == 0){
		 
         if(newFile.is_open())
         {	 
	       sprintf(strJS, "\n  {\"uid\" : 0}\n");
	       newFile<<strJS;  
         }	
         else {cout<<"Unable to open file"<<endl;}		 
	  }
	  else{
		 
         if(newFile.is_open())
         {	
			if(fState < 5){
				
			// This section is for:
			// (Sndwh, Nuggets, Wings, Fries, Drinks).json
			  
	          newFile<<"\n  {";
	          sprintf(strJS, "\"uid\" : %d, ", uid);
	          newFile<<strJS;
         
              win = data[0].c_str();
              sprintf(strJS, "\"window\" : \"%s\", ", win);
              newFile<<strJS;
         
              switch(fState){
		        case 0:
		          btype = data[1].c_str();
	              sprintf(strJS, "\"btype\" : \"%s\", ", btype);
	              break;
		    
		        case 1:
		          btype = data[1].c_str();		      
	              sprintf(strJS, "\"ftype\" : \"%s\", ", btype);
	              break;
		    
		        case 2:
		          btype = data[1].c_str();		      
	              sprintf(strJS, "\"dtype\" : \"%s\", ", btype);
	              break;
		    
		        case 3:
		          btype = data[1].c_str();		      
	              sprintf(strJS, "\"ntype\" : \"%s\", ", btype);
	              break;
		    
		        case 4:
		          btype = data[1].c_str();		      
	              sprintf(strJS, "\"wtype\" : \"%s\", ", btype);
	              break;	          
		      }	     
	          newFile<<strJS;
	     
	          if(curCnt < rowNum-1){
		        SepBag = data[2].c_str();			   
	            sprintf(strJS, "\"SepBag\" : \"%s\"},\n", SepBag);
	            newFile<<strJS;
	          }
	          else{
		        SepBag = data[2].c_str();			   
	            sprintf(strJS, "\"SepBag\" : \"%s\"}\n", SepBag);
	            newFile<<strJS; 
		      }
           }
           else{
			   
			// This section is for the ordcmpl.json   
			
	          newFile<<"  {";
	          sprintf(strJS, "\"uid\" : %d, ", uid);
	          newFile<<strJS;
         
              win = data2[0].c_str();
              sprintf(strJS, "\"window\" : \"%s\", ", win);
              newFile<<strJS;
         
		      btype = data2[1].c_str();		      
	          sprintf(strJS, "\"btype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[2].c_str();		      
	          sprintf(strJS, "\"wtype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[3].c_str();		      
	          sprintf(strJS, "\"ntype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[4].c_str();		      
	          sprintf(strJS, "\"ftype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[5].c_str();		      
	          sprintf(strJS, "\"dtype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[6].c_str();		      
	          sprintf(strJS, "\"samecar\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[7].c_str();		      
	          sprintf(strJS, "\"phone\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
		      btype = data2[8].c_str();		      
	          sprintf(strJS, "\"cartype\" : \"%s\", ", btype);
	          newFile<<strJS;
	     
	          sprintf(strJS, "\"mOrdCnt\" : %d, ", mOrdCnt);
	          newFile<<strJS;
	     	     
	          if(curCnt < rowNum-1){
		        SepBag = data2[9].c_str();			   
	            sprintf(strJS, "\"SepBag\" : \"%s\"},\n", SepBag);
	            newFile<<strJS;
	          }
	          else{
		        SepBag = data2[9].c_str();			   
	            sprintf(strJS, "\"SepBag\" : \"%s\"}\n", SepBag);
	            newFile<<strJS; 
		      }
		      
		      UID = uid;
		      
		      if(UID != lastID)
		      {
		         WriteReceiptData();
		         lastID = uid;
		      }	   
		   }
		 }  	
         else {
		   cout<<"Unable to open JSON file"<<endl;
	     }    		         
         
	  } // end else
   }
   
/************************
 * End of File Section
 * **********************/   
   
/*************************************
 *   The Getter section
 *************************************/ 
   int GetRowData()
   {  	  
	  return rowNum;
   }
   
   int GetfState()
   {
	  return fState;
   }

   int GetCurCnt()
   {  
	  return curCnt;
   }
   
   int Get_uid()
   {  
	  return uid;
   }
   
   const char* Get_wnd()
   {  
	  return win;
   }
   
   const char* Get_Sndwh()
   {  
	  return btype;
   }
   
   const char* Get_SepBag()
   {  
	  return SepBag;
   }

/**********************
 * End Getter Section
 **********************/
 
/**********************************
 * Setter Section
 **********************************/  
	  
   void SetRowData(int RN)
   {
 	  rowNum = RN;
   }

   void IncCurCnt()
   {
	  curCnt++;   
   }
   
   void SetCurCnt()
   {
 	  curCnt = 0;
   }
   
   void SetfState(int State)
   {
	  fState = State;
   }
   
   void IncfState()
   {
	  fState++;
   }
   
   void Set_uid(int ID)
   {
 	  uid = ID;
   }
   
   void Set_Win(const char* WND)
   {
	  if(fState < 5){
 	    data[0] = WND;
 	  }
 	  else{
		data2[0] = WND;  
	  }  
   }
   
   void Set_Sndwh(const char* sndw)
   {
	  if(fState < 5){
 	    data[1] = sndw;
 	  }
 	  else{
		data2[1] = sndw;  
	  }  
   }
   
   void Set_Bag(const char* Bag)
   {
	  if(fState < 5){
 	    data[2] = Bag;
 	  }
 	  else{
		data2[9] = Bag;  
	  }  	   
   }
   
   void Set_Other(const char* othr, int x)
   {
	  data2[x] = othr;
   }
   
   void Set_mOrd(int x)
   {
	  mOrdCnt = x;   
   }
   
   //newFile.close();	 
};


void MariaDB(JSONData *CP)
{
 /***************************************
 * Setup for mysql/mariadb connections
 ***************************************/
   const char* host = "localhost";
   const char* user = "root";
   const char* password = "CPword1";
   const char* dbname = "CP";

   unsigned int port = 3306;
   static char* unix_socket = NULL;
   unsigned int flag = 0;	
   
   char sqlText [1000];
   int rowNum;
      
   MYSQL *conn;
   MYSQL_RES *result;   
   MYSQL_ROW row = NULL;
   conn = mysql_init(NULL);

/*********************************************   
   Declaring variables for file manipulation
 *********************************************/
   
   int num1=0;
   vector<string> strMe;
   const char* buf;
       
       
// Making a connection with the database
       
  if(!(mysql_real_connect(conn, host, user, password, dbname, port, unix_socket, flag)))
  {
	  fprintf(stderr, "\nError: %s [%d]\n", mysql_error(conn), mysql_errno(conn));
	  exit(1);
  }


/* =============================================================
 
     Write (Sandwhich/Fries/Drink/Nug/Wings/OrdComp) JSON Data  
 
   =============================================================*/
  
  switch(CP->GetfState()){
	 case 0:
	   sprintf(sqlText, "SELECT uid, window, btype, SepBag FROM order1 WHERE bcomplete = 'NO' AND btype != 'none' LIMIT 8");
	   break;  

	 case 1:
	   sprintf(sqlText, "SELECT uid, window, ftype, SepBag FROM order1 WHERE fcomplete = 'NO' AND ftype != 'none' LIMIT 8");	 
	   break;

	 case 2:
	   sprintf(sqlText, "SELECT uid, window, dtype, SepBag FROM order1 WHERE dcomplete = 'NO' AND dtype != 'none' LIMIT 8");	 
	   break;
	   
	 case 3:
	   sprintf(sqlText, "SELECT uid, window, ntype, SepBag FROM order1 WHERE ncomplete = 'NO' AND ntype != 'none' LIMIT 3");
	   break;
	   
	 case 4:
	   sprintf(sqlText, "SELECT uid, window, wtype, SepBag FROM order1 WHERE wcomplete = 'NO' AND wtype != 'none' LIMIT 3");
	   break;
	 
	 default:
       sprintf(sqlText, "SELECT uid, window, btype, wtype, ntype, ftype, dtype, samecar, phone, cartype, mOrdCnt, SepBag, receipt FROM order1 WHERE ordcomplete = 'NO' LIMIT 1");	   
       break;
  } 
  
  mysql_query(conn, sqlText);
  result = mysql_store_result(conn);
  rowNum= mysql_num_rows(result);

  CP->OpenFile();
  CP->SetRowData(rowNum); 
   
  if(rowNum == 0){
/*   No records found. Release DB obj
     and tell JSON file */	  
     
     CP->WriteOpenBracket();	
     CP->WriteJsonData();    
  }
  else{  
    
//  Setting a default value for vector
    strMe.push_back("");
    CP->WriteOpenBracket();
       	    
    while((row = mysql_fetch_row(result)))
    {
	   if(CP->GetfState() < 5){	 
	           // uid
	      sscanf(row[0], "%d", &num1);
	      CP->Set_uid(num1);
	   
	          // window	   
	      strMe[0] = row[1];  		      	   
	      buf = strMe[0].c_str();
	      CP->Set_Win(buf);
	       
	          // btype
	      strMe[0] = row[2];
	      buf=strMe[0].c_str();
	      CP->Set_Sndwh(buf);
	    
	         // SepBag
	      strMe[0] = row[3];
	      buf=strMe[0].c_str();
	      CP->Set_Bag(buf);
   
        // Writing JSON Data
          CP->WriteJsonData();	     
	   
         //	 CurCnt++   
	      CP->IncCurCnt();
	   }
	   else {
		   		   
	           // uid
	      sscanf(row[0], "%d", &num1);
	      CP->Set_uid(num1);
	   
	          // window	   
	      strMe[0] = row[1];  		      	   
	      buf = strMe[0].c_str();
	      CP->Set_Win(buf);
	       
	          // btype
	      strMe[0] = row[2];
	      buf=strMe[0].c_str();
	      CP->Set_Sndwh(buf);
	      
	          // wtype
	      strMe[0] = row[3];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,2);
	      
	          // ntype
	      strMe[0] = row[4];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,3);
	      	      
	          // ftype
	      strMe[0] = row[5];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,4);
	      
	          // dtype
	      strMe[0] = row[6];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,5);
	      
	         // samecar
	      strMe[0] = row[7];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,6);
	      
	          // phone
	      strMe[0] = row[8];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,7);
	      
	         // cartype
	      strMe[0] = row[9];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,8);	      	      	    

	         // mOrdCnt
	      sscanf(row[10], "%d", &num1);
	      CP->Set_mOrd(num1);	    
	    
	         // SepBag
	      strMe[0] = row[11];
	      buf=strMe[0].c_str();
	      CP->Set_Bag(buf);

	         // receipt
	      strMe[0] = row[12];
	      buf=strMe[0].c_str();
	      CP->Set_Other(buf,10);
   
        // Writing JSON Data
          CP->WriteJsonData();	     
	   
         //	 CurCnt++   
	      CP->IncCurCnt();		  
	   }   
    }

  }     
  
  CP->WriteCloseBracket();
  CP->CloseFile();    
 
  mysql_free_result(result);
  mysql_close(conn); 
}

void ShowDisplay(){
	
	cout<<"================================"<<endl;
	cout<<"        JSON File Creator       "<<endl;
	cout<<"================================"<<endl;
	cout<<"powered by C++ & MySQL\n\n\n";
	cout<<"Writing JSON, QRCodes, and Receipt"<<endl;
	cout<<"files with data that comes from"<<endl;
	cout<<"the database."<<endl;
	cout<<"\nProgrammed by Cornelius Hemphill"<<endl;
}

/********************
 *   Main Section   *
 ********************/
int main()
{
// Making the class object
	JSONData cp;

/*  Setting the default values for object */ 
	cp.setup();
	
	ShowDisplay();
	
	while(true){
		
/*    This will pass the object by pointer to the function */
      for(int i=0; i<6; i++){
		 cp.SetfState(i);
		 cp.Get_FName(cp.GetfState());
		 cp.SetCurCnt();
		 MariaDB(&cp);
	  }
	// 2sec delay
       delay(2);
    }
  return 0;
}