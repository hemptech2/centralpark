 pCP = {
    Mcnt: 0,
    Mcnt2: 0,	 
	mState: 0,
    CarSel: 0,
    MultiOrder: 0,
    DisCnt: 0,
    OrdCurCnt: 0,	
	OrdState: 0,
	pCarSel: 0,
    BoxID: 0,
    FootState: 0,
    tbID: 0,
    stInner: 0,
    retState: 0,
    cmbDrk: 0,
    cmbIce: 0,
    cmbColorU: 0,
    nPrior: 0,
    bPrior: 0,
    wPrior: 0,
    RfPrior: 0,
    RdPrior: 0,
    LdPrior: 0,
    mbtn1: 0,
    winW: 0,
    winH: 0,
	pCarType: "",
	StoredId: "",
	MoreId: "",
	WinSide: "",
	BagSep: "",
	BodyMessage: "",
	Receipt: "",
	TxtNum: "",
	header: "",
    sList1: [],
    dList1: [],
    nList1: [],
    wList1: [],
    fList1: [],
    mTotal: [],
    mTotTitle: [],
    Prices: [],     // All prices
    hPrices: [],    // hold prices
    tPrices: [],    // temp prices
    cmbPlcHldr: [], // Place holder for combo
    AryLen: [],     // holds how many are in the combo array
    cmbT: [],
    NcmbF:[],
    NcmbT: [],
    cmbColor: [],
    snwh: false,
    snwhMod: false,
    nug: false,
    wng: false,
    fry: false,
    drk: false,
    dProceed: false,
    modify: false,
    RgIceState: false,
    LgIceState: false,
    Combo: false     	 
 };

function PhoneSetup2() {

   var pWSide = document.getElementsByClassName("pWSide");
   pCP.winW = window.innerWidth;
   pCP.winH = window.innerHeight;
      
   p2CreateAry();
   p2Clear();
   	
   document.getElementById("pSelector14").style.display = "block";
   document.getElementById("pSelector1").style.display = "block";
      
   //document.getElementById("pMessBoxContainer").style.display = "block";
   //document.getElementById("p2MessBoxBody").innerHTML = "";
   
   pCP.header = document.getElementById("dBoxHead").innerHTML;
   
   //pCmbLoad(pTitle, qty);
         
   p2MoreHTML();
   p2SandwhichHTML(47);
   WindowSideDefault();
   SetBagDefault();
   DisplayPrice();
   document.getElementById("DisPrice1").innerHTML = "0.0";
   
   pCP.FootState = 14;     	      	
   pSetFooter();	
}

function p2ShowPopup(){
   var MessBox = document.getElementById("pMessBoxContainer");
   var PopHeader = document.getElementById("p2MessBoxHead");
   var BodyMessage = document.getElementById("p2MessBoxBody");

   BodyMessage.innerHTML = pCP.BodyMessage;
   MessBox.style.display = "block";
   PopHeader.style.display = "block";
   BodyMessage.style.display = "block";   	
}

function p2ClosePopup(){
   var MessBox = document.getElementById("pMessBoxContainer");
   var PopHeader = document.getElementById("p2MessBoxHead");
   var BodyMessage = document.getElementById("p2MessBoxBody");

   MessBox.style.display = "none";
   PopHeader.style.display = "none";
   BodyMessage.style.display = "none";   
}

function pPlaceOrder() {
	
	var sCnt = 0;
	var nCnt = 0;
	var wCnt = 0;
	var fCnt = 0;
	var dCnt = 0;
	var lCnt = 0;
	var pSndStr = "";
	var pNugStr = "";
	var pWngStr = "";
	var pFryStr = "";
	var pDrkStr = "";
	var pWinStr = "";
   var pCarStr = "";
   var pPhone = "";
   var pPrice = "0.0";
   var PassData1 = "";
   var pSepBag = "";
   var pReceipt = "";
   
/////////////////////////////////	
/////   Check Window data.  ////
////////////////////////////////
	
	pWinStr = "1&" + pCP.WinSide;	


/////////////////////////////////	
/////    Check Bag data.    ////
////////////////////////////////

    pSepBag = pCP.BagSep;

////////////////////////////////	
///// Check Sandwhich data. ////
////////////////////////////////
   
   lCnt = 0;
/*   
   for(var i=0; i<pCP.mTotal.length; i++) {
	  if(pCP.mTotal[lCnt] == "pSelector20") {
	     sCnt++;
	     lCnt++;	   
	  }
   }
*/
   for(var i=0; i<pCP.cmbT.length; i++) {
	  if(pCP.cmbT[i][0] == "s") {
	     sCnt++;
	     lCnt++;	   
	  }
   }
   
   if (sCnt !== 0) {
	   for(var i=0; i<sCnt; i++) {
		   for(var j=0; j<11; j++) {
			   if (pCP.sList1[i][j] !== "") {
       	          if (j == 0) { 
        	          pSndStr += "#" + pCP.sList1[i][j] + " "; 
        	      }        	  	                     	  
        	      else if (j < pCP.sList1[i].length-1) { 
        	         pSndStr += pCP.sList1[i][j] + " "; 
        	      }
        	      else { pSndStr += pCP.sList1[i][j]; } 
			   }
			   else { break;}  
		   }
	   }
   }
   else {pSndStr = "none";} 
   
//////////////////////////////////	
/////   Check Nugget data.  /////
////////////////////////////////
   
   var Reg;
   var rSauce;
   var Buck;
   var bSauce;
     
   for (var i = 0; i<pCP.nList1.length; i++) {     
      switch(i) {
		 case 0: 
		   Reg = pCP.nList1[i][0];
		   rSauce = pCP.nList1[i][1];
		   
		   if (Reg > 0 ) {
             pNugStr += "#" + Reg + " 5pcN " + rSauce;
           }
           break;
           
         case 1:  

           Buck = pCP.nList1[i][0];
           bSauce = pCP.nList1[i][1];
   
           if (Buck > 0) {
             pNugStr += "#" + Buck + " BKN " + bSauce;          	         	
           }
           break;  
      }
   }   
   if(pNugStr == "") {pNugStr = "none";}

   
//////////////////////////////////	
/////    Check Wing data.   /////
////////////////////////////////   
	
     var Wng;
     var rSauce;	   
     for (var i=0; i<pCP.wList1.length; i++) {
       Wng =    pCP.wList1[i][0];
       rSauce = pCP.wList1[i][1];

	   if (Wng > 0) {
	  	  switch(i) { 
	  	     case 0:
	  	       pWngStr += "#" + Wng + " 5W " + rSauce; break;
	  	     case 1:
	  	       pWngStr += "#" + Wng + " 10W " + rSauce; break;
	  	     case 2:
	  	       pWngStr += "#" + Wng + " 15W " + rSauce; break;     
	  	     case 3:
	  	       pWngStr += "#" + Wng + " 20W " + rSauce; break;
	  	     case 4:
	  	       pWngStr += "#" + Wng + " 25W " + rSauce; break;
	  	     case 5:
	  	       pWngStr += "#" + Wng + " 30W " + rSauce; break;
	  	     default:   break;  	  	 
	  	  }	  	           	
	   }	   
     }
     if (pWngStr == "") {pWngStr = "none";} 
   
//////////////////////////////////	
/////     Check Fry data.   /////
////////////////////////////////   
   
   lCnt = 0;
   
   for(var i=0; i<pCP.fList1.length; i++) {
     if (pCP.fList1[i] !== 0) {
		 switch (i) {   
	       case 0:
	  	      pFryStr += "#" + pCP.fList1[i] + " RegFries"; break;
	  	   case 1: 
		      pFryStr += "#" + pCP.fList1[i] + " BkFries"; break;
		   default: break;     		   
	     }
     }	   
   }
   if (pFryStr == ""){ pFryStr = "none";} 

     
//////////////////////////////////	
/////   Check Drink data.   /////
////////////////////////////////   
   
   var dReg;
   var dLG;
   var dTitle="";
   var dIceType="";
   var x1;
	  
   for(var j=0; j<pCP.dList1.length; j++) { 
      dReg = pCP.dList1[j][1];
      dLG = pCP.dList1[j][2];
         
      if(dReg > 0) {
        dTitle = pCP.dList1[j][0];
	    dIceType = "";
		  	  
	    for (x=0; x<3; x++) {
	       x1=pCP.dList1[j][3][x];
	             
	       if (x1>0) {
	         switch(x) {
	       	    case 0:
	       	      dIceType +=  x1 + ' ExIce ';
	       	      break;
	       	    case 1:
	       	      dIceType += x1 + ' LtIce ';
	       	      break;
	       	    case 2:
	       	      dIceType += x1 + ' NoIce ';
	       	      break;
	       	    default: break;  	         		  	         		   
	         }	         	
	       }	         	
	    }
	          
	    pDrkStr += "#" + dReg + " Reg " + dTitle + " " + dIceType; 
	 }
		 
     if(dLG > 0) {
	   dTitle = pCP.dList1[j][0];
	   dIceType = "";		  	
	   for (x=0; x<3; x++) {
	      x1=pCP.dList1[j][4][x];

	      if (x1>0) {
	   	    switch(x) {
	          case 0:
	            dIceType +=  x1 + ' ExIce ';
	            break;
	          case 1:
	            dIceType += x1 + ' LtIce ';
	            break;
	          case 2:
	            dIceType += x1 + ' NoIce ';
	            break;
	          default: break;  	         		  	         		   
	        }	         	
	     }	         	
	   }
	   pDrkStr += "#" + dLG + " LG " + dTitle + " " + dIceType; 
     }
		      
   }	      
   if (pDrkStr == "") {pDrkStr = "none";}
   
//////////////////////////////////////////	
///// Check user car and phone data. ////
/////////////////////////////////////////

   if(pCP.pCarType !== "") {
	   pCarStr = "&" + pCP.pCarType;
   }
   else { pCarStr = "&Window"; }
	
   if (pCP.TxtNum !== "") {
	  pPhone = "&" + pCP.TxtNum;
   }
   else { pPhone = "&none"; }

	
//////////////////////////////////////////	
///// Check user car and phone data. ////
/////////////////////////////////////////
   var MulOrd = pCP.MultiOrder;
   var CurOrd = 0;
   var SmeCar = "&NO";
   var mDisCnt = 0;
     
   if (MulOrd <= 1) {
	  SmeCar = "&NO";
   }
   else {
      SmeCar = "&YES";
      if (pCP.OrdCurCnt == 0) {
         pCP.OrdCurCnt = pCP.MultiOrder;      
     	 pCP.DisCnt = (pCP.MultiOrder - pCP.OrdCurCnt) + 1;
     	 mDisCnt = pCP.DisCnt;
     	 pCP.OrdCurCnt--;	        
       }
       else {
     	   pCP.DisCnt = (pCP.MultiOrder - pCP.OrdCurCnt)+1;
     	   mDisCnt = pCP.DisCnt;     	        	 
     	   pCP.OrdCurCnt--;     	  
       }  
     }    	 
     
     pPrice = document.getElementById("DisPrice1").innerHTML;
     pReceipt = pCP.Receipt + "@Grand Total    $" + pPrice;
     
     PassData1 = pWinStr + "&" + pSndStr + "&" + pWngStr + "&" + 
                 pNugStr + "&" + pFryStr + "&" + pDrkStr + SmeCar + 
                 pPhone + pCarStr + "&" + MulOrd + "&" + mDisCnt + 
                 "&" + pPrice + "&" + pSepBag + "&" + pReceipt;
                
     pSendDB(PassData1);

     p2Clear();
     p2ClearToggles();
     p2KillArys();
     p2CreateAry();
     SetBagDefault();
     DisplayPrice();
     
     document.getElementById("pSelector1").style.display = "block";
     pCP.FootState = 14;     	      	
     pSetFooter();                
}

function pSendDB(pData1) {
   
   pCP.BodyMessage = "Order Placed";
   p2ShowPopup();
   
   var CP_URL= "http://"+window.location.hostname+":8500/cporder.com/?";
   var xmlhttp= new XMLHttpRequest();
   xmlhttp.open("POST", CP_URL, true);
   xmlhttp.send(pData1);	
}

function p2KillArys(){	

// Clearing out Arrays	
   pCP.mTotTitle.length = 0;
   pCP.mTotal.length = 0;
   pCP.sList1.length = 0;
   pCP.nList1.length = 0;
   pCP.wList1.length = 0;
   pCP.fList1.length = 0;
   pCP.dList1.length = 0;
   pCP.AryLen.length = 0;
   pCP.NcmbF.length = 0;
   pCP.NcmbT.length = 0;
   pCP.cmbT.length = 0;
   pCP.cmbColor.length = 0;
   pCP.tPrices.length = 0;
   pCP.hPrices.length = 0;   
   pCP.Prices.length = 0;
   pCP.cmbPlcHldr.length = 0;
   
// Setting data back to default conditions
   pCP.stOutr = 0;
   pCP.stInner = 1;
   pCP.Mcnt = 0;
   pCP.Mcnt2 = 0;
   pCP.Scnt = 0;
   pCP.StoredId = 0;
   pCP.FootState = 0;       
   pCP.BoxID = "";
   pCP.dSelected = "";       
   pCP.ShowExtra = false;
   pCP.snwh = false;
   pCP.snwhMod = false;
   pCP.nug = false;
   pCP.wng = false;
   pCP.fry = false;
   pCP.drk = false;
   pCP.modify = false;
    
   if(pCP.OrdCurCnt == 0) {
      pCP.MultiOrder = 0;
      pCP.DisCnt = 0;
      pCP.OrdCurCnt = 0;
      pCP.pCarType = "";
      pCP.TxtNum = "";	   
   }
   
   WindowSideDefault();
   p2Clear();
   document.getElementById("pSelector1").style.display = "block";   
   document.getElementById("pSelector21").style.display = "none";
   p2MoreHTML();
   p2SandwhichHTML(47);      	
}

function WindowSideDefault(){

   var pWSide = document.getElementsByClassName("pWSide");
   
   pWSide[0].style.background = '';
   pWSide[0].style.border = '';    	  
   pWSide[0].style.color = '';
   pWSide[0].style.textShadow = ''; 	

   pWSide[1].style.background = '';
   pWSide[1].style.border = '';    	  
   pWSide[1].style.color = '';
   pWSide[1].style.textShadow = '';
   
   
   pWSide[1].style.background = '#777';
   pWSide[1].style.border = '4px solid #08A';    	  
   pWSide[0].style.color = '#08A';
   pWSide[0].style.textShadow = '1px 1px 1px #FFF'; 
    
   pCP.WinSide = pWSide[0].innerHTML; 	
}

function SetBagDefault(){
    var BagCont = document.getElementById("BagContainer");
   	
    BagCont.style.background = '';
    BagCont.style.border = '';    	  
    BagCont.style.color = '';
    BagCont.style.textShadow = '';
    pCP.BagSep = "No";	
}

function p2CreateAry() {

/* Creating and populating an 8x11 array for sandwhiches */      
   for (var i=0; i<9; i++) {
     pCP.sList1[i]= new Array();
     for (var j=0; j<11; j++) {
     	 pCP.sList1[i][j]="";
     }	
   }

/*****************************************************************   
    Note to self:
   ---------------  
     This will access (Name, Rg, Lg) => alert(pCP.dList1[0][1])   
     
     This will access (Ex, Lt, No) => alert(pCP.dList1[0][4][1])              
        
******************************************************************/   
//                                 REG         LG
//                       Rg  Lg  Ex Lt No   Ex Lt No 
   pCP.dList1= [["PEP",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["DIET",  0, 0, [0, 0, 0], [0, 0, 0]],
                ["DEW",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["DOC",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["FP",    0, 0, [0, 0, 0], [0, 0, 0]],
                ["MIST",  0, 0, [0, 0, 0], [0, 0, 0]],
                ["TEA",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["WATER", 0, 0, [0, 0, 0], [0, 0, 0]],
                ["ICE",   0, 0, [0, 0, 0], [0, 0, 0]]
               ];

/* Creating and populating an array for fries */         
   pCP.fList1 = [0, 0];
   pCP.NcmbF = [0, 0];
   
   pCP.cmbDrk = 100; 
   pCP.cmbIce = 100;    	


/* Creating and populating an 20x10 array for combo storage 
   example will look like:
  
                     "n"  elm  "CB"   qty    Rfry    Lfry   Rdrk    Ldrk   RdName    LdName   RdElm    LdElm   RExIce   RLtIce    RNoIce   LExIce   LLtIce    LNoIce
   pCP.cmbT[0][0] = ["",  "",   "",   "",     "",     "",    "",     "",     "",       "",     "",      "",     "",      "",      "",        "",      "",      ""];
*/ 
   for (var i=0; i<20; i++){
	 pCP.cmbT[i] = new Array();
     for (var j=0; j<18; j++){
		pCP.cmbT[i][j] = "";
	 }	   
   }


/* Creating and populating an 9x4 array for combo storage 
   example will look like:
   
                     elm,  Rtype,   Ltype,   Rqty,   Lqty,  R_ice,  L_ice
   pCP.NcmbT[0][0] = ["",   "",      "",      ""      ""     ""      ""];
*/   
   for (var i=0; i<9; i++){
	 pCP.NcmbT[i] = new Array();
     for (var j=0; j<8; j++){
		pCP.NcmbT[i][j] = "";
	 }	   
   }
 
         
/* Creating and populating an 2x2 array for nuggets */  
   for (var i=0; i<2; i++) {
     pCP.nList1[i]= new Array();
     for (var j=0; j<2; j++) {
		if(j == 0){
     	  pCP.nList1[i][j] = 0;
     	}
     	else {
		  pCP.nList1[i][j] = "";
		}    
     }	
   }        


/* Coloring for combo items */
   pCP.cmbColor = ['rgb(125,5,25)', 
                   'rgb(141, 54, 50)', 
                   'rgb(51, 51, 51)'
                  ];   

/* Setting Seperate bags to default value of "No" */
   pCP.BagSep = "No";   

/* Creating and populating an 9x2 array for wings */
   for (var i=0; i<6; i++) {
     pCP.wList1[i]= new Array();
     for (var j=0; j<2; j++) {
		if(j == 0){
     	  pCP.wList1[i][j] = 0;
     	}
     	else {
		  pCP.wList1[i][j] = ""; 
		}   
     }	
   }

              //  Item               Reg   Cmbo  1 UpSz  both    
   pCP.Prices = [["B",              3.05,  4.69,  5.01,  5.34],   //0
                 ["BB",             3.16,  5.12,  5.45,  5.78],   //1
                 ["DB",             4.36,  6.33,  6.65,  6.98],   //2
                 ["CB",             3.59,  4.91,  5.23,  5.56],   //3
                 ["BC",             4.36,  6.54,  6.84,  7.20],   //4
                 ["DCB",            5.01,  7.20,  7.53,  7.86],   //5
                 ["DBCB",           5.78,  7.75,  8.07,  8.40],   //6
                 ["TCB",            6.75,  8.72,  9.05,  9.37],   //7
          //   ---------------------- Chicken ---------------------
                 ["REG CK",         4.36,  6.54,  6.84,  7.20],   //8
                 ["GRILL",          3.27,  5.45,  5.75,  6.05],   //9
                 ["CLUB",           4.80,  6.76,  7.09,  7.42],   //10
                 ["CKB",            4.36,  6.54,  6.84,  7.20],   //11
                 ["GRILL B",        3.27,  5.45,  5.75,  6.05],   //12
                 ["CLUB B",         4.80,  6.60,  6.90,  7.20],   //13
                 ["GRILL CL",       4.14,  6.11,  6.43,  6.76],   //14
          //    --------------------- HOT DOG ---------------------
                 ["HOT DOG",        1.96,  4.36,  4.69,  5.01],   //15
                 ["DBL HOT DOG",    3.84,  5.89,  6.22,  6.54],   //16
          //     --------------------- NUGGET ---------------------
                 ["5PC",            1.96,  4.36,  4.69,  5.01],   //17                                                  
                 ["BUCK",           4.36,  6.33,  6.65,  6.98],   //18
          //    ---------------------- WINGS ---------------------
                 ["5W",             4.03,  6.00,  6.33,  6.65],   //19                                                  
                 ["10W",            8.73,  10.70, 11.02, 11.35],  //20
                 ["15W",            12.09, 14.06, 14.39, 14.72],  //21                                                  
                 ["20W",            16.13, 18.09, 18.42, 18.75],  //22
                 ["25W",            20.16, 22.12, 22.45, 22.78],  //23                                                   
                 ["30W",            24.19, 26.15, 26.48, 26.81],  //24
          //---- FRIES ---------------------
                 [1.74, 2.17],                                    //25
          //---- DRINKS --------------------
                 [1.63, 1.85],                                    //26
        //-MISC --CHES--BAC--PATTIE--------
                 [.33, .55, 1.64]                                 //27                                                                                            
                ];        
	
}

function cmbClrSlider(){
	
   var sDrk1 = document.getElementsByClassName("sDrk1");
   var sIce1 = document.getElementsByClassName("sIce1");	
	
   for(var i=0; i<sDrk1.length; i++){
	 sDrk1[i].style.background = "";
   }
         
   for(var i=0; i<sIce1.length; i++){
     sIce1[i].style.background = "";
   }
   
   pCP.cmbDrk = 100; 
   pCP.cmbIce = 100;
}

function showDrinkScroll(){   
   	
	switch(document.getElementById("pScroll").style.display){
	  case "none": 
		 document.getElementById("pScroll").style.display = "block"; 
		 break;
	  
	  case "block": 
		 document.getElementById("pScroll").style.display = "none";
         cmbClrSlider();
		 break;
		 
      default: break;		 
	}  
}

function cmbDrkSelect(pElm){
   
   var sDrk1 = document.getElementsByClassName("sDrk1");
   
   for(var i=0; i<sDrk1.length; i++){
	  sDrk1[i].style.background = "";
   }
   
   sDrk1[pElm].style.background = '#08A';
   pCP.cmbDrk = parseInt(pElm);
   pCP.cmbIce = 100; 
   
}

function cmbICE(pElm){
   
   var sIce1 = document.getElementsByClassName("sIce1");
   
   for(var i=0; i<sIce1.length; i++){
	  sIce1[i].style.background = "";
   }
   
   sIce1[pElm].style.background = '#08A';   
   pCP.cmbIce = parseInt(pElm);
   
}

function pCmbHideItems(){
   
   var cmbWraps = document.getElementsByClassName("cmbWraps");
   
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];

/*   
   for(var i=0; i<cmbWraps.length; i++){
	  cmbWraps[i].style.display = "none";
   }
*/   
   for(var i=0; i<cmbAry1.length; i++){
	  document.getElementById(""+cmbAry1[i]).innerHTML = "";	   
   }
}

function pCmbLoad(pTitle, qty, DisCnt, CurCnt, ActCnt){
	
   var cmbWraps = document.getElementsByClassName("cmbWraps");
   	
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];  
   
   var pElm;               
   pCP.Combo = true;
   colorView = false;
   var Str1="";
   var priceHld;
   var hnum;
     
   for(var i=1; i<DisCnt+1; i++){
	  cmbWraps[i].style.display = "block";
   }
   
   if(qty > 1){
	  for(var i=0; i<cmbAry1.length; i++){
		 if(document.getElementById(""+ cmbAry1[i]).innerHTML == ""){
			pElm = i;
			break;
		 }
	  }
	  
	  /* note to self: 
	     =============
	     Go back to the pSetPrice function and add 
	     pCP.AryLen and push 0's in it. This way it
	     will match CurCnt when you access it.
	  */
	  
	  //alert(pCP.cmbPlcHldr[CurCnt]); 
	  
	  if(pCP.cmbPlcHldr[CurCnt]){
		 priceHld = pCP.tPrices[CurCnt]/qty;
		 pCP.tPrices[CurCnt] = new Array(qty+1);
		 
	     //hnum = qty;
	     pCP.AryLen[CurCnt] = qty;
	     
	     pCP.tPrices[CurCnt][qty]= new Array(qty+1);	 
		 
		 //alert(pCP.tPrices[CurCnt].length);
		 for(var i=0; i<pCP.tPrices[CurCnt].length-1; i++){
			pCP.tPrices[CurCnt][i] = 0;
		 }
		 for(var i=0; i<pCP.tPrices[CurCnt][qty].length; i++){
			pCP.tPrices[CurCnt][qty][i]=0;
		 }
	  }
	  //alert(pCP.tPrices[CurCnt]);
	  var xx=0;
	  for(var i=pElm; i<pElm + qty; i++){
		  
		 document.getElementById(""+ cmbAry1[i]).innerHTML = pTitle;
		 if(!colorView){
		   if(pCP.cmbColorU == 0){ pCP.cmbColorU++; }
		   else { pCP.cmbColorU--; }
		   colorView = true;
		 }  
         
         document.getElementById(""+ cmbAry1[i]).style.background = 
                                           pCP.cmbColor[pCP.cmbColorU];

         pCP.tPrices[CurCnt][xx] = cmbAry1[i];
         pCP.tPrices[CurCnt][qty][xx] = priceHld;
         xx++;                                  	   
	  }
	  
	  pCP.tPrices[CurCnt][xx+qty] = xx;
   }
   else{
	 for(var i=0; i<cmbAry1.length; i++){
	   if(document.getElementById(""+ cmbAry1[i]).innerHTML == ""){
		  pElm = i;
		  break;
	   }
	 }	   
     document.getElementById(""+ cmbAry1[pElm]).innerHTML = pTitle;	   
   }
   
}

function pCmbSetup(){

    var pSel = document.getElementsByClassName("pSel");
    var cmbWraps = document.getElementsByClassName("cmbWraps");
    var cmbWrap = document.getElementsByClassName("cmbWrap");
    
    var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                   "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                   "p2Cmb41"
                  ];     
    	
	var pTitle;
	var qty, qty2;
	var cnt = cnt2 = 0;
	var mulCnt = 0;

    if (!pCP.snwh && !pCP.nug && !pCP.wng && !pCP.fry & !pCP.drk) {
   	  
   	  pCP.BodyMessage = "Nothing Ordered";
   	  p2ShowPopup();
   	  
   	  for(var i=0; i<7; i++){
   	    if (pSel[i].style.color == 'rgb(0, 136, 170)') { 
 	     
   	      if(i == 0 || i == 1 || i == 3 || i == 4){
             pCP.FootState = 15;     	      	
             pSetFooter();
             break;			 
		  }
		  else if(i == 2){
             pCP.FootState = 14;     	      	
             pSetFooter();
             break;			  
		  }
		  else{
             pCP.FootState = 11;     	      	
             pSetFooter();
             break;			  
		  }
		  
   	    } 
	  }   	 
   	  return;
    }
    //else if(pCP.fry || pCP.drk){ return;}
	
	p2Clear();
	pCmbHideItems();
	
	for(var i=0; i<pCP.sList1.length-1; i++){
	   if(parseInt(pCP.sList1[i][0]) > 0){
		 cnt++;
		 cnt2++;
		 
		 if(parseInt(pCP.sList1[i][0]) > 1){
			mulCnt += parseInt(pCP.sList1[i][0]);
		 }
	   }
	   else{ break; }
	}
    
    for(var i=0; i<pCP.nList1.length; i++){
      if(parseInt(pCP.nList1[i][0]) > 0){
	     cnt++;
	     cnt2++;
	     if(parseInt(pCP.nList1[i][0]) > 1){
		    mulCnt += parseInt(pCP.nList1[i][0]);
	     }
	  }
    }
    
    if (pCP.winW < 406) {
   
      for(var i=0; i<cmbWrap.length; i++){
    	   cmbWrap[i].style.width = 17 + "%";
      }
    }
    else {
      for(var i=0; i<cmbWrap.length; i++){
    	   cmbWrap[i].style.width = "";
      }
    }	
   
    if(mulCnt > 0){	  	
      cnt += mulCnt;
    }  
    //alert("Added cnt is:(" + cnt + "} Actual count is:(" + cnt2 + ")");

    for( var i=0; i<pCP.cmbT.length; i++){
	   switch(pCP.cmbT[i][0]){
		  case "s":
	        qty = parseInt(pCP.cmbT[i][3]);
	        if(qty > 0){
	          pTitle = pCP.cmbT[i][2];
	          pCmbLoad(pTitle, qty, cnt, i, cnt2);
	        }  		     
		    break;
	     
		  case "n":
		    pTitle = pCP.cmbT[i][2];
	        qty = parseInt(pCP.cmbT[i][3]);
            if(qty > 0){
		      pCmbLoad(pTitle, qty, cnt, i, cnt2);
		    }
		    break;
		     
		  case "w":
		    pTitle = pCP.cmbT[i][2];
	        qty = parseInt(pCP.cmbT[i][3]);
            if(qty > 0){
		      pCmbLoad(pTitle, qty, cnt, i, cnt2);
		    }		  
		    break;
		     
		  default: break;         
	   }
	}

 // Setting unused combo display boxes to invisible
     
    for(var i=0; i<cmbAry1.length; i++){
	  if(document.getElementById(""+cmbAry1[i]).innerHTML == ""){
	    cmbWraps[i+1].style.display = "none";
	  }  
    }    
	
	document.getElementById("pSelector1").style.display = "none";
	document.getElementById("pSelector22").style.display = "block";
	
	pCP.FootState = 21;     	      	
    pSetFooter();
	
}

function p2MoreHTML(){

   var mWrap = document.getElementsByClassName("mWrap"); 
      
   var htmlTags= "pChce1,p2EXT1,p2LT1,pChce1,p2EXT1,p2LT1," +
                 "pChce3,p2EXT3,p2LT3,pChce3,p2EXT3,p2LT3," +
                 "pChce4,p2EXT4,p2LT4,pChce5,p2EXT5,p2LT5," +
                 "pChce6,p2EXT6,p2LT6,pChce7,p2EXT7,p2LT7," + 
                 "pChce8,p2EXT8,p2LT8,pChce9,p2EXT9,p2LT9," + 
                 "pChce10,p2EXT10,p2LT10"; 
                                                   
   for(var i=0; i<mWrap.length; i++){
	  mWrap[i].style.display = "none";
   }
                    	
}

function Xtra1(){

	var mWrap = document.getElementsByClassName("mWrap");
	           
	var SndwhTags = "pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29";
	var hld = SndwhTags.split(",");
	
	var btest = false;
	
	for(var i=0; i<hld.length; i++){
	   if (document.getElementById(""+hld[i]).style.color == 'rgb(0, 136, 170)') {
		  btest = true;
	   }
	}
	

	if(!btest){
	   pCP.BodyMessage = "Select food items";
   	   p2ShowPopup();
	   return;
	}
	else{
	  p2Clear();
	  document.getElementById("pSelector1").style.display = "none";
	  document.getElementById("pSelector21").style.display = "block";
	  var x=0;
	  
	  for(var i=0; i<hld.length; i++){

	     if (document.getElementById("" + hld[i]).style.color == 'rgb(0, 136, 170)') {
			 
		    mWrap[i].style.display = "block";
			
		    if(i+1 < hld.length-1){
              pCP.MoreId += "" + i + ",";
            }
            else{ pCP.MoreId += "" + i }           
	     }
	  }	   
	}
    
    pCP.FootState = 16;     	      	
    pSetFooter();		
	  
   
}

function mSelect(mID, op){

   var SndwhTags = "pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29";
   
   var mChoice   = "pChce1,pChce2,pChce3,pChce4,pChce5," +
                   "pChce6,pChce7,pChce8,pChce9,pChce10";
                 
   var hld = SndwhTags.split(",");
   var hld1 = mChoice.split(",");
   var ptext = document.getElementById(""+hld1[mID]).innerHTML;
   var x;
   
   if((ptext.search("Ex ")) !== -1){
	  x=ptext.split("Ex ");
	  document.getElementById(""+hld1[mID]).innerHTML = x[1];   
   }
 
   if((ptext.search("Lt ")) !== -1){
	  x=ptext.split("Lt ");
	  document.getElementById(""+hld1[mID]).innerHTML = x[1];   
   }
    	
   switch(op){
	  case 1:
	    document.getElementById(""+hld1[mID]).innerHTML = "Ex " +
	    document.getElementById(""+hld1[mID]).innerHTML; 
	    break;
	    
	  case 2:
	    document.getElementById(""+hld1[mID]).innerHTML = "Lt " +
	    document.getElementById(""+hld1[mID]).innerHTML;	  
	    break; 
	    
	  default:break;      	
   }
     
}

function p2OrderIt(){
   
    switch(pCP.OrdState){
	   case 1:
	     p2StoreSandwhiches();
	     break;
	     
	   case 2:
	     p2StoreFriedItems();	   
	     break;	
	     
	   case 3:
	     p2StoreDrinks();	   
	     break;
	     
	   default: break;  	          
	}
    
}

function p2RetView(){
	
	var pAry2 = ["cmbTxt1",  "cmbTxt2",  "cmbTxt3",  "cmbTxt4",   // 3
	             "cmbTxt5",  "cmbTxt6",  "cmbTxt7",  "cmbTxt8",   // 7
	             "cmbTxt9",  "cmbTxt10", "cmbTxt11", "cmbTxt12", // 11
	             "cmbTxt13", "cmbTxt14", "cmbTxt15", "cmbTxt16", // 15
	             "cmbTxt17", "cmbTxt18", "cmbTxt19", "cmbTxt20", // 19
	             "cmbTxt21", "cmbTxt22", "cmbTxt23", "cmbTxt24", // 23
	             "cmbTxt25", "cmbTxt26", "cmbTxt27", "cmbTxt28", // 27
	             "cmbTxt30", "cmbTxt31", "cmbTxt29", "cmbTxt32", // 31
	             "cmbTxt33", "cmbTxt34", "cmbTxt35", "cmbTxt36"  // 35        
	            ];           
	
	var pAry3 = [[0, 1, 2, 3],
	             [4, 5, 6, 7],
	             [8, 9, 10, 11],
	             [12, 13, 14, 15],
	             [16, 17, 18, 19], 
	             [20, 21, 22, 23], 
	             [24, 25, 26, 27], 
	             [28, 29, 30, 31], 
	             [32, 33, 34, 35]
	            ];    
    
    var pElm;
    
    p2Clear();
    document.getElementById("pSelector1").style.display = "block";    
              
    switch(pCP.retState){	   
	   
	   case 1:  // Hot Dog
          document.getElementById("pSelector3").style.display = "block";
	      break;

	   case 2:  // Nuggets   
	       document.getElementById("pSelector7").style.display = "block";
	       document.getElementById("pSelector12").style.display = "block";
           document.getElementById("pSce1").style.marginLeft = ".1%";	                 
		  break;
	   
	   case 3:  // Wings
	       document.getElementById("pSelector6").style.display = "block";
	       document.getElementById("pSelector12").style.display = "block";   
	      break;
	      
	   case 4:  // Fries
 	       document.getElementById("pSelector8").style.display = "block";         
		  break;
	   
	   case 5:  // Burgers
          if (pCP.winW < 378) {
 	         document.getElementById("pSelector1").style.display = "none";	          
 	         document.getElementById("pSelector2").style.display = "block";	     
	         document.getElementById("pSelector3").style.display = "block";	        
          }
          else{
 	        document.getElementById("pSelector1").style.display = "block";
 	        document.getElementById("pSelector2").style.display = "block";	     
	        document.getElementById("pSelector3").style.display = "block";	         
          }   
	      break;	      
	   
	   case 6:  // Chicken
          if (pCP.winW < 378) {
 	         document.getElementById("pSelector1").style.display = "none";	          
 	         document.getElementById("pSelector5").style.display = "block";	     
	         document.getElementById("pSelector3").style.display = "block";	        
          }
          else{
 	        document.getElementById("pSelector1").style.display = "block";
 	        document.getElementById("pSelector5").style.display = "block";	     
	        document.getElementById("pSelector3").style.display = "block";	         
          }	      	        	             
		  break;
	   
	   case 7:  // Drinks
	       document.getElementById("pSelector9").style.display = "block";	     
	       document.getElementById("pSelector10").style.display = "block";   
	      break;
	      
	   case 8:  // Combo Screen
	       for(var i=0; i<pAry2.length; i++){
			  if(pAry2[i] == pCP.BoxID){
	             pElm = i;
	             break
			  }
            }
			if(pElm < 4){
			  pElm = 0; 
			}
			else if(pElm == 4 && pElm < 8){
			  pElm = 1;				  
			}
			else if(pElm == 8 && pElm < 12){
			  pElm = 2;				  
			}
			else if(pElm == 12 && pElm < 16){
			  pElm = 3;				  
			}
			else if(pElm == 16 && pElm < 20){
			  pElm = 4;				  
			}
			else if(pElm == 20 && pElm < 24){
			  pElm = 5;				  
			}
			else if(pElm == 24 && pElm < 28){
			  pElm = 6;				  
			}
			else if(pElm == 28 && pElm < 32){
			  pElm = 7;				  
			}
			else{
			  pElm = 8;				  
		    }
			  
			for(var x=0; x<4; x++){				   
			  if(document.getElementById("" + pAry2[pAry3[pElm][x]]).id == pCP.BoxID){
			    if(x == 0){
			 	  document.getElementById("" + pAry2[pAry3[pElm][x+1]]).innerHTML = 0;
				}
				else if(x == 1){
				  document.getElementById("" + pAry2[pAry3[pElm][x-1]]).innerHTML = 0;
			    }					   
				else if(x == 2){
				  document.getElementById("" + pAry2[pAry3[pElm][x+1]]).innerHTML = 0;
				}
				else{
				  document.getElementById("" + pAry2[pAry3[pElm][x-1]]).innerHTML = 0;  
				}
				break;
			  }
		   }
     
	      document.getElementById('pDisplay').innerHTML = 0;
          document.getElementById("pSelector1").style.display = "none";	   
          document.getElementById("pSelector22").style.display = "block";
	      break;	      
	      	      	      	      
	   default: break;        
	}
	
	if(pCP.modify){		
	   pCP.FootState = 17;     	      	
       pSetFooter();		
	}
	else{
	   pCP.FootState = 15;     	      	
       pSetFooter();	
	}
}

function p2StoreFriedItems() {
	
	var p2CheckAry2 = ["pS4", "pS5", "pS6"];
	var pFry = ["pTxt11", "pTxt12"];
	var pNug = ["pTxt9", "pTxt10"];
	var pWngs = ["pTxt3", "pTxt4", "pTxt5", "pTxt6", "pTxt7", "pTxt8"];
	var Suace = ["pS70", "pS71", "pS72"];

	var pSel= document.getElementsByClassName("pSel");	
	var pSauce = "";
	var pProceed = false;
	var pTitle;
	
	for(i=0; i<p2CheckAry2.length; i++){
	   if(document.getElementById(""+p2CheckAry2[i]).style.color == 'rgb(0, 136, 170)') {
		   pTitle = document.getElementById(""+p2CheckAry2[i]).innerHTML;
		   break;
	   }
	}
		
    switch (pTitle) {
    	 
    	 case "FRIES":
		        
           var tmpNum = 0;
         
		   for (var i=0; i<2; i++) {
             tmpNum = parseInt(document.getElementById("" + pFry[i]).innerHTML);
             
             if (tmpNum !== 0) { 
				 pProceed = true; 
			 }				
		   }			
		   if (!pProceed) {
			   
	          pCP.BodyMessage = "Select a Qty";
   	          p2ShowPopup();
			  return;
		   }
							     
        // Reading REG fries number box
           tmpNum = parseInt(document.getElementById("" + pFry[0]).innerHTML);
           pCP.RfPrior = pCP.fList1[0];
           if(tmpNum > 0){
			 if(pCP.modify){
			   pCP.fList1[0] = tmpNum; 
			   if(tmpNum > pCP.RfPrior){
				 tmpNum= tmpNum - pCP.RfPrior;
				 pSetFriedPrice(pTitle, tmpNum, 0);
				 tmpNum= tmpNum + pCP.RfPrior;
			   }
			 }  
		     else{ 	 
                 pCP.fList1[0] = tmpNum + pCP.RfPrior;
                 pSetFriedPrice(pTitle, tmpNum, 0);
                 pCP.NcmbF[0] =  tmpNum + pCP.RfPrior;
		     }
             
           } 
     
        // Reading BK fries number box         
           tmpNum = parseInt(document.getElementById("" + pFry[1]).innerHTML);
           pCP.RfPrior = pCP.fList1[1];
           if(tmpNum > 0){
			 if(pCP.modify){
			   pCP.fList1[1] = tmpNum; 
			   if(tmpNum > pCP.RfPrior){
				 tmpNum= tmpNum - pCP.RfPrior;
				 pSetFriedPrice(pTitle, tmpNum, 1);
				 tmpNum= tmpNum + pCP.RfPrior;
			   }
			 }  
		     else{ 	 
                 pCP.fList1[1] = tmpNum + pCP.RfPrior;
                 pSetFriedPrice(pTitle, tmpNum, 1);
         		 pCP.NcmbF[1] =  tmpNum + pCP.RfPrior;	
		     }             
           }            
                        
         //clearing out the number boxes;         
           document.getElementById("" + pFry[0]).innerHTML = 0;
           document.getElementById("" + pFry[1]).innerHTML = 0;		
  	       pCP.fry = true;
  	       
  	       ReadFries();
    	   break;
    	   
// //////////////////////////////////////////////////    	    
    	 case "NUGGETS":
         
           var tmpNum = 0;
           
		   for (var i=0; i<pNug.length; i++) {
             tmpNum = parseInt(document.getElementById("" + pNug[i]).innerHTML);
             if (tmpNum !== 0) { 
				 pProceed = true; 
			 }				
		   }
		   if(!pProceed){ 
	           pCP.BodyMessage = "Select a Qty";
   	           p2ShowPopup();			   
			   return; 
			}
		   				              
            tmpNum = parseInt(document.getElementById("" + pNug[0]).innerHTML);
            pCP.nPrior = pCP.nList1[0][0]; 
            
            //pCP.nList1[0][0] += tmpNum;
           if(tmpNum > 0){
			 if(pCP.modify){
			   pCP.nList1[0][0] = tmpNum; 
			   if(tmpNum > pCP.nPrior){
				 tmpNum= tmpNum - pCP.nPrior;
				 pSetFriedPrice("5PC", tmpNum, 1);
				 tmpNum= tmpNum + pCP.nPrior;
			   }
			 }  
		     else{ 	 
                 pCP.nList1[0][0] = tmpNum + pCP.nPrior;
                 pSetFriedPrice("5PC", tmpNum, 1);
         			
		     }
             
             for (j=0; j<Suace.length; j++) {
		   	   if (document.getElementById("" + Suace[j]).style.color == 'rgb(0, 136, 170)') {
		   	     pSauce += document.getElementById("" + Suace[j]).innerHTML + " ";		   	          	      
		   	   }                  
        	 }
             if (!pSauce == "") {
           	   pCP.nList1[0][1] = pSauce;
             }
             else {
               pCP.nList1[0][1] = "";
             }	             	               	                        	           	  
	         
             pSauce = "";              
           }    
                       
            tmpNum = parseInt(document.getElementById("" + pNug[1]).innerHTML);
            pCP.nPrior = pCP.nList1[1][0]; 
            
            //pCP.nList1[0][0] += tmpNum;
           if(tmpNum > 0){
			 if(pCP.modify){
			   pCP.nList1[1][0] = tmpNum; 
			   if(tmpNum > pCP.nPrior){
				 tmpNum= tmpNum - pCP.nPrior;
				 pSetFriedPrice("BUCK", tmpNum, 1);
				 tmpNum= tmpNum + pCP.nPrior;
			   }
			 }  
		     else{ 	 
                 pCP.nList1[1][0] = tmpNum + pCP.nPrior;
                 pSetFriedPrice("BUCK", tmpNum, 1);
         			
		     }
            
             for (var j=0; j<Suace.length; j++) {
		   	   if (document.getElementById("" + Suace[j]).style.color == 'rgb(0, 136, 170)') {
		   	      pSauce += document.getElementById("" + Suace[j]).innerHTML + " ";		   	          	      
		       }                  
             }
             if (!pSauce == "") {
               pCP.nList1[1][1] = pSauce;
             }
             else {
           	   pCP.nList1[1][1] = "";
             }	             	               	                        	           	               
           }            
                       
                  
          //clearing out the number boxes;         
            document.getElementById("" + pNug[0]).innerHTML = 0;
            document.getElementById("" + pNug[1]).innerHTML = 0;		

	   	   for (var j=0; j<Suace.length; j++) {
        	//Clearing out data
			  document.getElementById("" + Suace[j]).style.background = '#333';
			  document.getElementById("" + Suace[j]).style.border = "";
			  document.getElementById("" + Suace[j]).style.color = '#999';     	  
			  document.getElementById("" + Suace[j]).style.textShadow = "";				          	
	   	   }
	   	   
	   	   ReadNuggets();
           pCP.nug = true;         
         	   				  					  					 	     						      	 
    	   break;

// //////////////////////////////////////////////////      	    
    	 case "WINGS" :
    	   
         var wAry1 = ["5W", "10W", "15W", "20W", "25W", "30W"];
         var tmpNum = 0;
         
			for (var i=0; i<pWngs.length; i++) {
              tmpNum = parseInt(document.getElementById("" + pWngs[i]).innerHTML);
              if (tmpNum !== 0) {
           	    pProceed = true;
              }				
			}	
			
			if (!pProceed) { 
	           pCP.BodyMessage = "Select a Qty";
   	           p2ShowPopup();				
			   return; 
			}
                                                 	   
			for (var i=0; i<pWngs.length; i++) {				     

              tmpNum = parseInt(document.getElementById("" + pWngs[i]).innerHTML);
              pCP.wPrior =  pCP.wList1[i][0];
              
              if(tmpNum > 0){
				 if(pCP.modify){
					pCP.wList1[i][0] = tmpNum;
					if(tmpNum > pCP.wPrior){
					   tmpNum = tmpNum - pCP.wPrior;
					   pSetFriedPrice(wAry1[i], tmpNum, 1);
					   tmpNum = tmpNum + pCP.wPrior;
					}
				 }
                 else{
				   pCP.wList1[i][0] = tmpNum + pCP.wPrior; 
				   pSetFriedPrice(wAry1[i], tmpNum, 1);
			     }
			     
                 pSauce = "";             
                 for (var j=0; j<Suace.length; j++) {
		   	       if (document.getElementById("" + Suace[j]).style.color == 'rgb(0, 136, 170)') {
		   	          pSauce += document.getElementById("" + Suace[j]).innerHTML + " ";		   	          	      
		           }                  
                 }
                 if (!pSauce == "") {
                   pCP.wList1[i][1] = pSauce;
                 }
                 else {
           	       pCP.wList1[i][1] = "";
                 }	             	               	             			  			  			  					  					 	     						  
			  }  				  
            }  
                                                   
			
			//clearing out the number boxes;
			for (var i=0; i<pWngs.length; i++) {
			   document.getElementById("" + pWngs[i]).innerHTML = 0;
			   if (i == 5) {
	   	         for (var j=0; j<Suace.length; j++) {
        	      //Clearing out data
			       document.getElementById("" + Suace[j]).style.background = '#333';
			       document.getElementById("" + Suace[j]).style.border = "";
			       document.getElementById("" + Suace[j]).style.color = '#999';     	  
			       document.getElementById("" + Suace[j]).style.textShadow = "";				          	
	   	        }	
			  }
		   }	  
           
           pCP.wng = true;			
		   ReadWings();	    	 
    	   break;
    	   
    	 default: break;        
    }

    if(pCP.modify) {
      pCP.modify = false; 
 	  p2Clear(); 
      pCP.FootState = 20;     	      	
	  pSetFooter();	  
	  MVP();		
	}

	DisplayPrice();	
}

function p2StoreSandwhiches() {
	
	var SndwhTags = "pS18,pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29";
	var pTitle = document.getElementById("p2Cut").innerHTML;
	var pTitle2;
	var pMyNum;
	var pMyNum2;
	var pProceed = false;
	
	var hld = SndwhTags.split(",");

	
// check the txt box to see if it has a value > 0.
// then start coping the selected div's inner text into
// pCP.fList1[i][j] array

   if (pCP.modify) {
	  pMyNum =  pCP.Mcnt2;  
	  
	  for(var i=2; i<11; i++){
		 pCP.sList1[pMyNum][i] = "";
	  } 
   }
   else {			
      for (var i=0; i<pCP.sList1.length; i++) {
         if(pCP.sList1[i][0] == "") {
            pMyNum = i;         
            break;     
         }
      }
   }   

   if (pMyNum >7) {
	 pCP.BodyMessage = "Have reached max sandwhich count for this order";
   	 p2ShowPopup();	   	
   }	
   else {

/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/       	
   	for (var i=0; i<hld.length; i++) {
   	   if (document.getElementById("" + hld[i]).style.color == 'rgb(0, 136, 170)') {   	     
   	      pProceed = true;
   	   }      	      	   	   	       		   
   	}   	   	
   	
   	if (parseInt(document.getElementById("pTxt1").innerHTML) == 0 ) {
 	   pCP.BodyMessage = "Check Qty";
   	   p2ShowPopup();	   	
   	   return;
    }
      
    else if (!pProceed) {
 	   pCP.BodyMessage = "You must select food items";
   	   p2ShowPopup();	   	
       return;      
    }
/**************  END OF CHECKS ***************************/      
      	
   	else {	
   	  pCP.sList1[pMyNum][0] = document.getElementById("pTxt1").innerHTML;
   	  pCP.sList1[pMyNum][1] = pTitle;   	  
   	  pMyNum2 = 2;
   	
   	  for (var i=0; i<hld.length; i++) {
   	     if (document.getElementById("" + hld[i]).style.color == 'rgb(0, 136, 170)') {
   	        pCP.sList1[pMyNum][pMyNum2] = document.getElementById("" + hld[i]).innerHTML;
   	        pMyNum2++;   	      
   	     }      	      	   	   	       		   
   	  }
   	  pCP.snwh = true 
   	
        for (var j=0; j<hld.length; j++) {
        	//Clearing out data
		   document.getElementById("" + hld[j]).style.background = '#333';
		   document.getElementById("" + hld[j]).style.border = "";
		   document.getElementById("" + hld[j]).style.color = '#999';     	  
		   document.getElementById("" + hld[j]).style.textShadow = "";	
        }

        if(pCP.modify) {
          pCP.modify = false;
          pCP.snwhMod = false; 
	      p2Clear(); 
          pCP.FootState = 20;     	      	
	      pSetFooter();	  
	      MVP();		   
		}         		
	  }
	}
	document.getElementById("pTxt1").innerHTML = 1;
	
 // setting up prices for each sndwhich ordered	
	pTitle2 = pHalfCheck(pCP.sList1[pMyNum][1]);
	pSetPrice(pTitle2, pMyNum, pCP.sList1[pMyNum][0]);
	DisplayPrice();
				
}


function p2StoreSandwhiches2() {
	
	var SndwhTags = "pS18,pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29";
	var pTitle = document.getElementById("p2Cut").innerHTML;
	var pTitle2;
	var pMyNum;
	var pMyNum2;
	var pProceed = false;
	
	var hld = SndwhTags.split(",");

	
// check the txt box to see if it has a value > 0.
// then start coping the selected div's inner text into
// pCP.fList1[i][j] array

   if (pCP.modify) {
	  pMyNum =  pCP.Mcnt2;  
	  
	  for(var i=2; i<11; i++){
		 pCP.sList1[pMyNum][i] = "";
	  } 
   }
   else {			
      for (var i=0; i<pCP.sList1.length; i++) {
         if(pCP.sList1[i][0] == "") {
            pMyNum = i;         
            break;     
         }
      }
   }   

   if (pMyNum >7) {
	 pCP.BodyMessage = "Have reached max sandwhich count for this order";
   	 p2ShowPopup();	
   }	
   else {

/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/       	
   	for (var i=0; i<hld.length; i++) {
   	   if (document.getElementById("" + hld[i]).style.color == 'rgb(0, 136, 170)') {   	     
   	      pProceed = true;
   	   }      	      	   	   	       		   
   	}   	   	
   	
   	if (parseInt(document.getElementById("pTxt1").innerHTML) == 0 ) {
	   pCP.BodyMessage = "Check Qty";
   	   p2ShowPopup();
   	   return;
    }
      
    else if (!pProceed) {
	   pCP.BodyMessage = "You must select food items";
   	   p2ShowPopup();
       return;      
    }
/**************  END OF CHECKS ***************************/      
      	
   	else {	
   	  pCP.sList1[pMyNum][0] = document.getElementById("pTxt1").innerHTML;
   	  pCP.sList1[pMyNum][1] = pTitle;   	  
   	  pMyNum2 = 2;
   	
   	  for (var i=0; i<hld.length; i++) {
   	     if (document.getElementById("" + hld[i]).style.color == 'rgb(0, 136, 170)') {
   	        pCP.sList1[pMyNum][pMyNum2] = document.getElementById("" + hld[i]).innerHTML;
   	        pMyNum2++;   	      
   	     }      	      	   	   	       		   
   	  }
   	  pCP.snwh = true 
   	
        for (var j=0; j<hld.length; j++) {
        	//Clearing out data
		   document.getElementById("" + hld[j]).style.background = '#333';
		   document.getElementById("" + hld[j]).style.border = "";
		   document.getElementById("" + hld[j]).style.color = '#999';     	  
		   document.getElementById("" + hld[j]).style.textShadow = "";	
        }

        if(pCP.modify) {
          pCP.modify = false;
          pCP.snwhMod = false; 
	      p2Clear(); 
          //pCP.FootState = 20;     	      	
	      //pSetFooter();	  
	      //MVP();		   
		}         		
	  }
	}
	document.getElementById("pTxt1").innerHTML = 1;
				
}

function pHalfCheck(ptext){
	
   var hld;
             
   if ((ptext.search("1/2 ")) !== -1) {
  // removing the cut sandwhich in half
      hld=ptext.split("1/2 ");	
  	  ptext = hld[1];
   }
   return ptext;  	
}

function pSetPrice(pTitle, pElm, qty){
   
   var Cheese = Bacon = false;
   var fdStr = "";
   var xElm;
   
   for(var i=0; i<pCP.Prices.length; i++){
	  if(pCP.Prices[i][0] == pTitle){
		 if(qty > 1){
			pCP.cmbPlcHldr.push(true);
			pCP.AryLen.push(1);
		 }
		 else{ 
			pCP.cmbPlcHldr.push(false);
			pCP.AryLen.push(0);
			 
		 }
		 
		 for(var j=0; j<pCP.cmbT.length; j++){
			if(pCP.cmbT[j][0] == ""){
			   xElm = j;
			   break;
			}
		 }
	 
		 pCP.cmbT[xElm][0] = "s";
		 pCP.cmbT[xElm][1] = pElm;
		 pCP.cmbT[xElm][2] = pTitle;
		 pCP.cmbT[xElm][3] = qty;
		 pCP.cmbT[xElm][4] = 0;       // Reg Fry
		 pCP.cmbT[xElm][5] = 0;       // LG Fry
		 pCP.cmbT[xElm][6] = 0;       // Reg Drk
		 pCP.cmbT[xElm][7] = 0;       // Lg Drk
		 pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		 pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name 		 		 		 
		 pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		 pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		 		 
		 pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		 pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		 pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		 pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		 pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		 pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		 
		 //alert("pCP.cmbT[xElm] values are " + pCP.cmbT[xElm]);
		 
		 for(var j=0; j<pCP.sList1[pElm].length; j++){
			if(pCP.sList1[pElm][j] == "BACON" && 
			   pTitle !== "CLUB" && pTitle !== "CLUB B" &&
			   pTitle !== "GRILL CL"){
		       Bacon = true;     			   
			}
			else if(pCP.sList1[pElm][j] == "CHEESE" && 
			   pTitle !== "CLUB" && pTitle !== "CLUB B" &&
			   pTitle !== "GRILL CL"){
		       Cheese = true;				
			}
		 }
	     pCP.hPrices.push(qty * pCP.Prices[i][1]);
		 pCP.tPrices.push(qty * pCP.Prices[i][1]);		 
		 
		 if(Bacon){
            pCP.hPrices.push(qty * pCP.Prices[27][1]);
		    pCP.tPrices.push(qty * pCP.Prices[27][1]);			 
		 }
		 else if(Cheese){
			pCP.hPrices.push(qty * pCP.Prices[27][0]);
		    pCP.tPrices.push(qty * pCP.Prices[27][0]); 
		 } 
		 break;
	  }  
   }
}

function pSetFriedPrice(pTitle, qty, Sel1){
   
   var bTest = false;
   var xElm;
   
   for(var i=0; i<pCP.Prices.length; i++){
	  if(pTitle == "FRIES"){
	 	 pCP.hPrices.push(qty * pCP.Prices[25][Sel1]);
	 	 pCP.tPrices.push(qty * pCP.Prices[25][Sel1]);	 		  
	 	 break;		  
	  }
	  else{
	    if(pCP.Prices[i][0] == pTitle){
		   
		   for(var j=0; j<pCP.cmbT.length; j++){
			  if(pCP.cmbT[j][0] == ""){
			     xElm = j;
			     break;
			  }
		   }
		   		   
		   if(qty > 1){
			  pCP.cmbPlcHldr.push(true);
			  pCP.AryLen.push(1);
		   }
		   else{ 
			  pCP.cmbPlcHldr.push(false);
			  pCP.AryLen.push(0); 
		   }			
		   
		   switch(pTitle){
			  case "5PC":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);	 		  
	 		    pCP.cmbT[xElm][0] = "n";
		        pCP.cmbT[xElm][1] = 0;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name 	 		    
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        
	 		    bTest = true;
			    break;
			  
			  case "BUCK":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "n";
		        pCP.cmbT[xElm][1] = 1;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name		 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        
	 		    bTest = true;			    
			    break;
			    
			  case "5W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 0;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name		 		     		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
			  case "10W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 1;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name			 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
			  case "15W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 2;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name			 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
			  case "20W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 3;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name			 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
			  case "25W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 4;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name			 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element	 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
			  case "30W":
	 		    pCP.hPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.tPrices.push(qty * pCP.Prices[i][Sel1]);
	 		    pCP.cmbT[xElm][0] = "w";
		        pCP.cmbT[xElm][1] = 5;
		        pCP.cmbT[xElm][2] = pTitle;
		        pCP.cmbT[xElm][3] = qty;
		        pCP.cmbT[xElm][4] = 0;       // Reg Fry
		        pCP.cmbT[xElm][5] = 0;       // LG Fry
		        pCP.cmbT[xElm][6] = 0;       // Reg Drk
		        pCP.cmbT[xElm][7] = 0;       // Lg Drk
		        pCP.cmbT[xElm][8] = "NA";    // Rg Drk Name
		        pCP.cmbT[xElm][9] = "NA";    // Lg Drk Name			 		    	 		  
		        pCP.cmbT[xElm][10] = "NA";   // Rg Drk Element
		        pCP.cmbT[xElm][11] = "NA";   // Lg Drk Element		 		    
    		    pCP.cmbT[xElm][12] = 0;      // Rg Drk ExIce
		        pCP.cmbT[xElm][13] = 0;      // Rg Drk LtIce		 
		        pCP.cmbT[xElm][14] = 0;      // Rg Drk NoIce
		        pCP.cmbT[xElm][15] = 0;      // Lg Drk ExIce
		        pCP.cmbT[xElm][16] = 0;      // Lg Drk LtIce
		        pCP.cmbT[xElm][17] = 0;      // Lg Drk NoIce		 		 		 
		        	 		    
	 		    bTest = true;
	 		    break;
	 		    
	 		  default: break;  	 		    	 		    	 		    	 		    	 		    			      
		   }
		   if(bTest){ break; }
	    }
	  }  
   }	
}

function p2StoreDrinks() {
	
   var dAry1 = ["pS54", "pS55", "pS56", "pS57", "pS58", 
                "pS59", "pS60", "pS61", "pS62"];		

   var pProceed = false;
   var tmpNum = 0;
   var tmpNum2 = 0;
   var num1 = num2 = 0;
   var pElm = 0;
   var pIceType = "";
   var pTitle;
   
/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/
          
    tmpNum = parseInt(document.getElementById("pTxt13").innerHTML);
    tmpNum2 =  parseInt(document.getElementById("pTxt14").innerHTML);

    if (tmpNum !== 0 || tmpNum2 !== 0) { pProceed = true; }				
			
	if (!pProceed) { 
	  pCP.BodyMessage = "Select a Qty";
   	  p2ShowPopup();
	  return; 
	}
	
/**************  END OF CHECKS ***************************/

	for(i=0; i<dAry1.length; i++){
	   if(document.getElementById(""+dAry1[i]).style.color == 'rgb(0, 136, 170)') {
		   pTitle = document.getElementById(""+dAry1[i]).innerHTML;
		   break;
	   }
	}
   
   for (var i=0; i<dAry1.length; i++) {
   	if (pCP.dList1[i][0] == pTitle) { 
   	  pElm = i; 
   	  break; 
   	}
   }
   
   num1 = pCP.dList1[pElm][1];
   num2 = pCP.dList1[pElm][2];     
   
   //pCP.NcmbT[0][0] = [elm, type, qty, n + ice]
/*   
   //pCP.dList1[pElm][1] = document.getElementById("pTxt13").innerHTML;
   //pCP.dList1[pElm][2] = document.getElementById("pTxt14").innerHTML;
   
   pCP.dList1[pElm][3][0] = document.getElementById("pTxt15").innerHTML;
   pCP.dList1[pElm][3][1] = document.getElementById("pTxt16").innerHTML;
   pCP.dList1[pElm][3][2] = document.getElementById("pTxt17").innerHTML;   	 
   
   pCP.dList1[pElm][4][0] = document.getElementById("pTxt18").innerHTML;
   pCP.dList1[pElm][4][1] = document.getElementById("pTxt19").innerHTML;
   pCP.dList1[pElm][4][2] = document.getElementById("pTxt20").innerHTML;  
*/   
   // ------------------------------
   //  Reading REG drink number box
   // ------------------------------
   tmpNum = parseInt(document.getElementById("pTxt13").innerHTML);   
   if(tmpNum > 0){
	 if(pCP.modify){
        pCP.dList1[pElm][1] = tmpNum;
        if(tmpNum > num1){
		  tmpNum= tmpNum - num1;
          pCP.hPrices.push(tmpNum * pCP.Prices[26][0]);
          pCP.tPrices.push(tmpNum * pCP.Prices[26][0]);
          pCP.dList1[pElm][1] = tmpNum + num1;          			
		}
     }
     else{
       pCP.dList1[pElm][1] = tmpNum + num1;
       //pCP.NcmbT[0][0] = [elm, type, qty, n + ice]
       pCP.NcmbT[pElm][0] = pElm;
       pCP.NcmbT[pElm][1] = 1;
       pCP.NcmbT[pElm][3] = tmpNum;
       pCP.hPrices.push(tmpNum * pCP.Prices[26][0]);
       pCP.tPrices.push(tmpNum * pCP.Prices[26][0]);     
     }
     //document.getElementById("pdDis1").innerHTML = pCP.dList1[pElm][1];
     //alert(pCP.dList1[pElm][1]);
   }  
   
   document.getElementById("pdDis1").innerHTML = pCP.dList1[pElm][1];
   
   tmpNum2 = parseInt(document.getElementById("pTxt15").innerHTML);    
   if(tmpNum2 > 0){
	 if(pCP.modify){
	   pCP.dList1[pElm][3][0] = tmpNum2; 
	 }
	 else{
       pCP.dList1[pElm][3][0] += tmpNum2;
       if(pCP.NcmbT[pElm][3] == ""){
         pCP.NcmbT[pElm][5] =  "(" + tmpNum2 + ") Ex Ice";
       }
       else{
		  pCP.NcmbT[pElm][5] += ",(" + tmpNum2 + ") Ex Ice"; 
	   }        
     }     	 
   }
   
   tmpNum2 = parseInt(document.getElementById("pTxt16").innerHTML);    
   if(tmpNum2 > 0){   
	 if(pCP.modify){
	   pCP.dList1[pElm][3][1] = tmpNum2; 
	 }
	 else{
       pCP.dList1[pElm][3][1] += tmpNum2;
       if(pCP.NcmbT[pElm][5] == ""){
         pCP.NcmbT[pElm][5] =  "(" + tmpNum2 + ") Lt Ice";
       }
       else{
		  pCP.NcmbT[pElm][5] += ",(" + tmpNum2 + ") Lt Ice"; 
	   }        
     }
   }
   	
   tmpNum2 = parseInt(document.getElementById("pTxt17").innerHTML);    
   if(tmpNum2 > 0){   
	 if(pCP.modify){
	   pCP.dList1[pElm][3][2] = tmpNum2; 
	 }
	 else{
       pCP.dList1[pElm][3][2] += tmpNum2;
       if(pCP.NcmbT[pElm][5] == ""){
         pCP.NcmbT[pElm][5] =  "(" + tmpNum2 + ") No Ice";
       }
       else{
		  pCP.NcmbT[pElm][5] += ",(" + tmpNum2 + ") No Ice"; 
	   }        
    }
   }  
   
 //clearing out the ice number boxes;
   for(var i=13; i<18; i++){
	  if(i !== 14){
        document.getElementById("pTxt" + i).innerHTML = 0; 		  
	  }
   }     	  
  
   // -----------------------------
   //   Reading LG drink number box
   // ----------------------------
   tmpNum = parseInt(document.getElementById("pTxt14").innerHTML);
   if(tmpNum > 0){
	 if(pCP.modify){
        pCP.dList1[pElm][2] = tmpNum;
        if(tmpNum > num2){
		  tmpNum= tmpNum - num2;
          pCP.hPrices.push(tmpNum * pCP.Prices[26][1]);
          pCP.tPrices.push(tmpNum * pCP.Prices[26][1]);
          pCP.dList1[pElm][2] = tmpNum + num2;          			
		}
     }
     else{
       pCP.dList1[pElm][2] = tmpNum + num2;
       pCP.hPrices.push(tmpNum * pCP.Prices[26][1]);
       pCP.tPrices.push(tmpNum * pCP.Prices[26][1]);
       pCP.NcmbT[pElm][0] = pElm;
       pCP.NcmbT[pElm][2] = 2;
       pCP.NcmbT[pElm][4] = tmpNum;
   // elm,  Rtype,   Ltype,   Rqty,   Lqty,  R_ice,  L_ice            
     }
     //document.getElementById("pdDis1").innerHTML = pCP.dList1[pElm][1];
     //alert(pCP.dList1[pElm][1]);
   }
   
   document.getElementById("pdDis2").innerHTML = pCP.dList1[pElm][2];
    
   tmpNum2 = parseInt(document.getElementById("pTxt18").innerHTML);
   if(tmpNum2 > 0){
	 if(pCP.modify){
	   pCP.dList1[pElm][4][0] = tmpNum2; 
	 }
	 else{
       pCP.dList1[pElm][4][0] += tmpNum2;
       if(pCP.NcmbT[pElm][6] == ""){
         pCP.NcmbT[pElm][6] =  "(" + tmpNum2 + ") Ex Ice";
       }
       else{
		  pCP.NcmbT[pElm][6] += ",(" + tmpNum2 + ") Ex Ice"; 
	   }        
     }
   }
      	 
   tmpNum2 = parseInt(document.getElementById("pTxt19").innerHTML);    
   if(tmpNum2 > 0){   
	 if(pCP.modify){
	   pCP.dList1[pElm][4][1] = tmpNum2; 
	 }
	 else{
       pCP.dList1[pElm][4][1] += tmpNum2;
       if(pCP.NcmbT[pElm][5] == ""){
         pCP.NcmbT[pElm][6] =  "(" + tmpNum2 + ") Lt Ice";
       }
       else{
		  pCP.NcmbT[pElm][6] += ",(" + tmpNum2 + ") Lt Ice"; 
	   }        
     }
   }
   	
   tmpNum2 = parseInt(document.getElementById("pTxt20").innerHTML);    
   if(tmpNum2 > 0){
	 if(pCP.modify){
	   pCP.dList1[pElm][4][2] = tmpNum2; 
	 }
	 else{
       if(pCP.NcmbT[pElm][6] == ""){
         pCP.NcmbT[pElm][6] =  "(" + tmpNum2 + ") No Ice";
       }
       else{
		  pCP.NcmbT[pElm][6] += ",(" + tmpNum2 + ") No Ice"; 
	   }        
     }
   }
   
 //clearing out the ice number boxes;
   document.getElementById("pTxt14").innerHTML = 0; 
   for(var i=18; i<21; i++){
     document.getElementById("pTxt" + i).innerHTML = 0; 		  
   }
   
   pCP.drk = true;
   pCP.dSelected = "";
   
   if (pCP.modify) {
     pCP.modify = false; 
	 p2Clear(); 
     pCP.FootState = 20;     	      	
	 pSetFooter();	  
	 MVP(); 
   }
   
   //p2ReadDrinks(""+dAry1[pElm]);
   DisplayPrice();	  
              	        	
}

function p2ReadSandwhiches() {
	
	var pSel= document.getElementsByClassName("pSel");	

    document.getElementById("pTxt1").innerHTML = pCP.sList1[pCP.Mcnt2][0];
           
    for (var i=2; i<9; i++) {
	  if (pCP.sList1[pCP.Mcnt2][i] !== "") {
		for (var j=0; j<pSel.length; j++) {	
		   if (pCP.sList1[pCP.Mcnt2][i] == pSel[j].innerHTML) {
			  pTogState(j);
			  break;
		   }	
		}
	  }
	  else { break; }
	}
	pCP.snwhMod = true;	
}

function p2ReadDrinks2(){
                
   var dAry1 = ["pS54", "pS55", "pS56", "pS57", "pS58",
                "pS59", "pS60", "pS61", "pS62"];
   
   //var pElm;
   
   for(var i=0; i<dAry1.length; i++){
	 if(document.getElementById(""+dAry1[i]).style.color == 'rgb(0, 136, 170)') {
       document.getElementById("pdDis1").innerHTML = pCP.dList1[i][1];
       document.getElementById("pdDis2").innerHTML = pCP.dList1[i][2];
       //pElm = i;	   
	 }  
   } 	   	
                	
}

function p2ReadDrinks(id) {
   
   var dAry1 = ["pS54", "pS55", "pS56", "pS57", "pS58",
                "pS59", "pS60", "pS61", "pS62"];
             	
   var pTitle; 
   var dSplit;	
   var pElm = 0;
        
   if(pCP.modify && pCP.dSelected == "" && id == undefined){
	   //alert("Select a drink");
	   pCP.dProceed = true;
	   return;
   }

   
   if(pCP.dSelected == undefined || pCP.dSelected == "" && id == undefined){
	 pTitle = document.getElementById("" + id).innerHTML;
	 //alert("pCP.dSelected = " + pCP.dSelected + " and the id = " + id);
   }
   else {
	 pTitle = pCP.dSelected;
	 p2CTog("none", 17, 26);
	 
	 for(var i=0; i<dAry1.length; i++){
		if(document.getElementById("" + dAry1[i]).innerHTML == pTitle){
	      document.getElementById("" + dAry1[i]).style.background = '#333';   	  	
	      document.getElementById("" + dAry1[i]).style.border = '2px solid #08A';    	  
	      document.getElementById("" + dAry1[i]).style.color = '#08A';
	      document.getElementById("" + dAry1[i]).style.textShadow = '1px 1px 1px #FFF';
		}
	 }
     	 
   }	  
      
   for (var i=0; i<pCP.dList1.length; i++) {
   	 if (pCP.dList1[i][0] == pTitle) { 
   	   pElm = i; 
   	   break; 
   	 }
   }  
   
   if(pCP.modify){
       document.getElementById("pTxt13").innerHTML = pCP.dList1[pElm][1];
       document.getElementById("pTxt14").innerHTML = pCP.dList1[pElm][2];
   } 
   document.getElementById("pTxt15").innerHTML = pCP.dList1[pElm][3][0];
   document.getElementById("pTxt16").innerHTML = pCP.dList1[pElm][3][1];
   document.getElementById("pTxt17").innerHTML = pCP.dList1[pElm][3][2];   	 
   
   document.getElementById("pTxt18").innerHTML = pCP.dList1[pElm][4][0];
   document.getElementById("pTxt19").innerHTML = pCP.dList1[pElm][4][1];
   document.getElementById("pTxt20").innerHTML = pCP.dList1[pElm][4][2];      
   	
}

function ReadFries() {	
   document.getElementById("pfDis1").innerHTML = pCP.fList1[0];
   document.getElementById("pfDis2").innerHTML = pCP.fList1[1];			
}

function ReadWings() {
	
    var testStr;
    var testStr2;
    
	var pWngs = ["pwDis1", "pwDis2", "pwDis3", 
	             "pwDis4", "pwDis5", "pwDis6"];
	             
	var Suace = ["pS70", "pS71", "pS72"];    
    
    for (var i=0; i<pWngs.length; i++) {
	  document.getElementById("" + pWngs[i]).innerHTML = pCP.wList1[i][0];
	}
	
    if(pCP.wList1[0][1] !== ""){
	   testStr2 = pCP.wList1[0][1];
	   testStr = testStr2.split(" ");
	   
	   if(testStr.length > 2) {
		   for(var i=0; i<testStr.length; i++){
			   switch(testStr[i]){
				   case "BBQ":
				     document.getElementById("pS70").style.background = '#333';   	  	
				     document.getElementById("pS70").style.border = '4px solid #08A';    	  
				     document.getElementById("pS70").style.color = '#08A';
				     document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "HON_MUST":
				     document.getElementById("pS71").style.background = '#333';   	  	
				     document.getElementById("pS71").style.border = '4px solid #08A';    	  
				     document.getElementById("pS71").style.color = '#08A';
				     document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "RANCH":
				     document.getElementById("pS72").style.background = '#333';   	  	
				     document.getElementById("pS72").style.border = '4px solid #08A';    	  
				     document.getElementById("pS72").style.color = '#08A';
				     document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   default: break;  				      				      
			   }
		   }
	   }
	   else{
		   switch(testStr[0]){
			 case "BBQ":
			   document.getElementById("pS70").style.background = '#333';   	  	
			   document.getElementById("pS70").style.border = '4px solid #08A';    	  
			   document.getElementById("pS70").style.color = '#08A';
			   document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "HON_MUST":
			   document.getElementById("pS71").style.background = '#333';   	  	
			   document.getElementById("pS71").style.border = '4px solid #08A';    	  
			   document.getElementById("pS71").style.color = '#08A';
			   document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "RANCH":
			   document.getElementById("pS72").style.background = '#333';   	  	
			   document.getElementById("pS72").style.border = '4px solid #08A';    	  
			   document.getElementById("pS72").style.color = '#08A';
			   document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 default: break;  				      				      
		   }		    
	   }
	}   		
}


function ReadNuggets() {

    var testStr;
    var testStr2;
    
	document.getElementById("pnDis1").innerHTML = pCP.nList1[0][0];
    document.getElementById("pnDis2").innerHTML = pCP.nList1[1][0];
    
    if(pCP.nList1[0][1] !== ""){
	   testStr2 = pCP.nList1[0][1];
	   testStr = testStr2.split(" ");
	   
	   if(testStr.length > 2) {
		   for(var i=0; i<testStr.length; i++){
			   switch(testStr[i]){
				   case "BBQ":
				     document.getElementById("pS70").style.background = '#333';   	  	
				     document.getElementById("pS70").style.border = '4px solid #08A';    	  
				     document.getElementById("pS70").style.color = '#08A';
				     document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "HON_MUST":
				     document.getElementById("pS71").style.background = '#333';   	  	
				     document.getElementById("pS71").style.border = '4px solid #08A';    	  
				     document.getElementById("pS71").style.color = '#08A';
				     document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "RANCH":
				     document.getElementById("pS72").style.background = '#333';   	  	
				     document.getElementById("pS72").style.border = '4px solid #08A';    	  
				     document.getElementById("pS72").style.color = '#08A';
				     document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   default: break;  				      				      
			   }
		   }
	   }
	   else{
		   switch(testStr[0]){
			 case "BBQ":
			   document.getElementById("pS70").style.background = '#333';   	  	
			   document.getElementById("pS70").style.border = '4px solid #08A';    	  
			   document.getElementById("pS70").style.color = '#08A';
			   document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "HON_MUST":
			   document.getElementById("pS71").style.background = '#333';   	  	
			   document.getElementById("pS71").style.border = '4px solid #08A';    	  
			   document.getElementById("pS71").style.color = '#08A';
			   document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "RANCH":
			   document.getElementById("pS72").style.background = '#333';   	  	
			   document.getElementById("pS72").style.border = '4px solid #08A';    	  
			   document.getElementById("pS72").style.color = '#08A';
			   document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 default: break;  				      				      
		   }		    
	   }
	}
	
}

function p2StoreMoreAns(){
	
   var p2CheckAry1 = ["pS8",  "pS9",  "pS10", "pS11",
                      "pS12", "pS13", "pS14", "pS15",
                      "pS36", "pS37", "pS38", "pS39",
                      "pS40", "pS41", "pS42", "pS43",
                      "pS54", "pS55", "pS56", "pS57",
                      "pS58", "pS59", "pS60", "pS61",
                      "pS62", "pS2"];                   	
    
   var SndwhTags = "pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29";
   var mChoice   = "pChce1,pChce2,pChce3,pChce4,pChce5," +
                   "pChce6,pChce7,pChce8,pChce9,pChce10";
                      
   var hld = SndwhTags.split(",");
   var hld2 = pCP.MoreId.split(",");
   var hld3 = mChoice.split(","); 
   var test = false;
   var ptext;
   var x;        

/*
   if (pCP.winW < 378) {
	  p2SandwhichHTML(73); 
   }
   else{
	  p2SandwhichHTML(47); 
   }
*/   
// Loading all the info from more screen to main screen   
   for(var i=0; i<hld.length; i++){ 			   
     document.getElementById("" + hld[i]).innerHTML = 
	 document.getElementById("" + hld3[i]).innerHTML; 
   }
   
// Checking for burgers 
   for(var i=0; i<9; i++){
	 if (document.getElementById(""+p2CheckAry1[i]).style.color == 'rgb(0, 136, 170)') {	
		test = true;
		p2Clear();
		        
        if (pCP.winW < 378) {		
		  document.getElementById("pSelector1").style.display = "none";
		  document.getElementById("pSelector2").style.display = "block";
		  document.getElementById("pSelector3").style.display = "block";
		  		  
		}
		else {
		  document.getElementById("pSelector1").style.display = "block";  
		  document.getElementById("pSelector2").style.display = "block";
		  document.getElementById("pSelector3").style.display = "block";
		  
		}		
		
		//alert(pCP.modify);
		if(pCP.modify){
          pCP.FootState = 17;     	      	
	      pSetFooter();										
		  break;
		}
		else{
          pCP.FootState = 15;     	      	
	      pSetFooter();										
		  break;			
		}  
	 }  
  }
  if(test){ p2ClearMoreItems(); return; }
      
// Checking for chicken 
  for(var i=8; i<16; i++){
	 if (document.getElementById(""+p2CheckAry1[i]).style.color == 'rgb(0, 136, 170)') {	
		test = true;
		p2Clear();

        if (pCP.winW < 378) {		
		  document.getElementById("pSelector1").style.display = "none";
		  document.getElementById("pSelector5").style.display = "block";
		  document.getElementById("pSelector3").style.display = "block";
		  		  
		}
		else {
		  document.getElementById("pSelector1").style.display = "block";  
		  document.getElementById("pSelector5").style.display = "block";
		  document.getElementById("pSelector3").style.display = "block";
		  
		}			
		
		if(pCP.modify){
          pCP.FootState = 17;     	      	
	      pSetFooter();										
		  break;
		}
		else{
          pCP.FootState = 15;     	      	
	      pSetFooter();										
		  break;			
		}  
	 }  
  }
  if(test){ p2ClearMoreItems(); return; }
  
// Checking for Hot Dogs
  if (document.getElementById(""+p2CheckAry1[p2CheckAry1.length-1]).style.color == 'rgb(0, 136, 170)') {	
	p2Clear();
    test = true;
    
	document.getElementById("pSelector1").style.display = "block";
	document.getElementById("pSelector3").style.display = "block";			   
		
	if(pCP.modify){
       pCP.FootState = 17;     	      	
	   pSetFooter();										
	}
	else{
       pCP.FootState = 15;     	      	
	   pSetFooter();													
	} 										
		 
  }
  if(test){ p2ClearMoreItems(); return; }      
         	
}

function p2ClearMoreItems(){

   var mChoice   = "pChce1,pChce2,pChce3,pChce4,pChce5," +
                   "pChce6,pChce7,pChce8,pChce9,pChce10";
                  
   var hld3 = mChoice.split(",");
   var ptext;
   var x;                       

   //setting back to orignal wording 
     for(var i=0; i<hld3
     .length; i++){
	   ptext=document.getElementById(""+hld3[i]).innerHTML;
	 
       if((ptext.search("Ex ")) !== -1){
	      x=ptext.split("Ex ");
	      document.getElementById(""+hld3[i]).innerHTML = x[1];   
       }
 
       if((ptext.search("Lt ")) !== -1){
	      x=ptext.split("Lt ");
	      document.getElementById(""+hld3[i]).innerHTML = x[1];   
       }	  
    }
    
    p2MoreHTML(); 	
}

function p2SandwhichHTML(width) {
	
   var Sndwh = document.getElementById("pSelector3");
   var SndwhAry1 = ['<div class="pSel3" id="p2Cut" onclick="pModifyItem(this.id)">HALF</div>',
                    '<div class="pSel3" id="pTxt1" onclick="pChangeVal(this.id)">0</div>', //0
                    '<div class="pSel" id="pS16" onclick="pTxtDis(this.id)">QTY</div>',    //2
                    '<div class="pSel" id="pS17" onclick="TogItem(this.id)">REG</div>',    //3 
                    '<div class="pSel" id="pS18" onclick="TogItem(this.id)">PLAIN</div>',  //4
                    '<div class="pSel" id="pS19" onclick="TogItem(this.id)">TOM</div>',    //5
                    '<div class="pSel" id="pS20" onclick="TogItem(this.id)">MAYO</div>',   //6
                    '<div class="pSel" id="pS21" onclick="TogItem(this.id)">KET</div>',    //7
                    '<div class="pSel" id="pS22" onclick="TogItem(this.id)">PIC</div>',    //8
                    '<div class="pSel" id="pS23" onclick="TogItem(this.id)">ONION</div>',  //9
                    '<div class="pSel" id="pS24" onclick="TogItem(this.id)">LET</div>',    //10
                    '<div class="pSel" id="pS25" onclick="TogItem(this.id)">MUST</div>',   //11
                    '<div class="pSel" id="pS27" onclick="TogItem(this.id)">BACON</div>',  //12
                    '<div class="pSel" id="pS28" onclick="TogItem(this.id)">CHEESE</div>', //13
                    '<div class="pSel" id="pS29" onclick="TogItem(this.id)">CHILI</div>',  //14
                    '<div class="pSel3" id="p2Choice" onclick="Xtra1()">MORE</div>'];      //15
                    
   var p2TagsAry1 = ["p2Cut", "pTxt1", "pS16", "pS17", "pS18", "pS19",
                     "pS20",  "pS21", "pS22", "pS23", "pS24", 
                     "pS25",  "pS27", "pS28", "pS29", "p2Choice"];
                     
   var SndwhCSS = ["float: left; width: 97%; " +
		           "height: 45px; margin-top: 1%; " +
		           "margin-bottom: 1%; padding-top: 5%; " +
		           "background: #333; border: 2px solid #AAA;" +     //0
		           "text-align: center; color: #999; " + 
		           "font-weight: bold; font-size: 16pt; " +
		           "cursor: pointer;",
		           
                   "float: left; width: 23.3%; " +
		           "height: 45px; margin-top: 1%; " +
		           "margin-bottom: 1%; padding-top: 5%; " +
		           "background: #FFF; border: 2px solid #AAA;" +     //0
		           "text-align: center; color: #000; " + 
		           "font-weight: bold; font-size: 16pt; " +
		           "cursor: pointer;",
		           
		           "float: left; width: 72%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //1
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
 		           "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //2
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
		           "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //3
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //4
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //5
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //6
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //7
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //8
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " +  //9
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " + //10
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " + //11
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " + //12
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 47.5%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " + //13
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;",
                   
                   "float: left; width: 97%; height: 45px; " +
                   "margin-top: 1%; margin-bottom: 1%; " +
                   "padding-top: 5%; background: #333; " +
                   "border: 2px solid #AAA; text-align: center; " + //13
                   "color: #999; font-weight: bold; " +
                   "font-size: 16pt; cursor: pointer;"                                     
                   ];                                      
 
   Sndwh.innerHTML = "";

   Sndwh.style.float = "left";
   Sndwh.style.width = width + "%";
   Sndwh.style.heigth = "auto";   
   
                         
// HTML Load
//-------------

   for(var i=0; i<SndwhAry1.length; i++){
	  if(i == 0){
		Sndwh.innerHTML = SndwhAry1[i];  
	  }
	  else{
		Sndwh.innerHTML += SndwhAry1[i];  
	  }  
   }
            
// CSS Load
//-------------

    for(var i=0; i<SndwhAry1.length; i++){
	   document.getElementById(""+p2TagsAry1[i]).style.cssText= SndwhCSS[i];
	}           
}

function WinSet2(id) {
   var pWSide = document.getElementsByClassName("pWSide");
   
   for (var i=0; i<pWSide.length; i++) {   	
     if (pWSide[i].id == id ) {
     	  
     	  // Clear old data
     	  pWSide[0].style.background = '#333';
     	  pWSide[0].style.border = "";
     	  pWSide[0].style.color = '#999';     	  
     	  pWSide[0].style.textShadow = "";	
     	       	  
     	  pWSide[1].style.background = '#333';
     	  pWSide[1].style.border = "";     	  
     	  pWSide[1].style.color = '#999';     	  
     	  pWSide[1].style.textShadow = "";
     	  
     	  if (i<1) { 
     	    pWSide[1].style.background = '#777';
     	    pWSide[1].style.border = '4px solid #08A';      	     
     	  }
     	  else { 
     	    pWSide[0].style.background = '#777';
     	    pWSide[0].style.border = '4px solid #08A'; 
     	  }
     	      	  
     	  pWSide[i].style.color = '#08A';
     	  pWSide[i].style.textShadow = '1px 1px 1px #FFF';
     	  pCP.WinSide = pWSide[i].innerHTML;	
     }
   }  
}

function BagSet(){
  var BagCont = document.getElementById("BagContainer");
  
  if (BagCont.style.color !== 'rgb(0, 136, 170)') {
    BagCont.style.background = '#333';
    BagCont.style.border = '1px solid #FFF';
    BagCont.style.color = '#08A';
    BagCont.style.textShadow = '#FFF';
    pCP.BagSep = "Yes";
  }
  else{
    BagCont.style.background = '';
    BagCont.style.border = '';    	  
    BagCont.style.color = '';
    BagCont.style.textShadow = '';
    pCP.BagSep = "No";    
  } 
}

function p2SelViews(id, state1, RetState){
	
	pCP.retState = RetState;
	
	if(state1 < 7){
	  if(pCP.mState !== state1){
		 p2ClearToggles();
	  }
	  
	  if(state1 == 0 || state1 == 1 || state1 == 2){
		 pCP.OrdState = 1;
	  }
	  else if(state1 == 3 || state1 == 4 || state1 == 5){
		 pCP.OrdState = 2; 
	  }
	  else{
		 pCP.OrdState = 3;
	  }
	}
	  
	switch(state1){
// ------------------ Main Menu Burgers ------------------------------		
	   case 0:
	     p2Clear();
	     if (document.getElementById(id).style.color == 'rgb(0, 136, 170)') {
           document.getElementById("pSelector2").style.display = "none";	       
 	       pCP.FootState = 14;     	      	
           pSetFooter();			 
		 }
		 else{
	       document.getElementById("pSelector2").style.display = "block";
	       p2SandwhichHTML(47);
           p2CTog(0, 8);
	       pCP.FootState = 14;     	      	
           pSetFooter();           	       			 
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;
	     
// ------------------ Main Menu Hot Dog ------------------------------
	   case 1:
	     p2Clear();
	     if (document.getElementById(id).style.color == 'rgb(0, 136, 170)') {
           document.getElementById("pSelector3").style.display = "none";
 	       pCP.FootState = 14;     	      	
           pSetFooter();                     			 
		 }
		 else{
	       document.getElementById("pSelector3").style.display = "block";
	       p2SandwhichHTML(73);	       
	       document.getElementById("p2Cut").innerHTML = document.getElementById(""+id).innerHTML;
	       pCP.FootState = 15;     	      	
           pSetFooter();	       			 
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;

// ------------------ Main Menu Chicken ------------------------------
	   case 2:
	     p2Clear();
	     if (document.getElementById(""+id).style.color == 'rgb(0, 136, 170)') {
            document.getElementById("pSelector5").style.display = "none";
	        pCP.FootState = 14;     	      	
            pSetFooter();            	          			 
		 }
		 else{
	       document.getElementById("pSelector5").style.display = "block";
	       p2SandwhichHTML(47);	       
           p2CTog(id, 8, 16);
	       pCP.FootState = 14;     	      	
           pSetFooter();           	
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;
	     
// ------------------ Main Menu Nuggets ------------------------------
	   case 3:
	     p2Clear();
	     if (document.getElementById(""+id).style.color == 'rgb(0, 136, 170)') {
            document.getElementById("pSelector7").style.display = "none";
	       document.getElementById("pSelector12").style.display = "none";
	       document.getElementById("pSce1").style.marginLeft = "";	       	       
	       pCP.FootState = 14;     	      	
           pSetFooter();	                   	          			 
		 }
		 else{
	       document.getElementById("pSelector7").style.display = "block";
	       document.getElementById("pSelector12").style.display = "block";
	       document.getElementById("pSce1").style.marginLeft = ".1%";	       
	       ReadNuggets();
	       pCP.FootState = 15;     	      	
           pSetFooter();	       	       	
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;
	     
	     
// ------------------ Main Menu Wings ------------------------------
	   case 4:
	     p2Clear();
	     if (document.getElementById(""+id).style.color == 'rgb(0, 136, 170)') {
            document.getElementById("pSelector6").style.display = "none";
	       document.getElementById("pSelector12").style.display = "none";
	       pCP.FootState = 14;     	      	
           pSetFooter();	                   	          			 
		 }
		 else{
	       document.getElementById("pSelector6").style.display = "block";
	       document.getElementById("pSelector12").style.display = "block";
	       document.getElementById("pSce1").style.marginLeft = "";	       
	       ReadWings();
	       pCP.FootState = 15;     	      	
           pSetFooter();	       	       
		 }
		 
		 ReadWings(); 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;	
	     
	     
// ------------------ Main Menu Fries --------------------------------
	   case 5:
	     p2Clear();
	     if (document.getElementById(""+id).style.color == 'rgb(0, 136, 170)') {
            document.getElementById("pSelector8").style.display = "none";
	        pCP.FootState = 14;     	      	
            pSetFooter();                        	          			 
		 }
		 else{
	       document.getElementById("pSelector8").style.display = "block";
	       ReadFries();
	       pCP.FootState = 11;     	      	
           pSetFooter();	       	       
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;
	     
// ------------------ Main Menu Drinks --------------------------------
	   case 6:
	     p2Clear();
	     if (document.getElementById(""+id).style.color == 'rgb(0, 136, 170)') {
            document.getElementById("pSelector9").style.display = "none";
	        pCP.FootState = 14;     	      	
            pSetFooter();                        	          			 
		 }
		 else{
	       document.getElementById("pSelector9").style.display = "block";
          p2AdjustMenuView();
          p2CTog(id, 16, 25);
	       pCP.FootState = 14;     	      	
          pSetFooter();           	       	       
		 } 
	     pCP.mState = state1;
	     pTogState(id); 
	     break;	     	      	     	     

// ------------------ Second Menu Burger Choices ----------------------	     	     	     
	   case 7:
	     p2Clear();
	     if (document.getElementById(id).style.color == 'rgb(0, 136, 170)') {
	     	 document.getElementById("pSelector1").style.display = "block";	      	
	       document.getElementById("pSelector2").style.display = "block";			 
           document.getElementById("pSelector3").style.display = "none";
	       pCP.FootState = 14;     	      	
           pSetFooter();           
		 
		 }
		 else {
           p2AdjustMenuView();
	       document.getElementById("pSelector2").style.display = "block";	     
	       document.getElementById("pSelector3").style.display = "block";
	       	       
	       document.getElementById("p2Cut").innerHTML = document.getElementById(""+id).innerHTML;
           p2CTog(id, 0, 8);
	       pCP.FootState = 15;     	      	
           pSetFooter();           	       	       
	     } 
	     pCP.mState = state1;     	     
	     pTogState(id);
	     break; 

// ------------------ Second Menu Chicken Choices --------------------	     	     	     
	   case 16:
	     p2Clear();
	     if (document.getElementById(id).style.color == 'rgb(0, 136, 170)') {
	     	 document.getElementById("pSelector1").style.display = "block";	     
	       document.getElementById("pSelector5").style.display = "block";	     
	       document.getElementById("pSelector3").style.display = "none";
	       pCP.FootState = 14;     	      	
           pSetFooter();	        
	     }
	     else {
	       document.getElementById("pSelector5").style.display = "block";	     
	       document.getElementById("pSelector3").style.display = "block";
	       
	       //p2SandwhichHTML(47);
	       p2AdjustMenuView();	       
	       document.getElementById("p2Cut").innerHTML = document.getElementById(""+id).innerHTML;	       
           p2CTog(id, 8, 16);
	       pCP.FootState = 15;     	      	
           pSetFooter();           				 
		 }  
	     pCP.mState = state1;
	     pTogState(id); 
	     break; 	        
	
// ------------------ Second Menu Drink Choices --------------------	     	     	     
	   case 17:
	     p2Clear();
	     if(document.getElementById(id).style.color == 'rgb(0, 136, 170)') {
	       document.getElementById("pSelector1").style.display = "block";	     		     
	       document.getElementById("pSelector9").style.display = "block";	     
	       document.getElementById("pSelector10").style.display = "none";
	       pCP.FootState = 14;     	      	
           pSetFooter();	        
	     }
	     else {
	       document.getElementById("pSelector1").style.display = "none";	       
	       document.getElementById("pSelector9").style.display = "block";	     
	       document.getElementById("pSelector10").style.display = "block";
	       //document.getElementById("pdDis1").innerHTML = "0";
           //document.getElementById("pdDis2").innerHTML = "0";   
          p2AdjustMenuView();	
	       p2DrinkHTML();
           p2ReadDrinks(id);		       
           p2CTog(id, 16, 25);
	       pCP.FootState = 11;     	      	
           pSetFooter();           				 
		 }  
	     pCP.mState = state1;
	     pTogState(id);
	     p2ReadDrinks2(); 
	     break;

// ------------------ Top Nav Menu Choices --------------------	     
	  case 18:
	     p2Clear();
	     document.getElementById("pSelector1").style.display = "none";	     	  
	     document.getElementById("pSelector15").style.display = "block";	     
	     document.getElementById("pSelector16").style.display = "block";
	     document.getElementById("p2OverlayRight").style.display = "block";
         AdjScrnItems();
	     p2NavSelect(0);
	     p2CarRender(id);
	     pCP.tbID = 1;
	     pCP.FootState = 100;     	      	
         pSetFooter(); 	     	  
	     break; 
	     
	  case 19:
	     p2Clear();
	     pDisableOvrLays();
	     document.getElementById("pSelector1").style.display = "none";	     	  
	     document.getElementById("pSelector13").style.display = "block";	     
	     p2NavSelect(1);
	     pCP.tbID = 2;	     
	     pCP.FootState = 8;     	      	
         pSetFooter(); 	     	  
	     break;
	     
	  case 20:
	     p2Clear();
	     pDisableOvrLays();	     
	     document.getElementById("pSelector1").style.display = "none";	     	  
	     document.getElementById("pSelector13").style.display = "block";	     
	     p2NavSelect(2);
	     pCP.tbID = 3;	     
	     pCP.FootState = 8;     	      	
         pSetFooter();	     	  
	     break;	     	        	         
	}	
}

function p2AdjustMenuView(){

  if (pCP.winW < 378) {
    document.getElementById("pSelector1").style.display = "none";
    p2SandwhichHTML(73);
  }
  else {
 	 document.getElementById("pSelector1").style.display = "block";
 	 p2SandwhichHTML(47);
  }		     

}

function pModifyItem(id) {
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3");
	var ptext = document.getElementById("p2Cut").innerHTML;
	
	var TmpSndwh = "B,BB,DB,CB,BC,DCB,DBCB,TCB,REG CK,CLUB,GRILL," +
	               "CKB,CLUB B,GRILL B,GRILL CL";
	var hld;
	var x;
	
	hld=TmpSndwh.split(",");
	
	for(var i=0; i<hld.length; i++){
	   if(hld[i] == ptext){
		  x=i;
		  break;
	   }
	}
	
	switch(id) {
		
   // Basically saying cut the sandwhich in half	   
	   case "p2Cut":
         if(ptext !== "HOT DOG" && ptext !== "DBL HOT DOG"){
           if((ptext.search("1/2 ")) == -1) {
	           document.getElementById("p2Cut").innerHTML = "1/2 " + 
	           hld[x];
		   }
		   else {
	         // removing the cut sandwhich in half
	          hld=ptext.split("1/2 ");	
		   	  document.getElementById("p2Cut").innerHTML = hld[1];
		   }
		 }
		 else{
			if((ptext.search("DBL ")) == -1){
	           document.getElementById("p2Cut").innerHTML = "DBL " +
	           "HOT DOG";			   
			}
			else{
	          hld=ptext.split("DBL ");	
		   	  document.getElementById("p2Cut").innerHTML = hld[1];			   
			} 
		 }     	  	      		   
	     break;
	   
   // Adding Ex to the selected food item	   
	   case "pS74":

         ptext = pSel[pCP.StoredId].innerHTML;
                 
	      if ((ptext.search("Lt ")) == 0) {
	      	ptext = ptext.replace("Lt ", "Ex ");
            pSel[pCP.StoredId].innerHTML = ptext;           	      		      	 
	      }
	      else if ((ptext.search("Ex ")) == -1) {
	      	pSel[pCP.StoredId].innerHTML = "Ex " +
	      	pSel[pCP.StoredId].innerHTML;	      	            	      		      		      	
	      }
	      else {
	         ptext = ptext.replace("Ex ", "");
            pSel[pCP.StoredId].innerHTML = ptext;         	
	      }         	   
	      break;

   // Adding Lt to the selected food item	      
	   case "pS75":

         ptext = pSel[pCP.StoredId].innerHTML;
	      if ((ptext.search("Ex ")) == 0) {
	      	ptext = ptext.replace("Ex ", "Lt ");
            pSel[pCP.StoredId].innerHTML = ptext;            	      		      	
	      } 	                  
	      else if ((ptext.search("Lt ")) == -1) {
	      	pSel[pCP.StoredId].innerHTML = "Lt " +
	      	pSel[pCP.StoredId].innerHTML;	      		      	 
	      }
	      else {
	         ptext = ptext.replace("Lt ", "");
            pSel[pCP.StoredId].innerHTML = ptext;	         	
	      } 	   
	      break;
	      
	   default: break;         	
	}		
}

function pNumKey(id) {
   var NumData= document.getElementsByClassName('pData');
   var GetDispay = document.getElementById('pDisplay').innerHTML;
     
   for (var i=0; i< NumData.length; i++) {
      if (NumData[i].id == id) {
        if (NumData[i].innerHTML == "BS" || NumData[i].innerHTML == "Clr") {
           document.getElementById('pDisplay').innerHTML = "0";        	    
        }
        else {
           if (GetDispay == "0") {        	 	 	
        	 document.getElementById('pDisplay').innerHTML = NumData[i].innerHTML;
           }
           else {
        	  document.getElementById('pDisplay').innerHTML += NumData[i].innerHTML;        	 	 	
           }   
        }
        break;
      }	 	
   }  	  
}


function pDisableOvrLays(){
    document.getElementById("p2OverlayLeft").style.display = "none";	
    document.getElementById("p2OverlayRight").style.display = "none";	
}

function AdjScrnItems() {
   
   var winW = window.innerWidth;
   var winH = window.innerHeight;	
   var hghtL = document.getElementById("pSelector15");
   var hghtR = document.getElementById("pSelector16");
   var ovrLayLeft = document.getElementById("p2OverlayLeft");
   var ovrLayRight = document.getElementById("p2OverlayRight");
   var Calc = document.getElementById("pSelector13");   
   
   pCP.winW = winW;
   pCP.winH = winH;
   
   ovrLayLeft.style.top = hghtL.offsetTop + "px";
   ovrLayLeft.style.height = hghtL.clientHeight + "px";
   ovrLayLeft.style.width = hghtL.offsetWidth + 7 + "px";
   ovrLayRight.style.top = hghtR.offsetTop + "px";
   ovrLayRight.style.height = hghtR.clientHeight + "px"; 
   
   Calc.style.marginLeft = (winW / 2) - 150 + "px";
 	
}

function pChangeVal(id) {
	
   var IceTxtBx = document.getElementsByClassName("pNtxt2");	
   var RegTxtBx = document.getElementById("pTxt13").innerHTML;
   var LGTxtBx = document.getElementById("pTxt14").innerHTML;
   var pProceed = false;
          	
	for (var i =0; i<IceTxtBx.length; i++) {

		if (IceTxtBx[i].id == id) {
			if (i == 3){
				if( RegTxtBx < 1 ) {
				  pProceed = true; break;
				}
				else {
				  pCP.BoxID = id;					 
				  pProceed = false; 
				  break;
				}
			}
			else {
				if( LGTxtBx < 1 ) {				  
				  pProceed = true; break;
				}
				else {
				  pCP.BoxID = id;				  	 
				  pProceed = false; 
				  break;
				}				
			}	
				
		}
	}

	if (!pProceed) {
		pCP.BoxID = id;
      showDrink();
	}      	
}

function p2ChangeVal(id){   
   pCP.BoxID = id;
   p2Clear();
   document.getElementById("pSelector13").style.display = "block";
   pCP.retState = 8;
   pCP.FootState = 8;     	      	
   pSetFooter();   
}

function showDrink() {
   p2Clear();
   document.getElementById("pSelector1").style.display = "none";   
   document.getElementById("pSelector13").style.display = "block";
   document.getElementById('pDisplay').innerHTML = 0;
   pCP.FootState = 8;     	      	
   pSetFooter();       
      	
}

/***********************************************
 ***********************************************/
 
function pTxtDis(id) {
	var pSel = document.getElementsByClassName("pSel");
	
	var pAry1 = ["pS16",     "pS30",     "pS44",     "pS45",     // 3 
	             "pS46",     "pS47",     "pS48",     "pS49",     // 7
	             "pS50",     "pS51",     "pS52",     "pS53",     // 11
	             "pS63",     "pS64",     "pTxtEx1",  "pTxtLt1",  // 15
	             "pTxtNo1",  "pTxtEx2",  "pTxtLt2",  "pTxtNo2",  // 19
                 "p2Cmb2",   "p2Cmb3",   "p2Cmb4",   "p2Cmb5",   // 23
	             "p2Cmb7",   "p2Cmb8",   "p2Cmb9",   "p2Cmb10",  // 27
	             "p2Cmb12",  "p2Cmb13",  "p2Cmb14",  "p2Cmb15",  // 31
	             "p2Cmb17",  "p2Cmb18",  "p2Cmb19",  "p2Cmb20",  // 35
	             "p2Cmb22",  "p2Cmb23",  "p2Cmb24",  "p2Cmb25",  // 39
	             "p2Cmb27",  "p2Cmb28",  "p2Cmb29",  "p2Cmb30",  // 43
	             "p2Cmb32",  "p2Cmb33",  "p2Cmb34",  "p2Cmb35",  // 47
	             "p2Cmb37",  "p2Cmb38",  "p2Cmb39",  "p2Cmb40",  // 51
	             "p2Cmb42",  "p2Cmb43",  "p2Cmb44",  "p2Cmb45"	 // 55       
	            ];
	             
	var pAry2 = ["pTxt1",    "pTxt2",    "pTxt3",    "pTxt4",    // 3
	             "pTxt5",    "pTxt6",    "pTxt7",    "pTxt8",    // 7
	             "pTxt9",    "pTxt10",   "pTxt11",   "pTxt12",   // 11
	             "pTxt13",   "pTxt14",   "pTxt15",   "pTxt16",   // 15
	             "pTxt17",   "pTxt18",   "pTxt19",   "pTxt20",   // 19
                 "cmbTxt1",  "cmbTxt2",  "cmbTxt3",  "cmbTxt4",  // 23
	             "cmbTxt5",  "cmbTxt6",  "cmbTxt7",  "cmbTxt8",  // 27
	             "cmbTxt9",  "cmbTxt10", "cmbTxt11", "cmbTxt12", // 31
	             "cmbTxt13", "cmbTxt14", "cmbTxt15", "cmbTxt16", // 35
	             "cmbTxt17", "cmbTxt18", "cmbTxt19", "cmbTxt20", // 39
	             "cmbTxt21", "cmbTxt22", "cmbTxt23", "cmbTxt24", // 43
	             "cmbTxt25", "cmbTxt26", "cmbTxt27", "cmbTxt28", // 47
	             "cmbTxt29", "cmbTxt30", "cmbTxt31", "cmbTxt32", // 51
	             "cmbTxt33", "cmbTxt34", "cmbTxt35", "cmbTxt36"	 // 55       
	            ];           
	
	var pAry3 = [[20, 21, 22, 23],
	             [24, 25, 26, 27],
	             [28, 29, 30, 31],
	             [32, 33, 34, 35],
	             [36, 37, 38, 39], 
	             [40, 41, 42, 43], 
	             [44, 45, 46, 47], 
	             [48, 49, 50, 51], 
	             [52, 53, 54, 55]
	            ];
	            
	var pcntr = 0;
	var btest = false; 
 	//alert(id);
 	            
	for (var i=0; i<pAry1.length; i++) {
      if (pAry1[i] == id) {
         pcntr = parseInt(document.getElementById("" + pAry2[i]).innerHTML);
         if(i < 20){ 
		   pcntr++; 
		 }
         else{
           if(pcntr >= 1){ return; }
           else{ pcntr++; }
         }  
         document.getElementById("" + pAry2[i]).innerHTML = pcntr;
         
         if(i>19){
         // This is used to toggle between Reg and Lg (Fry or Drink)         
           for(var j=0; j<pAry3.length; j++){
			 for(var x=0; x<4; x++){
			   
			   if(document.getElementById("" + pAry1[pAry3[j][x]]).id == id){
				  if(x == 0){
					document.getElementById("" + pAry2[pAry3[j][x+1]]).innerHTML = 0;
				  }
				  else if(x == 1){
					document.getElementById("" + pAry2[pAry3[j][x-1]]).innerHTML = 0;
				  }					   
				  else if(x == 2){
					document.getElementById("" + pAry2[pAry3[j][x+1]]).innerHTML = 0;
				  }
				  else{
					document.getElementById("" + pAry2[pAry3[j][x-1]]).innerHTML = 0;  
				  }
				  btest = true;
				  break;
			   }
			 }
			 if(btest){ break; }
		   }
		 }
         break;
      }	 	
	}
              
} 

/***********************************************
 ***********************************************/

function TogItem(id) {
	
	var pSel= document.getElementsByClassName("pSel");
	var pTitle= document.getElementById("p2Cut").innerHTML;
	var pStart = 0; 
	var pEnd = 0;
	var x=0;
	var TagList1, TagList2;
	var MainNavAry = ["pS1", "pS2", "pS3"];
	
	            // Everything
	            // Plain Tom  May  Ket Pic  On   Let  Mus  Bac  Ches Chili 
    var TagAry1= ["pS18,pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28,pS29",
                  
               // Burgers & Chicken Burgers
                  "pS19,pS20,pS21,pS22,pS23,pS24,pS25",
                  
               // Club Burgers
                  "pS19,pS20,pS21,pS22,pS23,pS24,pS25,pS27,pS28",                  
                  
	           // Club
                  "pS19,pS20,pS24,pS27,pS28",
                  
               // Reg/Grill Ck
                  "pS19,pS20,pS24",

               // Hot Dog                  
                  "pS23,pS25,pS29",

	           // Gr Club
                  "pS19,pS20,pS24,pS27,pS28",                  
                  ];
                  
    
    switch(id){
	// Reg	
	   case "pS17":
         for(var i=0; i<MainNavAry.length; i++){
	       if (document.getElementById(""+MainNavAry[i]).style.color == 'rgb(0, 136, 170)') {
		      x=i; break;
	       }	
	     }
	     
	  // Burgers/Chicken	     
         if(x == 0 || x == 2){
           switch(pTitle){
	         case "REG CK":
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[4];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                       pTogState(j);	
		               break; 	
			       }				  
			     }	
			   }
		    	   
	           break;
	      
	         case "GRILL":
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[4];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                      pTogState(j);	
		              break;
		           }  					  
			     }	
			   }	   
	           break;
	           
	         case "CLUB":
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[3];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                      pTogState(j);	
		              break;			  
			       }				  
			     }	
			   }	   
	           break;	
	           
	         case "GRILL CL":
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[6];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                      pTogState(j);	
		              break;
			       }				  
			     }	
			   }	   
	           break;

	         case "CLUB B":
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[2];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                      pTogState(j);	
		              break;				  	
			       }				  
			     }	
			   }	   
	           break;	           
	         default:
			   TagList1 = TagAry1[0];
			   TagList2= TagList1.split(",");

            // Clearing out data			
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
	                  pSel[j].style.background = '#333';
	                  pSel[j].style.border = "";
	                  pSel[j].style.color = '#999';     	  
	                  pSel[j].style.textShadow = "";				    
				      break;
			       }				  
			     }	
			   }						

			   TagList1 = TagAry1[1];
			   TagList2 = TagList1.split(",");
						
			   for(var i=0; i<TagList2.length; i++){
			     for(var j=0; j<pSel.length; j++){
			       if(pSel[j].id == TagList2[i]){
                      pTogState(j);	
		              break;			    	
			       }				  
			     }	
			   }	         
	           break;  	                          
	       }		
	     }
	     
	  // Hot Dog
	     else{
			 
			TagList1 = TagAry1[0];
			TagList2= TagList1.split(",");

         // Clearing out data			
			for(var i=0; i<TagList2.length; i++){
			  for(var j=0; j<pSel.length; j++){
			    if(pSel[j].id == TagList2[i]){
	               pSel[j].style.background = '#333';
	               pSel[j].style.border = "";
	               pSel[j].style.color = '#999';     	  
	               pSel[j].style.textShadow = "";				    
				   break;
			    }				  
			  }	
			}						

			TagList1 = TagAry1[5];
			TagList2= TagList1.split(",");
						
			for(var i=0; i<TagList2.length; i++){
			  for(var j=0; j<pSel.length; j++){
			    if(pSel[j].id == TagList2[i]){
	               pSel[j].style.background = '#333';   	  	
	               pSel[j].style.border = '2px solid #08A';    	  
	               pSel[j].style.color = '#08A';
	               pSel[j].style.textShadow = '1px 1px 1px #FFF';	
				   break;
			    }				  
			  }	
			}
		 }
	     	     	   
	     break;
	
	// Plain     
	   case "pS18":

		 TagList1 = TagAry1[0];
		 TagList2= TagList1.split(",");

         // Clearing out data			
		 for(var i=1; i<TagList2.length; i++){
		   for(var j=0; j<pSel.length; j++){
		     if(pSel[j].id == TagList2[i]){
	             pSel[j].style.background = '#333';
	             pSel[j].style.border = "";
	             pSel[j].style.color = '#999';     	  
	             pSel[j].style.textShadow = "";				    
		         break;
			 }				  
		   }	
		 }
	     for(var j=0; j<pSel.length; j++){
		   if(pSel[j].id == "pS18"){
              pTogState(j);	
		      break;
		   }				  
		 }			 	     
	     break;
	     
	// Food Item     
	   default:
         for (var i=0; i<pSel.length; i++) {
	       if (pSel[i].id == id) {
		  // Setting Plain button to default
			 document.getElementById("pS18").style.background = "#333";
			 document.getElementById("pS18").style.border = "";
			 document.getElementById("pS18").style.color = "#999";
			 document.getElementById("pS18").style.textShadow = "";
			
			 pTogState(i);  	   
	         break; 
		   }
		 }   	
	}
	

}


// ================================================
function p2CarRender(id, pRow) {
   var pSel= document.getElementsByClassName("pSel");
   	
   switch(pRow){
	 case 1:
        p2CTog(id, 21, 21+13);
	    pTogState(id);
	    document.getElementById("p2OverlayLeft").style.display = "block";
	    document.getElementById("p2OverlayRight").style.display = "none";
	    pCP.pCarType = document.getElementById(""+id).innerHTML;	       
	    break;

	 case 2:
	    pTogState(id);
	    document.getElementById("p2OverlayLeft").style.display = "none";
	    document.getElementById("p2OverlayRight").style.display = "none";
	    pCP.pCarType += " " + document.getElementById(""+id).innerHTML;
        p2NavClear();
        
        pCP.FootState = 14;     	      	
	    pSetFooter();
	    
	    p2CTog(0,26,40);        	       
	    break;
	       	                   
	 default: break;	
   }  	       	  	
}
 

// ==========================================
function p2NavSelect(Select1){
	
   var pSelect1 = document.getElementsByClassName("pSelect1");
   
   for(var i=0; i<pSelect1.length; i++){
	  pSelect1[i].style.background = "";
   }
   
   pSelect1[Select1].style.background = "#999";
  
}

// ==========================================
function p2NavClear(){

   var pSelect1 = document.getElementsByClassName("pSelect1");
   var pSel = document.getElementsByClassName("pSel");
   
   for(var i=0; i<pSelect1.length; i++){
	  pSelect1[i].style.background = "";
   }
   
   p2ClearToggles(); 
   p2Clear();
   document.getElementById("pSelector1").style.display = "block";      	
}

// ==============================================
function p2CTog(pSkip, start1, end1){
	
   p2ClearAry1 = ["pS8",     "pS9",     "pS10",    "pS11",
                  "pS12",    "pS13",    "pS14",    "pS15",
                  "pS36",    "pS37",    "pS38",    "pS39",
                  "pS40",    "pS41",    "pS42",    "pS43",
                  "pS54",    "pS55",    "pS56",    "pS57",
                  "pS58",    "pS59",    "pS60",    "pS61",
                  "pS62",    "pcd1",    "pcd2",    "pcd3",
                  "pcd4",    "pcd5",    "pcd6",    "pcd7",
                  "pcd8",    "pcd9",    "pcd10",   "pcd11",
                  "pcd12",   "pcd13",   "pcd14",   "pcd15",
                  "p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"                  
                 ];
        
   for(var i=start1; i<end1; i++){
	  if(p2ClearAry1[i] !== "" + pSkip){  
	    if(!pCP.Combo){
	      document.getElementById(""+ p2ClearAry1[i]).style.background = '#333';
	    }  
	    document.getElementById(""+ p2ClearAry1[i]).style.border = "";
	    document.getElementById(""+ p2ClearAry1[i]).style.color = '#999';     	  
	    document.getElementById(""+ p2ClearAry1[i]).style.textShadow = "";
	  }   
   }	
}


function p2ClearToggles(){
   
   var pSel= document.getElementsByClassName("pSel");
   
   for(var i=0; i<15; i++){
	  if(!pCP.Combo){
	    pSel[i].style.background = '#333';
	  }   
	  pSel[i].style.border = "";
	  pSel[i].style.color = '#999';     	  
	  pSel[i].style.textShadow = "";
   }
   
   for(var i=35; i<42; i++){
	  if(!pCP.Combo){
	    pSel[i].style.background = '#333';
	  }  
	  pSel[i].style.border = "";
	  pSel[i].style.color = '#999';     	  
	  pSel[i].style.textShadow = "";
   } 
           
   for(var i=59; i<73; i++){
	 if(!pCP.Combo){
	   pSel[i].style.background = '#333';
	 }  
	 pSel[i].style.border = "";
	 pSel[i].style.color = '#999';     	  
	 pSel[i].style.textShadow = "";
   }     
}
 
function pTogState(pStatus) {

	var pSel= document.getElementsByClassName("pSel");
    
	if (pSel[pStatus].style.color == 'rgb(0, 136, 170)') {
	
	  if(!pCP.Combo){
	    pSel[pStatus].style.background = '#333';
	  }
	  
	  pSel[pStatus].style.border = "";
	  pSel[pStatus].style.color = '#999';     	  
	  pSel[pStatus].style.textShadow = "";	     	     	   		
	}
	else {
	  if(!pCP.Combo){
	    pSel[pStatus].style.background = '#333';
	  }
	     	  	
	  pSel[pStatus].style.border = '2px solid #08A';    	  
	  pSel[pStatus].style.color = '#08A';
	  pSel[pStatus].style.textShadow = '1px 1px 1px #FFF';
	}	   	
}

function p2CmbItem(id){

	var pSel= document.getElementsByClassName("pSel");
	var pElm;
	
	for(var i=0; i<pSel.length; i++){
	    if(pSel[i].id == id){
		   pElm = i;
		   break;
		}
	}
	
	pTogState(pElm);
	p2CTog(id, 40, 49);		
}

function DisplayPrice(){

   var DisPrice1 = document.getElementById("DisPrice1");   
   var Price1 = 0;
   var num1;
   
   if(pCP.hPrices.length > 0){
     for(var i=0; i<pCP.hPrices.length; i++){
	   num1 = pCP.hPrices[i];
	   Price1 += num1; 	    
     }
     DisPrice1.innerHTML = parseFloat(Price1).toFixed(2);
   }
   else{
	  DisPrice1.innerHTML = "0.0"; 
   }
}

function p2SetPrice(id){
   
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];
    
    var cmbAry2 = [["cmbTxt1",  "cmbTxt2",  "cmbTxt3",  "cmbTxt4"],
                   ["cmbTxt5",  "cmbTxt6",  "cmbTxt7",  "cmbTxt8"],
                   ["cmbTxt9",  "cmbTxt10", "cmbTxt11", "cmbTxt12"],
                   ["cmbTxt13", "cmbTxt14", "cmbTxt15", "cmbTxt16"],
                   ["cmbTxt17", "cmbTxt18", "cmbTxt19", "cmbTxt20"],
                   ["cmbTxt21", "cmbTxt22", "cmbTxt23", "cmbTxt24"],
                   ["cmbTxt25", "cmbTxt26", "cmbTxt27", "cmbTxt28"],
                   ["cmbTxt29", "cmbTxt30", "cmbTxt31", "cmbTxt32"],
                   ["cmbTxt33", "cmbTxt34", "cmbTxt35", "cmbTxt36"]
                  ];
    
    var bC = document.getElementById(""+id).id;
    var bC2 = "";
    var bC2 = bC.split("cbtn");
    var btnClk = bC2[1];
                          
    var pTitle;
    var test = false;
    var pElm = 0;
    var Rf = 0;   // Reg Fry
    var Bf = 0;   // Bk Fry
    var Rd = 0;   // Reg Drink
    var Ld = 0;   // Lg Drink
    var ItemID;
                
    for(var i=0; i<cmbAry1.length; i++){
	   if (document.getElementById(""+ cmbAry1[i]).style.color == 'rgb(0, 136, 170)') {
		   pTitle = pHalfCheck(document.getElementById(""+ cmbAry1[i]).innerHTML);
		   ItemID = cmbAry1[i];
		   pElm = i;
		   test=true;
		   break;
	   }	
	}
	if(!test){
	  pCP.BodyMessage = "Select an Item";
   	  p2ShowPopup();
	}
	else{
      Rf = document.getElementById("" + cmbAry2[pElm][0]).innerHTML;
      Bf = document.getElementById("" + cmbAry2[pElm][1]).innerHTML;
      Rd = document.getElementById("" + cmbAry2[pElm][2]).innerHTML;
      Ld = document.getElementById("" + cmbAry2[pElm][3]).innerHTML;
      
      if(Rf==0 && Bf==0 && Rd==0 && Ld==0){
  	     pCP.BodyMessage = "Enter values for fries and drink";
   	     p2ShowPopup();
		 return;
	  }
	  else{
		 //alert(pCP.tPrices[btnClk-1]);
		 switch(pTitle){		      
		    case "5PC":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "BUCK":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "5W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "10W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "15W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "20W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "25W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "30W":
		      cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, ItemID);
		      break;
		    case "B":
		      BrgCB(pTitle, Rf, Bf, Rd, Ld, btnClk-1, ItemID);
		      break;
			case "CB":
		      BrgCB(pTitle, Rf, Bf, Rd, Ld, btnClk-1, ItemID);
		      break;		            		      		      		      		      		      		      		      		      		      			       		        			  			  
		    default: 
		      pSandwhiches2(pTitle, Rf, Bf, Rd, Ld, ItemID);		    
		    break;	  
		 }
	  }  
    }
}

function cmbFriedItems(pTitle, Rf, Bf, Rd, Ld, id){

   
   var sDrk1 = document.getElementsByClassName("sDrk1");
   
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];
   
   var BinCode;
   var bTest = false;
   var pOtr = pInr = pLstVal = num1= 0;
   var pElm;
   
   for(var i=0; i<pCP.cmbPlcHldr.length; i++){
	  if(pCP.cmbPlcHldr[i]){
		for(var j=0; j<pCP.tPrices[i].length; j++){
		   if(pCP.tPrices[i][j] == id){
			  pOtr = i;
			  pInr = j;
			  bTest=true;
			  break; 
		   }
		}
        if(bTest){ break; }		
	 }
   }
 
// Burger with a Reg fry and nothing else
   if(Rf > 0 && Bf < 1 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1] + 
			                                    pCP.Prices[25][0];
			                                                                     
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			   			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);				 
			 DisplayPrice();
			 //alert(pCP.hPrices);
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			pCP.cmbT[pElm][5] = parseInt(Bf);
			DisplayPrice();
		    break;
		  }  
	   }
	 }
   }

// Burger with a Bk Fry and nothing else 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[25][1];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;		 
			 
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pElm = j;					                  
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);
			pCP.cmbT[pElm][4] = parseInt(Rf);						
			DisplayPrice();
		    break;
		  }  
	   }
	 }				  
   }			  

// Burger with a Reg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
	 
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[26][0];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 			 
		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";
				      pCP.cmbT[pOtr][12] += num1;               
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";
				      pCP.cmbT[pOtr][13] += num1;				      				      
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";
				      pCP.cmbT[pOtr][14] += num1;				   
				      break;
				      
				   default: break;          		   
				}
			 }
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j;                 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j;                 
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j-1;                 					  
				  }   
				  break;
			   }
			}
			
		 // Storing the Reg drink element for later use			 
     	    pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
           
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];
			pCP.cmbT[pElm][9]= "NA";
			pCP.cmbT[pElm][11]= "";

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1
                    pCP.cmbT[pElm][15]= "";				    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";				    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }									
						
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }
   }  

// Burger with a Lg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd < 1 && Ld > 0){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }	   
	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[26][1];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];				 
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";
				      pCP.cmbT[pOtr][15] += num1;				                    
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][17] += num1;				      
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();            
			 break;   
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j;					                  
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}

		 // Storing the Lg drink element for later use			 
			pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
				
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }				  
   }			  

// Burger with a Reg fry and Reg drink 
   else if(Rf > 0 && Bf < 1 && Rd > 0 && Ld < 1){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;			 

		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][12] += num1;				      
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";
                      pCP.cmbT[pOtr][14] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					  
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][2];
					 pCP.hPrices[j-1] = pCP.Prices[i][2];
					 pElm = j-1;					 					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			pCP.cmbT[pElm][5] = parseInt(Bf);
			
		  // Storing the Reg drink element for later use			 
			 pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
			//alert("pCP.cmbT values are " + pCP.cmbT[pElm]);
								 
			cmbClrSlider();
			DisplayPrice();			
		    break;
		  }  
	   }
	 }      // End for(pCP.Prices)				  
   }			  

// Burger with a Bk fry and Reg drink 
   else if(Rf < 1 && Bf > 0 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;			 

		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";
                      pCP.cmbT[pOtr][12] += num1;				      
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][14] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);
			 						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					  
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][3];
					 pCP.hPrices[j-1] = pCP.Prices[i][3];
					 pElm = j-1;					 					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			
			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);

		 // Storing the Reg drink element for later use			 
			pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();
			DisplayPrice();			
		    break;
		  }  
	   }
	 }				  
   }				  

// Burger with a Reg fry and Lg drink 
   else if(Rf > 0 && Bf < 1 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;				 

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;				      
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 //alert(pCP.hPrices);
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j; 
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][3];
					 pCP.hPrices[j-1] = pCP.Prices[i][3];
					 pElm = j-1;					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;

			num1= pCP.cmbT[pElm][4] + parseInt(Rf);
			pCP.cmbT[pElm][4] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);

		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();
			DisplayPrice();		
		    break;
		  }  
	   }
	 }				  
   }

// Burger with a Bk fry and Lg drink 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][4];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;			 

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][16] += num1;				      
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);	
						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j; 
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][4];
					 pCP.hPrices[j-1] = pCP.Prices[i][4];
					 pElm = j-1;					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;

			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);

		  // Storing the Reg drink element for later use			 
			pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12] += "";                    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13] += "";                    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14] += "";                    
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();			
			DisplayPrice();
		    break;
		  }  
	   }
	 }
  } 
}

function cmbDrkSet1(Sel1, dSel1, cSel1){
   var test1 = false;
   var num1;
   
   //alert("Sel1 in cmbDrkSet1() is:" +Sel1);
   
   switch(Sel1){
	   case 100:
  	     pCP.BodyMessage = "Select a drink from the sliding menu";
   	     p2ShowPopup();		 
   	     
		 if(document.getElementById("pScroll").style.display == "none"){
		   showDrinkScroll();
		 }
		 test1 = true;  
		 break;
	     
	   default:
	     switch(dSel1){
	       case 1:
		    num1 = parseInt(pCP.dList1[pCP.cmbDrk][1]) + parseInt(cSel1);
		    pCP.dList1[pCP.cmbDrk][1] = num1;	     
	        break;
	     
	      case 2:
		    num1 = parseInt(pCP.dList1[pCP.cmbDrk][2]) + parseInt(cSel1);
		    pCP.dList1[pCP.cmbDrk][2] = num1;	    
	        break;
	      
	      default: break;
	    }	   
	   break; 			 
   }
   
   if(test1){ return test1; }
	      
}

function cmbIceSet1(Sel1, AryMode, iSel){
   var num1;
   
   if(Sel1 !== 100){
	   //alert("Ice function");
	   switch(AryMode){
		  case 3:
			num1 = parseInt(pCP.dList1[pCP.cmbDrk][AryMode][iSel]) + 1;
			pCP.dList1[pCP.cmbDrk][AryMode][iSel] = num1;		  
		    break;
		    
		  case 4:
			num1 = parseInt(pCP.dList1[pCP.cmbDrk][AryMode][iSel]) + 1;
			pCP.dList1[pCP.cmbDrk][AryMode][iSel] = num1;			  
		    break;
		    
		  default: break;   
	   }

   }    	   
}

function BrgCB(pTitle, Rf, Bf, Rd, Ld, btnClk, id){
   
   var sDrk1 = document.getElementsByClassName("sDrk1");
   
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];
   
   var tmpAry = [];                 
   
   var BinCode;
   var bTest = false;
   var pOtr = 0; 
   var pInr = 0;
   var pLstVal = 0;
   var num1 = 0;
   var pElm;
   
   for(var i=0; i<pCP.cmbPlcHldr.length; i++){
	  if(pCP.cmbPlcHldr[i]){
		for(var j=0; j<pCP.tPrices[i].length; j++){
		   if(pCP.tPrices[i][j] == id){
			  pOtr = i;
			  pInr = j;
			  bTest=true;
			  break; 
		   }
		}
        if(bTest){ break; }		
	 }
   }
   
// Burger with a Reg fry and nothing else
   if(Rf > 0 && Bf < 1 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2]
			                                                                     
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			   			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;			 
			 DisplayPrice();
			 break;
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][2];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			pCP.cmbT[pElm][5] = parseInt(Bf);
			DisplayPrice();
		    break;
		  }  
	   }
	 }
   }

// Burger with a Bk fry and nothing else 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;		 
			 DisplayPrice();
			 break;			   
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;
					 pCP.tPrices[pElm] = pCP.Prices[i][2];
					 pCP.hPrices[pElm] = pCP.Prices[i][2];					 					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);
			pCP.cmbT[pElm][4] = parseInt(Rf);						
			DisplayPrice();
		    break;		    
		  }  
	   }
	 } 				  
   }

// Burger with a Reg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
	 
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;

		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 //pCP.cmbT[pOtr][7] = parseInt(Ld);
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 
             cmbIceSet1(pCP.cmbDrk, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][12] += 1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += 1;				      
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][14] += 1;			      
				      break;
				      
				   default: break;          		   
				}
			 }
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][2];
					 pCP.hPrices[pElm] = pCP.Prices[i][2];					  
				  }   
				  break;
			   }
			}
			
		 // Storing the Reg drink element for later use			 
			pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];
			pCP.cmbT[pElm][9]= "NA";
			pCP.cmbT[pElm][11]= "";
			
            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1
                    pCP.cmbT[pElm][15]= "";;				    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";				    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }									
			
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 } 				  
   }			  

// Burger with a Lg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd < 1 && Ld > 0){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }	   
	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;				      
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";
                      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }            
						 
			 cmbClrSlider();
			 DisplayPrice();            
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2]; 
					 pElm = j;					 
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][2];
					 pCP.hPrices[pElm] = pCP.Prices[i][2];					  
				  }   
				  break;
			   }
			}

		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
			
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }				  
   }			  

// Burger with a Reg fry and Reg drink 
   else if(Rf > 0 && Bf < 1 && Rd > 0 && Ld < 1){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;			 

			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 
             cmbIceSet1(pCP.cmbDrk, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][12] += 1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += 1;			      
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][14] += 1;			      
				      break;
				      
				   default: break;          		   
				}
			 }
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][2];
					 pCP.hPrices[pElm] = pCP.Prices[i][2];					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			
		  // Storing the Reg drink element for later use			 
			 pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();
			DisplayPrice();			
		    break;
		  }  
	   }
	 }      // End for(pCP.Prices)
	  
   }			  

// Burger with a Bk fry and Reg drink 
   else if(Rf < 1 && Bf > 0 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;
			 //pCP.cmbT[pElm][4] = parseInt(Rf);			 

			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 //pCP.cmbT[pOtr][7] = parseInt(Ld);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 
             cmbIceSet1(pCP.cmbDrk, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][12] += 1;
                      //pCP.cmbT[pOtr][15]= "";
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += 1;
                      //pCP.cmbT[pOtr][16]= "";				      
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][14] += 1;
                      //pCP.cmbT[pOtr][17]= "";				      
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][3];
					 pCP.hPrices[pElm] = pCP.Prices[i][3];					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			
			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);
			
		  // Storing the Reg drink element for later use			 
			 pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
			//alert("pCP.cmbT values are " + pCP.cmbT[pElm]);
						 
			cmbClrSlider();
			DisplayPrice();			
		    break;
		  }  
	   }
	 } 				  
   }				  

// Burger with a Reg fry and Lg drink 
   else if(Rf > 0 && Bf < 1 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";
                      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][3];
					 pCP.hPrices[pElm] = pCP.Prices[i][3];					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;

			num1= pCP.cmbT[pElm][4] + parseInt(Rf);
			pCP.cmbT[pElm][4] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);
			
		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
			//alert("pCP.cmbT values are " + pCP.cmbT[pElm]);
						 
			cmbClrSlider();
			DisplayPrice();		
		    break;
		  }  
	   }
	 }			  
   }

// Burger with a Bk fry and Lg drink 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][4];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;			 

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;				      
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
                      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j;					  
				  }
				  else{
					 pElm = j-1;					  
					 pCP.tPrices[pElm] = pCP.Prices[i][4];
					 pCP.hPrices[pElm] = pCP.Prices[i][4];					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;

			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);
			
		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12] += "";                    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13] += "";                    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14] += "";                    
				    break;
				     
				  default: break;          		   
				}
			 }						
			//alert("pCP.cmbT values are " + pCP.cmbT[pElm]);
						 
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }
  } 
}

function pSandwhiches2(pTitle, Rf, Bf, Rd, Ld, id){
   
   var sDrk1 = document.getElementsByClassName("sDrk1");
   
   var cmbAry1 = ["p2Cmb1",  "p2Cmb6",  "p2Cmb11", "p2Cmb16",
                  "p2Cmb21", "p2Cmb26", "p2Cmb31", "p2Cmb36",
                  "p2Cmb41"
                 ];
   
   var BinCode;
   var bTest = false;
   var pOtr = pInr = pLstVal = num1= 0;
   var pElm;
   
   for(var i=0; i<pCP.cmbPlcHldr.length; i++){
	  if(pCP.cmbPlcHldr[i]){
		for(var j=0; j<pCP.tPrices[i].length; j++){
		   if(pCP.tPrices[i][j] == id){
			  pOtr = i;
			  pInr = j;
			  bTest=true;
			  break; 
		   }
		}
        if(bTest){ break; }		
	 }
   }
 
// Burger with a Reg fry and nothing else
   if(Rf > 0 && Bf < 1 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1] + 
			                                    pCP.Prices[25][0];
			                                                                     
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			   			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);				 
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][0];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			pCP.cmbT[pElm][5] = parseInt(Bf);
			DisplayPrice();
		    break;
		  }  
	   }
	 }
   }

// Burger with a Bk Fry and nothing else 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld < 1){
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[25][1];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
			 
			 DisplayPrice();
			 //alert(pCP.hPrices);
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pElm = j;					                  
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[25][1];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);
			pCP.cmbT[pElm][4] = parseInt(Rf);						
			DisplayPrice();
		    break;
		  }  
	   }
	 }				  
   }			  

// Burger with a Reg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
	 
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[26][0];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			
		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][12] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
                      pCP.cmbT[pOtr][13] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";
                      pCP.cmbT[pOtr][14] += num1;				   
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j;                 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j;                 
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][0];
					 pElm = j-1;                 					  
				  }   
				  break;
			   }
			}
			
		 // Storing the Reg drink element for later use			 
			pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 

			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			

			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];
			pCP.cmbT[pElm][9]= "NA";
			pCP.cmbT[pElm][11]= "";

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1
                    pCP.cmbT[pElm][15]= "";				    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";				    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }									
					
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }
   }  

// Burger with a Lg drink and nothing else 
   else if(Rf < 1 && Bf < 1 && Rd < 1 && Ld > 0){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }	   
	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][1]  + 
			                                    pCP.Prices[26][1];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;

		  // Storing the Lg drink element for later use	
			 num1= pCP.cmbT[pOtr][11];

			 if(num1 == "NA"){
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][11] = num1 + "," + pCP.cmbDrk;
			 }
			 		  		 			  			 
		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
                      pCP.cmbT[pOtr][15] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 // ("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j;					                  
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j;					                  
				  }
				  else{
					 pCP.tPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					                  
					 pCP.hPrices[j] = pCP.Prices[i][1] +
					                  pCP.Prices[26][1];
					 pElm = j-1;					                  					  
				  }   
				  break;
			   }
			}
			
		 // Storing the Lg drink element for later use			 
			pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
					
			cmbClrSlider();
			DisplayPrice();
		    break;
		  }  
	   }
	 }				  
   }			  

// Burger with a Reg fry and Reg drink 
   else if(Rf > 0 && Bf < 1 && Rd > 0 && Ld < 1){

     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][2];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;			 

		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
				      pCP.cmbT[pOtr][12] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][13] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][14] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
			 						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					 
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][2];
					 pCP.hPrices[j] = pCP.Prices[i][2];
					 pElm = j;					  
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][2];
					 pCP.hPrices[j-1] = pCP.Prices[i][2];
					 pElm = j-1;					 					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;
			pCP.cmbT[pElm][4] = parseInt(Rf);
			pCP.cmbT[pElm][5] = parseInt(Bf);
			
		  // Storing the Reg drink element for later use			 
			 pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
								 
			cmbClrSlider();
			DisplayPrice();			
			break;
		  }  
	   }
	 }      // End for(pCP.Prices)				  
   }			  

// Burger with a Bk fry and Reg drink 
   else if(Rf < 1 && Bf > 0 && Rd > 0 && Ld < 1){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 1, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;			 
			
		  // Storing the Reg drink element for later use			 
			 //pCP.cmbT[pOtr][10] = pCP.cmbDrk;			 			 
			 			 
			 cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
			 num1= pCP.cmbT[pOtr][6] + parseInt(Rd);
			 pCP.cmbT[pOtr][6] = num1;
			 
			 num1 = pCP.cmbT[pOtr][8];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][8] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][10] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][8] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][10];
				pCP.cmbT[pOtr][10] = num1+ "," + pCP.cmbDrk;				
			 }			 
			 			 
             cmbIceSet1(pCP.cmbDrk, pCP.cmbIce, 3, parseInt(pCP.cmbIce));
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][8];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][8] = num1 + " Ex Ice";              
				      pCP.cmbT[pOtr][12] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][8] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][13] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][8] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][14] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);
			 						 
			 cmbClrSlider();
			 DisplayPrice();			 
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;					  
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][3];
					 pCP.hPrices[j-1] = pCP.Prices[i][3];
					 pElm = j-1;					 					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;
			
			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);

		  // Storing the Reg drink element for later use			 
			 pCP.cmbT[pElm][10] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 1, parseInt(Rd));
            cmbIceSet1(pCP.cmbIce, 3, parseInt(pCP.cmbIce));
			
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][8] = pCP.dList1[pCP.cmbDrk][0];

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][8];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][8] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][12] += num1;
                    pCP.cmbT[pElm][15]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][8] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][13] += num1;
                    pCP.cmbT[pElm][16]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][8] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][14] += num1;
                    pCP.cmbT[pElm][17]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();
			DisplayPrice();			
			break;
		  }  
	   }
	 }				  
   }				  

// Burger with a Reg fry and Lg drink 
   else if(Rf > 0 && Bf < 1 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][3];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			 pCP.fList1[0] = num1;
			 
			 num1= pCP.cmbT[pOtr][4] + parseInt(Rf);
			 pCP.cmbT[pOtr][4] = num1;				 
			
		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
				      pCP.cmbT[pOtr][15] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);			 
						 
			 cmbClrSlider();
			 DisplayPrice();		
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][3];
					 pCP.hPrices[j] = pCP.Prices[i][3];
					 pElm = j; 
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][3];
					 pCP.hPrices[j-1] = pCP.Prices[i][3];
					 pElm = j-1;					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[0]) + parseInt(Rf);
			pCP.fList1[0] = num1;

			num1= pCP.cmbT[pElm][4] + parseInt(Rf);
			pCP.cmbT[pElm][4] = num1;
			pCP.cmbT[pElm][5] = parseInt(Bf);
			
		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pElm][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12]= "";
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13]= "";
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14]= "";
				    break;
				     
				  default: break;          		   
				}
			 }						
						 
			cmbClrSlider();
			DisplayPrice();		
		    break;
		  }  
	   }
	 }				  
   }

// Burger with a Bk fry and Lg drink 
   else if(Rf < 1 && Bf > 0 && Rd < 1 && Ld > 0){
	   
     var test1=	cmbDrkSet1(pCP.cmbDrk, 2, 0);
     if(test1){ return; }
     	   
	 for(var i=0; i<pCP.Prices.length; i++){
	   if(pCP.Prices[i][0] == pTitle){
		  if(bTest){
             
			 pLstVal = pCP.AryLen[pOtr];
			 pCP.tPrices[pOtr][pLstVal][pInr] = pCP.Prices[i][4];
			 
			 for(var j=0; j<pLstVal; j++){
				num1 += pCP.tPrices[pOtr][pLstVal][j];
			 }
			 
			 pCP.tPrices[pOtr][pLstVal][pLstVal] = num1;
			 pCP.hPrices[pOtr] = num1;
			 
			 num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			 pCP.fList1[1] = num1;
			 
			 num1= pCP.cmbT[pOtr][5] + parseInt(Bf);
			 pCP.cmbT[pOtr][5] = num1;			 

		  // Storing the Lg drink element for later use			 
			 //pCP.cmbT[pOtr][11] = pCP.cmbDrk;			 			 
			  			 
			 cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			 
			 num1= pCP.cmbT[pOtr][7] + parseInt(Ld);
			 pCP.cmbT[pOtr][7] = num1;
			 
			 num1 = pCP.cmbT[pOtr][9];
			 if(num1 == "NA"){
				pCP.cmbT[pOtr][9] = pCP.dList1[pCP.cmbDrk][0];
				pCP.cmbT[pOtr][11] = pCP.cmbDrk;
			 }
			 else{
				pCP.cmbT[pOtr][9] = num1 + "," + pCP.dList1[pCP.cmbDrk][0];
				num1 = pCP.cmbT[pOtr][11];
				pCP.cmbT[pOtr][11] = num1+ "," + pCP.cmbDrk;
			 }
			 			 
             cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));
             
             if(pCP.cmbIce !== 100){
				num1 = pCP.cmbT[pOtr][9];
				 
				switch(pCP.cmbIce){
				   case 0:
				      pCP.cmbT[pOtr][9] = num1 + " Ex Ice";              
				      pCP.cmbT[pOtr][15] += num1;
				      break;
				    
				   case 1:
				      pCP.cmbT[pOtr][9] = num1 + " Lt Ice";				      
				      pCP.cmbT[pOtr][16] += num1;
				      break;
				      
				   case 2:
				      pCP.cmbT[pOtr][9] = num1 + " No Ice";				   
				      pCP.cmbT[pOtr][17] += num1;
				      break;
				      
				   default: break;          		   
				}
			 }
			 //alert("pCP.cmbT values are " + pCP.cmbT[pOtr]);	
						 
			 cmbClrSlider();
			 DisplayPrice();
			 break;  
		  }
		  else{
			for(var j=0; j<cmbAry1.length; j++){
			   if(cmbAry1[j] == id){
				  //alert(j);
				  if(j==0){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j;
				  }
				  else if(j < pCP.hPrices.length){
					 pCP.tPrices[j] = pCP.Prices[i][4];
					 pCP.hPrices[j] = pCP.Prices[i][4];
					 pElm = j; 
				  }
				  else{
					 pCP.tPrices[j-1] = pCP.Prices[i][4];
					 pCP.hPrices[j-1] = pCP.Prices[i][4];
					 pElm = j-1;					  
				  }   
				  break;
			   }
			}
			
			num1 = parseInt(pCP.fList1[1]) + parseInt(Bf);
			pCP.fList1[1] = num1;

			 num1= pCP.cmbT[pElm][5] + parseInt(Bf);
			 pCP.cmbT[pElm][5] = num1;
			 pCP.cmbT[pElm][4] = parseInt(Rf);

		  // Storing the Lg drink element for later use			 
			 pCP.cmbT[pElm][11] = pCP.cmbDrk;			 			 
			
			cmbDrkSet1(pCP.cmbDrk, 2, parseInt(Ld));
			pCP.cmbT[pElm][7] = parseInt(Ld);
			pCP.cmbT[pOtr][6] = parseInt(Rd);
			pCP.cmbT[pElm][9] = pCP.dList1[pCP.cmbDrk][0];
			            
            cmbIceSet1(pCP.cmbIce, 4, parseInt(pCP.cmbIce));

            if(pCP.cmbIce !== 100){
			   num1 = pCP.cmbT[pElm][9];
				 
			   switch(pCP.cmbIce){
				  case 0:
				    pCP.cmbT[pElm][9] = num1 + " Ex Ice";              
                    pCP.cmbT[pElm][15] += num1;
                    pCP.cmbT[pElm][12] += "";                    
				    break;
				    
				  case 1:
				    pCP.cmbT[pElm][9] = num1 + " Lt Ice";				      
                    pCP.cmbT[pElm][16] += num1;
                    pCP.cmbT[pElm][13] += "";                    
				    break;
				      
				  case 2:
				    pCP.cmbT[pElm][9] = num1 + " No Ice";				   
                    pCP.cmbT[pElm][17] += num1;
                    pCP.cmbT[pElm][14] += "";                    
				    break;
				     
				  default: break;          		   
				}
			 }						

			cmbClrSlider();			
			DisplayPrice();
		    break;
		  }  
	   }
	 }
  }    
}

function p2Clear(){

   document.getElementById("pSelector2").style.display = "none";
   document.getElementById("pSelector3").style.display = "none";
   document.getElementById("pSelector5").style.display = "none";
   document.getElementById("pSelector6").style.display = "none";
   document.getElementById("pSelector7").style.display = "none";
   document.getElementById("pSelector8").style.display = "none";
   document.getElementById("pSelector9").style.display = "none";
   document.getElementById("pSelector10").style.display = "none"; 
   document.getElementById("pSelector12").style.display = "none";
   document.getElementById("pSelector13").style.display = "none";
   document.getElementById("pSelector15").style.display = "none";        
   document.getElementById("pSelector16").style.display = "none";
   document.getElementById("pSelector20").style.display = "none";
   document.getElementById("pSelector21").style.display = "none";
   document.getElementById("pSelector22").style.display = "none";
   document.getElementById("pSelector23").style.display = "none";     
   //document.getElementById("pScroll").style.display = "none";                     
            		 
}

function RestartEverything(){
   p2KillArys();
   p2CreateAry();
   SetBagDefault();
   DisplayPrice();	
}

function pDelete() {
	
   var Selt1 = document.getElementById("pSelector20").childNodes.length;
   var Selt2 = document.getElementById("pSelector20").childNodes;
	     
   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
	  pDelMstrList();
      pDelSndWch();

	  if(pCP.mTotal.length == 1){
		 pCP.mTotal[0]="";
		 pCP.dProceed = true;
	  }      
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
	   
	  var test = false;	 
	  
	  for(var i=0; i<Selt1; i++){
		if(Selt2[i].style.color == 'rgb(0, 136, 170)'){
		  test = true;
		  break;
		}
	  }
	  
	  if(test){
		//alert("here");
	    pDelMstrList();
	    pDelDrinks();			  
	  }
	  else{
  	    pCP.BodyMessage = "Select a drink";
   	    p2ShowPopup();		 
		return;  
	  }

	  if(Selt1 == 2){
		 pCP.mTotal[0]="";
		 pCP.dProceed = true;
	  }	     
	   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
	  pDelMstrList();
	  pDelWings();
	  
	  if(pCP.mTotal.length == 1){
		 pCP.mTotal[0]="";
		 pCP.dProceed = true;
	  }
 		  
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
	  pDelMstrList(); 
	  pDelNuggest();   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
      pDelMstrList();
      pDelFries();
      
	  if(pCP.mTotal.length == 1){
		 pCP.mTotal[0]="";
		 pCP.dProceed = true;
	  }      
   }

   if(pCP.mTotal[pCP.Mcnt2] !== "" && pCP.mTotal.length > 1) {
      pCP.Mcnt2=0;
      p2Clear();
      pViewOrder();
   }
   else {
	   var x=0;
	   var x1=0;
	   
	  //alert(pCP.mTotal.length);
	  for(var i=0; i<pCP.dList1.length; i++){
		x=pCP.dList1[i][1];
		x1=pCP.dList1[i][2];
		if(x>0 && x1>0){
		   pCP.dProceed = true;
		   break;
		} 
	  }


	  
	  //alert(pCP.dProceed);
	  if(pCP.dProceed){ 
        pCP.Mcnt2=0;
        p2Clear();
        p2ClearToggles();
        document.getElementById("pSelector1").style.display = "block";
        pCP.FootState = 14;     	      	
	    pSetFooter();
	  }
	  else{
        document.getElementById("pSelector1").style.display = "none";
        document.getElementById("pSelector20").style.display = "block";        		  
	  } 
       	   
   }   

}

function pDelFries(){
	pCP.fList1[0] = 0;
	pCP.fList1[1] = 0;
}

function pDelWings(){
   		
   pCP.wList1[pCP.Mcnt2][0] -= pCP.cmbT[pCP.Mcnt2][3];
   pCP.wList1[pCP.Mcnt2][1] = "";

}

function pDelNuggest(){

   pCP.nList1[pCP.Mcnt2][0] -= pCP.cmbT[pCP.Mcnt2][3];
   pCP.nList1[pCP.Mcnt2][1] = "";
	
}

function pDelDrinks(){

    var pElm;
    	
	if(pCP.dSelected == "") {
  	   pCP.BodyMessage = "Select a drink first";
   	   p2ShowPopup();	    
	}
	else {
	   for(var i=0; i<pCP.dList1.length; i++){
		  if(pCP.dSelected == pCP.dList1[i][0]){
			  pElm = i;
		  }
	   }
	   
	   pCP.dList1[pElm][1] = 0;
	   pCP.dList1[pElm][2] = 0;
	
	   for(var i=3; i<5; i++) {
	      for(var x=0; x<3; x++){
		      pCP.dList1[pElm][i][x] = 0;
	      }
	   }
	}
}

function pDelMstrList(){
   var delAry1 = new Array(pCP.mTotal.length);
   var strHld = pCP.mTotal[pCP.Mcnt2];
   var myCnt = 0;	
	
   for(var i=0; i<delAry1.length; i++) {
	  delAry1[i] = "";
   }
   
   myCnt =0;  
   
   for(var i=0; i<delAry1.length; i++) {
	  if ( pCP.mTotal[pCP.Mcnt2] !== ""){
	    delAry1[myCnt] = pCP.mTotal[i];
	    myCnt++;
	  }  
   }
   
   for(var i=0; i<delAry1.length; i++) {
	  if(delAry1[i] !== "") {
	     pCP.mTotal[i] = delAry1[i];
	  }
	  else {break;}
   }
   
   myCnt = 0;
   for(var i=0; i<pCP.mTotal.length; i++) {
	  if(pCP.mTotal[i] !== "") {
	     myCnt++;
	  }
   }	
}

function pDelSndWch(){
	
  var swhLst1 = [];
  swhLst1.push("","","","","","","","","","");
  pCP.sList1.splice(pCP.Mcnt2, 1);
  pCP.sList1.push(swhLst1);

}

function pDelCmbo(){
  
  switch(pCP.cmbT[pCP.Mcnt2][0]){
	  
	case "s":
      p2CmbCheck1();
      break;
    case "n":  
      p2CmbCheck1();
      break;
    case "w":
      p2CmbCheck1();
      break;
    default: 
      //NoComboDel();
      break;        
  } // End switch(pCP.cmbT[pCP.Mcnt2][0])  
    
}

function NoComboDel(id){
    var psOrder1 = document.getElementsByClassName("psOrder1");
    var NodeList1;  // psbList
    var tempS;
    var x, x1, x2, x3, x4;
    var pElm1, pStr;
    var StorePrice;
    var cBool = false;
    var fdPrices = [1.74, 2.17, 1.63, 1.85];
    
    for(var i=0; i<psOrder1.length; i++){
	   
	   if(psOrder1[i].childNodes[1].id == id){
		  pElm1 = i;
		  break; 
	   }
	}
	
	NodeList1 = psOrder1[pElm1].getElementsByClassName("psbList");
	tempS = "" + NodeList1[0].childNodes[0].innerHTML;
	
	x = tempS.split("(");
	x1 = x[1];
	tempS = ""+x[1];
	x = tempS.split(") ");
	
	switch(x[1]){
	   case "REG FRY":
	     if(pCP.NcmbF[0] > 0){
	       //pCP.fList1[0] -= pCP.NcmbF[0];
	       //pCP.NcmbF[0] -= pCP.NcmbF[0];
	       pCP.fList1[0] -= parseInt(x[0]);
	       
	       if(pCP.fList1[0] < 0){
			  pCP.fList1[0] = 0;
		   }
		   
	       pCP.NcmbF[0] -= parseInt(x[0]);
	       StorePrice = x[0] * fdPrices[0]; 
	     }	     
	     break;
	   
	   case "BK FRY":
	     if(pCP.NcmbF[1] > 0){
	       //pCP.fList1[1] -= pCP.NcmbF[1];
	       //pCP.NcmbF[1] -= pCP.NcmbF[1];
	       pCP.fList1[1] -= parseInt(x[0]);

	       if(pCP.fList1[1] < 0){
			  pCP.fList1[1] = 0;
		   }
	       
	       pCP.NcmbF[1] -= parseInt(x[0]);

	       StorePrice = x[0] * fdPrices[1];
	     }	     	     
	     break;

// ====================
//  Drinks start here
// ==================== 	   
       default: 
         tempS = x[1];
         x2 = tempS.split(" ");
         if(x2[0] == "REG"){
			for(var i=0; i<pCP.dList1.length; i++){
			   if(pCP.dList1[i][0] == x2[1]){
				  pCP.dList1[i][1] -= parseInt(x1[0]);
				  pCP.NcmbT[i][1] -= parseInt(x1[0]);
				  
				  pStr = pCP.NcmbT[i][5];
                  if((pStr.search(",")) !== -1){
 		            x3 = pStr.split(",");
 		            cBool = true;
			      }
			      else{
			        x3 = pStr;
			      }				  

                  if(cBool){
			        for(var j=0; j<3; j++){
			          if(pCP.dList1[i][3][j] > 0){  
                        pCP.dList1[i][3][j] -= 1;                  
                      }                            	   
			        }			      		    
	              }   // End if(cBool)     
	              else{
					if(pStr !== ""){
                      x3 = pStr.split("(");
	                  x4 = x3[1];
	                  pStr = ""+x3[1];
	                  x3 = pStr.split(") ");
	                  pStr = x3[1];

                      if((pStr.search(" ")) !== -1){
 		                x4 = pStr.split(" ");
 		                pStr = x4[0];
			          }				  
	                                    
                      switch(pStr){
					    case "Ex":
					      if(pCP.dList1[i][3][0] > 0){
					         pCP.dList1[i][3][0] -= parseInt(x3[0]);
					      }   
					      break;
					     
					     case "Lt":
					       if(pCP.dList1[i][3][1] > 0){
					         pCP.dList1[i][3][1] -= parseInt(x3[0]);
					       }   
					       break;
					     
					     case "No":
					       if(pCP.dList1[i][3][2] > 0){
					         pCP.dList1[i][3][2] -= parseInt(x3[0]);
					       }   					   
					       break;
					     
					     default: break;    
					  }
					}
					  
				    pCP.NcmbT[i][5] = "";
				    StorePrice = x[0] * fdPrices[2];
				    break;
			     } // End Else
			}      // End if(pCP.dList1[i][0] == x2[1])
		 }         // End for(i)
	  }            // End if(x2[0] == "REG")
	  else{		  
		 for(var i=0; i<pCP.dList1.length; i++){
		   if(pCP.dList1[i][0] == x2[1]){
			  pCP.dList1[i][2] -= parseInt(x1[0]);
			  pCP.NcmbT[i][2] -= parseInt(x1[0]);
			  
			  pStr = pCP.NcmbT[i][6];
              if((pStr.search(",")) !== -1){
 		         x3 = pStr.split(",");
 		         cBool = true;
		      }
		      else{
		        x3 = pStr;
		      }				  

              if(cBool){
		        for(var j=0; j<3; j++){
		          if(pCP.dList1[i][4][j] > 0){  
                     pCP.dList1[i][4][j] -= 1;                  
                  }                            	   
		        }			      		    
	          }   // End if(cBool)     
	          else{
				if(pStr !== ""){
                  x3 = pStr.split("(");
	              x4 = x3[1];
	              pStr = ""+x3[1];
	              x3 = pStr.split(") ");
	              pStr = x3[1];

                  if((pStr.search(" ")) !== -1){
 		            x4 = pStr.split(" ");
 		            pStr = x4[0];
		          }				  
	                                    
                  switch(pStr){
				    case "Ex":
				      if(pCP.dList1[i][4][0] > 0){
				         pCP.dList1[i][4][0] -= parseInt(x3[0]);
				      }   
				      break;
				     
				     case "Lt":
				       if(pCP.dList1[i][4][1] > 0){
				         pCP.dList1[i][4][1] -= parseInt(x3[0]);
				       }   
				       break;
					     
				     case "No":
				       if(pCP.dList1[i][4][2] > 0){
				         pCP.dList1[i][4][2] -= parseInt(x3[0]);
				       }   					   
				       break;
					     
		 		     default: break;    
				  } // End switch()
				}   // End if(pStr !== "")
					  
				pCP.NcmbT[i][6] = "";
				StorePrice = x[0] * fdPrices[3];
				break;
			     } // End Else
			}      // End if(pCP.dList1[i][0] == x2[1])
		 }         // End for(i)
			 
	  }            // End Else
	  break;	   
	}              // End switch(x[1])    
	
	for(var i=0; i<pCP.hPrices.length; i++){
	  if(pCP.hPrices[i] == StorePrice){
		pElm1 = i;
		break;
	  }	
	}
	
    pCP.hPrices.splice(pElm1, 1);
    pNewOrderView();
	DisplayPrice();   	
}

function p2CmbCheck1(){
  var ptext,ptext2, x, x1, x2, x3, x4;
  var cBool = false;
  var xcnt1;

/* 
	  ===============
	   Fry Section
	  ===============
*/   
	   if(pCP.cmbT[pCP.Mcnt2][4] > 0){
	     pCP.fList1[0] = pCP.fList1[0] - pCP.cmbT[pCP.Mcnt2][4];
	   }
	   if(pCP.cmbT[pCP.Mcnt2][5] > 0){
		 pCP.fList1[1] = pCP.fList1[1] - pCP.cmbT[pCP.Mcnt2][5];
	   }  

/* 
	  ===================
	   Reg Drink Section
	  ===================
*/   
       if(pCP.cmbT[pCP.Mcnt2][6] > 0){
         ptext = "" + pCP.cmbT[pCP.Mcnt2][10];
         cBool = false;
            
         if((ptext.search(",")) !== -1){
 		    x1 = ptext.split(",");
 	        cBool = true;
	     }
	     else{
		    x = parseInt(ptext);
	     }
       
         if(cBool){
            for(var i=0; i<x1.length; i++){
			  x = parseInt(x1[i]);
			  for(var j=0; j<3; j++){
			    if(pCP.dList1[x][3][j] > 0){  
                   pCP.dList1[x][3][j] -= 1;                  
                }                            	   
			  }
			  pCP.dList1[x][1] = pCP.dList1[x][1] - 1;			   
		    }            	        		    
	     }   // End if(cBool)     
	     else{
		    for(var j=0; j<3; j++){
			  if(pCP.dList1[x][3][j] > 0){
			 	 pCP.dList1[x][3][j] -= 1;  
			  } 
		    }
            pCP.dList1[x][1] = pCP.dList1[x][1] - pCP.cmbT[pCP.Mcnt2][6];
	     }
	   }	   
/* 
	  ===================
	   Lg Drink Section
	  ===================
*/   

	   if(pCP.cmbT[pCP.Mcnt2][7] > 0){
		 //pCP.qList1[x][2] = pCP.fList1[x][2] - pCP.cmbT[pCP.Mcnt2][7];
          ptext = "" + pCP.cmbT[pCP.Mcnt2][11];
          cBool = false;
            
          if((ptext.search(",")) !== -1){
 		    x1 = ptext.split(",");
 	        cBool = true;
	      }
	      else{
		    x = parseInt(ptext);
	      }
       
          if(cBool){
            for(var i=0; i<x1.length; i++){
			  x = parseInt(x1[i]);
			  for(var j=0; j<3; j++){
			    if(pCP.dList1[x][4][j] > 0){  
                  pCP.dList1[x][4][j] -= 1;                  
                }                            	   
			  }
			  pCP.dList1[x][2] = pCP.dList1[x][2] - 1;			   
		    }            	        		    
	      }   // End if(cBool)     
	      else{
		    for(var j=0; j<3; j++){
			  if(pCP.dList1[x][4][j] > 0){
				pCP.dList1[x][4][j] -= 1;  
			  } 
		    }
            pCP.dList1[x][2] = pCP.dList1[x][2] - pCP.cmbT[pCP.Mcnt2][7];
	      }
		 
	   }  
        
       pCP.cmbT.splice(pCP.Mcnt2, 1);
/*       
       for(var i=pCP.Mcnt2; i<pCP.cmbT.length; i++){
	     if(pCP.cmbT[i][0] !== ""){
	       pCP.cmbT[i][1] = pCP.cmbT[i][1] - 1;
	     }
	     else{ break; }   
       }
*/ 	
}

function p2StoreKeyPadAns() {

   var pSelect1 = document.getElementsByClassName("pSelect1");	
   var pValue = document.getElementById('pDisplay').innerHTML;
  
   var p2CheckAry1 = ["pS8",   "pS9",   "pS10",  "pS11",
                      "pS12",  "pS13",  "pS14",  "pS15",
                      "pS36",  "pS37",  "pS38",  "pS39",
                      "pS40",  "pS41",  "pS42",  "pS43",
                      "pS54",  "pS55",  "pS56",  "pS57",
                      "pS58",  "pS59",  "pS60",  "pS61",
                      "pS62",  "pcd1",  "pcd2",  "pcd3",
                      "pcd4",  "pcd5",  "pcd6",  "pcd7",
                      "pcd8",  "pcd9",  "pcd10", "pcd11",
                      "pcd12", "pcd13", "pcd14"
                     ];
                      
   var p2CheckAry2 = ["pS2",   "pS4",   "pS5",   "pS6"];                      
                     
   var tmpTitle;
   var test = false;	 
   var hld;
   var hld1;
 

   if(pCP.tbID == 2){
      pCP.MultiOrder = pValue;
      document.getElementById('pDisplay').innerHTML = 0;

  	  pCP.BodyMessage = "There are (" + pCP.MultiOrder + ") Orders";
   	  p2ShowPopup();	  
	  pCP.tbID = 0;
      p2NavClear();
      pCP.FootState = 14;     	      	
	  pSetFooter();	  
   }
   else if(pCP.tbID == 3){
   	  if (pValue.length < 10) {
  	     pCP.BodyMessage = "Not enough numbers entered";
   	     p2ShowPopup();		  
   	  }	
   	  else {			
		 pCP.TxtNum = pValue;
		 document.getElementById('pDisplay').innerHTML = 0;
  	     pCP.BodyMessage = "Receipt is being texted to " + pCP.TxtNum;
   	     p2ShowPopup();
		 pCP.tbID = 0;
         p2NavClear();		 
         pCP.FootState = 14;     	      	
	     pSetFooter();        
	  }	 	   
   }
   else{
	 if(pValue > 1){ return; }
	 else{	 
	   document.getElementById(""+pCP.BoxID).innerHTML = pValue;  
	   p2RetView();
	 }           
   } 
}

function FindMstrElm() {
	var hld;
	for (var j=0; j<pCP.mTotal.length; j++) {
	  if (pCP.mTotal[j] == "") {	
	    hld = j;
	    break;
	  }
	}
	return hld;    
}

function ShowMstrSand(sCnt) {
   
   var pCnt = 76;
   var j=0;   
   var pDiv;
   var pp;
   var unit; 
       
       document.getElementById("pSelector1").style.display = "none"; 	
	   document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
	   document.getElementById("pOr1").innerHTML = "";
	   
	   for (var i=1; i<pCP.sList1[pCP.Mcnt2].length; i++) {
	   	if (i==1) {
	   	  	LoadMstrNav();
	   	  	if (pCP.sList1[pCP.Mcnt2][i] !== "") {
		   	   pp="pS"+ (pCnt+j);
		   	   unit = pCP.sList1[pCP.Mcnt2][0];
		   	   
		   	   pDiv = '<div class="pSel" id="'+ pp +'"></div>';
		   	   document.getElementById("pSelector20").innerHTML += pDiv;
		   	   document.getElementById("pS"+ (pCnt+j)).innerHTML = '(' + unit + ') ' + 
		   	   pCP.sList1[pCP.Mcnt2][i];
		   	   j++;		   	   			   
			}
	   	}
	   	else {
		   	if (pCP.sList1[pCP.Mcnt2][i] !== "") {
		   	  pp="pS"+ (pCnt+j);
		   	  
		   	  pDiv = '<div class="pSel" id="'+ pp +'"></div>';
		   	  
		   	  document.getElementById("pSelector20").innerHTML += pDiv;
		   	  document.getElementById("pS"+ (pCnt+j)).innerHTML = pCP.sList1[pCP.Mcnt2][i];
		   	  j++;
		   	}
		   	else {break;}
		  } 	  
	   }
	          	
}

function ShowMstrNug() {
	var Reg;
	var Buck;
	var rSauce; 
	var pDiv;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();

   pDiv = '<div class="pSel">NUGGETS</div>';
   document.getElementById("pSelector20").innerHTML += pDiv;   
   
   
   Reg = pCP.nList1[0][0];
   rSauce = pCP.nList1[0][1];
      
   if (Reg > 0) {
      pDiv = '<div class="pSel">(' + Reg + ') 5pc ' + rSauce + '</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }
 
   Buck = pCP.nList1[1][0];
   bSauce = pCP.nList1[1][1];
   
   if (Buck > 0) {
      pDiv = '<div class="pSel">(' + Buck + ') BK ' + rSauce + '</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }  
}

function ShowMstrFries() {
	var Reg;
	var LG;
	var pDiv;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   pDiv = '<div class="pSel">FRIES</div>';
   document.getElementById("pSelector20").innerHTML += pDiv;   
   
   Reg = pCP.fList1[0];
   LG  = pCP.fList1[1];
      
   if (Reg > 0) {
      pDiv = '<div class="pSel">(' + Reg + ') Reg</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }
   
   if (LG > 0) {
      pDiv = '<div class="pSel">(' + LG + ') BK</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;           	
   }
        
}

function ShowMstrWings() {
	var Wng;
	var rSauce; 
	var pDiv;
	var j;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   pDiv = '<div class="pSel">WINGS</div>';
   document.getElementById("pSelector20").innerHTML += pDiv;   
   
   for (var i=0; i<pCP.wList1.length; i++) {
     Wng =    pCP.wList1[i][0];
     rSauce = pCP.wList1[i][1];

	  if (Wng > 0) {
	  	 switch(i) { 
	  	    case 0:
	  	      j="5W"; break;
	  	    case 1:
	  	      j="10W"; break;
	  	    case 2:
	  	      j="15W"; break;     
	  	    case 3:
	  	      j="20W"; break;
	  	    case 4:
	  	      j="25W"; break;
	  	    case 5:
	  	      j="30W"; break;
	  	    default:   break;  	  	 
	  	 }
	  	 
       pDiv = '<div class="pSel">(' + Wng + ') ' + j + " " + rSauce + '</div>';
       document.getElementById("pSelector20").innerHTML += pDiv;
       rSauce ="";          	
	  }     
   }  
}

function ShowMstrDrink() {
	var Reg;
	var LG;
	var IceType;
	var dTitle;
	var pDiv;
	var x1;
	var xCnt = 0;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   for (var i=0; i<pCP.dList1.length; i++) {
          
	   Reg = pCP.dList1[i][1];
	   LG  = pCP.dList1[i][2];
	      
	   if (Reg > 0) {
	   	dTitle = pCP.dList1[i][0];
	   	  xCnt= i+1;
	      pDiv = '<div class="pSel" id="dr' + xCnt + '" onclick="mDrk1(this.id)">(' + Reg + ') Reg ' + dTitle + '</div>';
	      document.getElementById("pSelector20").innerHTML += pDiv;
	      
	      
	      for (j=0; j<3; j++) {
	         x1=pCP.dList1[i][3][j];
	         IceType = "";
	         if (x1>0) {
	         	switch(j) {
	         		case 0:
	         		  IceType +=  x1 + ': Ex Ice ';
	         		  break;
	         		case 1:
	         		  IceType += x1 + ': Lt Ice ';
	         		  break;
	         		case 2:
	         		  IceType += x1 + ': No Ice ';
	         		  break;
	         		default: break;  	         		  	         		   
	         	}
			      pDiv = '<div class="pSel">' + IceType + '</div>';
			      document.getElementById("pSelector20").innerHTML += pDiv;	         	
	         }	
	      }          	
	   }
	   
	   if (LG > 0) {
	   	dTitle = pCP.dList1[i][0];
	   	xCnt= i+1;	   	
	    pDiv = '<div class="pSel" id="dr' + xCnt + '" onclick="mDrk1(this.id)">(' + LG + ') LG ' + dTitle + '</div>';
	      document.getElementById("pSelector20").innerHTML += pDiv; 
	      

	      for (j=0; j<3; j++) {
	         IceType = "";	      	
	         x1=pCP.dList1[i][4][j];
	         if (x1>0) {
	         	switch(j) {
	         		case 0:
	         		  IceType += x1 + ': Ex Ice';
	         		  break;
	         		case 1:
	         		  IceType += x1 + ': Lt Ice';
	         		  break;
	         		case 2:
	         		  IceType += x1 + ': No Ice';
	         		  break;
	         		default: break;  	         		  	         		   
	         	}
			      pDiv = '<div class="pSel">' + IceType + '</div>';
			      document.getElementById("pSelector20").innerHTML += pDiv;	         	
	         }	
	      } 	                	
	   }    
   }
}

function mDrk1(id) {
   var dSplit;
   var Hld;
   var Elm;
   
   pSel = document.getElementsByClassName("pSel");

   for(var i=0; i<pSel.length; i++) {
	  if (pSel[i].id == id) {
	     Elm = i;
	     break;	  
	  }   
   }
   
   Hld = document.getElementById("" + id).innerHTML; 
   dSplit = Hld.split(" ");
   pCP.dSelected = dSplit[2];
   pTogState(Elm);
 	
}

function pMstrModify() {
   
   p2Clear();
   document.getElementById("pSelector1").style.display = "block";

                              //  Sandwhiches   
   if(pCP.cmbT[pCP.Mcnt2][0] == "s") {
      document.getElementById("pSelector3").style.display = "block";
      pCP.modify = true; 
            
      pCP.FootState = 17;     	      	
	  pSetFooter();                 
      p2ReadSandwhiches();            
   }                               // Wings
   else if (pCP.cmbT[pCP.Mcnt2][0] == "w") {
      document.getElementById("pSelector6").style.display = "block";
      document.getElementById("pSelector12").style.display = "block";
      pCP.modify = true;   	        
      pCP.FootState = 17;     	      	
	  pSetFooter(); 
	  ReadWings();        	
   	
   }                             // Nuggests      
   else{
      document.getElementById("pSelector7").style.display = "block";
      document.getElementById("pSelector12").style.display = "block";      
      pCP.modify = true;   	        
      pCP.FootState = 17;     	      	
	  pSetFooter(); 
      ReadNuggets();       	
   	
   } 
}

function p2Update() {
	
   document.getElementById("pSelector1").style.display = "none";
   	
	                             // Burgers
   if(pCP.cmbT[pCP.Mcnt2][0] == "s") {
	  pCP.modify = true;
      p2StoreSandwhiches2();               
   }                             // Wings
   else if (pCP.cmbT[pCP.Mcnt2][0] == "w") {
 	  pCP.modify = true;            	         
	  p2StoreFriedItems();
   }                             // Nuggests
   else{
  	  pCP.modify = true;            	         
	  p2StoreFriedItems();      	
   	
   }	        
   pNewOrderView();
}

function LoadMstrNav() {
	
	   var pSel4 = document.getElementsByClassName("pSel4");
	   pSel4[0].innerHTML = '<div class="pSel5" id="pB" onclick="pNavList(this.id)">&lt</div>' +
	                        '<div class="pSel5" id="pF" onclick="pNavList(this.id)">&gt</div>';
	                        
	   var HTML1 = document.getElementsByClassName('pSel5');
	   
	   HTML1[0].style.cssText = "display: inline-block; float: left; width: 23.3%; height: 33px;" +
	                            "background: #333; margin-bottom: 4px; margin-top: 1%; margin-left: 3px;" +
	                            "padding-top: 1%; border: 2px solid #AAA;" +
	                            "text-align: center; font-weight: bold; cursor: pointer";	 
	     
	     
	   HTML1[1].style.cssText = "display: inline-block; float: left; width: 23.3%; height: 33px;" +
	                            "background: #333; margin-bottom: 4px; margin-top: 1%; margin-left: 48.9%;" +
	                            "padding-top: 1%; border: 2px solid #AAA;" +
	                            "text-align: center; font-weight: bold; cursor: pointer";	   	  	
	
}

function pNavList(id) {
		
	switch(id) {
		case "pB":
		    pCP.Mcnt2--;
		    if (pCP.Mcnt2 < 0) {
		    	pCP.Mcnt2 = 0;
		    }
		    else {
		    	MVP();		    	
		    }	
		  break;
		  
		case "pF":
		    pCP.Mcnt2++;
		    if (pCP.Mcnt2 >= pCP.mTotal.length) {
		    	pCP.Mcnt2--;
		    }
		    else {		    	
		    	MVP();		    			    	
		    }		
		  break;
		  
		default: break;  		  
	}
}

function MVP() {
   
   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
	  document.getElementById("pSelector20").innerHTML = "";
	  document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';   	
      ShowMstrSand();   
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
      document.getElementById("pSelector20").style.display = "block";   	
	  document.getElementById("pSelector20").innerHTML = "";
	  document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';
      ShowMstrDrink(); 		   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
      document.getElementById("pSelector20").style.display = "block";   	
      document.getElementById("pSelector20").innerHTML = "";
	  document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrWings();        	
   	
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
      document.getElementById("pSelector20").style.display = "block";   	
	  document.getElementById("pSelector20").innerHTML = "";
	  document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrNug();        	
   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
      document.getElementById("pSelector20").style.display = "block";   	
	  document.getElementById("pSelector20").innerHTML = "";
	  document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrFries();        	
   }   
}


function pViewOrder() {
	var a=b=c=d=e=f=g=h=0;
	var mTot = 0;

   if (!pCP.snwh && !pCP.nug && !pCP.wng && !pCP.fry & !pCP.drk) {
   	alert("Nothing Ordered");
   	return;
   }
   
// Burger List	
	for (var i=0; i<pCP.sList1.length; i++) {
		a=pCP.sList1[i][0];
		
		if (a > 0) { b++; }				
	}

// Nugget List	
	for (var i=0; i<pCP.nList1.length; i++) {
		a=pCP.nList1[i][0];
		if (a > 0) { c=1; break; }				
	}
	
// Wings List	
	for (var i=0; i<pCP.wList1.length; i++) {
		a=pCP.wList1[i][0];
		if (a > 0) { d=1; break; }				
	}
	
// Fries List	
	for (var i=0; i<pCP.fList1.length; i++) {
		a=pCP.fList1[i];
		if (a > 0) { e=1; break; }				
	}
	
// Drink List	
	for (var i=0; i<pCP.dList1.length; i++) {
		a=pCP.dList1[i][1];

		if (a > 0) { f=a; }
		
		a=pCP.dList1[i][2];
			
		if (a > 0) { g=a; }
		
		if (f > 0 && g > 0) { h++; }
		else if (f > 0 && g == 0) { h++; }
		else if (f ==0 && g > 0) { h++ }
		f=g=0;						
	}
	
	
// Master Order List		
	mTot = b + c + d + e + h;
	
	if (mTot < 1) { return; }
	else {
	  	pCP.mTotal = new Array(mTot);
	  	pCP.mTotTitle = new Array(mTot);
	  	
	  	for (i=0; i<pCP.mTotal.length; i++) {
	  		pCP.mTotal[i] = "";
	  		pCP.mTotTitle[i] = "";
	  	} 
	}

 
	if (b > 0) {
// Sandwhiches
		for (var i=FindMstrElm(); i<b; i++) {
			pCP.mTotal[i] = "pSelector20";
			pCP.mTotTitle[i] = pCP.sList1[i][1];
		}
	}
	
	if (c > 0) {
// Nuggets				
		pCP.mTotal[FindMstrElm()] = "pSelector7";		
	}
	
	if (d > 0) {
// Wings				
		pCP.mTotal[FindMstrElm()] = "pSelector6";		
	}			

	if (e > 0) {
// Fries				
		pCP.mTotal[FindMstrElm()] = "pSelector8";		
	}
	
	if (h > 0) {
// Drinks						
	  for (var i=FindMstrElm(); i<mTot; i++) {
		 pCP.mTotal[i] = "pSelector10";
	  }			
	}	

	p2Clear();
	document.getElementById("pSelector1").style.display = "none";
	MVP();
   
    //pCP.FootState = 11;
    pCP.FootState = 20;     	      	
	pSetFooter(); 
	
}

 function p2Back() {
 	p2Clear();
 	p2RetView();
 	
 }

function p2DrinkHTML(){
	
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3");
	var pIceSel= document.getElementsByClassName("pIceSel");
   var Drink1 = document.getElementById("pSelector10");
   
   var styleWidth = 67.98;	
	
	if (pCP.winW < 371) {
     Drink1.style.float = "left";
     Drink1.style.width = 73 + "%";
     Drink1.style.heigth = "auto";
     styleWidth = 65.98; 			
	}

	else if (pCP.winW == 371 || pCP.winW < 545) {
     Drink1.style.float = "left";
     Drink1.style.width = 47 + "%";
     Drink1.style.heigth = "auto";
     styleWidth = 65.98; 	
		
	}
	
	else if (pCP.winW >= 545) {
     Drink1.style.float = "left";
     Drink1.style.width = "";
     Drink1.style.heigth = "auto"; 	
	}
		
	document.getElementById("pTxt13").style.float = "left";
	document.getElementById("pTxt13").style.width = 8 + "%";
	document.getElementById("pTxt13").style.height = 6 + "%";
	document.getElementById("pTxt13").style.marginLeft = .5 + "%";
	document.getElementById("pTxt13").style.marginTop = .38 + "%";		
	document.getElementById("pTxt13").style.paddingTop = 6 + "%";
	document.getElementById("pTxt13").style.paddingLeft = 10 + "%";
	document.getElementById("pTxt13").style.paddingRight = 10 + "%";
	document.getElementById("pTxt13").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt13").style.color = "#000";
	document.getElementById("pTxt13").style.background = "#FFF";				
			
   document.getElementById("pS63").style.width = styleWidth + "%";
   document.getElementById("pS63").style.height = 6 + "%";
   document.getElementById("pS63").style.marginTop = .38 + "%";
   document.getElementById("pS63").style.paddingTop = 6 + "%"; 
	document.getElementById("pS63").style.paddingBottom = 5 + "%";
	document.getElementById("pS63").style.textAlign = "center";	

//------------  Reg  -------------------------------------					

    pIceSel[0].style.float = "left";			
    pIceSel[0].style.width = styleWidth + "%";
    pIceSel[0].style.height = 6 + "%";
    pIceSel[0].style.marginTop = .038 + "%";
    pIceSel[0].style.marginLeft = .78 + "%";
    pIceSel[0].style.paddingTop = 6 + "%"; 
	pIceSel[0].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt15").style.float = "left";
	document.getElementById("pTxt15").style.width = 9 + "%";
	document.getElementById("pTxt15").style.height = 6 + "%";
	document.getElementById("pTxt15").style.marginLeft = .5 + "%";
	document.getElementById("pTxt15").style.marginTop = .38 + "%";		
	document.getElementById("pTxt15").style.paddingTop = 6 + "%";
	document.getElementById("pTxt15").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt15").style.paddingRight = 9 + "%";
	document.getElementById("pTxt15").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt15").style.color = "#000";
	document.getElementById("pTxt15").style.background = "#FFF";		
//-------------------------------------------------					

    pIceSel[1].style.float = "left";			
    pIceSel[1].style.width = styleWidth + "%";
    pIceSel[1].style.height = 6 + "%";
    pIceSel[1].style.marginTop = .038 + "%";
    pIceSel[1].style.marginLeft = .78 + "%";
    pIceSel[1].style.paddingTop = 5 + "%"; 
	pIceSel[1].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt16").style.float = "left";
	document.getElementById("pTxt16").style.width = 9 + "%";
	document.getElementById("pTxt16").style.height = 6 + "%";
	document.getElementById("pTxt16").style.marginLeft = .5 + "%";
	document.getElementById("pTxt16").style.marginTop = .38 + "%";		
	document.getElementById("pTxt16").style.paddingTop = 6 + "%";
	document.getElementById("pTxt16").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt16").style.paddingRight = 9 + "%";
	document.getElementById("pTxt16").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt16").style.color = "#000";
	document.getElementById("pTxt16").style.background = "#FFF";

//-------------------------------------------------					

    pIceSel[2].style.float = "left";			
    pIceSel[2].style.width = styleWidth + "%";
    pIceSel[2].style.height = 5 + "%";
    pIceSel[2].style.marginTop = .038 + "%";
    pIceSel[2].style.marginLeft = .78 + "%";
    pIceSel[2].style.paddingTop = 5 + "%"; 
	pIceSel[2].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt17").style.float = "left";
	document.getElementById("pTxt17").style.width = 9 + "%";
	document.getElementById("pTxt17").style.height = 5 + "%";
	document.getElementById("pTxt17").style.marginLeft = .5 + "%";
	document.getElementById("pTxt17").style.marginTop = .38 + "%";		
	document.getElementById("pTxt17").style.paddingTop = 5 + "%";
	document.getElementById("pTxt17").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt17").style.paddingRight = 9 + "%";
	document.getElementById("pTxt17").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt17").style.color = "#000";
	document.getElementById("pTxt17").style.background = "#FFF";
	
//----------- LG ----------------------------------------
	
	document.getElementById("pTxt14").style.float = "left";
	document.getElementById("pTxt14").style.width = 8 + "%";
	document.getElementById("pTxt14").style.height = 5 + "%";
	document.getElementById("pTxt14").style.marginLeft = .5 + "%";
	document.getElementById("pTxt14").style.marginTop = .38 + "%";		
	document.getElementById("pTxt14").style.paddingTop = 5 + "%";
	document.getElementById("pTxt14").style.paddingLeft = 10 + "%";
	document.getElementById("pTxt14").style.paddingRight = 10 + "%";
	document.getElementById("pTxt14").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt14").style.color = "#000";
	document.getElementById("pTxt14").style.background = "#FFF";				
			
    document.getElementById("pS64").style.width = styleWidth + "%";
    document.getElementById("pS64").style.height = 5 + "%";
    document.getElementById("pS64").style.marginTop = .38 + "%";
    document.getElementById("pS64").style.paddingTop = 5 + "%"; 
	document.getElementById("pS64").style.paddingBottom = 5 + "%";
	document.getElementById("pS64").style.textAlign = "center";
//-------------------------------------------------					

    pIceSel[3].style.float = "left";			
    pIceSel[3].style.width = styleWidth + "%";
    pIceSel[3].style.height = 5 + "%";
    pIceSel[3].style.marginTop = .038 + "%";
    pIceSel[3].style.marginLeft = .78 + "%";
    pIceSel[3].style.paddingTop = 5 + "%"; 
	pIceSel[3].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt18").style.float = "left";
	document.getElementById("pTxt18").style.width = 9 + "%";
	document.getElementById("pTxt18").style.height = 5 + "%";
	document.getElementById("pTxt18").style.marginLeft = .5 + "%";
	document.getElementById("pTxt18").style.marginTop = .38 + "%";		
	document.getElementById("pTxt18").style.paddingTop = 5 + "%";
	document.getElementById("pTxt18").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt18").style.paddingRight = 9 + "%";
	document.getElementById("pTxt18").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt18").style.color = "#000";
	document.getElementById("pTxt18").style.background = "#FFF";		
//-------------------------------------------------					

    pIceSel[4].style.float = "left";			
    pIceSel[4].style.width = styleWidth + "%";
    pIceSel[4].style.height = 5 + "%";
    pIceSel[4].style.marginTop = .038 + "%";
    pIceSel[4].style.marginLeft = .78 + "%";
    pIceSel[4].style.paddingTop = 5 + "%"; 
	pIceSel[4].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt19").style.float = "left";
	document.getElementById("pTxt19").style.width = 9 + "%";
	document.getElementById("pTxt19").style.height = 5 + "%";
	document.getElementById("pTxt19").style.marginLeft = .5 + "%";
	document.getElementById("pTxt19").style.marginTop = .38 + "%";		
	document.getElementById("pTxt19").style.paddingTop = 5 + "%";
	document.getElementById("pTxt19").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt19").style.paddingRight = 9 + "%";
	document.getElementById("pTxt19").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt19").style.color = "#000";
	document.getElementById("pTxt19").style.background = "#FFF";

//-------------------------------------------------					

    pIceSel[5].style.float = "left";
    pIceSel[5].style.width = styleWidth + "%";    			
    pIceSel[5].style.height = 5.5 + "%";
    pIceSel[5].style.marginTop = .038 + "%";
    pIceSel[5].style.marginLeft = .78 + "%";
    pIceSel[5].style.paddingTop = 5.5 + "%"; 
	pIceSel[5].style.paddingBottom = 5 + "%";

	document.getElementById("pTxt20").style.float = "left";
	document.getElementById("pTxt20").style.width = 9 + "%";
	document.getElementById("pTxt20").style.height = 5.8 + "%";
	document.getElementById("pTxt20").style.marginLeft = .5 + "%";
	document.getElementById("pTxt20").style.marginTop = .38 + "%";		
	document.getElementById("pTxt20").style.paddingTop = 5.8 + "%";
	document.getElementById("pTxt20").style.paddingLeft = 9 + "%";
	document.getElementById("pTxt20").style.paddingRight = 9 + "%";
	document.getElementById("pTxt20").style.paddingBottom = 5 + "%";					
	document.getElementById("pTxt20").style.color = "#000";
	document.getElementById("pTxt20").style.background = "#FFF";	
}

function pNewOrderView(){
   
   var pSel = document.getElementsByClassName("pSel");
   
   if (!pCP.snwh && !pCP.nug && !pCP.wng && !pCP.fry & !pCP.drk) {
  	 pCP.BodyMessage = "Nothing Ordered";
   	 p2ShowPopup();	   
   	 for(var i=0; i<7; i++){
   	   if (pSel[i].style.color == 'rgb(0, 136, 170)') { 
 	     
   	      if(i == 1 || i == 3 || i == 4){
             pCP.FootState = 15;     	      	
             pSetFooter();
             break;			 
		  }
		  else if(i == 0 || i == 2){
             pCP.FootState = 14;     	      	
             pSetFooter();
             break;			  
		  }
		  else{
             pCP.FootState = 11;     	      	
             pSetFooter();
             break;			  
		  }
		  
   	   } 
	 }   	 
   	 return;
   }
   else{
	  
     pCP.FootState = 20;     	      	
     pSetFooter();	   
   }
   	
   p2Clear();
   document.getElementById("pSelector1").style.display="none";   
   document.getElementById("pSelector23").innerHTML ="";   
   document.getElementById("pSelector23").style.display="block";
   pNewOrderViewHTML();
   
}

function testing(id){
   
   var pmBtn = document.getElementsByClassName("pmBtn");
   var strip1 = id;
   var strip2;
   var x, x1;
   var delCnt1 = 0;
   var ModCnt1 = 0;
   var fType = "";
      
   var delBtn = [];
   var modBtn = [];
   var Wngs = ["5W", "10W", "15W", "20W", "25W", "30W"];
   
   for(var i=0; i<pmBtn.length; i++){
	  if(pmBtn[i].innerHTML == "Delete"){
		 delBtn.push(pmBtn[i].id);
	  }
	  else{
		 modBtn.push(pmBtn[i].id); 
	  }
   }
   
   if((strip1.search("pmBtn")) !== -1){
	  strip2 = strip1.split("pmBtn");
	  if(document.getElementById(""+id).innerHTML == "Delete"){

		 for(var i=0; i<delBtn.length; i++){
			if(pmBtn[strip2[1]].id == delBtn[i]){
			   delCnt1 = i;
			   break;
			}
		 }
		 
		 fType = pCP.cmbT[delCnt1][0];
		 pCP.Mcnt2 = pCP.cmbT[delCnt1][1];
		 
		 switch(fType){
			case "s":
		      pDelSndWch();			  
			  break;
			case "n":
			  if(pCP.cmbT[delCnt1][2] == "5PC"){
				 pCP.Mcnt2 = 0; 
			  }
			  else{
				 pCP.Mcnt2 = 1;				  
			  }
			  pDelNuggest();
			  break;
			case "w":
			  
			  for(var i=0; i<Wngs.length; i++){
				 if(pCP.cmbT[delCnt1][2] == Wngs[i]){
					pCP.Mcnt2 = i;
					break;
				 }
			  }
			  
			  pDelWings();
			  break;    
		 }

		 pCP.Mcnt2 = delCnt1;		 
		 pDelCmbo();
		 pCP.hPrices.splice(pCP.Mcnt2, 1);
		 pNewOrderView();
		 DisplayPrice();
		 
	  }
	  else{
		 pCP.Mcnt2 = parseInt(strip2[1]) - 1;
		 pMstrModify();
		                                       
	  }
	 
   }
}

function pNewOrderViewHTML(){
	
	pSelector23 = document.getElementById("pSelector23");
	pCP.mbtn1 = 0;
	pCP.Receipt = "";
		
	var pNovAry1 = ["<div class='psOrder1'></div>",  //0
	                "<div class='psbList'></div>",   //1
	                "<div class='psub1'></div>",     //2
	                "<div class='psub2'></div>",     //3
	                "<div class='psub3'></div>",     //4
	                "<div class='psub4'></div>",     //5
	                "<div class='psub5'></div>",     //6
	                "<div class='psub6'></div>",     //7	                	                
	                "<div class='pmBtn' onclick='testing(this.id)'></div>",	     //8
	                "<div class='pmBtn' onclick='NoComboDel(this.id)'></div>"	 //9 	                 	         	         	          
	               ];
	               
	var tmpDrk = [[0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0], 
	              [0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0], 
	              [0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0], 
	              [0,0,0,0,0,0,0,0], [0,0,0,0,0,0,0,0], 
	              [0,0,0,0,0,0,0,0]
	             ]; 
    	                           

	var cBool = fbool = false;
	var var1 = var2 = var3 = var4 = 0;
	var pSlt;
	var rFrLeft = 0;
	var bFrLeft = 0;
	var rDrLeft = 0;
	var lDrLeft = 0;
	var Prc=0;
	var pElm = 0;
	var pElm2 = 0;
	var ptext, ptext2, ptext3, x, x1, x2, x3, xcnt;
	
// Preloading Drink data for later use   
    for(var i=0; i<pCP.dList1.length; i++){
	   if(pCP.dList1[i][1] > 0){
		  if(pElm == 0){
		     pElm = i;
	      }
	      else{	    
             pElm +=  "," + i;
		  }
		  tmpDrk[i][0] = pCP.dList1[i][1];
		  
		  var1 = 2;
		  for(var j=0; j<3; j++){
			 tmpDrk[i][var1] = pCP.dList1[i][3][j];
			 var1++; 
		  }  
       }
	   
	   if(pCP.dList1[i][2] > 0){
		  if(pElm2 == 0){
		     pElm2 = i;
	      }
	      else{	    
             pElm2 += "," + i;
		  }
		  tmpDrk[i][1] = pCP.dList1[i][2];
		  
		  var1 = 5;
		  for(var j=0; j<3; j++){
			 tmpDrk[i][var1] = pCP.dList1[i][4][j];
			 var1++; 
		  }		  		   
	   }
	}

//  Preloading Fry data for later use	
	//rFrLeft = pCP.NcmbF[0];
	//bFrLeft = pCP.NcmbF[1];
//	rFrLeft = pCP.fList1[0];
//	bFrLeft = pCP.fList1[1];


	        
	for(var i=0; i<pCP.cmbT.length; i++){
	   if(pCP.cmbT[i][0] !== ""){	   
		 
      // Adding pSOrder1 div to pSelector23 div  
	     document.getElementById("pSelector23").innerHTML += 
	                                                       pNovAry1[0];
	  
      // Adding psbList div to psOrder1 div	  
	     document.getElementsByClassName("psOrder1")[i].innerHTML += 
	                                                       pNovAry1[1];
	     
      // Adding psub1 div to psbList div	     
	     document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                       pNovAry1[2];
	     
	     if(pCP.cmbT[i][0] == "s"){                                                  
	       document.getElementsByClassName("psub1")[i].innerHTML = 
	                      "(" + pCP.cmbT[i][3] + ") " + 
	                                     //pCP.cmbT[i][2];
	                                    pCP.sList1[i][1];
	       
	       pCP.Receipt += "@" + "(" + pCP.cmbT[i][3] + ") " + 
	                                    pCP.sList1[pCP.cmbT[i][1]][1]; 	                                    
	     }
	     else{
	       document.getElementsByClassName("psub1")[i].innerHTML = 
	                      "(" + pCP.cmbT[i][3] + ") " + pCP.cmbT[i][2];
	       
	       pCP.Receipt += "@" +  "(" + pCP.cmbT[i][3] + ") " + pCP.cmbT[i][2];	                      
	     
	     }                               

      // Adding psub2 div to psbList div	     	     
	     document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                       pNovAry1[3];
	   
         switch(pCP.cmbT[i][0]){
		   case "s":  
            // Adding food items to sub2 div	     
	           for(j=2; j<pCP.sList1[pCP.cmbT[i][1]].length; j++){
			      if(pCP.sList1[pCP.cmbT[i][1]][j] !== ""){
			         if(j == 2){
			           document.getElementsByClassName("psub2")[i].innerHTML +=
			                             pCP.sList1[pCP.cmbT[i][1]][j];
			                             
                    pCP.Receipt += " " + pCP.sList1[pCP.cmbT[i][1]][j];				                             
			         }
			         else{
			           document.getElementsByClassName("psub2")[i].innerHTML +=
			                      ", " + pCP.sList1[pCP.cmbT[i][1]][j];
			                      
			           pCP.Receipt +=  ", " + pCP.sList1[pCP.cmbT[i][1]][j]; 			                      
			         }  
			      }
			      else{ break; }
		      }
		      break;
		   
		   case "n":
            // Adding sauce items to sub2 div		   
		      if(pCP.cmbT[i][2] == "5PC"){
			    document.getElementsByClassName("psub2")[i].innerHTML +=
		                                              pCP.nList1[0][1];
		                                              
		        pCP.Receipt += " " + pCP.nList1[0][1];		                                              
		      }
		      else{
			    document.getElementsByClassName("psub2")[i].innerHTML +=
		                                              pCP.nList1[1][1];
		                                              
			     pCP.Receipt += " " + pCP.nList1[1][1];		                                              				  
			  } 
			  break;
			  
		   default:
            // Adding sauce items to sub2 div		   
			  document.getElementsByClassName("psub2")[i].innerHTML +=
		                                              pCP.wList1[i][1];
		                                              
			  pCP.Receipt += " " + pCP.wList1[i][1];		                                              				  
		   	  break;     
         }     	     	     

      // Adding psub3 to psbList div for Fries		     
	     if(pCP.cmbT[i][4] > 0){
	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                      pNovAry1[4];
	                                                      
	        var1 = document.getElementsByClassName("psub3").length - 1;
	                	 
			document.getElementsByClassName("psub3")[var1].innerHTML = 
			"(" + pCP.cmbT[i][4] + ") REG FRY";
			
			 pCP.Receipt += "%" + "(" + pCP.cmbT[i][4] + ") REG FRY";			
			 rFrLeft = rFrLeft - pCP.cmbT[i][4];
			 fbool = true;			 
		 }
		 
	     if(pCP.cmbT[i][5] > 0){
	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                       pNovAry1[4];
	                                                       
	        var1 = document.getElementsByClassName("psub3").length - 1;	        
	        if(fbool){			 
			  document.getElementsByClassName("psub3")[var1].innerHTML += 
			  "(" + pCP.cmbT[i][5] + ") BK FRY";
			  
			  pCP.Receipt += "%" + "(" + pCP.cmbT[i][5] + ") BK FRY";		
			  bFrLeft = bFrLeft - pCP.cmbT[i][5];			  			 
			}
			else{
			  document.getElementsByClassName("psub3")[var1].innerHTML += 
			  "(" + pCP.cmbT[i][5] + ") BK FRY";

			  pCP.Receipt += "%" + "(" + pCP.cmbT[i][5] + ") BK FRY";			  
			  bFrLeft = bFrLeft - pCP.cmbT[i][5];			  				
			}
			fbool = false; 
		 }
		 
      // Adding psub3 to psbList div for Drinks		     
	     if(pCP.cmbT[i][6] > 0){
	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                      pNovAry1[4];
	                                                      
	        var1 = document.getElementsByClassName("psub3").length - 1;	        			 
			document.getElementsByClassName("psub3")[var1].innerHTML = 
			"(" + pCP.cmbT[i][6] + ") REG DRK(s)";
			
			  pCP.Receipt += "%" + "(" + pCP.cmbT[i][7] + ") REG DRK(s)";			

	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                      pNovAry1[7];
	                                                      
	        var1 = document.getElementsByClassName("psub6").length - 1;	        			 
			document.getElementsByClassName("psub6")[var1].innerHTML = 
			                                               pNovAry1[6];
			                                               
			var1 = document.getElementsByClassName("psub5").length - 1;

            ptext = "" + pCP.cmbT[i][8];
            cBool = false;
            
            if((ptext.search(",")) !== -1){
 		       x1 = ptext.split(",");
 		       cBool = true;
			}
			else{
			   x = ptext;
			}
			
			if(cBool){
              for(var j=0; j<x1.length; j++){
			     document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                     x1[j];
			        pCP.Receipt += "%      REG " + x1[j]; 
                 ptext2 = "" + pCP.cmbT[i][10];
                 ptext3 = x1[j];
                 
                 if((ptext2.search(",")) !== -1){
			        x2 = ptext2.split(",");
			     }
/*                 
                 if((ptext3.search(" Ex Ice")) !== -1){
			        tmpDrk[x2[j]][2] = tmpDrk[x2[j]][2] - 1;
			     }
                 else if((ptext3.search(" Lt Ice")) !== -1){
			        tmpDrk[x2[j]][3] = tmpDrk[x2[j]][3] - 1;
			     }
                 else if((ptext3.search(" No Ice")) !== -1){
			        tmpDrk[x2[j]][4] = tmpDrk[x2[j]][4] - 1;
			     }			     
*/			                                                     
			     if(j < (x1.length - 1)){
			       document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                     "<br>";
				 } 
			  }
			}
			else{
			  document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                         x;
			     pCP.Receipt += "%      REG " + x;
			     
			     ptext2 = "" + pCP.cmbT[i][10];
			     if((ptext2.search(",")) == -1){
					x3 = parseInt(ptext2);
				 }                                                    			                                                         
                 ptext3 = x;
/*                 
                 if((ptext3.search(" Ex Ice")) !== -1){
			        tmpDrk[x3][2] = tmpDrk[x3][2] - 1;
			     }
                 else if((ptext3.search(" Lt Ice")) !== -1){
			        tmpDrk[x3][3] = tmpDrk[x3][3] - 1;
			     }
                 else if((ptext3.search(" No Ice")) !== -1){
			        tmpDrk[x3][4] = tmpDrk[x3][4] - 1;
			     }
*/ 			     			
			}
/*			
         // Making the drinks math match							            
            ptext = "" + pCP.cmbT[i][10];
            cBool = false;
            
            if((ptext.search(",")) !== -1){
			   x1 = ptext.split(",");
			   xcnt = x1.length;
			   cBool = true;
			}
			else{
			   x = parseInt(ptext);
			}
            
            if(cBool){
			   for(var j=0; j<xcnt; j++){
			     //tmpDrk[x1[j]][0] = tmpDrk[x1[j]][0] - pCP.cmbT[i][6];
			     tmpDrk[x1[j]][0] = tmpDrk[x1[j]][0] - 1;
			   }				
			}
			else{
			   tmpDrk[x][0] = tmpDrk[x][0] - pCP.cmbT[i][6];
			}
			
			cbool = false;
			fbool = true;
*/ 			 
		 }
		 	 
	     if(pCP.cmbT[i][7] > 0){
	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                       pNovAry1[4];
	        
	        var1 = document.getElementsByClassName("psub3").length - 1;	        		 
			document.getElementsByClassName("psub3")[var1].innerHTML += 
			 "(" + pCP.cmbT[i][7] + ") LG DRK(s)";

			  pCP.Receipt += "%" + "(" + pCP.cmbT[i][7] + ") LG DRK(s)";

	        document.getElementsByClassName("psbList")[i].innerHTML += 
	                                                      pNovAry1[7];
	                                                      
	        var1 = document.getElementsByClassName("psub6").length - 1;	        			 
			document.getElementsByClassName("psub6")[var1].innerHTML = 
			                                               pNovAry1[6];
			                                               
			var1 = document.getElementsByClassName("psub5").length - 1;

            ptext = "" + pCP.cmbT[i][9];
            cBool = false;
            
            if((ptext.search(",")) !== -1){
 		       x1 = ptext.split(",");
 		       cBool = true;
			}
			else{
			   x = ptext;
			}
			
			if(cBool){
              for(var j=0; j<x1.length; j++){
			     document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                     x1[j];

                 pCP.Receipt += "%     LG " + x1[j];
                 ptext2 = "" + pCP.cmbT[i][11];
                 ptext3 = x1[j];
/*                 
                 if((ptext2.search(",")) !== -1){
			        x2 = ptext2.split(",");
			     }
                 
                 if((ptext3.search(" Ex Ice")) !== -1){
			        tmpDrk[x2[j]][5] = tmpDrk[x2[j]][5] - 1;
			     }
                 else if((ptext3.search(" Lt Ice")) !== -1){
			        tmpDrk[x2[j]][6] = tmpDrk[x2[j]][6] - 1;
			     }
                 else if((ptext3.search(" No Ice")) !== -1){
			        tmpDrk[x2[j]][7] = tmpDrk[x2[j]][7] - 1;
			     }			     
*/
			     if(j < (x1.length - 1)){
			       document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                     "<br>";
				 } 
			  }
			}
			else{
			  document.getElementsByClassName("psub5")[var1].innerHTML +=	                                              
			                                                         x;

  			     pCP.Receipt += "%     LG " + x;
			     ptext2 = "" + pCP.cmbT[i][11];
			     if((ptext2.search(",")) == -1){
					x3 = parseInt(ptext2);
				 }                                                    			                                                         
                 ptext3 = x;
/*                 
                 if((ptext3.search(" Ex Ice")) !== -1){
			        tmpDrk[x3][5] = tmpDrk[x3][5] - 1;
			     }
                 else if((ptext3.search(" Lt Ice")) !== -1){
			        tmpDrk[x3][6] = tmpDrk[x3][6] - 1;
			     }
                 else if((ptext3.search(" No Ice")) !== -1){
			        tmpDrk[x3][7] = tmpDrk[x3][7] - 1;
			     }
*/			      			     			
			}			                                                         				
			
         // Making the drinks math match							            
            ptext = "" + pCP.cmbT[i][11];
            cBool = false;
            
            if((ptext.search(",")) !== -1){
			    x1 = ptext.split(",");
			    xcnt = x1.length;
			    cBool = true;
			}
			else{
				x = parseInt(ptext);
			}
/*
            if(cBool){
			   for(var j=0; j<xcnt; j++){
			     tmpDrk[x1[j]][1] = tmpDrk[x1[j]][1] - pCP.cmbT[i][7];
			   }				
			}
			else{
			   tmpDrk[x][1] = tmpDrk[x][1] - pCP.cmbT[i][7];
			}
			
			cbool = false;
*/ 		 
		 }
              
		 fbool = false; 			 		 		 		    
	     

      // Adding psub4 div to psbList div for price	     
	     document.getElementsByClassName("psbList")[i].innerHTML += 
	     pNovAry1[5];
	     
	     var1 = document.getElementsByClassName("psub4").length - 1;
	     Prc = parseFloat(pCP.hPrices[i]).toFixed(2);
	    
	     document.getElementsByClassName("psub4")[var1].innerHTML +=
	                                                         "$" + Prc;
		  pCP.Receipt += "%items(s) total " + "$" + Prc;	                                                      
                                        
      // Adding pmBtn div to psbList div	     
	     document.getElementsByClassName("psOrder1")[i].innerHTML += 
	                                                        pNovAry1[8];
	     
	     var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	     document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                           "Delete";
	     pCP.mbtn1 = var1+1;                                                      
	     document.getElementsByClassName("psOrder1")[i].innerHTML += 
	                                                        pNovAry1[8];
	     
	     var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	     document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                           "Modify";
	     pCP.mbtn1 = var1+1;                                        

	   }  // End of if(pCP.cmbT[i][0] !== "")
	   else{ break; }  
	}
	
	/* //////////////////////////////////////////////
            No fries are associated with a combo.
            Show them anyway.
       ///////////////////////////////////////////// */
     	
	if(pCP.NcmbF[0] > 0){

    // Adding pSOrder1 div to pSelector23 div  
       document.getElementById("pSelector23").innerHTML += pNovAry1[0];
       var1 = document.getElementsByClassName("psOrder1").length - 1;	  
	    
    // Adding psbList div to psOrder1 div	  
	   document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                       pNovAry1[1];
       
       var2 = document.getElementsByClassName("psbList").length - 1;
	   document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                       pNovAry1[4];
	   
       var3 = document.getElementsByClassName("psub3").length - 1;
       	
	   document.getElementsByClassName("psub3")[var3].innerHTML = 
	   "(" + pCP.NcmbF[0] + ") REG FRY";

	    pCP.Receipt += "@" + "(" + pCP.NcmbF[0] + ") REG FRY";
	    
	   document.getElementsByClassName("psbList")[var2].innerHTML +=
	                                                       pNovAry1[5];
	                                                        	   
       var3 = document.getElementsByClassName("psub4").length - 1;
       //alert("psub4 value is: " + var3);
	   
	   if(pCP.NcmbF[0] > 1){
         document.getElementsByClassName("psub4")[var3].innerHTML =
                                        "$1.74 ea <br> Total $"+
                                                        1.74 * rFrLeft;
		   
		   pCP.Receipt += "%" + "$1.74 ea Total $" + 1.74 * rFrLeft;                                                        
	   }
	   else{                                                         	   
         document.getElementsByClassName("psub4")[var3].innerHTML =
                                                               "$1.74";                                        
		   pCP.Receipt += "%Item total     $1.74";
       }
       
       document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                       pNovAry1[9];
	     
	   var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	   document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                          "Delete";
	                                                           
	}
		
	if(pCP.NcmbF[1] > 0){
    // Adding pSOrder1 div to pSelector23 div  
       document.getElementById("pSelector23").innerHTML += pNovAry1[0];
       var1 = document.getElementsByClassName("psOrder1").length - 1;	  
	   
    // Adding psbList div to psOrder1 div	  
	   document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                        pNovAry1[1];
       
       var2 = document.getElementsByClassName("psbList").length - 1;
	   document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                        pNovAry1[4];
	   
       var3 = document.getElementsByClassName("psub3").length - 1;
       	
	   document.getElementsByClassName("psub3")[var3].innerHTML = 
	   "(" + pCP.NcmbF[1] + ") BK FRY";

	   pCP.Receipt += "@" + "(" + pCP.NcmbF[1] + ") BK FRY";
	   
	   document.getElementsByClassName("psbList")[var2].innerHTML +=
	                                                        pNovAry1[5];
	                                                        	   
       var3 = document.getElementsByClassName("psub4").length - 1;
	                                                            	   
	   if(pCP.NcmbF[1] > 1){
         document.getElementsByClassName("psub4")[var3].innerHTML =
                                        "$2.17 ea <br> Total $"+
                                                        2.17 * bFrLeft;
	      
	      pCP.Receipt += "%" + "$2.17 ea Total $" + 2.17 * bFrLeft;
	   }
	   else{                                                         	   
         document.getElementsByClassName("psub4")[var3].innerHTML =
                                                               "$2.17";                                        
	      pCP.Receipt += "%item total     $2.17";
       }
       
       document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                       pNovAry1[9];
	     
	   var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	   document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                           "Delete";	   
		
	}
	

	/* //////////////////////////////////////////////
            No drinks are associated with a combo.
            Show them anyway.
       ///////////////////////////////////////////// */
/*    
    var tmpHld = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0," +
                 "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0," +
                 "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0," +
                 "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0";
*/                 
    //if(tmpDrk !== tmpHld){

      // ================
      //  Reg drinks
      // ================		
		for(var i=0; i<pCP.NcmbT.length; i++){
		   if((pCP.NcmbT[i][1] == 1) && (pCP.NcmbT[i][3] > 0)){
			   
           // Adding pSOrder1 div to pSelector23 div  
             document.getElementById("pSelector23").innerHTML += 
                                                           pNovAry1[0];
                                                           
             var1 = document.getElementsByClassName("psOrder1").length - 1;	  
	    
           // Adding psbList div to psOrder1 div	  
	         document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                        pNovAry1[1];
       
             var2 = document.getElementsByClassName("psbList").length - 1;
	         document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                       pNovAry1[4];
	   
             var3 = document.getElementsByClassName("psub3").length - 1;
       	
	         document.getElementsByClassName("psub3")[var3].innerHTML = 
	         "(" + pCP.NcmbT[i][3] + ") REG " + pCP.dList1[i][0];

	         pCP.Receipt += "@(" + pCP.NcmbT[i][3] + ") REG " + pCP.dList1[i][0];
	         
	         document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                       pNovAry1[6];

             var4 = document.getElementsByClassName("psub5").length - 1;
             
             ptext = "" + pCP.NcmbT[i][5];
             cBool = false;
            
             if((ptext.search(",")) !== -1){
 		       x1 = ptext.split(",");
 		       cBool = true;
			 }
			 else{
			   x = ptext;
			 }

			if(cBool){
              for(var j=0; j<x1.length; j++){
			     document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                     x1[j];                 
	            pCP.Receipt += " " + x1[j];
	           
			     if(j < (x1.length - 1)){
			       document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                     "<br>";
				 } 
			  }
			}
			else{
			  document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                         x;
	        pCP.Receipt += " " + x;         
			}			                                                         				

	         document.getElementsByClassName("psbList")[var2].innerHTML +=
	                                                       pNovAry1[5];
	                                                        	   
             var3 = document.getElementsByClassName("psub4").length - 1;
       
	         if(pCP.NcmbT[i][3] > 1){
               document.getElementsByClassName("psub4")[var3].innerHTML =
                                        "$1.63 ea <br> Total $" +
                                                  1.63*pCP.NcmbT[i][3];
	            
	            pCP.Receipt += "%" + "$1.63 ea Total $" + 1.63*pCP.NcmbT[i][3];
	         }
	         else{                                                         	   
               document.getElementsByClassName("psub4")[var3].innerHTML =
                                                               "$1.63";                                        
	            pCP.Receipt += "%item total     $1.63";
             }
       
             document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                       pNovAry1[9];
	     
	         var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	         document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                          "Delete";
			  
		   }

        // ================
        //  LG drinks
        // ================
                
		   if((pCP.NcmbT[i][2] == 2) && (pCP.NcmbT[i][4] > 0)){
			   
           // Adding pSOrder1 div to pSelector23 div  
             document.getElementById("pSelector23").innerHTML += 
                                                           pNovAry1[0];
                                                           
             var1 = document.getElementsByClassName("psOrder1").length - 1;	  
	    
           // Adding psbList div to psOrder1 div	  
	         document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                        pNovAry1[1];
       
             var2 = document.getElementsByClassName("psbList").length - 1;
	         document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                       pNovAry1[4];
	   
             var3 = document.getElementsByClassName("psub3").length - 1;
       	
	         document.getElementsByClassName("psub3")[var3].innerHTML = 
	         "(" + pCP.NcmbT[i][4] + ") LG " + pCP.dList1[i][0];

	         pCP.Receipt += "@(" + pCP.NcmbT[i][4] + ") LG " + pCP.dList1[i][0];
	         
	         document.getElementsByClassName("psbList")[var2].innerHTML += 
	                                                       pNovAry1[6];

             var4 = document.getElementsByClassName("psub5").length - 1;

             //elm,  Rtype,   Ltype,   Rqty,   Lqty,  R_ice,  L_ice
             
             ptext = "" + pCP.NcmbT[i][6];
             cBool = false;
            
             if((ptext.search(",")) !== -1){
 		       x1 = ptext.split(",");
 		       cBool = true;
			 }
			 else{
			   x = ptext;
			 }

			if(cBool){
              for(var j=0; j<x1.length; j++){
			     document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                     x1[j];                 
	           pCP.Receipt += " " + x1[j];
	           
			     if(j < (x1.length - 1)){
			       document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                     "<br>";
				 } 
			  }
			}
			else{
			  document.getElementsByClassName("psub5")[var4].innerHTML +=	                                              
			                                                         x;
	        pCP.Receipt += " " + x;                 
			}			                                                         				
	         
	         document.getElementsByClassName("psbList")[var2].innerHTML +=
	                                                       pNovAry1[5];
	                                                        	   
             var3 = document.getElementsByClassName("psub4").length - 1;
      
 
	         if(pCP.NcmbT[i][4] > 1){
               document.getElementsByClassName("psub4")[var3].innerHTML =
                                        "$1.85 ea <br> Total $"+
                                                  1.85*pCP.NcmbT[i][4];
	            
	            pCP.Receipt += "%" + "$1.85 ea Total $"+ 1.85*pCP.NcmbT[i][4];
	         }
	         else{                                                         	   
               document.getElementsByClassName("psub4")[var3].innerHTML =
                                                               "$1.85";                                        
	            pCP.Receipt += "%item total     $1.85";
             }
       
             document.getElementsByClassName("psOrder1")[var1].innerHTML += 
	                                                       pNovAry1[9];
	     
	         var1 = document.getElementsByClassName("pmBtn").length - 1;	     
	         document.getElementsByClassName("pmBtn")[var1].innerHTML = 
	                                                          "Delete";
			  
		   }		   
		   
		}
	//}
	
      var pmBtn = document.getElementsByClassName("pmBtn");
      for(var i=0; i<pmBtn.length; i++){
		 pmBtn[i].id = "pmBtn" + i;
	  }
	  
	  if(pmBtn.length<1){
	    for(var i=0; i<pCP.cmbT.length; i++){
		   for(var j=0; j<12; j++){
		      pCP.cmbT[i][j] = "";
		   }   
	    }
	    
        RestartEverything();
        pCP.FootState = 14;     	      	
        pSetFooter();        
	  }  
}

function ClearPriceArys(){
    pCP.tPrices.length = 0;	
}

function pSetFooter() {
	var pFootSet = ["<button class='btn1' onclick='SelWin()'>Select</button>",             //   0
	                "<button class='btn1' onclick='pOrderIt()'>Order It</button>",         //   1
	                '<button class="btn1" onclick="pBack()">Back</button>',                //   2
                    '<button class="btn1" onclick="pMore()">More</button>',	               //   3           
	                '<button class="btn1" onclick="ShowExtra()">More</button>',            //   4	                
	                '<button class="btn1" onclick="ShowExtra()">Close</button>',           //   5 
                    "<button class='btn1' onclick='p2StoreKeyPadAns()'>OK</button>",       //   6
	                "<button class='btn1' onclick='pBack2()'>Back</button>",               //   7
	               // "<button class='btn1' onclick='pViewOrder()'>View Order</button>",    //  8
	                "<button class='btn1' onclick='pNewOrderView()'>View Order</button>",   //  8	                
	                "<button class='btn1' onclick='pMstrModify()'>Modify</button>",         //  9
	                "<button class='btn1' onclick='pPlaceOrder()'>Place Order</button>",   //  10
	                "<button class='btn1' onclick='pUpdate()'>Update</button>",            //  11
	                "<button class='btn1' onclick='pDelete()'>Del</button>",               //  12
                    "<button class='btn1' onclick='p2StoreMoreAns()'>OK</button>",          // 13
	                "<button class='btn1' onclick='p2OrderIt()'>Order It</button>",         // 14  
	                "<button class='btn1' onclick='p2Update()'>Update</button>",           //  15
       	            "<button class='btn1' onclick='p2Back()'>Back</button>",               //  16 
       	            "<button class='btn1' onclick='pCmbSetup()'>Combo</button>"    	       //  17                         	                	                	                	                                  	         
	               ];	
	               
   switch(pCP.FootState) {
  	  
  	  case 1:                                         // SelWin()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[0];  	  
  	     break;
  	     
  	  case 2:                                        // pViewOrder()     pMore()      pBack()
  	     document.getElementById("dBoxFoot").innerHTML = pFootSet[8] + pFootSet[3] + pFootSet[2];
  	     break;
  	     
  	  case 3:                                          // pBack()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[2];  	     
  	     break;
  	     
  	  case 4:                                          // OrderIt()      pBack()
  	     document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[2];  	  
  	     break;
  	     
  	  case 5:                                         // OrderIt()    ShowExtra()     pBack()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[4] + pFootSet[2];  	  
  	     break;
  	     
  	  case 6:                                          // OrderIt()   ShowExtra()     pBack()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[5] + pFootSet[2];  	  
  	     break;
  	     
  	  case 7:                                          // OrderIt()   ShowExtra()     pBack()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[4] + pFootSet[2];  	  
  	     break;
  	     
  	  case 8:                                      // StoreKeyPadAns()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[6];  	  
  	     break;
  	     
  	  case 9:                                         // pBack2()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[7];  	  
  	     break;  	       	       	       	     
  	  
  	  case 10:                                        // pUpdate()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11];  	    	  
  	     break;
  	     
  	  case 11:                                      // pPlaceOrder()  pMstrModify()   pDelete()      pBack2()
        //document.getElementById("dBoxFoot").innerHTML = pFootSet[10] + pFootSet[9] + pFootSet[12] + pFootSet[7];
        
                                                     // p2OrderIt()    pViewOrder()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[14] + pFootSet[8]  	  
  	     break;

  	  case 12:                                      //   pUpdate()     ShowExtra()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11] + pFootSet[4];  	  
  	     break;

  	  case 13:                                       //  pUpdate()     ShowExtra()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11] + pFootSet[5];  	  
  	     break;
  	     
  	  case 14:                                     // pNewViewOrder()  cmbCombo()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[8] + pFootSet[17];  	  
  	     break;   	
 
   	  case 15:                                       // p2OrderIt()     cmbCombo()   pViewOrder()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[14] + pFootSet[17] + pFootSet[8];  	  
  	     break;

   	  case 16:                                     // p2StoreMoreAns()
        document.getElementById("dBoxFoot").innerHTML = pFootSet[13];  	  
  	     break; 	     

   	  case 17:                                         // p2Update
        document.getElementById("dBoxFoot").innerHTML = pFootSet[15];  	  
  	     break;
  	     
      case 18:                                      // pPlaceOrder()  pMstrModify()   pDelete()      pBack2() 	     
        document.getElementById("dBoxFoot").innerHTML = pFootSet[10] + pFootSet[9] + pFootSet[12] + pFootSet[7];
        break; 
        
      case 19:                                         // p2Back() 	     
        document.getElementById("dBoxFoot").innerHTML = pFootSet[16];          	        	            	       	        
   	    break;
   	     
      case 20:                                      // pPlaceOrder()  pMstrModify()   pDelete()      pBack2() 	     
        //document.getElementById("dBoxFoot").innerHTML = pFootSet[10] + pFootSet[9] + pFootSet[12] + pFootSet[16];
        
                                                    // pPlaceOrder()     pBack2() 
        document.getElementById("dBoxFoot").innerHTML = pFootSet[10] + pFootSet[16]; 
        break;
         	  
      case 21:                                    // pNewOrderView()    pBack2() 	     
        document.getElementById("dBoxFoot").innerHTML = pFootSet[8] + pFootSet[16]; 
        break;
                 	  
  	  default: 
        document.getElementById("dBoxFoot").innerHTML = ""; 	    
  	    break;  	       	       	       	       	     
  }

}
 
/* When the window loads, call the setup function */
window.addEventListener("load", PhoneSetup2, false);