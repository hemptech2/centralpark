function TabSetup() {
	
// Defining a user's object   
   CPinfo = {
     winPk: "",	
     TxtNum: "",
     BurStr: "",
     WngStr: "",
     NugStr: "",
     FryStr: "",
     DrkStr: "",
     MultiOrder: "",
     TxtNum: "",
     TxtBxID: "",
     CarType: "",
     OrdCurCnt: 0,
     DisCnt: 0        
   };	
      
/* This function presets everything to a default state. */

/* Making all slide out nav menu's default to hiding. */	
	document.getElementById("menuToggle").checked = false;
   document.getElementById("menuToggle2").checked = false;
   document.getElementById("menuToggle3").checked = false;
   document.getElementById("menuToggle4").checked = false; 
   document.getElementById("menuToggle5").checked = false;
   document.getElementById("menuToggle6").checked = false; 
   document.getElementById("menuToggle7").checked = false;   
   
/* Global varialbes to be used by several functions */   
	divCol = [0, 10, 20, 30, 40, 50, 60, 70, 80, 81, 82, 83, 84, 85];
	foodIndex=0;
	 
/* Storing Dom Objects in variables */	 
   var TxtBx = document.getElementsByClassName("qtyN");
   var ClrDivData = document.getElementsByClassName("Ordercell");
   var cOption1 = document.getElementsByClassName("condments");
   var ctitleLoad = document.getElementsByClassName("titleLoad");   
   var dCont = document.getElementsByClassName("drinkContainer");
   
   var dTitles1 = "Drinks";
   //var dTitles2 = "Ice";
   var dSpaces = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + 
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
                 
/* Setting textboxes and page div inner values to default values */       
   for (var i=0; i<TxtBx.length; i++) {TxtBx[i].value = 0;}
   for (var i=0; i<ClrDivData.length; i++) {ClrDivData[i].innerHTML = "";}
   for (var i=5; i<cOption1.length; i++) { cOption1[i].checked = false; }   
   for (var i=0; i<ctitleLoad.length; i++) { ctitleLoad[i].innerHTML = dTitles1 + dSpaces; }
   for (var i=0; i<dCont.length; i++) { dCont[i].style.display = "none"; }
         
   document.getElementById("c81").innerHTML = "";
   document.getElementById("c82").innerHTML = "";
   document.getElementById("c83").innerHTML = "";
   document.getElementById("c84").innerHTML = "";
   document.getElementsByName("WSide")[0].checked = true;   
   
/* Creating and populating an 8x10 array. */   
   fList1 = new Array();
   for (var i=0; i<8; i++) {
     fList1[i]= new Array();
     for (var j=0; j<10; j++) {
     	 fList1[i][j]="";
     }	
   }  
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function TabClear() {
	
   CPinfo.winPk = ""; 
   CPinfo.BurStr = "";
   CPinfo.WngStr = "";
   CPinfo.NugStr = "";
   CPinfo.FryStr = "";
   CPinfo.DrkStr = "";
     
/* This function presets everything to a default state. */

/* Making all slide out nav menu's default to hiding. */	
	document.getElementById("menuToggle").checked = false;
   document.getElementById("menuToggle2").checked = false;
   document.getElementById("menuToggle3").checked = false;
   document.getElementById("menuToggle4").checked = false; 
   document.getElementById("menuToggle5").checked = false;
   document.getElementById("menuToggle6").checked = false; 
   document.getElementById("menuToggle7").checked = false;   
   
	foodIndex=0;
	 
/* Storing Dom Objects in variables */	 
   var TxtBx = document.getElementsByClassName("qtyN");
   var ClrDivData = document.getElementsByClassName("Ordercell");
   var cOption1 = document.getElementsByClassName("condments");
   var ctitleLoad = document.getElementsByClassName("titleLoad"); 
   var dCont = document.getElementsByClassName("drinkContainer");  

   var dTitles1 = "Drinks";
   //var dTitles2 = "Ice";
   var dSpaces = "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" + 
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
                 "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";
                 
/* Setting textboxes and page div inner values to default values */       
   for (var i=0; i<TxtBx.length; i++) {TxtBx[i].value = 0;}
   for (var i=0; i<ClrDivData.length; i++) {ClrDivData[i].innerHTML = "";}
   for (var i=5; i<cOption1.length; i++) { cOption1[i].checked = false; }   
   for (var i=0; i<ctitleLoad.length; i++) { ctitleLoad[i].innerHTML = dTitles1 + dSpaces; } 
   for (var i=0; i<dCont.length; i++) { dCont[i].style.display = "none"; }
        
   document.getElementById("c81").innerHTML = "";
   document.getElementById("c82").innerHTML = "";
   document.getElementById("c83").innerHTML = "";
   document.getElementById("c84").innerHTML = "";
  	
  	if (CPinfo.OrdCurCnt == 0) { 
  	  document.getElementsByName("WSide")[0].checked = true; 
  	}     
      
   
/* Creating and populating an 8x10 array. */   
   
   for (var i=0; i<8; i++) {   
     for (var j=0; j<10; j++) {
     	 fList1[i][j]="";
     }	
   }  	
}

function MenuStates(id) {
	switch(id) {
		
		case "menuToggle":
		  if (document.getElementById("menuToggle").checked == true) {
   		  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle3").checked = false;
   		  document.getElementById("menuToggle4").checked = false; 
   		  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = true;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break; 
		
		case "menuToggle2":
		  if (document.getElementById("menuToggle2").checked == true) {
   		  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle3").checked = false;
   		  document.getElementById("menuToggle4").checked = false; 
   		  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = true;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break;
		
		case "menuToggle3":
		  if (document.getElementById("menuToggle3").checked == true) {
   		  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle4").checked = false; 
   		  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = true;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle3").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break;
   	  		
		case "menuToggle4":
		  if (document.getElementById("menuToggle4").checked == true) {
   		  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle3").checked = false; 
   		  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = true;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle4").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break;   	     	     	  
		
		case "menuToggle5":
		  if (document.getElementById("menuToggle5").checked == true) {
   		  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle3").checked = false; 
   		  document.getElementById("menuToggle4").checked = false;
   		  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = false;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break;
   	  
		
		case "menuToggle6":
		  if (document.getElementById("menuToggle6").checked == true) {
   		  document.getElementById("menuToggle").checked = false;
   		  document.getElementById("menuToggle2").checked = false;
   		  document.getElementById("menuToggle3").checked = false; 
   		  document.getElementById("menuToggle4").checked = false;
   		  document.getElementById("menuToggle5").checked = false;
   		  document.getElementById("menuToggle7").checked = false;   		     		  		  	 
		  }
		  else {
		  	  document.getElementById("menuToggle6").checked = false;
   		  document.getElementById("menuToggle7").checked = false;
   	  }
   	  break;
   	     	     	  
   	  default:
   	    break;
   }	     		  	
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/
function navFunc(id) {
	

	//alert(NavData.length);
	
	var DivData = document.getElementsByClassName("Ordercell");
	var bTest = false;
	
	var NavData = document.getElementsByTagName("a");
   var TxtBx = document.getElementsByClassName("qtyN");	

/* Finding the correct nav item the user selected. Then storing
   what they select in the middle section on the page. Also storing
   that choice in the 2D array. for later use */
   	
	for (var i=0; i<NavData.length; i++) { 

	  if (NavData[i].id == id && NavData[i].title !=="") { 

       for (var x=0; x<8; x++) {
       
          if (DivData[divCol[x]].innerHTML == "") {	    

	          DivData[divCol[x]].innerHTML = NavData[i].title;	          
	          
	          fList1[divCol[x]/10][0]= NavData[i].title;

	          foodIndex = divCol[x]/10;
		       TxtBx[foodIndex].value = parseInt(TxtBx[foodIndex].value) + 1; 	          
	          bTest = true;
	          break;       
          }       
       }
       if (bTest) {
       	
       	var autoFoodIndex;
       	
         switch(id) {
         	
         	/* Chicken Burger */
           case "item2c":
              autoLoadFood(autoFoodIndex=7, NavData[i].title);
              break;
           
         	/* CLUB Burger */           
           case "item4c":
              autoLoadFood(autoFoodIndex=9, NavData[i].title);
              break;           

         	/* Grill Burger */           
           case "item6c":
              autoLoadFood(autoFoodIndex=7, NavData[i].title);
              break;           
           
           default:
             
         }
                	
       	break;
       }   
	      
	  }	   
	} //alert(NavData[i].id
	//if (!bTest) { alert("Maxium Sandwhich count met for this order.");}
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function navFood(id) {
/* This function takes what the user selects from the food nav
   and loads it in the middle section of the page. */
   	
	var breakLoop = false;
	var leaveFunc = false;
	var cntr1 = 1;
	var DivData = document.getElementsByClassName("Ordercell");	
	var NavData = document.getElementsByTagName("a");
	
/* Check to make sure a main item (sandwich) was selected
   before food items can be added. */ 
   if (fList1[foodIndex][0] =="") {
   	Alert.render("Error! You haven't selected a sandwich");
   	return;
   }
   
   if (id == "item1g") {
   	switch(fList1[foodIndex][0]) {
   	  case "HD":
   	    autoLoadFood(AutoFoodIndex=3, FoodTitle="HD");
   	    break;
   	    
   	  case "CK": 
   	    autoLoadFood(AutoFoodIndex=3, FoodTitle="CK");
   	    break;
   	    
   	  case "GR":   	  
   	    autoLoadFood(AutoFoodIndex=3, FoodTitle="GR");
   	    break;  
   	    
   	    
   	  case "CLUB":
   	    autoLoadFood(AutoFoodIndex=5, FoodTitle="CLUB");
   	    break; 
   	    
   	  default:   	  
   	    autoLoadFood(AutoFoodIndex=7, FoodTitle="B");
   	    break;   	      	     	       	   
      }
      
   }/* end if */
   
   else {  
   
  /* This part only loads the single item the user selected
     for the sandwich */    	
 	
	  for (var i=0; i<NavData.length; i++) { 
	    if (NavData[i].id == id ) {
	  	   for (var j=1; j<10; j++) {
	  	 	
	  	 	  if (fList1[foodIndex][j] == "") {
	  	 	    fList1[foodIndex][j] = NavData[i].innerHTML;
	  	 	    breakLoop = true
	  	 	    break;
	  	 	  
	  	 	  }  /* end if */ 
	  	   }    /* end for */	  	  	
	  	 
       }      /* end if */
       
       if (breakLoop == true) { break;}
     
     }  /* end for */
   }    /* end else */  

/* Showing the food items in center screen */   
   for (var i=divCol[foodIndex]+1; i<divCol[foodIndex] + 9; i++ ) {
   	DivData[i].innerHTML=fList1[foodIndex][cntr1];
   	cntr1++;
   }
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function NavNugWgs(id) {
	var DivData = document.getElementsByClassName("fTitles");	
	var NavData = document.getElementsByTagName("a");
   var TxtBx = document.getElementsByClassName("qtyN");	
	
	switch(id) {
		
		case "item1d":
		  TxtBx[12].value = parseInt(TxtBx[12].value) + 1;
         //alert(TxtBx[12].value);
		  break;
		  
		case "item2d":
		  TxtBx[13].value = parseInt(TxtBx[13].value) + 1;		
		  break;
		  
		case "item3d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break; 
 		  
		case "item4d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break;		   
 		  
		case "item5d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break;
 		    		  
		case "item6d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break;	   
 		  
		case "item7d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break;		    
 		  
		case "item8d":
		   for (var i = 0; i<5; i++) {
		     if (DivData[i].innerHTML == "") {
 		       DivData[i].innerHTML = document.getElementById(""+id).title;
 		       for (var j=8; j<12; j++) {
 		       	if (parseInt(TxtBx[j].value) == 0) {
		           TxtBx[j].value = parseInt(TxtBx[j].value) + 1;
		           break;
		         }
		       }     		       
 		       break;
 		    }  		    
 		  }
 		  break;
 		   
 		default:
 		   break;    		     		   		    		   		   		  
	}
}


/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function NavFriesDrink(id) {
	
/* This function loads the correct textbox with a
   numerical value whenever a fry or drink is
   selected from the nav menu. */
   	
   var DivData = document.getElementsByClassName("fTitles");
   var dCont = document.getElementsByClassName("drinkContainer"); 	
   var TxtBx = document.getElementsByClassName("qtyN");	
   var TxtBxAry1 = ["item1f",  "item2f",  "item3f",  "item4f",  "item5f",  "item6f",  
                    "item7f",  "item8f",  "item9f",  "item10f", "item11f", "item12f",
                    "item13f", "item14f", "item15f", "item16f", "item17f", "item18f"];
                    
   var TxtBxAry2 = ["d1",  "d2",  "d3",  "d4",  "d5",  "d6",
                    "d7",  "d8",  "d9",  "d10", "d11", "d12",
                    "d13", "d14", "d15", "d16", "d17", "d18"];
                                                          
   var TmpDrk;
	       
	switch(id) {
			
     //***** Reg Fry ***** 		
		case "item1e":
		  TxtBx[14].value = parseInt(TxtBx[14].value) + 1;
		  break;
	  
     //***** LG Fry ***** 		  
		case "item2e":
		  TxtBx[15].value = parseInt(TxtBx[15].value) + 1;		
		  break;
		
		default: 
			   
     //***** Drinks ***** 
        var x;	         
	    for (var i=0; i<18; i++ ) {
	    	if (id == TxtBxAry1[i]) {
			  x = Math.floor(i/2);
			  dCont[x].style.display = "block";    		 
	    	  TmpDrk = parseInt(document.getElementById("" + TxtBxAry2[i]).value);
	    	  TmpDrk++;
	    	  document.getElementById("" + TxtBxAry2[i]).value = TmpDrk;
	    	  break;
		   }		   
		 }
	}       

}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function autoLoadFood(AutoFoodIndex, FoodTitle){
	
	var DivData = document.getElementsByClassName("Ordercell");		
   var cntr1 = 1;
    
   switch(AutoFoodIndex) {
   	
   	case 3:
   	  if (FoodTitle == "HD") {
   	  	  fList1[foodIndex][1]="Chili";
   	  	  fList1[foodIndex][2]="Must";
   	  	  fList1[foodIndex][3]="Onion";
   	  }
   	  
 	  /* This is a Reg Chicken or Grill */
   	  else {
   	  	  fList1[foodIndex][1]="Tom";
   	  	  fList1[foodIndex][2]="Mayo";
   	  	  fList1[foodIndex][3]="Lett";   	  	
   	  }
   	  break;

 	  /* This is a CLUB Chicken */   	  
   	case 5:
   	  	  fList1[foodIndex][1]="Tom";
   	  	  fList1[foodIndex][2]="Mayo";
   	  	  fList1[foodIndex][3]="Lett";
   	  	  fList1[foodIndex][4]="Cheese";
   	  	  fList1[foodIndex][5]="Bacon";   	  	     	  	  
   	     break;

 	  /* This is a Reg Burgers or (Chicken/Grill) Burgers */     	     
   	case 7:
   	  	  fList1[foodIndex][1]="Tom";
   	  	  fList1[foodIndex][2]="Mayo";
   	  	  fList1[foodIndex][3]="Ket";   	  	  
   	  	  fList1[foodIndex][4]="Pickle";
   	  	  fList1[foodIndex][5]="Onion";
   	  	  fList1[foodIndex][6]="Lett";
   	  	  fList1[foodIndex][7]="Must"; 
           
           for (var i=divCol[foodIndex]+1; i<divCol[foodIndex] + 10; i++ ) {
   	       DivData[i].innerHTML=fList1[foodIndex][cntr1];
   	       cntr1++;
           }   	  	    	  	     	  	     	  	  
   	     break; 

 	  /* This is a CLUB Burger */     	       	
   	case 9:
   	  	  fList1[foodIndex][1]="Tom";
   	  	  fList1[foodIndex][2]="Mayo";
   	  	  fList1[foodIndex][3]="Ket";   	  	  
   	  	  fList1[foodIndex][4]="Pickle";
   	  	  fList1[foodIndex][5]="Onion";
   	  	  fList1[foodIndex][6]="Lett";
   	  	  fList1[foodIndex][7]="Must";
   	  	  fList1[foodIndex][8]="Bacon";
   	  	  fList1[foodIndex][9]="Cheese"; 
   	  	  
           for (var i=divCol[foodIndex]+1; i<divCol[foodIndex] + 10; i++ ) {
   	       DivData[i].innerHTML=fList1[foodIndex][cntr1];
   	       cntr1++;
           }  
           break;
              	  	    	  	     	  	     	
   	default:
   	
   }

}  

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function FirstItemsCenter(id){
// This function checks to see if the first item in 
// each row (8 in total) in the center screen has any 
// data. If so, show it cut in half or remove it.
 
  var DivData = document.getElementsByClassName("Ordercell");
  var x2 = 0;

  for (var i=0; i<DivData.length; i++) {
	 if (DivData[i].id == id) {
	 	
	 	                               // no data found 
		if (DivData[i].innerHTML == "") { return; }		
		else { 
		
  		  var text1= DivData[i].innerHTML;
		  var resp = text1.search("1/2 ");
		  		  
		  switch(resp) {
			 case -1:					 
				text1 = "1/2 " + text1;
				break;
				
			 default:
			   text1 = "";
			   break; 		   					 					 
	     } 					   
   						  		  		
	  // data found	 
	     x2 = i; 
	     break;
	   }   
	 }
  }
  
//divCol = [0, 10, 20, 30, 40, 50, 60, 70];    
  for (var i=0; i<8; i++) {
     if (x2 == divCol[i]) { 
     	 DivData[x2].innerHTML = text1;
     	 fList1[i][0] = text1;
     	 break; 
     }	    	
  }
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function ItemsCenterClick(id) {
	
// This function checks to see if any food items  
// in the center screen has any data. If so, add 
// "Et" or "Lt" to what's there. Or totally remove it.
	
	var DivData = document.getElementsByClassName("Ordercell");
   var x1 = 0;
   
// Making sure the div select isn't empty   
	for (var i=0; i<DivData.length; i++) {
		if (DivData[i].id == id) {
			if (DivData[i].innerHTML == "") { return; }
			
//       div has data. See if what's stored has "Ex" in it.
//       if it doesn't make it say "Ex"			
			else {
				var text1= DivData[i].innerHTML;
				var resp = text1.search("Ex");
				var resp2= text1.search("Lt");
				
				if (resp == -1 && resp2 == -1) {
					text1 = "Ex " + text1;
					x1= parseInt(i/10);
					   
					for (var j=0; j<10; j++) {
					  if(fList1[x1][j] == DivData[i].innerHTML){
					   	fList1[x1][j] = text1;
					   	foodIndex = x1;
					   	break;
					   }
					} 
				}
				
//         div has "Ex" in it. Make it say "Lt"				
				else if (resp !== -1 && resp2 == -1) {
					text1 = text1.replace("Ex", "Lt");
					x1= parseInt(i/10);
					   
					for (var j=0; j<10; j++) {
					  if(fList1[x1][j] == DivData[i].innerHTML){
					   	fList1[x1][j] = text1;
					   	foodIndex = x1;
					   	break;
					  }
					}			  	
				}
				
//         div has "Lt" in it. Clear it out				
				else {
					x1= parseInt(i/10);
					var cntr2 = 0;
					   
					var temp1= new Array(10);
					for (var j=0; j<10; j++) { temp1[j]=""; }
					   
					  for (var j=1; j<10; j++) {
					     if(fList1[x1][j] !== DivData[i].innerHTML){
					   	  temp1[cntr2] = fList1[x1][j];
					   	  cntr2++; 
					     }
					  }
					  foodIndex = x1;
					   
					  for (var j=1; j<10; j++) {
					    fList1[x1][j] = temp1[j-1]; 
					  }					
				}
			}
			
		}
		
	}		

   var cntr1=1;
// Showing the food items in center screen    
   for (var i=divCol[foodIndex]+1; i<divCol[foodIndex] + 9; i++ ) {
   	DivData[i].innerHTML=fList1[foodIndex][cntr1];
   	cntr1++;
   }
  	  
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function DrinkItemClick(id) {
	var DivData = document.getElementsByClassName("fTitles");
   
   for (var i=0; i<DivData.length; i++) {
 	  if (DivData[i].id == id) {
 	  	
  		  var text1= DivData[i].innerHTML;
		  var resp = text1.search("LG ");
	  
		  switch(resp) {
			 case -1:					 
				text1 = "LG " + text1;
				DivData[i].innerHTML = text1;
				break;
				
			 default:
			   text1 = text1.substring(3);
				DivData[i].innerHTML = text1;
			   break; 		   					 					 
	     } 					   
		 break;   	  	 	  	 
 	  } 	  
   }	
		
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function ExIceQty(id) {
  
  var ExIceAry1 =  ["arrow1",   "arrow7", "arrow13", "arrow19", "arrow25", "arrow31", "arrow37", "arrow43", 
                    "arrow49", "arrow55", "arrow61", "arrow67", "arrow73", "arrow79", "arrow85", "arrow91"];
                    
  var ExIceAry2 =  ["arrow2",  "arrow8",  "arrow14", "arrow20", "arrow26", "arrow32", "arrow38", "arrow44", 
                    "arrow50", "arrow56", "arrow62", "arrow68", "arrow74", "arrow80", "arrow86", "arrow92"];
                    
  var ExIceAry3 =  [  "a1",      "a4",       "a7",      "a10",     "a13",     "a16",     "a19",     "a22",    
                      "a25",     "a28",      "a31",     "a34",     "a37",     "a40",     "a43",     "a46"];
  var Extemp; 
  var ExIceTest = false;

  for (var i=0; i<16; i++) { if (id == ExIceAry1[i]) { ExIceTest = true; } }

  if (ExIceTest == true) {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry1[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;

	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;	    		  

	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  	    		    		  	    		  	    		  	    		  	    		  	    		  	    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for
  }       // end for(ExIceTest)
  
  else {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry2[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
									    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for  	
  }       // end else
}


function LtIceQty(id) {
  
 
  var ExIceAry1 =  ["arrow3",  "arrow9",  "arrow15", "arrow21", "arrow27", "arrow33", "arrow39", "arrow45", 
                    "arrow51", "arrow57", "arrow63", "arrow69", "arrow75", "arrow81", "arrow87", "arrow93"];
                    
  var ExIceAry2 =  ["arrow4",  "arrow10", "arrow16", "arrow22", "arrow28", "arrow34", "arrow40", "arrow46", 
                    "arrow52", "arrow58", "arrow64", "arrow70", "arrow76", "arrow82", "arrow88", "arrow94"];
                    
  var ExIceAry3 =  [  "a2",      "a5",      "a8",      "a11",     "a14",     "a17",      "a20",     "a23",
                     "a26",      "a29",    "a32",      "a35",     "a38",     "a41",      "a44",     "a47" ];
  var Extemp; 
  var ExIceTest = false; 
  
  for (var i=0; i<16; i++) { if (id == ExIceAry1[i]) { ExIceTest = true; } }
  
  if (ExIceTest == true) {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry1[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for
  }       // end for(ExIceTest)
  
  else {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry2[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for  	
  }       // end else
  
}

function NoIceQty(id) {

 
  var ExIceAry1 =  ["arrow5",  "arrow11", "arrow17", "arrow23", "arrow29", "arrow35", "arrow41", "arrow47", 
                    "arrow53", "arrow59", "arrow65", "arrow71", "arrow77", "arrow83", "arrow89", "arrow95"];
                    
  var ExIceAry2 =  ["arrow6",  "arrow12", "arrow18", "arrow24", "arrow30", "arrow36", "arrow42", "arrow48", 
                    "arrow54", "arrow60", "arrow66", "arrow72", "arrow78", "arrow84", "arrow90", "arrow96"];
                    
  var ExIceAry3 =  [  "a3",      "a6",      "a9",      "a12",     "a15",     "a18",     "a21",      "a24",    
                      "a27",     "a30",     "a33",     "a36",     "a39",     "a42",     "a45",      "a48"];
                      
  var Extemp; 
  var ExIceTest = false; 
  
  for (var i=0; i<16; i++) { if (id == ExIceAry1[i]) { ExIceTest = true; } }
  
  if (ExIceTest == true) {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry1[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "add");     		
	    		  break;
	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for
  }       // end for(ExIceTest)
  
  else {
	  for (var i=0; i<16; i++) { 
	    if (id == ExIceAry2[i]) {
	
	//IceModify(MyId, MyVal, operand1)    	
	    	switch(i) {
	    		case 0:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");  		
	    		   break;
	    		  
	    		case 1:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 2:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 3:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 4:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 5:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 6:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 7:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 8:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 9:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 10:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 11:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 12:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 13:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 14:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  
	    		case 15:
	            Extemp = document.getElementById("" + ExIceAry3[i]).value;            
	            IceModify(ExIceAry3[i], Extemp, "sub");     		
	    		  break;
	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  	    		  
	    		default:
	    		  break;      		      		      		    
	    	}
	    	break;
	    }  // end if(id)		  	
	  }    // end for  	
  }       // end else
}



/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function IceModify(MyId, MyVal, operand1) {
   
   if (operand1 == "add") { 
     MyVal++;
     document.getElementById("" + MyId).value = MyVal;
   }
   else {
     MyVal--;
     if (MyVal >= 0) { document.getElementById("" + MyId).value = MyVal; }     	
   }     	
}

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function PlaceOrder() {
	
/* This function will copy all the orders the user makes
   into temp variables and store them into a CP class object
   that will eventually send to the database. */
  
    
/***************************************************
 **************    Window Section    ***************   
 ***************************************************/   
   
   var WinPos = document.getElementsByName("WSide");
   
   if(WinPos[0].checked == true){
   	// Side A 
      CPinfo.winPk = document.getElementById("lbl1").innerHTML;
   }
   else { 
     // Side B
      CPinfo.winPk = document.getElementById("lbl2").innerHTML;   
   }    	
  
   
/***************************************************
 **************    Burger Section    ***************   
 ***************************************************/
    
   var TxtBx = document.getElementsByClassName("qtyN");
   var PlaceOrderAry1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      
	var BurgerString = "";	
   var BurgerCnt=0;
   var BurItemCnt=0;
   var TempCnt=0;   
   
/* Checking to see if the user has selected any sandwhiches
   if so, store the count. */   
   
   for (var i=0; i<8; i++) {
      if (fList1[i][0] !== "") { BurgerCnt++; }   	   	
   }
   
/* Checking to see if the there is any food items for the sandwhiches
   if so, store the count in the Place Order array. */      
   if (BurgerCnt !== 0) {
      for (var i=0; i<BurgerCnt; i++) {
      	for (var j=1; j<10; j++) {
      		if (fList1[i][j] !== "") { BurItemCnt++; }
      		else { break; }							 	
      	}
      	if (BurItemCnt !== 0) { 
      	  PlaceOrderAry1[TempCnt] = BurItemCnt;
      	  BurItemCnt=0; 
      	  TempCnt++;  
      	}      	
      }
 
/*    Storing all the sandwhich data in the variable
      to be transferred to the database. */  
      for (var i=0; i<BurgerCnt; i++) {        	
        for (var j=0; j<PlaceOrderAry1[i]+1; j++) {

        	  if (j == 0) { 
        	    BurgerString += "#" + TxtBx[i].value + " " + fList1[i][j] + " "; 
        	  }        	  	                     	  
        	  else if (j < PlaceOrderAry1[i]) { 
        	     BurgerString += fList1[i][j] + " "; 
        	  }
        	  else { BurgerString += fList1[i][j]; }              
        }         
      }  // end for(BurgerCnt)  
  	}     // end if(BurgerCnt)
  	
  	if (BurgerString !== "") { CPinfo.BurStr = BurgerString; }
    

/***************************************************
 *************      Wings Section     **************   
 ***************************************************/
 
   var WngDivData = document.getElementsByClassName("fTitles");
	var WingString = "";
   var WingCnt = 0;
	     	
  	for (var i=8; i<12; i++) {
      if (parseInt(TxtBx[i].value) !== 0) {
         WingString += "#" + TxtBx[i].value + " " + WngDivData[WingCnt].innerHTML;
         WingCnt++;	  		
  		}
  		else { break; }
  	}
  	if (WingString !== "") { CPinfo.WngStr = WingString; }

  	
/***************************************************
 ************      Nugget Section      *************   
 ***************************************************/
   
   var NuggetCnt=0;	
	var NuggetString = "";	
 	
	for (var i=12; i<14; i++) {
      if (parseInt(TxtBx[i].value) !== 0) {
      	switch(i) {
      		case 12:
         	    NuggetString += "#" + TxtBx[i].value + " 5pcN";
         	    break;         	
         	case 13:
         	    NuggetString += "#" + TxtBx[i].value + " BKN";
         	    break;         	
         	default:
         	    break;
         }	                 	           	  		
  		} 	
	}
  	if (NuggetString !== "") { CPinfo.NugStr = NuggetString; }

  	
/***************************************************
 *************       Fry Section       *************   
 ***************************************************/
   
	var FryString = "";

	for (var i=14; i<16; i++) {
      if (parseInt(TxtBx[i].value) !== 0) {
      	switch(i) {
      		case 14:
         	    FryString += "#" + TxtBx[i].value + " RegFries";
         	    break;         	
         	case 15:
         	    FryString += "#" + TxtBx[i].value + " BKFries";
         	    break;         	
         	default:
         	    break;
         }	                 	           	  		
  		} 	
	}
  	if (FryString !== "") { CPinfo.FryStr = FryString; }  

  	
/***************************************************
 ***********       Drink Section        ************   
 ***************************************************/
     
	var DrinkReg1 =  [16, 24, 32, 40, 48, 56, 64, 72, 80];
	var DrinkLG1 =   [17, 25, 33, 41, 49, 57, 65, 73, 81];
	var DrinkNames = ["Pepsi", "Diet", "Dew",   "DrPep", "FP",
	                  "Mist",  "Tea",  "Water", "Ice"];	

   var IceReg1 = new Array();
   var IceLG1 = new Array();   

	var DrinkString = "";
	var IceCnt = 1;

/* Creating and populating (2) 8x6 arrays. */   
   
   for (var i=0; i<8; i++) {
     IceReg1[i]= new Array();
     IceLG1[i] = new Array();
     
     for (var j=0; j<6; j++) {
     	 IceReg1[i][j]="";
     	 IceLG1[i][j]="";     	 
     }	
   }

// PreLoading Reg Drink array
   for (var i=0; i<8; i++) {
  	  IceReg1[i][0] = DrinkReg1[i] + 2;    	
  	  IceReg1[i][1] = DrinkReg1[i] + 3;
  	  IceReg1[i][2] = DrinkReg1[i] + 4;
  	  IceReg1[i][3] = "ExIce";
  	  IceReg1[i][4] = "LtIce";
  	  IceReg1[i][5] = "NoIce";  	    	    	    	    	  
   }

// PreLoading LG Drink array
   for (var i=0; i<8; i++) {
  	  IceLG1[i][0] = DrinkLG1[i] + 4;    	
  	  IceLG1[i][1] = DrinkLG1[i] + 5;
  	  IceLG1[i][2] = DrinkLG1[i] + 6;
  	  IceLG1[i][3] = "ExIce";
  	  IceLG1[i][4] = "LtIce";
  	  IceLG1[i][5] = "NoIce";  	    	    	    	    	  
   }   
   
/* Finding the status of all the reg drinks and
   their ice preference. */   
      		                         
	for (var i=0; i<9; i++) {		
      if (parseInt(TxtBx[DrinkReg1[i]].value) !== 0) {
      	
      	DrinkString += "#" + TxtBx[DrinkReg1[i]].value +
      	" Reg " + DrinkNames[i];

      	if (i<8) {      	      	      	
	         for (var j=0; j<3; j++) {
	           if (parseInt(TxtBx[IceReg1[i][j]].value) !== 0) {
	           	  DrinkString += " " + TxtBx[IceReg1[i][j]].value +
	           	  " " + IceReg1[i][j + 3];
	           }            
	         }  //End for(j)
	      }   	//End if(i<8)   	                 	           	  		
      }        //End if(parseInt)
	}           //End for(i) 

/* Finding the status of all the LG drinks and
   their ice preference. */	
	for (var i=0; i<9; i++) {		
      if (parseInt(TxtBx[DrinkLG1[i]].value) !== 0) {
      	
      	DrinkString += "#" + TxtBx[DrinkLG1[i]].value +
      	" LG " + DrinkNames[i];

      	if (i<8) {      	      	
	         for (var j=0; j<3; j++) {
	           if (parseInt(TxtBx[IceLG1[i][j]].value) !== 0) {
	           	  DrinkString += " " + TxtBx[IceLG1[i][j]].value +
	           	  " " + IceLG1[i][j + 3];
	           }            
	         }  //End for(j)
	      }   	//End if(i<8)                 	           	  		
      }        //End if(parseInt)
	}           //End for(i)
	 				
  	if (DrinkString !== "") { CPinfo.DrkStr = DrinkString; } 
  	
  	SendDB();
  	//alert("current cnt is: " + CPinfo.OrdCurCnt);
  	if (CPinfo.OrdCurCnt > 0) { TabClear(); }
  	else { TabSetup(); }      			
}
                     
/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function SendDB() {
	
   var CP_URL= "http://localhost:8500?";	
	var PassData1 = "";
	var Win1 = "";
	var Bur1= "";
	var Wng1 = "";
	var Nug1 = "";
	var Fry1 = "";
	var Drk1 = "";
	var SmeCar = "";
	var Phone = "";
	var MulOrd = "";
	var carType = "";
	var mDisCnt = ""; 
   
   	
	Win1 = CPinfo.winPk;
	Bur1 = CPinfo.BurStr;		
	Wng1 = CPinfo.WngStr;	
	Nug1 = CPinfo.NugStr;		
	Fry1 = CPinfo.FryStr;		
	Drk1 = CPinfo.DrkStr;	
	SmeCar = "NO";
	Phone = CPinfo.TxtNum;
	carType = CPinfo.CarType;
	MulOrd = CPinfo.MultiOrder;
	mDisCnt = CPinfo.DisCnt;
		
   if (Win1 == "") { PassData1 = "&none"; }
   else { PassData1 = Win1; }
   
   if (Bur1 == "") { PassData1 += "&none"; }
   else { PassData1 += "&" + Bur1; }
   
   if (Wng1 == "") { PassData1 += "&none"; }
   else { PassData1 += "&" + Wng1; }
   
   if (Nug1 == "") { PassData1 += "&none"; }
   else { PassData1 += "&" + Nug1; }      
   
   if (Fry1 == "") { PassData1 += "&none"; }
   else { PassData1 += "&" + Fry1; }
   
   if (Drk1 == "") { PassData1 += "&none"; }
   else { PassData1 += "&" + Drk1; }      

   if (Phone == "") { Phone = "none"; }

   if (carType == "") { carType = "Window"; }
   
   if (MulOrd == "") { MulOrd = 0; }
   
   if (mDisCnt == "") { mDisCnt = 0; }          
       
   if (PassData1 === 'A Side&none&none&none&none&none' || 
       PassData1 === 'B Side&none&none&none&none&none' ) { 
         Alert.render("You haven't selected anything");          
   }
   else {

     if (CPinfo.MultiOrder <= 1) { SmeCar = "&NO"; }
     else {
       SmeCar = "&YES";
       if (CPinfo.OrdCurCnt == 0) {
         CPinfo.OrdCurCnt = CPinfo.MultiOrder;
         //alert("MultiOrder: " + CPinfo.MultiOrder + " and current cnt is: " + CPinfo.OrdCurCnt);       
     	   CPinfo.DisCnt = (CPinfo.MultiOrder - CPinfo.OrdCurCnt) + 1;
     	   mDisCnt = CPinfo.DisCnt;
     	   //alert("Display cnt is: " + CPinfo.DisCnt);
     	   CPinfo.OrdCurCnt--;	        
       }
       else {
     	   CPinfo.DisCnt = (CPinfo.MultiOrder - CPinfo.OrdCurCnt)+1;
     	   mDisCnt = CPinfo.DisCnt;     	   
         //alert("MultiOrder: " + CPinfo.MultiOrder + " and current cnt is: " + CPinfo.OrdCurCnt);     	 
     	   //alert("Display cnt is: " + CPinfo.DisCnt);     	 
     	   CPinfo.OrdCurCnt--;     	  
       }  
     }    	 
     
     PassData1 += SmeCar + "&" + Phone + "&" + carType + "&" + MulOrd + "&" + mDisCnt; 
     PassData1 = "1&" + PassData1;
     
       var xmlhttp= new XMLHttpRequest();
       xmlhttp.open("POST", CP_URL, true);
       xmlhttp.send(PassData1);   
   }   
}
   
//  var wURL= "http://localhost:8150?";  

/* ////////// Spacer ////////// */
/* /////////////////////////////*/

function ShowKeypad1(id) {

	cpDiaInfo.BoxID = id;
	MultiOrderDialogRender(1);
	
}
	
/* When the window loads, call the setup function */
window.addEventListener("load", TabSetup, false);