   // Defining a user's object   
   cpDiaInfo = { HTMLHold: "",
                 BoxID: "",
                 ChoiceSelect: ""
               };
   
   function SelectorAlert() {
   	this.startup = function () {
			var winW = window.innerWidth;
			var winH = window.innerHeight;
			
/*=========== Setting up the HTML and CSS for the dialog popup ===========*/			

         var test1= document.getElementById('dialogOverlay');
         if (test1 == null) {
				var NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogOverlay";
	         NewDiv.style.cssText = "display: none; opacity: .8; position: fixed; top: 0px;" +
	                                "left: 0px; width: 100%; background: #FFF; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);                       
	
	         NewDiv = document.createElement("Div");	
	         NewDiv.id = "dialogBox";
	         NewDiv.style.cssText = "display: none; position: fixed; width: 550px;" +
	                                "background: #000; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);
	         	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "diaHold";  
	         NewDiv.style.cssText = "background: #FFF; margin:8px;";	         
	         document.getElementById("dialogBox").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxHead";
//	         NewDiv.style.cssText = "background: #666; font-size:19pt; padding:10px; color:#CCC;";	         
	         NewDiv.style.cssText = "background: #08A; font-size:19pt; padding:10px; color:#CCC;";
	         document.getElementById("diaHold").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxBody";                                
	         NewDiv.style.cssText = "background: #333; padding:20px; color:#FFF;";
	         
	         
	         document.getElementById("diaHold").appendChild(NewDiv);	         
	         
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxFoot";                                
//	         NewDiv.style.cssText = "background: #666; font-size:10px; text-align: right;";	         
	         NewDiv.style.cssText = "background: #08A; font-size:10px; text-align: right;";
	         document.getElementById("diaHold").appendChild(NewDiv);                    
         }
			var dialogoverlay = document.getElementById('dialogOverlay');
			var dialogbox = document.getElementById('dialogBox');			
			
			dialogoverlay.style.display = "block";
			dialogoverlay.style.height = winH + "px";
			dialogbox.style.left = (winW/2) - (550/2) + "px";
			dialogbox.style.top = winH * .25 + "px";
			dialogbox.style.display = "block";			

/*=========== End of the HTML/CSS the dialog setup ===========*/
       			
// The rest of the function will set up internal buttons(div's) for the user to interact with       			
			
			document.getElementById('dialogBoxHead').innerHTML = "Selection Screen";
			document.getElementById('dialogBoxBody').innerHTML = '<div class="Select1" onclick="CarDialogRender()">Vehicle</div>' +
			                                                     '<div class="Select1" onclick="MultiOrderDialogRender(2)">Multiple Orders</div>' + 
			                                                     '<div class="Select1" onclick="TxtDialogRender()">Receipt</div>';
			                                                     
			document.getElementById('dialogBoxHead').innerHTML = "Selection Screen"; 
			
         var HTML1 = document.getElementsByClassName('Select1'); 
	      
		   HTML1[0].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
		                            "background: #000; margin-left: 5%; margin-right: 12px;" +
		                            "margin-bottom: 10px; margin-top: 4px; padding-top: 3px;" +
		                            "border-radius: 4px; border: 3px solid #555; text-align:" +
		                            "center; font-weight: bold; cursor: pointer";				                                                      
			                                                       		
		   HTML1[1].style.cssText = "display: inline-block; width: 147px; height: 25px;" +
		                            "background: #000; margin-left: 9%; margin-right: 12px;" +
		                            "margin-bottom: 10px; margin-top: 4px; padding-top: 3px;" +
		                            "border-radius: 4px; border: 3px solid #555; text-align:" +
		                            "center; font-weight: bold; cursor: pointer";
		                            
		   HTML1[2].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
		                            "background: #000; margin-left: 9%; margin-right: 12px;" +
		                            "margin-bottom: 10px; margin-top: 4px; padding-top: 3px;" +
		                            "border-radius: 4px; border: 3px solid #555; text-align:" +
		                            "center; font-weight: bold; cursor: pointer";					                            					                       			                                                    			                                                           
     
    }      
   }
   Selection = new SelectorAlert();    
// ************************************************************   

	function CustomAlert() {
		this.render = function (dialog) {

			var winW = window.innerWidth;
			var winH = window.innerHeight;
         var test1= document.getElementById('dialogOverlay');
         if (test1 == null) {
				var NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogOverlay";
	         NewDiv.style.cssText = "display: none; opacity: .8; position: fixed; top: 0px;" +
	                                "left: 0px; width: 100%; background: #FFF; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);                       
	
	         NewDiv = document.createElement("Div");	
	         NewDiv.id = "dialogBox";
	         NewDiv.style.cssText = "display: none; position: fixed; width: 550px;" +
	                                "background: #000; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);
	         	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "diaHold";  
	         NewDiv.style.cssText = "background: #FFF; margin:8px;";	         
	         document.getElementById("dialogBox").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxHead";         
	         NewDiv.style.cssText = "background: #08A; font-size:19pt; padding:10px; color:#CCC;";
	         document.getElementById("diaHold").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxBody";                                
	         NewDiv.style.cssText = "background: #333; padding:20px; color:#FFF;";
	         
	         
	         document.getElementById("diaHold").appendChild(NewDiv);	         
	         
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxFoot";                                         
	         NewDiv.style.cssText = "background: #08A; font-size:10px; text-align: right;";
	         document.getElementById("diaHold").appendChild(NewDiv);                    
         }
         			
			var dialogoverlay = document.getElementById('dialogOverlay');
			var dialogbox = document.getElementById('dialogBox');
			
			dialogoverlay.style.display = "block";
			dialogoverlay.style.height = winH + "px";
			dialogbox.style.left = (winW/2) - (550/2) + "px";
			dialogbox.style.top = winH * .25 + "px";
			dialogbox.style.display = "block";			
                   				
         document.getElementById('dialogBoxBody').innerHTML = dialog;                
         document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.lastok()">OK</button>';                        																
		}
		
		this.ok = function (MySelect, MyMessage) {
			
         var testLngth = MyMessage.length;         
         var testLngth2 = MyMessage.search("#");
         
         if ((MyMessage == "") && (MySelect == "Vehicle") || (testLngth < 7)){ 

            cpDiaInfo.HTMLHold = document.getElementById('dialogBoxBody').innerHTML;
            document.getElementById('dialogBoxBody').innerHTML = "Vehicle selection not completely set";
            document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.back(\'Vehicle\')">Back</button>';            
            
         }
         else {
           if (MySelect == "Vehicle") {
         	//store data in user Object for later use
              document.getElementById('dialogBoxBody').innerHTML = "The Vehicle description is " + MyMessage;
              CPinfo.CarType = MyMessage;              
	           document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.lastok()">OK</button>';                       	  
           }
           else {
           	  if (testLngth2 !== -1) {
      
		           cpDiaInfo.HTMLHold = document.getElementById('dialogBoxBody').innerHTML;
		           document.getElementById('dialogBoxBody').innerHTML = "You didn't enter a number to text";
		           document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.back(\'Receipt\')">Back</button>';            
           	  	  
         	     MyMessage = "";           	  	  
           	  }   	  	
           	  else{	
         	  //send to server to text receipt
         	    //alert("Texting your receipt to: " + MyMessage);
                var Display1 = document.getElementById('txtDisplay').innerHTML;         	    
                document.getElementById('dialogBoxBody').innerHTML = "Texting your receipt to: " + Display1;                                                                     
                CPinfo.TxtNum = MyMessage;                      
	             document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.lastok()">OK</button>';                    	    

         	  }  
           }                                      				
		   }
		}
		
		this.back = function (backSelect) {
			document.getElementById('dialogBoxBody').innerHTML = cpDiaInfo.HTMLHold;
			if (backSelect == "Vehicle") {
			   document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Getinfo(\'Vehicle\')">Vehicle Select</button>';
			}
			else {
			   document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Getinfo(\'Receipt\')">Text Receipt</button>';
			}    
		}
		
		this.lastok = function () {
         document.getElementById('dialogBox').style.display = "none";
         document.getElementById('dialogOverlay').style.display = "none";         
         document.getElementById('dialogBoxFoot').innerHTML = "";			
		}   
	}	
	Alert = new CustomAlert();
// ************************************************************	

	
	function CarSelect1(id) {
		var CarID1 = document.getElementsByClassName('carData');		
				
//       Clearing all color		
	   for (var i=0; i<9; i++) {
         	      
		   CarID1[i].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
		                             "background: #000; margin-right: 12px; margin-bottom: 10px;" +
		                             "margin-top: 4px; padding-top: 3px; border-radius: 4px;" +
		                             "border: 3px solid #555; text-align: center; font-weight: bold;" +
		                             "cursor: pointer";		   	
	   }  				
				
		for (var i=0; i<9; i++) {
						   		
         if (CarID1[i].id == id ) {
         	//alert(i);				          		
         		CarID1[i].style.background = "#08A";
         		CarID1[i].style.border= "3px solid #FFF"; 
         		CarID1[i].style.color= "#000";
         		break;         		         		
        	}
      }     		
			
	}

	function CarSelect2(id) {
		var CarID2 = document.getElementsByClassName('carData2');		
				
//       Clearing all color		
	   for (var i=0; i<5; i++) {
         CarID2[i].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
	                                "background: #000; margin-right: 12px; margin-bottom: 10px;" +
	                                "margin-top: 4px; padding-top: 3px; border-radius: 4px;" +
	                                "border: 3px solid #555; text-align: center; font-weight: bold;" +
	                                "cursor: pointer";		                                    
	   }  				      
         		
		for (var i=0; i<5; i++) {
						   		
         if (CarID2[i].id == id ) {
         	//alert(i);				          		
      		CarID2[i].style.background = "#08A";
      		CarID2[i].style.border= "3px solid #FFF"; 
      		CarID2[i].style.color= "#000";
      		break;         		         		
        	}
      }				
	}
	
// ************************************************************	
	function deletePost(id) {
		alert(id);
		if (id == "post1") { document.body.removeChild(document.getElementById("post_1")); }
		else { document.body.removeChild(document.getElementById("post_2")); }
	}

			
	function CustomConfirm() {
		this.render = function (dialog,op,id) {
			var winW = window.innerWidth;
			var winH = window.innerHeight;
			var dialogOverlay = document.getElementById('dialogOverlay');
			var dialogBox = document.getElementById('dialogBox');
			
			dialogOverlay.style.display = "block";
			dialogOverlay.style.height = winH + "px";
			dialogBox.style.left = (winW/2) - (550/2) + "px";
			dialogBox.style.top = winH * .35 + "px";
			dialogBox.style.display = "block";
			
         document.getElementById('dialogBoxHead').innerHTML = "Confirm This Action";
         document.getElementById('dialogBoxBody').innerHTML = dialog;
         document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Confirm.yes(\''+op+'\',\''+id+'\')">Yes</button> <button onclick="Confirm.no()">No</button>';                                                                                         																
		}
		
		this.no = function () {
         document.getElementById('dialogBox').style.display = "none";
         document.getElementById('dialogOverlay').style.display = "none";            				
		}
      
      this.yes = function (op,id) {
      	if (op == "delete_post") {        	
      		deletePost(id);
      	} 
         document.getElementById('dialogBox').style.display = "none";
         document.getElementById('dialogOverlay').style.display = "none";         	            
      }			
	}	
	Confirm = new CustomConfirm();
		
// *************************************************************	

   function changeText(val) {
   	document.getElementById('status').innerHTML = val;
   }

   function CustomPrompt() {
		this.render = function (dialog,func) {
			var winW = window.innerWidth;
			var winH = window.innerHeight;
			var dialogoverlay = document.getElementById('dialogOverlay');
			var dialogbox = document.getElementById('dialogBox');
			
			dialogoverlay.style.display = "block";
			dialogoverlay.style.height = winH + "px";
			dialogbox.style.left = (winW/2) - (550/2) + "px";
			dialogbox.style.top = winH * .35 + "px";
			dialogbox.style.display = "block";
			
         document.getElementById('dialogBoxHead').innerHTML = "Acknowledge This Message";
         document.getElementById('dialogBoxBody').innerHTML = dialog;
         document.getElementById('dialogBoxBody').innerHTML += '<br><input id="Prompt_value1">';         
         document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Prompt.ok(\''+func+'\')">OK</button> <button onclick="Prompt.cancel()">Cancel</button>';                        																
		}
		
		this.cancel = function () {
         document.getElementById('dialogBox').style.display = "none";
         document.getElementById('dialogOverlay').style.display = "none";            				
		}
   	
		this.ok = function (func) {
			var prompt_value1= document.getElementById('prompt_value1').value;
			alert(prompt_value1);
			changeText(prompt_value1);
         document.getElementById('dialogBox').style.display = "none";
         document.getElementById('dialogOverlay').style.display = "none";            				
		}   	
   }		
	Prompt = new CustomPrompt();
	
// *************************************************************	
	
	function TxtDialogRender() {
			var htmlLoad2 = ['<div id="txtTitle">Enter Number', 
			                 '<div id="txtDisplay">(###)###-####</div>',
			                 '</div>',
			                 '<div id="keypad">',
			                 '<div class="txtData" id="txt1" onclick="diaNumKey(this.id)">1</div>',
			                 '<div class="txtData" id="txt2" onclick="diaNumKey(this.id)">2</div>',
			                 '<div class="txtData" id="txt3" onclick="diaNumKey(this.id)">3</div>',
			                 '<div class="txtData" id="txt4" onclick="diaNumKey(this.id)">4</div>',
			                 '<div class="txtData" id="txt5" onclick="diaNumKey(this.id)">5</div>',
			                 '<div class="txtData" id="txt6" onclick="diaNumKey(this.id)">6</div>',
			                 '<div class="txtData" id="txt7" onclick="diaNumKey(this.id)">7</div>',
			                 '<div class="txtData" id="txt8" onclick="diaNumKey(this.id)">8</div>',
			                 '<div class="txtData" id="txt9" onclick="diaNumKey(this.id)">9</div>',
			                 '<div class="txtData" id="txt10" onclick="diaNumKey(this.id)">BS</div>',
			                 '<div class="txtData" id="txt11" onclick="diaNumKey(this.id)">0</div>',
			                 '<div class="txtData" id="txt12" onclick="diaNumKey(this.id)">Clr</div>',			                 			                 
			                 '</div>'];			                 
			                
         document.getElementById('dialogBoxHead').innerHTML = "Text Receipt";
         
         for (var i=0; i<16; i++) {
           if (i == 0) { document.getElementById('dialogBoxBody').innerHTML = htmlLoad2[i]; }  
           else { document.getElementById('dialogBoxBody').innerHTML += htmlLoad2[i]; }
         } 

         var HTML2 = document.getElementById('txtTitle');
         HTML2.style.cssText = "font-weight: bold; padding-left: 36.6%"; 
         
         var HTML2 = document.getElementById('txtDisplay');
         HTML2.style.cssText = "display: block; margin-left: 30%; width: 215px; height: 35px;" +
                               "background: #999; font-size: 16pt; font-weight: bold;" +
                               "text-align: center; padding-top: 2px"; 
                               
                                                  
         HTML2 = document.getElementById('keypad');                      
	      HTML2.style.cssText = "display: block; width: 260px; background: #999; margin-top:15px;" +
	                            "margin-left: 23%";

         HTML2 = document.getElementsByClassName('txtData');	                            
	      for (var i=0; i<12; i++) {
	         HTML2[i].style.cssText = "display: inline-block; width: 60px; height: 60px;" +
	                                  "background: #000; margin-left: 11px; margin-right: 9px;" +
	                                  "margin-bottom: 10px; margin-top: 4px; font-size: 20pt;" +
	                                  "font-weight: bold; text-align: center; padding-top: 3px;" +
	                                  "border-radius: 4px; border: 3px solid #555; cursor: pointer";
	                                  
            document.getElementById('keypad').appendChild(HTML2[i]);            	                                  	      	
	      }                      
         
         //document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.ok(\''+passMess+'\')">Text Receipt</button>';
         document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Getinfo(\'Receipt\')">Text Receipt</button>'; 	
   }
   
// *************************************************************	   
   
   function Getinfo(diaSelect) {
   	var passMess="";
   	var mm;
   	
   	if (diaSelect == "Receipt") {
   	  passMess = document.getElementById('txtDisplay').innerHTML;

/* === Getting the data from the phone number display box and stripping away
       unwanted characters to leave just the numbers. */     
        var TmpDis = passMess.split("(");
        var TmpDis2 = TmpDis[1];
        var TmpDis3 = TmpDis2.split(")");
        var TmpDis4 = TmpDis3[1]
        var TmpDis5 = TmpDis4.split("-");
        var TmpDis6 = "" + TmpDis3[0] + "" + TmpDis5[0] + "" + TmpDis5[1];
        
        passMess = TmpDis6;
           	  
   	}
   	else {
   		var CarID1 = document.getElementsByClassName('carData');
   		
   		for (var i=0; i<9; i++) {
   			mm = CarID1[i].style.border;

            if (mm == "3px solid rgb(255, 255, 255)") {
           	  //alert("found one");   			
   		      passMess = CarID1[i].innerHTML; break;
   		   }
   		}
   		
   		CarID1 = document.getElementsByClassName('carData2');
   		for (var i=0; i<5; i++) {
   			mm = CarID1[i].style.border;
           if (mm == "3px solid rgb(255, 255, 255)") {   			
   		     passMess += " " + CarID1[i].innerHTML;
   		     break;
   		  }   			
   		}
      }    	  
  		        
      Alert.ok(diaSelect, passMess);   	
   }	
   
// *************************************************************
   
   function CarDialogRender() {
   	   
			var htmlLoad1 = ['<div class="dialogDivide" style="width: 500px; margin-bottom: 15px; padding-left: 4px; border-bottom: 5px solid #000; font-weight: bold;">Vehicle Color</div>',
			                 '<div class="carData" id="cd1" onclick="CarSelect1(this.id)">Black</div> ', 
			                 '<div class="carData" id="cd2" onclick="CarSelect1(this.id)">Blue</div> ',
			                 '<div class="carData" id="cd3" onclick="CarSelect1(this.id)">Green</div> ', 
			                 '<div class="carData" id="cd4" onclick="CarSelect1(this.id)">Grey</div> ',
			                 '<div class="carData" id="cd5" onclick="CarSelect1(this.id)">Maroon</div> ', 
			                 '<div class="carData" id="cd6" onclick="CarSelect1(this.id)">Red</div> ', 
			                 '<div class="carData" id="cd7" onclick="CarSelect1(this.id)">Tan</div> ', 
			                 '<div class="carData" id="cd8" onclick="CarSelect1(this.id)">Yellow</div> ', 
			                 '<div class="carData" id="cd9" onclick="CarSelect1(this.id)">White</div> ',
			                 '<div class="dialogDivide" style="width: 500px; margin-bottom: 15px; padding-left: 4px; border-bottom: 5px solid #000; font-weight: bold;">Vehicle Type</div>',
			                 '<div class="carData2" id="cd10" onclick="CarSelect2(this.id)">Bike</div> ',
			                 '<div class="carData2" id="cd11" onclick="CarSelect2(this.id)">Car</div> ',
			                 '<div class="carData2" id="cd12" onclick="CarSelect2(this.id)">SUV</div> ',
			                 '<div class="carData2" id="cd13" onclick="CarSelect2(this.id)">Truck</div> ',
			                 '<div class="carData2" id="cd14" onclick="CarSelect2(this.id)">Van</div>'];
			                 
         document.getElementById('dialogBoxHead').innerHTML = "Select a Vehicle info";			                 
			                 
         for (var i=0; i<16; i++) {
           if (i == 0) { 
             document.getElementById('dialogBoxBody').innerHTML = htmlLoad1[i]; 
           }  
           else if (i == 10) { 
             document.getElementById('dialogBoxBody').innerHTML += htmlLoad1[i]; 
           }  
           else { document.getElementById('dialogBoxBody').innerHTML += htmlLoad1[i]; }
         }
         
         var HTML1 = document.getElementsByClassName('carData');         
         for (var i=0; i<9; i++) {
         	      
		      HTML1[i].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
		                               "background: #000; margin-right: 12px; margin-bottom: 10px;" +
		                               "margin-top: 4px; padding-top: 3px; border-radius: 4px;" +
		                               "border: 3px solid #555; text-align: center; font-weight: bold;" +
		                               "cursor: pointer";		                                 	         
         }
         
         var HTML1 = document.getElementsByClassName('carData2');         
         for (var i=0; i<5; i++) {
         	      
		      HTML1[i].style.cssText = "display: inline-block; width: 75px; height: 25px;" +
		                               "background: #000; margin-right: 12px; margin-bottom: 10px;" +
		                               "margin-top: 4px; padding-top: 3px; border-radius: 4px;" +
		                               "border: 3px solid #555; text-align: center; font-weight: bold;" +
		                               "cursor: pointer";		                                 	         
         }
         
         document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Getinfo(\'Vehicle\')">Vehicle Select</button>';                        
   
  } 
  
  function diaNumKey(id) {
  	  var NumData= document.getElementsByClassName('txtData');
     var GetDispay = document.getElementById('txtDisplay').innerHTML;
  	  var PhoneData = ["(", "#", "#", "#", ")", "#", "#", "#", "-", "#", "#", "#", "#"];
  	  var EmptySlot = "";

/* === Getting the data from the phone number display box and stripping away
       unwanted characters to leave just the numbers. */     
     var TmpDis = GetDispay.split("(");
     var TmpDis2 = TmpDis[1];
     var TmpDis3= TmpDis2.split(")");
     var TmpDis4= TmpDis3[1]
     var TmpDis5 = TmpDis4.split("-");
     var TmpDis6 = "" + TmpDis3[0] + "" + TmpDis5[0] + "" + TmpDis5[1];


// === Storing each number into the array.
     var str = "";
     for (var i=0; i<10; i++) {
       str = TmpDis6.substring(i,10);
       switch(i) {
       	case 0:
            PhoneData[1] = str[0]; break;
       	case 1:
            PhoneData[2] = str[0]; break;
       	case 2:
            PhoneData[3] = str[0]; break;
       	case 3:
            PhoneData[5] = str[0]; break;
       	case 4:
            PhoneData[6] = str[0]; break;
       	case 5:
            PhoneData[7] = str[0]; break;
       	case 6:
            PhoneData[9] = str[0]; break;
       	case 7:
            PhoneData[10] = str[0]; break;
       	case 8:
            PhoneData[11] = str[0]; break;
       	case 9:
            PhoneData[12] = str[0]; break;
         default:
            break;
       }                                                                                                                                              	
     }            
 	
 	
/*=== The rest of the function finds an empty place within the array
      to place the user numbers that will be displayed in the display box */
       	    	  
  	  for (var i=0; i<13; i++) { 
  	    if (PhoneData[i] == "#") { EmptySlot = i; break; }  	    
  	  }  	  
  	  
  	  for (var i=0; i<NumData.length; i++) {
  	  	  if (NumData[i].id == id) {
  	  	  	 switch(id) {
  	  	  	 	
  	  	  	 	case "txt10":
  	  	  	 	  
  	  	  	 	   switch(EmptySlot-1) {
  	  	  	 	   
  	  	  	 	   	case 0: 
  	  	  	 	   	   PhoneData[1] = "#"; break;
  	  	  	 	   	case 4:   
  	  	  	 	   	   PhoneData[3] = "#"; break;
  	  	  	 	   	case 8:
  	  	  	 	   	   PhoneData[7] = "#"; break;
  	  	  	 	   	case -1:
  	  	  	 	   	   PhoneData[12] = "#"; break;	   
  	  	  	 	   	default:      
      	  	  	 	   PhoneData[EmptySlot-1] = "#"; break;      	  	  	 	   
      	  	  	} 
      	  	  	break;
      	  	  	
  	  	  	 	case "txt12": 
               for (var j=0; j<13; j++) {
               	if ((j !== 0) && (j !== 4 ) && (j !== 8 )) {
               		PhoneData[j] = "#";
               	}
               }  	  	  	 	
  	  	  	 	   break;
  	  	  	 	   
  	  	  	 	default:  	  	  	 	  	  	  	 	 
  	  	        PhoneData[EmptySlot] = NumData[i].innerHTML; break;
  	  	    }     
  	  	  }  	  	  	    	  	  
  	  }
  	  
//=== Show the numbers in the display box ====  	  
  	  for (var i=0; i<13; i++) {
  	    if (i==0) { 
  	      document.getElementById('txtDisplay').innerHTML = PhoneData[i]; 
  	    }
  	    else { document.getElementById('txtDisplay').innerHTML += PhoneData[i]; }
  	  }  	  
  }
  
// *************************************************************  
  
function MultiOrderDialogRender(choice1) {
	      
	cpDiaInfo.ChoiceSelect = parseInt(choice1);
	      //alert(cpDiaInfo.ChoiceSelect);
	var winW = window.innerWidth;
	var winH = window.innerHeight;
			
/*=========== Setting up the HTML and CSS for the dialog popup ===========*/			

    var test1= document.getElementById('dialogOverlay');
         if (test1 == null) {
				var NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogOverlay";
	         NewDiv.style.cssText = "display: none; opacity: .8; position: fixed; top: 0px;" +
	                                "left: 0px; width: 100%; background: #FFF; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);                       
	
	         NewDiv = document.createElement("Div");	
	         NewDiv.id = "dialogBox";
	         NewDiv.style.cssText = "display: none; position: fixed; width: 550px;" +
	                                "background: #000; z-index: 1000";
	                                
	         document.getElementsByTagName("body")[0].appendChild(NewDiv);
	         	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "diaHold";  
	         NewDiv.style.cssText = "background: #FFF; margin:8px;";	         
	         document.getElementById("dialogBox").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxHead";
//	         NewDiv.style.cssText = "background: #666; font-size:19pt; padding:10px; color:#CCC;";	         
	         NewDiv.style.cssText = "background: #08A; font-size:19pt; padding:10px; color:#CCC;";
	         document.getElementById("diaHold").appendChild(NewDiv);
	
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxBody";                                
	         NewDiv.style.cssText = "background: #333; padding:20px; color:#FFF;";
	         
	         
	         document.getElementById("diaHold").appendChild(NewDiv);	         
	         
	         NewDiv = document.createElement("Div");
	         NewDiv.id = "dialogBoxFoot";                                
//	         NewDiv.style.cssText = "background: #666; font-size:10px; text-align: right;";	         
	         NewDiv.style.cssText = "background: #08A; font-size:10px; text-align: right;";
	         document.getElementById("diaHold").appendChild(NewDiv);                    
         }
			var dialogoverlay = document.getElementById('dialogOverlay');
			var dialogbox = document.getElementById('dialogBox');			
			
			dialogoverlay.style.display = "block";
			dialogoverlay.style.height = winH + "px";
			dialogbox.style.left = (winW/2) - (550/2) + "px";
			dialogbox.style.top = winH * .25 + "px";
			dialogbox.style.display = "block";	
			
	var htmlLoad2 = ['<div id="OrderTitle">Enter Number', 
	                 '<div id="OrderDisplay">0</div>',
	                 '</div>',
	                 '<div id="keypad">',
	                 '<div class="OrdData" id="Ord1" onclick="diaNumKey2(this.id)">1</div>',
	                 '<div class="OrdData" id="Ord2" onclick="diaNumKey2(this.id)">2</div>',
	                 '<div class="OrdData" id="Ord3" onclick="diaNumKey2(this.id)">3</div>',
	                 '<div class="OrdData" id="Ord4" onclick="diaNumKey2(this.id)">4</div>',
	                 '<div class="OrdData" id="Ord5" onclick="diaNumKey2(this.id)">5</div>',
	                 '<div class="OrdData" id="Ord6" onclick="diaNumKey2(this.id)">6</div>',
	                 '<div class="OrdData" id="Ord7" onclick="diaNumKey2(this.id)">7</div>',
	                 '<div class="OrdData" id="Ord8" onclick="diaNumKey2(this.id)">8</div>',
	                 '<div class="OrdData" id="Ord9" onclick="diaNumKey2(this.id)">9</div>',
	                 '<div class="OrdData" id="Ord10" onclick="diaNumKey2(this.id)">BS</div>',
	                 '<div class="OrdData" id="Ord11" onclick="diaNumKey2(this.id)">0</div>',
	                 '<div class="OrdData" id="Ord12" onclick="diaNumKey2(this.id)">Clr</div>',			                 			                 
	                 '</div>'];			                 
			                
   if (choice1 == 2) { 
      document.getElementById('dialogBoxHead').innerHTML = "How many orders";
   }
   else { 
      document.getElementById('dialogBoxHead').innerHTML = "Qty"; 
   }  
         
   for (var i=0; i<16; i++) {
     if (i == 0) { document.getElementById('dialogBoxBody').innerHTML = htmlLoad2[i]; }  
     else { document.getElementById('dialogBoxBody').innerHTML += htmlLoad2[i]; }
   } 

   var HTML2 = document.getElementById('OrderTitle');
   HTML2.style.cssText = "font-weight: bold; padding-left: 36.6%"; 
         
   var HTML2 = document.getElementById('OrderDisplay');
   HTML2.style.cssText = "display: block; margin-left: 30%; width: 215px; height: 35px;" +
                         "background: #999; font-size: 16pt; font-weight: bold;" +
                         "text-align: center; padding-top: 2px"; 
                               
                                                  
   HTML2 = document.getElementById('keypad');                      
	HTML2.style.cssText = "display: block; width: 260px; background: #999; margin-top:15px;" +
	                      "margin-left: 23%";

   HTML2 = document.getElementsByClassName('OrdData');	                            
	for (var i=0; i<12; i++) {
	   HTML2[i].style.cssText = "display: inline-block; width: 60px; height: 60px;" +
	                            "background: #000; margin-left: 11px; margin-right: 9px;" +
	                            "margin-bottom: 10px; margin-top: 4px; font-size: 20pt;" +
	                            "font-weight: bold; text-align: center; padding-top: 3px;" +
	                            "border-radius: 4px; border: 3px solid #555; cursor: pointer";
	                                  
      document.getElementById('keypad').appendChild(HTML2[i]);            	                                  	      	
	}                      
         
         //document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="Alert.ok(\''+passMess+'\')">Text Receipt</button>';
   document.getElementById('dialogBoxFoot').innerHTML = '<button onclick="StoreKeyPadAns()">OK</button>';
	
}


// *************************************************************  

function StoreKeyPadAns() {
	
	if (cpDiaInfo.ChoiceSelect == 1) {
	   document.getElementById("" + cpDiaInfo.BoxID).value = document.getElementById('OrderDisplay').innerHTML;
	   document.getElementById('dialogBoxHead').innerHTML = "";
	}
	else {
		CPinfo.MultiOrder = document.getElementById('OrderDisplay').innerHTML;
		document.getElementById('dialogBoxHead').innerHTML = "";
	}   
	Alert.lastok() 
}

function diaNumKey2(id) {
	var NumData= document.getElementsByClassName('OrdData');
   var GetDispay = document.getElementById('OrderDisplay').innerHTML;
     
   for (var i=0; i< NumData.length; i++) {
      if (NumData[i].id == id) {
        if (NumData[i].innerHTML == "BS" || NumData[i].innerHTML == "Clr") {
        	 	 //alert("Special pushed");
           document.getElementById('OrderDisplay').innerHTML = "0";        	    
        }
        else {
        	 if (GetDispay == "0") {        	 	 	
        	   document.getElementById('OrderDisplay').innerHTML = NumData[i].innerHTML;
        	 }
        	 else {
        	    document.getElementById('OrderDisplay').innerHTML += NumData[i].innerHTML;        	 	 	
        	 }   
        }
        break;
      }	 	
  }
  	  
}  