pCP = { stOutr: 0,
        stInner: 0,
        Mcnt: 0,
        Mcnt2: 0,
        Scnt: 0,
        CarSel: 0,
        MultiOrder: 0,
        DisCnt: 0,
        OrdCurCnt: 0,
        StoredId: 0,
        FootState: 0,       
        BoxID: "",
        CarType: "",
        dSelected: "",
        WinSide: "",
        TxtNum: "",        
        ScrAry1: [],
        TitleAry1: [],
        sList1: [],
        dList1: [],
        nList1: [],
        wList1: [],
        fList1: [],
        mTotal: [],
        mTotTitle: [],
        ShowExtra: false,
        snwh: false,
        snwhMod: false,
        nug: false,
        wng: false,
        fry: false,
        drk: false,
        modify: false,
        RgIceState: false,
        LgIceState: false   
      }; 

function PhoneSetup() {
	
   var pWSide = document.getElementsByClassName("pWSide");
   
   pCreateAry();
   pLoadAry1();  
   
   pClear();
   pCP.stOutr=0;
   pCP.stInner=0;
           
   if (pCP.Mcnt == 0) { 
      document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";           
      pWSide[1].style.background = '#777';
      pWSide[0].style.border = '4px solid #08A';    	  
      pWSide[0].style.color = '#08A';
      pWSide[0].style.textShadow = '1px 1px 1px #FFF';
      pCP.Mcnt = 0;      
   }  	
 
   document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.Mcnt];
   pCP.FootState = 1;     	      	
	pSetFooter();             		
}

function pCreateAry() {
	
/* Creating and populating an 7x5 array for screen display */      
   for (var i=0; i<7; i++) {
     pCP.ScrAry1[i]= new Array();
     for (var j=0; j<6; j++) {
     	 pCP.ScrAry1[i][j]="";
     }
     pCP.TitleAry1[i] = "";	
   }
   
/* Creating and populating an 8x11 array for sandwhiches */      
   for (var i=0; i<9; i++) {
     pCP.sList1[i]= new Array();
     for (var j=0; j<11; j++) {
     	 pCP.sList1[i][j]="";
     }	
   }

/*****************************************************************   
    Note to self:
   ---------------  
     This will access (Name, Rg, Lg) => alert(pCP.dList1[0][1])   
     
     This will access (Ex, Lt, No) => alert(pCP.dList1[0][4][1])              
        
******************************************************************/   
//                                 REG         LG
//                       Rg  Lg  Ex Lt No   Ex Lt No 
   pCP.dList1= [["PEP",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["DIET",  0, 0, [0, 0, 0], [0, 0, 0]],
                ["DEW",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["DOC",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["FP",    0, 0, [0, 0, 0], [0, 0, 0]],
                ["MIST",  0, 0, [0, 0, 0], [0, 0, 0]],
                ["TEA",   0, 0, [0, 0, 0], [0, 0, 0]],
                ["WATER", 0, 0, [0, 0, 0], [0, 0, 0]],
                ["ICE",   0, 0, [0, 0, 0], [0, 0, 0]]
               ];

/* Creating and populating an array for fries */         
   pCP.fList1 = [0, 0];
   	

/* Creating and populating an 2x2 array for nuggets */  
   for (var i=0; i<2; i++) {
     pCP.nList1[i]= new Array();
     for (var j=0; j<2; j++) {
     	 pCP.nList1[i][j]="";
     }	
   }        
   
      
/* Creating and populating an 9x2 array for wings */
   for (var i=0; i<6; i++) {
     pCP.wList1[i]= new Array();
     for (var j=0; j<2; j++) {
     	 pCP.wList1[i][j]="";
     }	
   } 	
}

function pLoadAry1() {
     
   pCP.TitleAry1[0] = "Set Window";
   pCP.TitleAry1[1] = "Select Item";   
  
// Burgers
   pCP.ScrAry1[0][0]  = "wCon";
   pCP.ScrAry1[0][1]  = "pSelector1";   
   pCP.ScrAry1[0][2]  = "pSelector2";
   pCP.ScrAry1[0][3]  = "pSelector3";
   pCP.ScrAry1[0][4]  = "pSelector11";
   pCP.ScrAry1[0][5]  = "pSelector12";
   
// Hot Dogs   
   pCP.ScrAry1[1][0] = "wCon";
   pCP.ScrAry1[1][1] = "pSelector1";
   pCP.ScrAry1[1][2] = "pSelector4";      
   
// Chicken   
   pCP.ScrAry1[2][0]  = "wCon";
   pCP.ScrAry1[2][1]  = "pSelector1";   
   pCP.ScrAry1[2][2]  = "pSelector5";
   pCP.ScrAry1[2][3]  = "pSelector3";
   pCP.ScrAry1[2][4]  = "pSelector11";
   pCP.ScrAry1[2][5]  = "pSelector12";                  

// Nugget
   pCP.ScrAry1[3][0] = "wCon";
   pCP.ScrAry1[3][1] = "pSelector1";      
   pCP.ScrAry1[3][2] = "pSelector7";
   pCP.ScrAry1[3][3] = "pSelector12";
   
// Wings
   pCP.ScrAry1[4][0] = "wCon";
   pCP.ScrAry1[4][1] = "pSelector1";   
   pCP.ScrAry1[4][2] = "pSelector6";
   pCP.ScrAry1[4][3] = "pSelector12";   

// Fries
   pCP.ScrAry1[5][0] = "wCon";
   pCP.ScrAry1[5][1] = "pSelector1";      
   pCP.ScrAry1[5][2] = "pSelector8";   

// Drinks
   pCP.ScrAry1[6][0] = "wCon";
   pCP.ScrAry1[6][1] = "pSelector1";      
   pCP.ScrAry1[6][2] = "pSelector9";
   pCP.ScrAry1[6][3] = "pSelector10";   
	
}

/***********************************************
 ***********************************************/

function pViewOrder() {
	var a=b=c=d=e=f=g=h=0;
	var mTot = 0;

   if (!pCP.snwh && !pCP.nug && !pCP.wng && !pCP.fry & !pCP.drk) {
   	alert("Nothing Ordered");
   	return;
   }
   
// Burger List	
	for (var i=0; i<pCP.sList1.length; i++) {
		a=pCP.sList1[i][0];
		
		if (a > 0) { b++; }				
	}

// Nugget List	
	for (var i=0; i<pCP.nList1.length; i++) {
		a=pCP.nList1[i][0];
		if (a > 0) { c=1; break; }				
	}
	
// Wings List	
	for (var i=0; i<pCP.wList1.length; i++) {
		a=pCP.wList1[i][0];
		if (a > 0) { d=1; break; }				
	}
	
// Fries List	
	for (var i=0; i<pCP.fList1.length; i++) {
		a=pCP.fList1[i];
		if (a > 0) { e=1; break; }				
	}
	
// Drink List	
	for (var i=0; i<pCP.dList1.length; i++) {
		a=pCP.dList1[i][1];

		if (a > 0) { f=a; }
		
		a=pCP.dList1[i][2];
			
		if (a > 0) { g=a; }
		
		//alert("f:"+ f + " g:" + g);
		if (f > 0 && g > 0) { h++; }
		else if (f > 0 && g == 0) { h++; }
		else if (f ==0 && g > 0) { h++ }
		f=g=0;						
	}
	
	
// Master Order List		
	mTot = b + c + d + e + h;
	
	//alert("mTot:"+mTot);
	if (mTot < 1) { return; }
	else {
	  	pCP.mTotal = new Array(mTot);
	  	pCP.mTotTitle = new Array(mTot);
	  	
	  	for (i=0; i<pCP.mTotal.length; i++) {
	  		pCP.mTotal[i] = "";
	  		pCP.mTotTitle[i] = "";
	  	} 
	}

 
	if (b > 0) {
// Sandwhiches
		for (var i=FindMstrElm(); i<b; i++) {
			pCP.mTotal[i] = "pSelector20";
			pCP.mTotTitle[i] = pCP.sList1[i][1];
		}
	}
	
	if (c > 0) {
// Nuggets				
		pCP.mTotal[FindMstrElm()] = pCP.ScrAry1[3][2];		
	}
	
	if (d > 0) {
// Wings				
		pCP.mTotal[FindMstrElm()] = pCP.ScrAry1[4][2];		
	}			

	if (e > 0) {
// Fries				
		pCP.mTotal[FindMstrElm()] = pCP.ScrAry1[5][2];		
	}
	
	if (h > 0) {
// Drinks						
	  for (var i=FindMstrElm(); i<mTot; i++) {
		 pCP.mTotal[i] = pCP.ScrAry1[6][3];
	  }			
	}	
	
	pClear();
	//ShowMstrSand(b);
	MVP();
   
   pCP.FootState = 11;     	      	
	pSetFooter(); 
	
}

function pPlaceOrder() {
	
	var sCnt = 0;
	var nCnt = 0;
	var wCnt = 0;
	var fCnt = 0;
	var dCnt = 0;
	var lCnt = 0;
	var pSndStr = "";
	var pNugStr = "";
	var pWngStr = "";
	var pFryStr = "";
	var pDrkStr = "";
	var pWinStr = "";
    var pCarStr = "";
    var pPhone = "";
    var PassData1 = "";

/////////////////////////////////	
/////   Check Window data.  ////
////////////////////////////////
	
	pWinStr = "1&" + pCP.WinSide;	

////////////////////////////////	
///// Check Sandwhich data. ////
////////////////////////////////
   
   lCnt = 0;
   
   for(var i=0; i<pCP.mTotal.length; i++) {
	  if(pCP.mTotal[lCnt] == "pSelector20") {
	     sCnt++;
	     lCnt++;	   
	  }
   }
   
   if (sCnt !== 0) {
	   for(var i=0; i<sCnt; i++) {
		   for(var j=0; j<11; j++) {
			   if (pCP.sList1[i][j] !== "") {
       	          if (j == 0) { 
        	          pSndStr += "#" + pCP.sList1[i][j] + " "; 
        	      }        	  	                     	  
        	      else if (j < pCP.sList1[i].length-1) { 
        	         pSndStr += pCP.sList1[i][j] + " "; 
        	      }
        	      else { pSndStr += pCP.sList1[i][j]; } 
			   }
			   else { break;}  
		   }
	   }
   }
   else {pSndStr = "none";} 
   
//////////////////////////////////	
/////   Check Nugget data.  /////
////////////////////////////////
   
   var Reg;
   var rSauce;
   var Buck;
   var bSauce;
     
   for (var i = 0; i<pCP.nList1.length; i++) {     
      switch(i) {
		 case 0: 
		   Reg = pCP.nList1[i][0];
		   rSauce = pCP.nList1[i][1];
		   
		   if (Reg > 0 ) {
             pNugStr += "#" + Reg + " 5pcN " + rSauce;
           }
           break;
           
         case 1:  

           Buck = pCP.nList1[i][0];
           bSauce = pCP.nList1[i][1];
   
           if (Buck > 0) {
             pNugStr += "#" + Buck + " BKN " + bSauce;          	         	
           }
           break;  
      }
   }   
   if(pNugStr == "") {pNugStr = "none";}

   
//////////////////////////////////	
/////    Check Wing data.   /////
////////////////////////////////   
	
     var Wng;
     var rSauce;	   
     for (var i=0; i<pCP.wList1.length; i++) {
       Wng =    pCP.wList1[i][0];
       rSauce = pCP.wList1[i][1];

	   if (Wng > 0) {
	  	  switch(i) { 
	  	     case 0:
	  	       pWngStr += "#" + Wng + " 5W " + rSauce; break;
	  	     case 1:
	  	       pWngStr += "#" + Wng + " 10W " + rSauce; break;
	  	     case 2:
	  	       pWngStr += "#" + Wng + " 15W " + rSauce; break;     
	  	     case 3:
	  	       pWngStr += "#" + Wng + " 20W " + rSauce; break;
	  	     case 4:
	  	       pWngStr += "#" + Wng + " 25W " + rSauce; break;
	  	     case 5:
	  	       pWngStr += "#" + Wng + " 30W " + rSauce; break;
	  	     default:   break;  	  	 
	  	  }	  	           	
	   }	   
     }
     if (pWngStr == "") {pWngStr = "none";} 
   
//////////////////////////////////	
/////     Check Fry data.   /////
////////////////////////////////   
   
   lCnt = 0;
   
   for(var i=0; i<pCP.fList1.length; i++) {
     if (pCP.fList1[i] !== 0) {
		 switch (i) {   
	       case 0:
	  	      pFryStr += "#" + pCP.fList1[i] + " RegFries"; break;
	  	   case 1: 
		      pFryStr += "#" + pCP.fList1[i] + " BkFries"; break;
		   default: break;     		   
	     }
     }	   
   }
   if (pFryStr == ""){ pFryStr = "none";} 

     
//////////////////////////////////	
/////   Check Drink data.   /////
////////////////////////////////   
   
   var dReg;
   var dLG;
   var dTitle="";
   var dIceType="";
   var x1;
	  
   for(var j=0; j<pCP.dList1.length; j++) { 
      dReg = pCP.dList1[j][1];
      dLG = pCP.dList1[j][2];
         
      if(dReg > 0) {
        dTitle = pCP.dList1[j][0];
	    dIceType = "";
		  	  
	    for (x=0; x<3; x++) {
	       x1=pCP.dList1[j][3][x];
	             
	       if (x1>0) {
	         switch(x) {
	       	    case 0:
	       	      dIceType +=  x1 + ' ExIce ';
	       	      break;
	       	    case 1:
	       	      dIceType += x1 + ' LtIce ';
	       	      break;
	       	    case 2:
	       	      dIceType += x1 + ' NoIce ';
	       	      break;
	       	    default: break;  	         		  	         		   
	         }	         	
	       }	         	
	    }
	          
	    pDrkStr += "#" + dReg + " Reg " + dTitle + " " + dIceType; 
	 }
		 
     if(dLG > 0) {
	   dTitle = pCP.dList1[j][0];
	   dIceType = "";		  	
	   for (x=0; x<3; x++) {
	      x1=pCP.dList1[j][4][x];

	      if (x1>0) {
	   	    switch(x) {
	          case 0:
	            dIceType +=  x1 + ' ExIce ';
	            break;
	          case 1:
	            dIceType += x1 + ' LtIce ';
	            break;
	          case 2:
	            dIceType += x1 + ' NoIce ';
	            break;
	          default: break;  	         		  	         		   
	        }	         	
	     }	         	
	   }
	   pDrkStr += "#" + dLG + " LG " + dTitle + " " + dIceType; 
     }
		      
   }	      
   if (pDrkStr == "") {pDrkStr = "none";}
   
//////////////////////////////////////////	
///// Check user car and phone data. ////
/////////////////////////////////////////

   if(pCP.CarType !== "") {
	   pCarStr = "&" + pCP.CarType;
   }
   else { pCarStr = "&Window"; }
	
   if (pCP.TxtNum !== "") {
	  pPhone = "&" + pCP.TxtNum;
   }
   else { pPhone = "&none"; }

	
//////////////////////////////////////////	
///// Check user car and phone data. ////
/////////////////////////////////////////
   var MulOrd = pCP.MultiOrder;
   var CurOrd = 0;
   var SmeCar = "&NO";
   var mDisCnt = 0;
     
   if (MulOrd <= 1) {
	  SmeCar = "&NO";
   }
   else {
      SmeCar = "&YES";
      if (pCP.OrdCurCnt == 0) {
         pCP.OrdCurCnt = pCP.MultiOrder;      
     	 pCP.DisCnt = (pCP.MultiOrder - pCP.OrdCurCnt) + 1;
     	 mDisCnt = pCP.DisCnt;
     	 pCP.OrdCurCnt--;	        
       }
       else {
     	   pCP.DisCnt = (pCP.MultiOrder - pCP.OrdCurCnt)+1;
     	   mDisCnt = pCP.DisCnt;     	        	 
     	   pCP.OrdCurCnt--;     	  
       }  
     }    	 
     
     PassData1 = pWinStr + "&" + pSndStr + "&" + pWngStr + "&" + 
                 pNugStr + "&" + pFryStr + "&" + pDrkStr + SmeCar + 
                 pPhone + pCarStr + "&" + MulOrd + "&" + mDisCnt;
                
     pSendDB(PassData1);

     pClear2();
     pLoadAry1();
     pBack();
                      
}

function pSendDB(pData1) {
   alert(pData1);
   var CP_URL= "http://localhost:8500?";
   var xmlhttp= new XMLHttpRequest();
   xmlhttp.open("POST", CP_URL, true);
   xmlhttp.send(pData1);	
}

function FindMstrElm() {
	var hld;
	for (var j=0; j<pCP.mTotal.length; j++) {
	  if (pCP.mTotal[j] == "") {	
	    hld = j;
	    break;
	  }
	}
	return hld;    
}

function ShowMstrNug() {
	var Reg;
	var Buck;
	var rSauce; 
	var pDiv;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   Reg = pCP.nList1[0][0];
   rSauce = pCP.nList1[0][1];
      
   if (Reg > 0) {
      pDiv = '<div class="pSel">(' + Reg + ') 5pc ' + rSauce + '</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }
 
   Buck = pCP.nList1[1][0];
   bSauce = pCP.nList1[1][1];
   
   if (Buck > 0) {
      pDiv = '<div class="pSel">(' + Buck + ') BK ' + rSauce + '</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }
   document.getElementById("dBoxHead").innerHTML = "NUGGETS";   
}

function ShowMstrFries() {
	var Reg;
	var LG;
	var pDiv;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   Reg = pCP.fList1[0];
   LG  = pCP.fList1[1];
      
   if (Reg > 0) {
      pDiv = '<div class="pSel">(' + Reg + ') Reg</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;          	
   }
   
   if (LG > 0) {
      pDiv = '<div class="pSel">(' + LG + ') BK</div>';
      document.getElementById("pSelector20").innerHTML += pDiv;           	
   }
   
   document.getElementById("dBoxHead").innerHTML = "FRIES";      
}

function ShowMstrDrink() {
	var Reg;
	var LG;
	var IceType;
	var dTitle;
	var pDiv;
	var x1;
	var xCnt = 0;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   for (var i=0; i<pCP.dList1.length; i++) {
          
	   Reg = pCP.dList1[i][1];
	   LG  = pCP.dList1[i][2];
	      
	   if (Reg > 0) {
	   	dTitle = pCP.dList1[i][0];
	   	  xCnt= i+1;
	      pDiv = '<div class="pSel" id="dr' + xCnt + '" onclick="mDrk1(this.id)">(' + Reg + ') Reg ' + dTitle + '</div>';
	      document.getElementById("pSelector20").innerHTML += pDiv;
	      
	      
	      for (j=0; j<3; j++) {
	         x1=pCP.dList1[i][3][j];
	         IceType = "";
	         if (x1>0) {
	         	switch(j) {
	         		case 0:
	         		  IceType +=  x1 + ': Ex Ice ';
	         		  break;
	         		case 1:
	         		  IceType += x1 + ': Lt Ice ';
	         		  break;
	         		case 2:
	         		  IceType += x1 + ': No Ice ';
	         		  break;
	         		default: break;  	         		  	         		   
	         	}
			      pDiv = '<div class="pSel">' + IceType + '</div>';
			      document.getElementById("pSelector20").innerHTML += pDiv;	         	
	         }	
	      }          	
	   }
	   
	   if (LG > 0) {
	   	dTitle = pCP.dList1[i][0];
	   	xCnt= i+1;	   	
	    pDiv = '<div class="pSel" id="dr' + xCnt + '" onclick="mDrk1(this.id)">(' + LG + ') LG ' + dTitle + '</div>';
	      document.getElementById("pSelector20").innerHTML += pDiv; 
	      

	      for (j=0; j<3; j++) {
	         IceType = "";	      	
	         x1=pCP.dList1[i][4][j];
	         if (x1>0) {
	         	switch(j) {
	         		case 0:
	         		  IceType += x1 + ': Ex Ice';
	         		  break;
	         		case 1:
	         		  IceType += x1 + ': Lt Ice';
	         		  break;
	         		case 2:
	         		  IceType += x1 + ': No Ice';
	         		  break;
	         		default: break;  	         		  	         		   
	         	}
			      pDiv = '<div class="pSel">' + IceType + '</div>';
			      document.getElementById("pSelector20").innerHTML += pDiv;	         	
	         }	
	      } 	                	
	   }    
   }
   document.getElementById("dBoxHead").innerHTML = "DRINKS";
}

function mDrk1(id) {
   var dSplit;
   var Hld;
   var Elm;
   
   pSel = document.getElementsByClassName("pSel");

   for(var i=0; i<pSel.length; i++) {
	  if (pSel[i].id == id) {
	     Elm = i;
	     break;	  
	  }   
   }
   
   Hld = document.getElementById("" + id).innerHTML; 
   dSplit = Hld.split(" ");
   pCP.dSelected = dSplit[2];
   pTogState(Elm);
 	
}

function ShowMstrWings() {
	var Wng;
	var rSauce; 
	var pDiv;
	var j;
	
   document.getElementById("pOr1").innerHTML = "";
   LoadMstrNav();
   
   for (var i=0; i<pCP.wList1.length; i++) {
     Wng =    pCP.wList1[i][0];
     rSauce = pCP.wList1[i][1];

	  if (Wng > 0) {
	  	 switch(i) { 
	  	    case 0:
	  	      j="5W"; break;
	  	    case 1:
	  	      j="10W"; break;
	  	    case 2:
	  	      j="15W"; break;     
	  	    case 3:
	  	      j="20W"; break;
	  	    case 4:
	  	      j="25W"; break;
	  	    case 5:
	  	      j="30W"; break;
	  	    default:   break;  	  	 
	  	 }
	  	 
       pDiv = '<div class="pSel">(' + Wng + ') ' + j + " " + rSauce + '</div>';
       document.getElementById("pSelector20").innerHTML += pDiv;
       rSauce ="";          	
	  }     
   } 
   document.getElementById("dBoxHead").innerHTML = "WINGS"; 

}

function ShowMstrSand(sCnt) {
   
   var pCnt = 76;
   var j=0;   
   var pDiv;
   var pp; 
        	
	   document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
	   document.getElementById("pOr1").innerHTML = "";
	   
	   for (var i=1; i<pCP.sList1[pCP.Mcnt2].length; i++) {
	   	  if (i==1) {
	   	  	LoadMstrNav();
	   	  }
	      else {
		   	if (pCP.sList1[pCP.Mcnt2][i] !== "") {
		   	  pp="pS"+ (pCnt+j);
		   	  
		   	  pDiv = '<div class="pSel" id="'+ pp +'"></div>';
		   	  
		   	  document.getElementById("pSelector20").innerHTML += pDiv;
		   	  document.getElementById("pS"+ (pCnt+j)).innerHTML = pCP.sList1[pCP.Mcnt2][i];
		   	  j++;
		   	}
		   	else {break;}
		  } 	  
	   }
	   
   document.getElementById("dBoxHead").innerHTML = '(' + pCP.sList1[pCP.Mcnt2][0] + ') ' +
                                                    pCP.mTotTitle[pCP.Mcnt2];       	
}

function LoadMstrNav() {
	
	   var pSel4 = document.getElementsByClassName("pSel4");
	   pSel4[0].innerHTML = '<div class="pSel5" id="pB" onclick="pNavList(this.id)">&lt</div>' +
	                        '<div class="pSel5" id="pF" onclick="pNavList(this.id)">&gt</div>';
	                        
	   var HTML1 = document.getElementsByClassName('pSel5');
	   
	   HTML1[0].style.cssText = "display: inline-block; float: left; width: 23.3%; height: 33px;" +
	                            "background: #333; margin-bottom: 4px; margin-top: 1%; margin-left: 3px;" +
	                            "padding-top: 1%; border: 2px solid #AAA;" +
	                            "text-align: center; font-weight: bold; cursor: pointer";	 
	     
	     
	   HTML1[1].style.cssText = "display: inline-block; float: left; width: 23.3%; height: 33px;" +
	                            "background: #333; margin-bottom: 4px; margin-top: 1%; margin-left: 48.9%;" +
	                            "padding-top: 1%; border: 2px solid #AAA;" +
	                            "text-align: center; font-weight: bold; cursor: pointer";	   	  	
	
}

function pNavList(id) {
		
	switch(id) {
		case "pB":
		    pCP.Mcnt2--;
		    if (pCP.Mcnt2 < 0) {
		    	pCP.Mcnt2 = 0;
		    }
		    else {
		    	MVP();		    	
		    }	
		  break;
		  
		case "pF":
		    pCP.Mcnt2++;
		    if (pCP.Mcnt2 >= pCP.mTotal.length) {
		    	pCP.Mcnt2--;
		    }
		    else {		    	
		    	MVP();		    			    	
		    }		
		  break;
		  
		default: break;  		  
	}
}

function MVP() {
  
  
   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
		document.getElementById("pSelector20").innerHTML = "";
		document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';   	
      ShowMstrSand();   
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
      document.getElementById("pSelector20").style.display = "block";   	
		document.getElementById("pSelector20").innerHTML = "";
		document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';
      ShowMstrDrink(); 		   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
      document.getElementById("pSelector20").style.display = "block";   	
		document.getElementById("pSelector20").innerHTML = "";
		document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrWings();        	
   	
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
      document.getElementById("pSelector20").style.display = "block";   	
		document.getElementById("pSelector20").innerHTML = "";
		document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrNug();        	
   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
      document.getElementById("pSelector20").style.display = "block";   	
		document.getElementById("pSelector20").innerHTML = "";
		document.getElementById("pSelector20").innerHTML = '<div class="pSel4" id="pOr1"></div>';        
      ShowMstrFries();        	
   }   
}

function pOrderIt() {
	
	var pTitle = document.getElementById("dBoxHead").innerHTML;
	var pProceed = false;
	var pIsDrink = false;
	var pFtitle = ["FRIES", "NUGGETS", "WINGS"];	
      
   if (pCP.TitleAry1[pCP.stInner - 1] == "DRINKS") {
      pProceed = pIsDrink = true;   	
   }
      
   for (var i=0; i<pFtitle.length; i++) {
   	if (pTitle == pFtitle[i] && !pProceed ) { 
   	  pProceed = true; break; 
   	}
   }
   
   if (pProceed) {
   	if (pIsDrink) { pStoreDrinks(); }
   	else { pStoreFriedItems(); }   		   	
   }
   else { pStoreSandwhiches(); }	
}

function pStoreFriedItems() {
	
	var pTitle = document.getElementById("dBoxHead").innerHTML;
	var pNtxt = document.getElementsByClassName("pNtxt");
	var pSel= document.getElementsByClassName("pSel");	
	var pSauce = "";
	var pProceed = false;
		
    switch (pTitle) {
    	 
    	 case "FRIES":
			     
         var tmpNum = 0;
         
			for (var i=10; i<12; i++) {
           tmpNum = parseInt(pNtxt[i].innerHTML) ;
           if (tmpNum !== 0) {
           	 pProceed = true;
           }				
			}
			
			if (!pProceed) {
				alert("Select a Qty");
				return;
			}
							     
      // Reading REG fries number box
         tmpNum = parseInt(pNtxt[10].innerHTML);
         pCP.fList1[0] = tmpNum;
   
      // Reading BK fries number box         
         tmpNum = parseInt(pNtxt[11].innerHTML);
         pCP.fList1[1] = tmpNum;
                 
       //clearing out the number boxes;         
         pNtxt[10].innerHTML = 0;
         pNtxt[11].innerHTML = 0;		
  	      pCP.fry = true;
  	      
    	   break;
    	   
// //////////////////////////////////////////////////    	    
    	 case "NUGGETS":
         
         var tmpNum = 0;
         
			for (var i=8; i<10; i++) {
           tmpNum = parseInt(pNtxt[i].innerHTML) ;
           if (tmpNum !== 0) {
           	 pProceed = true;
           }				
			}
			
			if (!pProceed) {
				alert("Select a Qty");
				return;
			}
							              
         tmpNum = parseInt(pNtxt[8].innerHTML);
         pCP.nList1[0][0] = tmpNum;
            
         for (j=69; j<72; j++) {
		   	if (pSel[j].style.color == 'rgb(0, 136, 170)') {
		   	   pSauce += pSel[j].innerHTML + " ";		   	          	      
		   	}                  
        	}
         if (!pSauce == "") {
           	pCP.nList1[0][1] = pSauce;
         }
         else {
            pCP.nList1[0][1] = "";
         }	             	               	                        	           	  
	         
         pSauce = "";
         
         tmpNum = parseInt(pNtxt[9].innerHTML);
         pCP.nList1[1][0] = tmpNum; 
           
         for (j=69; j<72; j++) {
		   	if (pSel[j].style.color == 'rgb(0, 136, 170)') {
		   	   pSauce += pSel[j].innerHTML + " ";		   	          	      
		      }                  
         }
         if (!pSauce == "") {
           pCP.nList1[1][1] = pSauce;
         }
         else {
           	pCP.nList1[1][1] = "";
         }	             	               	                        	           	  
                  
       //clearing out the number boxes;         
         pNtxt[8].innerHTML = 0;
         pNtxt[9].innerHTML = 0;		

	   	for (j=69; j<72; j++) {
        	//Clearing out data
			  pSel[j].style.background = '#333';
			  pSel[j].style.border = "";
			  pSel[j].style.color = '#999';     	  
			  pSel[j].style.textShadow = "";				          	
	   	}
         pCP.nug = true;         
         	   				  					  					 	     						      	 
    	   break;

// //////////////////////////////////////////////////      	    
    	 case "WINGS" :
    	   
         var wAry1 = [" 5W", " 10W", " 15W", " 20W", " 25W", " 30W"];
         var tmpNum = 0;
         
			for (var i=2; i<8; i++) {
           tmpNum = parseInt(pNtxt[i].innerHTML) ;
           if (tmpNum !== 0) {
           	 pProceed = true;
           }				
			}
			
			if (!pProceed) {
				alert("Select a Qty");
				return;
			}
         
                                         	   
			for (var i=2; i<8; i++) {				     

           tmpNum = parseInt(pNtxt[i].innerHTML) ;
           pCP.wList1[i-2][0] = tmpNum; 
           
           pSauce = "";             
           for (j=69; j<72; j++) {
		   	 if (pSel[j].style.color == 'rgb(0, 136, 170)') {
		   	    pSauce += pSel[j].innerHTML + " ";		   	          	      
		   	 }                  
           }
           if (!pSauce == "") {
             pCP.wList1[i-2][1] = pSauce;
           }
           else {
           	 pCP.wList1[i-2][1] = "";
           }	             	               	  
           			  			  			  					  					 	     						  
			}
			
			//clearing out the number boxes;
			for (var i=2; i<8; i++) {
			   pNtxt[i].innerHTML = 0;
			   if (i == 7) {
			   	for (j=69; j<72; j++) {
		        	//Clearing out data
					  pSel[j].style.background = '#333';
					  pSel[j].style.border = "";
					  pSel[j].style.color = '#999';     	  
					  pSel[j].style.textShadow = "";				          	
			   	}	
			   }	
			}
         pCP.wng = true;			
			    	 
    	   break;
    	   
    	 default: break;        
    }
    if(pCP.modify) {
      pCP.modify = false; 
 	  pClear(); 
      pCP.FootState = 11;     	      	
	  pSetFooter();	  
	  MVP();		
	}
	else {	
      pBack();
	}	
}

function pStoreDrinks() {

	var pTitle = document.getElementById("dBoxHead").innerHTML;
	var pSel3 = document.getElementsByClassName("pSel3");
	var pNtxt = document.getElementsByClassName("pNtxt");
	//var pNtxt2 = document.getElementsByClassName("pNtxt2");	
	var pSel= document.getElementsByClassName("pSel");
	var pIceSel= document.getElementsByClassName("pIceSel");	
    var dAry1 = ["PEP", "DIET", "DEW", "DOC", "FP", 
                 "MIST", "TEA", "WATER", "ICE"];		

   var pProceed = false;
   var tmpNum = 0;
   var tmpNum2 = 0;
   var pElm = 0;
   var pIceType = "";
   
/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/
          
	//for (var i=12; i<14; i++) {
     //tmpNum = parseInt(pNtxt[i].innerHTML) ;
     tmpNum = parseInt(pSel3[13].innerHTML);
     tmpNum2 = parseInt(pSel3[22].innerHTML);
     //alert(tmpNum + " " + tmpNum2);
     if (tmpNum !== 0 || tmpNum2 !== 0) { pProceed = true; }				
	//}
			
	if (!pProceed) { alert("Select a Qty"); return; }
	
/**************  END OF CHECKS ***************************/
   
   for (var i=0; i<dAry1.length; i++) {
   	if (dAry1[i] == pTitle) { 
   	  pElm = i; 
   	  break; 
   	}
   }
   
   pCP.dList1[pElm][1] = pSel3[13].innerHTML;
   pCP.dList1[pElm][2] = pSel3[22].innerHTML;
   
   pCP.dList1[pElm][3][0] = pSel3[17].innerHTML;
   pCP.dList1[pElm][3][1] = pSel3[19].innerHTML;
   pCP.dList1[pElm][3][2] = pSel3[21].innerHTML;   	 
   
   pCP.dList1[pElm][4][0] = pSel3[26].innerHTML;
   pCP.dList1[pElm][4][1] = pSel3[28].innerHTML;
   pCP.dList1[pElm][4][2] = pSel3[30].innerHTML;  
   
   // ------------------------------
   //  Reading REG drink number box
   // ------------------------------
   tmpNum = parseInt(pSel3[13].innerHTML);   
  	pCP.dList1[pElm][1] = tmpNum;
   
   	tmpNum2 = parseInt(pSel3[17].innerHTML);    
   	pCP.dList1[pElm][3][0] = tmpNum2;   	 

   	tmpNum2 = parseInt(pSel3[19].innerHTML);    
   	pCP.dList1[pElm][3][1] = tmpNum2;
   	
   	tmpNum2 = parseInt(pSel3[21].innerHTML);    
   	pCP.dList1[pElm][3][2] = tmpNum2;   	   

 //clearing out the ice number boxes;     	  
  	pSel3[13].innerHTML = 0;
  	pSel3[17].innerHTML = 0;
  	pSel3[19].innerHTML = 0;
  	pSel3[21].innerHTML = 0;  	  	     			             	               	                        	           	  
   
   
   // -----------------------------
   //   Reading LG drink number box
   // ----------------------------
   tmpNum = parseInt(pSel3[22].innerHTML);
   pCP.dList1[pElm][2] = tmpNum;
    
   tmpNum2 = parseInt(pSel3[26].innerHTML);
   pCP.dList1[pElm][4][0] = tmpNum2;
      	 
   tmpNum2 = parseInt(pSel3[28].innerHTML);    
   pCP.dList1[pElm][4][1] = tmpNum2;
   	
   tmpNum2 = parseInt(pSel3[30].innerHTML);    
   pCP.dList1[pElm][4][2] = tmpNum2;    
     	  
 //clearing out the ice number boxes;     	  
  	pSel3[22].innerHTML = 0;
  	pSel3[26].innerHTML = 0;
  	pSel3[28].innerHTML = 0;
  	pSel3[30].innerHTML = 0; 
   
   pCP.drk = true;
   
   if (pCP.modify) {
     pCP.modify = false; 
	 pClear(); 
     pCP.FootState = 11;     	      	
	 pSetFooter();	  
	 MVP(); 
   }	  
   else {       		             	               	                        	           	  
     pBack();
   }                 	        	
}

function pStoreSandwhiches() {
	
	var pSel= document.getElementsByClassName("pSel");
	var pTitle = document.getElementById("dBoxHead").innerHTML;
	var pMyNum;
	var pMyNum2;
	var pProceed = false;

	
// check the txt box to see if it has a value > 0.
// then start coping the selected div's inner text into
// pCP.fList1[i][j] array
  //alert(pCP.ScrAry1[pCP.stOutr][pCP.stInner]);
   if (pCP.modify) {
	  pMyNum =  pCP.Mcnt2;   
   }
   else {			
      for (var i=0; i<pCP.sList1.length; i++) {
         if(pCP.sList1[i][0] == "") {
            pMyNum = i;         
            break;     
         }
      }
   }   

   if (pMyNum >7) {
   	alert("Have reached max sandwhich count for this order");	
	}	
   else if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {

/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/       	
   	for (var i=17; i<29; i++) {
   	   if (pSel[i].style.color == 'rgb(0, 136, 170)') {   	     
   	      pProceed = true;
   	   }      	      	   	   	       		   
   	}   	   	
   	
   	if (parseInt(document.getElementById("pTxt1").innerHTML) == 0 ) {
   	   alert("Check Qty");
   	   return;
      }
      
      else if (!pProceed) {
         alert("You must select food items");
         return;      
      }
/**************  END OF CHECKS ***************************/      
      	
   	else {	
   	  pCP.sList1[pMyNum][0] = document.getElementById("pTxt1").innerHTML;
   	  pCP.sList1[pMyNum][1] = pTitle;   	  
   	  pMyNum2 = 2;
   	
   	  for (var i=17; i<29; i++) {
   	     if (pSel[i].style.color == 'rgb(0, 136, 170)') {
   	        pCP.sList1[pMyNum][pMyNum2] = pSel[i].innerHTML;
   	        pMyNum2++;   	      
   	     }      	      	   	   	       		   
   	  }
   	  pCP.snwh = true 
   	
        for (var j=17; j<29; j++) {
        	//Clearing out data
			  pSel[j].style.background = '#333';
			  pSel[j].style.border = "";
			  pSel[j].style.color = '#999';     	  
			  pSel[j].style.textShadow = "";	
        }
        //pCP.stInner--;
        if (pCP.modify) {
          pCP.modify = false;
          pCP.snwhMod = false; 
	      pClear(); 
          pCP.FootState = 11;     	      	
	      pSetFooter();	  
	      MVP();		   
		}
		else {
          pBack();
        }          		
	  }
	}
	  
	else {
			
// -- HOT DOG SECTION
	                   	
    	//pMyNum = document.getElementById("pTxt2").innerHTML;
    	
/**********************************************************   
    Checks to make sure user has entered all 
    the correct data before moving foward
 **********************************************************/       	
   	for (var i=31; i<35; i++) {
   	   if (pSel[i].style.color == 'rgb(0, 136, 170)') {   	     
   	      pProceed = true;
   	   }      	      	   	   	       		   
   	}   	   	
   	
   	if (parseInt(document.getElementById("pTxt2").innerHTML) == 0 ) {
   	   alert("Check Qty");
   	   return;
      }
      
      else if (!pProceed) {
         alert("You must select food items");
         return;      
      }
/**************  END OF CHECKS ***************************/     	

   	pCP.sList1[pMyNum][0] = document.getElementById("pTxt2").innerHTML;
   	pCP.sList1[pMyNum][1] = pTitle;   	
   	pMyNum2 = 2;

		for (var i=31; i<35; i++) {
	     if (pSel[i].style.color == 'rgb(0, 136, 170)') {
	        pCP.sList1[pMyNum][pMyNum2] = pSel[i].innerHTML;
	        pMyNum2++;   	      
	     } 		   					  					 	     						  
		}
		
      for (var j=31; j<35; j++) {
        	//Clearing out data
			  pSel[j].style.background = '#333';
			  pSel[j].style.border = "";
			  pSel[j].style.color = '#999';     	  
			  pSel[j].style.textShadow = "";	
      }
      pCP.snwh = true
      
      if (pCP.modify) {
         pCP.modify = false;
         pCP.snwhMod = false; 
	     pClear(); 
         pCP.FootState = 11;     	      	
	     pSetFooter();	  
	     MVP();		   
	  }
	  else {
         pBack();
      }      
      //pBack();     			            	       	           
	}
}

function ReadDrinks() {
	
	var pTitle;
	var dSplit;
	var pSel3 = document.getElementsByClassName("pSel3");
	//var pNtxt2 = document.getElementsByClassName("pNtxt2");	

   var pElm = 0;	  
   
   if (pCP.modify){
	  document.getElementById("dBoxHead").innerHTML = pCP.dSelected;
	  pTitle = document.getElementById("dBoxHead").innerHTML;
   }
   else {
	  pTitle = document.getElementById("dBoxHead").innerHTML;	   
   }
   
   for (var i=0; i<pCP.dList1.length; i++) {
   	if (pCP.dList1[i][0] == pTitle) { 
   	  pElm = i; 
   	  break; 
   	}
   }
   
   pSel3[13].innerHTML = pCP.dList1[pElm][1];
   pSel3[22].innerHTML = pCP.dList1[pElm][2];
		
   pSel3[17].innerHTML = pCP.dList1[pElm][3][0];
   pSel3[19].innerHTML = pCP.dList1[pElm][3][1];
   pSel3[21].innerHTML = pCP.dList1[pElm][3][2];   	 
   
   pSel3[26].innerHTML = pCP.dList1[pElm][4][0];
   pSel3[28].innerHTML = pCP.dList1[pElm][4][1];
   pSel3[30].innerHTML = pCP.dList1[pElm][4][2];      
   	
}

function ReadFries() {	
	
	document.getElementById("pTxt11").innerHTML = pCP.fList1[0];
	document.getElementById("pTxt12").innerHTML = pCP.fList1[1];			
}

function ReadNuggets() {

    var testStr;
    
	document.getElementById("pTxt9").innerHTML = pCP.nList1[0][0];
    document.getElementById("pTxt10").innerHTML = pCP.nList1[1][0];
    
    if(pCP.nList1[0][1] !== ""){
	   testStr = pCP.nList1[0][1].split(" ");
	   
	   if(testStr.length > 2) {
		   for(var i=0; i<testStr.length; i++){
			   switch(testStr[i]){
				   case "BBQ":
				     document.getElementById("pS70").style.background = '#333';   	  	
				     document.getElementById("pS70").style.border = '4px solid #08A';    	  
				     document.getElementById("pS70").style.color = '#08A';
				     document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "HON_MUST":
				     document.getElementById("pS71").style.background = '#333';   	  	
				     document.getElementById("pS71").style.border = '4px solid #08A';    	  
				     document.getElementById("pS71").style.color = '#08A';
				     document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "RANCH":
				     document.getElementById("pS72").style.background = '#333';   	  	
				     document.getElementById("pS72").style.border = '4px solid #08A';    	  
				     document.getElementById("pS72").style.color = '#08A';
				     document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
				     break;
				   default: break;  				      				      
			   }
		   }
	   }
	   else{
		   switch(testStr[0]){
			 case "BBQ":
			   document.getElementById("pS70").style.background = '#333';   	  	
			   document.getElementById("pS70").style.border = '4px solid #08A';    	  
			   document.getElementById("pS70").style.color = '#08A';
			   document.getElementById("pS70").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "HON_MUST":
			   document.getElementById("pS71").style.background = '#333';   	  	
			   document.getElementById("pS71").style.border = '4px solid #08A';    	  
			   document.getElementById("pS71").style.color = '#08A';
			   document.getElementById("pS71").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "RANCH":
			   document.getElementById("pS72").style.background = '#333';   	  	
			   document.getElementById("pS72").style.border = '4px solid #08A';    	  
			   document.getElementById("pS72").style.color = '#08A';
			   document.getElementById("pS72").style.textShadow = '1px 1px 1px #FFF';
			   break;
			 default: break;  				      				      
		   }		    
	   }
	}
	
}

function ReadWings() {
	
	var pNtxt = document.getElementsByClassName("pNtxt");
	var pSel =  document.getElementsByClassName("pSel");
    var testStr;
    
    for (var i=2; i<8; i++) {
		pNtxt[i].innerHTML = pCP.wList1[i-2][0]
	}
	
    if(pCP.wList1[0][1] !== ""){
	   testStr = pCP.wList1[0][1].split(" ");
	   
	   if(testStr.length > 2) {
		   for(var i=0; i<testStr.length; i++){
			   switch(testStr[i]){
				   case "BBQ":
				     pSel[69].style.background = '#333';   	  	
				     pSel[69].style.border = '4px solid #08A';    	  
				     pSel[69].style.color = '#08A';
				     pSel[69].style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "HON_MUST":
				     pSel[70].style.background = '#333';   	  	
				     pSel[70].style.border = '4px solid #08A';    	  
				     pSel[70].style.color = '#08A';
				     pSel[70].style.textShadow = '1px 1px 1px #FFF';
				     break;
				   case "RANCH":
				     pSel[71].style.background = '#333';   	  	
				     pSel[71].style.border = '4px solid #08A';    	  
				     pSel[71].style.color = '#08A';
				     pSel[71].style.textShadow = '1px 1px 1px #FFF';
				     break;
				   default: break;  				      				      
			   }
		   }
	   }
	   else{
		   switch(testStr[0]){
			 case "BBQ":
			   pSel[69].style.background = '#333';   	  	
			   pSel[69].style.border = '4px solid #08A';    	  
			   pSel[69].style.color = '#08A';
			   pSel[69].style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "HON_MUST":
			   pSel[70].style.background = '#333';   	  	
			   pSel[70].style.border = '4px solid #08A';    	  
			   pSel[70].style.color = '#08A';
			   pSel[70].style.textShadow = '1px 1px 1px #FFF';
			   break;
			 case "RANCH":
			   pSel[71].style.background = '#333';   	  	
			   pSel[71].style.border = '4px solid #08A';    	  
			   pSel[71].style.color = '#08A';
			   pSel[71].style.textShadow = '1px 1px 1px #FFF';
			   break;
			 default: break;  				      				      
		   }		    
	   }
	}		
}

function ReadSandwhiches() {
	
	var pSel= document.getElementsByClassName("pSel");	
    document.getElementById("dBoxHead").innerHTML = pCP.sList1[pCP.Mcnt2][1];
    
    for (var i=2; i<9; i++) {
	  if (pCP.sList1[pCP.Mcnt2][i] !== "") {
		for (var j=17; j<29; j++) {	
		   if (pCP.sList1[pCP.Mcnt2][i] == pSel[j].innerHTML) {
			  pTogState(j);
			  break;
		   }	
		}
	  }
	  else { break; }
	}
	pCP.snwhMod = true;	
}

function pMstrModify() {
   
   pClear();

   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
      document.getElementById("pSelector3").style.display = "block";
      
      pCP.FootState = 12;     	      	
	  pSetFooter();                 
      ReadSandwhiches();            
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
      pCP.modify = true;   	        
      pCP.FootState = 10;     	      	
	  pSetFooter();              
      ReadDrinks(); 		   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
      document.getElementById("pSelector12").style.display = "block";
      pCP.modify = true;   	        
      pCP.FootState = 10;     	      	
	  pSetFooter(); 
	  ReadWings();        	
   	
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
      document.getElementById("pSelector12").style.display = "block";      
      pCP.modify = true;   	        
      pCP.FootState = 10;     	      	
	  pSetFooter(); 
      ReadNuggets();       	
   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
      pCP.modify = true;   	        
      pCP.FootState = 10;     	      	
	  pSetFooter(); 
      ReadFries();      	
   }  
}

function pUpdate() {
	
   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
	  pCP.modify = true;
      pCP.ScrAry1[pCP.stOutr=0][pCP.stInner=3];
      pStoreSandwhiches();
      pCP.ScrAry1[pCP.stOutr=0][pCP.stInner=1];                 
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
	  pCP.modify = true;              
      pStoreDrinks(); 		   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
 	  pCP.modify = true;            	         
	  pStoreFriedItems();
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
  	  pCP.modify = true;            	         
	  pStoreFriedItems();      	
   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
 	  pCP.modify = true;            	         
	  pStoreFriedItems();     	
   }	
}

/***********************************************
 ***********************************************/

function WinSet(id) {
   var pWSide = document.getElementsByClassName("pWSide");
   
   for (var i=0; i<pWSide.length; i++) {   	
     if (pWSide[i].id == id ) {
     	  
     	  // Clear old data
     	  pWSide[0].style.background = '#333';
     	  pWSide[0].style.border = "";
     	  pWSide[0].style.color = '#999';     	  
     	  pWSide[0].style.textShadow = "";	
     	       	  
     	  pWSide[1].style.background = '#333';
     	  pWSide[1].style.border = "";     	  
     	  pWSide[1].style.color = '#999';     	  
     	  pWSide[1].style.textShadow = "";
     	  
     	  if (i<1) { 
     	    pWSide[1].style.background = '#777'; 
     	  }
     	  else { 
     	    pWSide[0].style.background = '#777'; 
     	  }
     	   
     	  pWSide[i].style.border = '4px solid #08A';    	  
     	  pWSide[i].style.color = '#08A';
     	  pWSide[i].style.textShadow = '1px 1px 1px #FFF';	
     }
   }  
}

/***********************************************
 ***********************************************/

function SelWin() {
   var pWSide = document.getElementsByClassName("pWSide");
   
   for (var i=0; i<pWSide.length; i++) {
   	if (pWSide[i].style.color == 'rgb(0, 136, 170)') {
   		pCP.WinSide = pWSide[i].innerHTML;
   		break;	   		
   	}
   }
   
   pCP.Mcnt++;
   pClear();
   pCP.stOutr=0;
   pCP.stInner=1;   
    
   document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.Mcnt];     
   document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";
   pCP.FootState = 2;     	      	
   pSetFooter();
		  	       	
}

/***********************************************
 ***********************************************/
function SelViews(id, Mcnt, outr, innr) {
	
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3"); 
	var winTitle;
	var tmpHold;
	var pProceed = false;
	var pFtitle = ["HOT DOG", "NUGGETS", "WINGS", "FRIES"];
	
	for (var i=0; i<pSel.length; i++) {
	  if (pSel[i].id == id) {
	  	  //alert(pSel[i].innerHTML);
        document.getElementById("dBoxHead").innerHTML = pSel[i].innerHTML;        
        break;	      	
	  }
	} 
	
	pCP.Mcnt = Mcnt;
    pCP.stOutr = outr;
    pCP.stInner = innr;	

    pClear();
	
   if (pCP.ScrAry1[outr][innr] == "pSelector3") {
	 document.getElementById("" + pCP.ScrAry1[outr][innr]).style.display = "block";
     pCP.FootState = 7;     	      	
	 pSetFooter();
	         
     for (var i=18; i<29; i++) {
       if (i !== 25) {
	      pSel[i].style.float = "left";
	      pSel[i].style.width = 72 + "%";
	   }
	   else {     	
	     pSel[i].style.float = "left";
	     pSel[i].style.width = 100 + "%";
	     pSel[i].style.cursor = "default";	     	
	   }  	
	 }
	 var pCnt = 7;
	 for (var i=25; i<29; i++) {	  	  
	    pSel[i].style.display = "none";
	    if (pCnt < 10) {
	      pSel3[pCnt].style.display = "none";
	      pCnt++;
	    }  		     
	 }    	            	     
   }
   else {
	   //alert(innr);
	   if (innr == 2) {	   	
	   	for (var i=0; i<pFtitle.length; i++) {
           if (document.getElementById("dBoxHead").innerHTML == pFtitle[i]) {
             pCP.FootState = 4;     	      	
	          pSetFooter();             
             document.getElementById("" + pCP.ScrAry1[outr][innr]).style.display = "block";
             
             if (pFtitle[i] == "HOT DOG") {
				   for (var i=32; i<35; i++) {				     
					  pSel[i].style.float = "left";
					  pSel[i].style.width = 72 + "%";					  					 	     						  
				   }            	
             }	           
             
             if (pFtitle[i] == "NUGGETS" || pFtitle[i] == "WINGS") {
               document.getElementById("" + pCP.ScrAry1[outr][innr+1]).style.display = "block";
             }  

             pProceed = true;           	 	 	          
 	          break;           	  
           }
	   	}
	   	if (!pProceed) {
             pCP.FootState = 3;     	      	
	          pSetFooter();
	   	}
	   }
	   else if (innr == 3) {
		  //ReadDrinks();
          //document.getElementById("pSelector10").innerHTML = "";
		  pDrinkHTML();		           		   	
		  ReadDrinks();
		           		   	
          pCP.FootState = 4;     	      	
	      pSetFooter();         
          document.getElementById("" + pCP.ScrAry1[outr][innr]).style.display = "block";		   	
 	   } 		
   }
	
   document.getElementById("" + pCP.ScrAry1[outr][innr]).style.display = "block";   
   pCP.TitleAry1[pCP.stInner] = document.getElementById("dBoxHead").innerHTML; 
		
}

function pDrinkHTML() {
	
// (Reg) HTML section
//=====================	
	document.getElementById("pSelector10").innerHTML = "";
	document.getElementById("pSelector10").innerHTML = '<div class="pSel3" id="pTxt13" onclick="pChangeVal(this.id)">0</div>';
	
	document.getElementById("pTxt13").style.float = "left";
	document.getElementById("pTxt13").style.width = 20 + "%";
	document.getElementById("pTxt13").style.color = "#000";
	document.getElementById("pTxt13").style.background = "#FFF";				
			
	document.getElementById("pSelector10").innerHTML += '<div class="pSel3" id="pS63" onclick="pTxtDis(this.id)">Reg</div>';
	document.getElementById("pS63").style.float = "left";
	document.getElementById("pS63").style.width = 51 + "%";	
	
	document.getElementById("pSelector10").innerHTML += '<div class="pSel3" id="bt1" onclick="pIce(this.id)">ICE</div>';
	document.getElementById("bt1").style.width = 25 + "%";
	
	document.getElementById("pSelector10").innerHTML += '<div id="pSelector17"></div>';
	document.getElementById("pSelector17").innerHTML = '<div class="pSel3" id="pTxtEx1" onclick="pTxtDis(this.id)">Ex&nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector17").innerHTML += '<div class="pSel3" id="pTxt15" onclick="pChangeVal(this.id)">0</div>';
	
	document.getElementById("pSelector17").innerHTML += '<div class="pSel3" id="pTxtLt1" onclick="pTxtDis(this.id)">Lt &nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector17").innerHTML += '<div class="pSel3" id="pTxt16" onclick="pChangeVal(this.id)">0</div>';
	document.getElementById("pSelector17").innerHTML += '<div class="pSel3" id="pTxtNo1" onclick="pTxtDis(this.id)">No&nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector17").innerHTML += '<div class="pSel3" id="pTxt17" onclick="pChangeVal(this.id)">0</div>';

	
// (Reg) CSS section
//====================		
 	document.getElementById("pTxtEx1").style.float = "left";
	document.getElementById("pTxtEx1").style.width = 79 + "%";
	document.getElementById("pTxtEx1").style.textAlign = "right";		
	
	document.getElementById("pTxt15").style.float = "left";
	document.getElementById("pTxt15").style.width = 18 + "%";
	document.getElementById("pTxt15").style.color = "#000";
	document.getElementById("pTxt15").style.background = "#FFF";
	
 	
 	document.getElementById("pTxtLt1").style.float = "left";
	document.getElementById("pTxtLt1").style.width = 79 + "%";
	document.getElementById("pTxtLt1").style.textAlign = "right";	
	
	
	document.getElementById("pTxt16").style.float = "left";
	document.getElementById("pTxt16").style.width = 18 + "%";
	document.getElementById("pTxt16").style.color = "#000";
	document.getElementById("pTxt16").style.background = "#FFF";
	
 	
 	document.getElementById("pTxtNo1").style.float = "left";
	document.getElementById("pTxtNo1").style.width = 79 + "%";
	document.getElementById("pTxtNo1").style.textAlign = "right";	
	
	
	document.getElementById("pTxt17").style.float = "left";
	document.getElementById("pTxt17").style.width = 18 + "%";
	document.getElementById("pTxt17").style.color = "#000";
	document.getElementById("pTxt17").style.background = "#FFF";

// ====================================================================

// (LG) HTML section
//=====================	
	document.getElementById("pSelector10").innerHTML += '<div class="pSel3" id="pTxt14" onclick="pChangeVal(this.id)">0</div>';	
	
	document.getElementById("pTxt14").style.float = "left";
	document.getElementById("pTxt14").style.width = 20 + "%";
	document.getElementById("pTxt14").style.color = "#000";
	document.getElementById("pTxt14").style.background = "#FFF";				
			
	document.getElementById("pSelector10").innerHTML += '<div class="pSel3" id="pS64" onclick="pTxtDis(this.id)">LG</div>';
	document.getElementById("pS64").style.float = "left";
	document.getElementById("pS64").style.width = 51 + "%";	
	
	document.getElementById("pSelector10").innerHTML += '<div class="pSel3" id="bt2" onclick="pIce(this.id)">ICE</div>';
	document.getElementById("bt2").style.width = 25 + "%";
	

	document.getElementById("pSelector10").innerHTML += '<div id="pSelector18"></div>';	
	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxtEx2" onclick="pTxtDis(this.id)">Ex&nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxt18" onclick="pChangeVal(this.id)">0</div>';
	
	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxtLt2" onclick="pTxtDis(this.id)">Lt &nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxt19" onclick="pChangeVal(this.id)">0</div>';

	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxtNo2" onclick="pTxtDis(this.id)">No&nbsp&nbsp&nbsp</div>';
	document.getElementById("pSelector18").innerHTML += '<div class="pSel3" id="pTxt20" onclick="pChangeVal(this.id)">0</div>';	

	
// (LG) CSS section
//====================
		
 	document.getElementById("pTxtEx2").style.float = "left";
	document.getElementById("pTxtEx2").style.width = 79 + "%";
	document.getElementById("pTxtEx2").style.textAlign = "right";		
	
	document.getElementById("pTxt18").style.float = "left";
	document.getElementById("pTxt18").style.width = 18 + "%";
	document.getElementById("pTxt18").style.color = "#000";
	document.getElementById("pTxt18").style.background = "#FFF";
	
 	
 	document.getElementById("pTxtLt2").style.float = "left";
	document.getElementById("pTxtLt2").style.width = 79 + "%";
	document.getElementById("pTxtLt2").style.textAlign = "right";	
	
	
	document.getElementById("pTxt19").style.float = "left";
	document.getElementById("pTxt19").style.width = 18 + "%";
	document.getElementById("pTxt19").style.color = "#000";
	document.getElementById("pTxt19").style.background = "#FFF";
	
 	
 	document.getElementById("pTxtNo2").style.float = "left";
	document.getElementById("pTxtNo2").style.width = 79 + "%";
	document.getElementById("pTxtNo2").style.textAlign = "right";	
	
	
	document.getElementById("pTxt20").style.float = "left";
	document.getElementById("pTxt20").style.width = 18 + "%";
	document.getElementById("pTxt20").style.color = "#000";
	document.getElementById("pTxt20").style.background = "#FFF";

	document.getElementById("pSelector10").style.display = "block";


	var pSel3 = document.getElementsByClassName("pSel3");

/*	
	for(var i=0; i<pSel3.length; i++){
		if(pSel3[i].id == "pTxt14"){
			alert(i);
			break;
		}
	}
*/
}

/***********************************************
 ***********************************************/
 
 function pBack() {
 	
   var pWSide = document.getElementsByClassName("pWSide");
	                    	
   var innr = pCP.stInner--;

   pClear();

   if (pCP.stInner == 0) {
     
     pCP.Mcnt=0;
     document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";           
     pWSide[1].style.background = '#777';
     pWSide[0].style.border = '4px solid #08A';    	  
     pWSide[0].style.color = '#08A';
     pWSide[0].style.textShadow = '1px 1px 1px #FFF';     
     
     document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.Mcnt];
     pCP.FootState = 1;     	      	
	  pSetFooter();
	}     	
   else {
     if (pCP.ScrAry1[pCP.stOutr][pCP.Inner] !== "pSelector3") {
     	  //alert(innr);
     	  if(innr == 3) {
     	     //document.getElementById("dBoxFoot").innerHTML = pFootSet[1];
           pCP.FootState = 3;     	      	
	        pSetFooter();      	       
     	  }
     	  else if (innr == 2) {
     	  	  //document.getElementById("dBoxFoot").innerHTML = pFootSet[0] + pFootSet[1];
           pCP.FootState = 2;     	      	
	        pSetFooter();     	  	    
     	  }	
     }   		
     document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";
     document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];
   }
   pCP.ShowExtra = false;          
 }
 
 function pBack2() {
 	pClear();
 	
 	if (pCP.stInner == 1) {
     pCP.FootState = 2;     	      	
	  pSetFooter();
	}
	else if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
     pCP.FootState = 7;     	      	
	  pSetFooter(); 		
	}   	 		
 	else {
     pCP.FootState = 4;     	      	
	  pSetFooter(); 	 		 		
 	}	
 	document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";

 }
 
/***********************************************
 ***********************************************/
 
 function pClear() {
   document.getElementById("wCon").style.display = "none";
   for (var i=1; i<21; i++) {
     document.getElementById("pSelector" + i).style.display = "none";
   }
     
 }
 
 function pClear2() {
  
   var pWSide = document.getElementsByClassName("pWSide");
   	 
   pCP.stOutr = 0;
   pCP.stInner = 1;
   pCP.Mcnt = 0;
   pCP.Mcnt2 = 0;
   pCP.Scnt = 0;
   pCP.StoredId = 0;
   pCP.FootState = 0;       
   pCP.BoxID = "";
   pCP.dSelected = "";       
   pCP.ShowExtra = false;
   pCP.snwh = false;
   pCP.snwhMod = false;
   pCP.nug = false;
   pCP.wng = false;
   pCP.fry = false;
   pCP.drk = false;
   pCP.modify = false;
   
/* Clearing out screen display */      
   for (var i=0; i<7; i++) {
     for (var j=0; j<6; j++) {
     	 pCP.ScrAry1[i][j]="";
     }
     pCP.TitleAry1[i] = "";	
   }
   
/* Clearing out sandwhiches */      
   for (var i=0; i<9; i++) {
     for (var j=0; j<11; j++) {
     	 pCP.sList1[i][j]="";
     }	
   }
   
/* Clearing out Drinkss */

   for(var i=0; i<pCP.dList1.length; i++) {
	 for(var j=1; j<5; j++){
	   if(j<3) {	 
	     pCP.dList1[i][j] = 0;
	   }
	   else {
		  for(var x=0; x<3; x++) {
			pCP.dList1[i][j][x] = 0;
		  }		   
	   }  
     }  
   }
   
/* Creating and populating an array for fries */         
   pCP.fList1[0] = 0;
   pCP.fList1[1] = 0;   	

/* Creating and populating an 2x2 array for nuggets */  
   for(var i=0; i<2; i++) {
     for(var j=0; j<2; j++) {
     	 pCP.nList1[i][j]="";
     }	
   }        
   
      
/* Creating and populating an 9x2 array for wings */
   for (var i=0; i<6; i++) {
     for (var j=0; j<2; j++) {
     	 pCP.wList1[i][j]="";
     }	
   }
   
   if(pCP.OrdCurCnt == 0) {
      pCP.MultiOrder = 0;
      pCP.DisCnt = 0;
      pCP.OrdCurCnt = 0;
      pCP.CarType = "";
      pCP.TxtNum = "";	   
   }   
          	 
 } 
 
/***********************************************
 ***********************************************/

function TogItem(id) {
	
	var pSel= document.getElementsByClassName("pSel");
	var pStart = 0; 
	var pEnd = 0;
	
	for (var i=0; i<pSel.length; i++) {
	  if (pSel[i].id == id) {
	  	  switch(pSel[i].innerHTML) {
	  	  	
           case "REG":
              //alert(i);
              switch (i) {
              	
           // ** BURGERS / REG (CHICKEN/GRILL) **
              	  case 16:
		              for (var j=17; j<25; j++) {
		              	//Clearing out data
							  pSel[j].style.background = '#333';
							  pSel[j].style.border = "";
							  pSel[j].style.color = '#999';     	  
							  pSel[j].style.textShadow = "";	
		              }
		              
		              if (document.getElementById("dBoxHead").innerHTML == "REG CK" ||
		                  document.getElementById("dBoxHead").innerHTML == "GRILL") {
		                 
		                 var ViewAry1 = [18, 19, 23];
		                 
		                 for (var j=0; j<ViewAry1.length; j++) {
	                    	  pSel[ViewAry1[j]].style.background = '#333';   	  	
	   		              pSel[ViewAry1[j]].style.border = '4px solid #08A';    	  
	     	                 pSel[ViewAry1[j]].style.color = '#08A';
	     	                 pSel[ViewAry1[j]].style.textShadow = '1px 1px 1px #FFF';
	                    } 	   	                      	                 
		              	  
		              }
		              else if (document.getElementById("dBoxHead").innerHTML == "CLUB" ||
		                       document.getElementById("dBoxHead").innerHTML == "GRILL CLUB") {
		                 
		                 var ViewAry1 = [18, 19, 23, 26, 27];
		                 
		                 for (var j=0; j<ViewAry1.length; j++) {
	                    	  pSel[ViewAry1[j]].style.background = '#333';   	  	
	   		              pSel[ViewAry1[j]].style.border = '4px solid #08A';    	  
	     	                 pSel[ViewAry1[j]].style.color = '#08A';
	     	                 pSel[ViewAry1[j]].style.textShadow = '1px 1px 1px #FFF';
	                    }
	                 }    		                       		
		              
		              else {  
		                          	  
	                    for (var j=18; j<25; j++) {
	                    	  pSel[j].style.background = '#333';   	  	
	   		              pSel[j].style.border = '4px solid #08A';    	  
	     	                 pSel[j].style.color = '#08A';
	     	                 pSel[j].style.textShadow = '1px 1px 1px #FFF';
	                    }
	                }    
                  break;

           // ** HOT DOG **                                 	  
              	  case 30:
		              for (var j=31; j<35; j++) {
		              	//Clearing out data
							  pSel[j].style.background = '#333';
							  pSel[j].style.border = "";
							  pSel[j].style.color = '#999';     	  
							  pSel[j].style.textShadow = "";	
		              }  
		                          	  
                    for (var j=32; j<35; j++) {
                    	  pSel[j].style.background = '#333';   	  	
   		              pSel[j].style.border = '4px solid #08A';    	  
     	                 pSel[j].style.color = '#08A';
     	                 pSel[j].style.textShadow = '1px 1px 1px #FFF';
                    }

                  break;              	  

           // ** CHICKEN **              	  
              	  case 28:

              	   break;
              	   
              	  default: break;              	    
              } 
             break;
           
           case "PLAIN":
              //alert(i);
              switch(i) {
              	  case 17:
              	     pStart = 18;
              	     pEnd = 29;
              	   break;
              	   
              	  case 31:
              	     pStart = 32;
              	     pEnd = 35;              	  
              	   break;
              	   
              	  case 29:
              	     pStart = 18;
              	     pEnd = 29;              	  
              	   break;
              	   
              	  default: break;              	                 	                 	   
              }
           
              for (var j=pStart; j<pEnd; j++) {
              	//Clearing out data
					  pSel[j].style.background = '#333';
					  pSel[j].style.border = "";
					  pSel[j].style.color = '#999';     	  
					  pSel[j].style.textShadow = "";	
              }
              pStart = 60;
              pEnd = 67;
           
              for (var j=pStart; j<pEnd; j++) {
              	//Clearing out data
					  pSel[j].style.background = '#333';
					  pSel[j].style.border = "";
					  pSel[j].style.color = '#999';     	  
					  pSel[j].style.textShadow = "";	
              }              
                           	  
              pTogState(i);           
              break;
               
           default: 
              pTogState(i);            
              break;                	  	  	
	  	  }
	  	  
	
	  }
	}	
}

/***********************************************
 ***********************************************/
 
function pTogState(pStatus) {

	var pSel= document.getElementsByClassName("pSel");
   
	if (pSel[pStatus].style.color == 'rgb(0, 136, 170)') {
	
	  pSel[pStatus].style.background = '#333';
	  pSel[pStatus].style.border = "";
	  pSel[pStatus].style.color = '#999';     	  
	  pSel[pStatus].style.textShadow = "";	     	     	   		
	}
	else {
	  pSel[pStatus].style.background = '#333';   	  	
	  pSel[pStatus].style.border = '4px solid #08A';    	  
	  pSel[pStatus].style.color = '#08A';
	  pSel[pStatus].style.textShadow = '1px 1px 1px #FFF';
	}	   	
}

/***********************************************
 ***********************************************/
 
function ShowExtra() {	
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3");
	var pCnt; 
	
	if(pCP.snwhMod) {
       if (!pCP.ShowExtra) {
		   pCnt = 7
		  for (var i=25; i<29; i++) {	  	  
		     pSel[i].style.display = "block";
		     if (pCnt < 10) {
		       pSel3[pCnt].style.display = "block";
		       pCnt++;
		     }  		     
		  }
		  pCP.ShowExtra = true;
          pCP.FootState = 13;     	      	
	      pSetFooter();                 
          		  		   
	   }
	   else {
		  pCnt = 7;
		  for (var i=25; i<29; i++) {	  	  
		     pSel[i].style.display = "none";
		     if (pCnt < 10) {
		       pSel3[pCnt].style.display = "none";
		       pCnt++;
		     }  		     
		  }
		  pCP.ShowExtra = false;
		  pCP.FootState = 12;
		  pSetFooter();
		  		   
	   }	   
	}	 
	else if (!pCP.ShowExtra) {
	  if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
		  pCnt = 7;
		  for (var i=25; i<29; i++) {	  	  
		     pSel[i].style.display = "block";
		     if (pCnt < 10) {
		       pSel3[pCnt].style.display = "block";
		       pCnt++;
		     }  		     
		  } 
	  }	  	

     pCP.FootState = 6;     	      	
	  pSetFooter();      
     pCP.ShowExtra = true;                
	}
	else {

	  if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
		  pCnt = 7;
		  for (var i=25; i<29; i++) {	  	  
		     pSel[i].style.display = "none";
		     if (pCnt < 10) {
		       pSel3[pCnt].style.display = "none";
		       pCnt++;
		     }  		     
		  } 
	  }	  	     

     pCP.FootState = 5;     	      	
	  pSetFooter();           
     pCP.ShowExtra = false;     		 
	}	
}


function ShowExtra2(id) {
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3");
	var pSelAry1 = [18, 19, 20, 21, 22, 23, 
	                24, 26, 27, 28, 32, 33, 34];
	
	pClear();
	document.getElementById("pSelector19").style.display = "block";
	for (var i=0; i<pSel3.length; i++) {
		if (pSel3[i].id == id) {
         pCP.StoredId = pSelAry1[i]; 						
		}
	}	
	pCP.FootState = 9;     	      	
	pSetFooter(); 
}

/***********************************************
 ***********************************************/
 
function pTxtDis(id) {
	var pAry1 = ["pS16", "pS30", "pS44", "pS45", "pS46", "pS47", "pS48", 
	             "pS49", "pS50", "pS51", "pS52", "pS53", "pS63", "pS64",
	             "pTxtEx1", "pTxtLt1", "pTxtNo1", "pTxtEx2", "pTxtLt2", 
	             "pTxtNo2"];
	             
	var pAry2 = ["pTxt1", "pTxt2", "pTxt3",  "pTxt4",  "pTxt5",  "pTxt6",  "pTxt7", 
	             "pTxt8", "pTxt9", "pTxt10", "pTxt11", "pTxt12", "pTxt13", "pTxt14",
	             "pTxt15", "pTxt16", "pTxt17", "pTxt18", "pTxt19",
	             "pTxt20"];
	
	var pcntr = 0;             
	             
	for (var i=0; i<pAry1.length; i++) {
      if (pAry1[i] == id) {
         pcntr = parseInt(document.getElementById("" + pAry2[i]).innerHTML);         
         pcntr++;
         document.getElementById("" + pAry2[i]).innerHTML = pcntr;
         break;
      }	 	
	}             
} 

function pNumKey(id) {
	var NumData= document.getElementsByClassName('pData');
   var GetDispay = document.getElementById('pDisplay').innerHTML;
     
   for (var i=0; i< NumData.length; i++) {
      if (NumData[i].id == id) {
        if (NumData[i].innerHTML == "BS" || NumData[i].innerHTML == "Clr") {
        	 	 //alert("Special pushed");
           document.getElementById('pDisplay').innerHTML = "0";        	    
        }
        else {
        	 if (GetDispay == "0") {        	 	 	
        	   document.getElementById('pDisplay').innerHTML = NumData[i].innerHTML;
        	 }
        	 else {
        	    document.getElementById('pDisplay').innerHTML += NumData[i].innerHTML;        	 	 	
        	 }   
        }
        break;
      }	 	
  }
  	  
}

function pChangeVal(id) {
	
	var IceTxtBx = document.getElementsByClassName("pNtxt2");	
   var RegTxtBx = document.getElementById("pTxt13").innerHTML;
   var LGTxtBx = document.getElementById("pTxt14").innerHTML;
   var pProceed = false;
      
   alert(id);    	
	for (var i =0; i<IceTxtBx.length; i++) {
		//alert(IceTxtBx[i].id);

		if (IceTxtBx[i].id == id) {
			//alert(i);
			if (i == 3){
				if( RegTxtBx < 1 ) {
				  //alert("here");
				  pProceed = true; break;
				}
				else {
				  pCP.BoxID = id;
             // showDrink();					 
				  pProceed = false; 
				  break;
				}
			}
			else {
				if( LGTxtBx < 1 ) {				  
				  pProceed = true; break;
				}
				else {
				  pCP.BoxID = id;				  
				  //showDrink();	 
				  pProceed = false; 
				  break;
				}				
			}	
				
		}
	}

   //alert(pProceed);
	if (!pProceed) {
		pCP.BoxID = id;
      showDrink();
	}      	
}

function showDrink() {
   pClear();
   document.getElementById("pSelector13").style.display = "block";
   document.getElementById("dBoxHead").innerHTML = "QTY";
   document.getElementById('pDisplay').innerHTML = 0;
   pCP.FootState = 8;     	      	
	pSetFooter();       
      	
}

function pStoreKeyPadAns() {
	
	var DisTitle = document.getElementById("dBoxHead").innerHTML;
	var pValue = document.getElementById('pDisplay').innerHTML;
	var tmpTitle;	
	
	if (DisTitle == "QTY") {
      document.getElementById("" + pCP.BoxID).innerHTML = pValue;
      pClear();
       
      if (pCP.modify) {
		   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
		      document.getElementById("pSelector3").style.display = "block";
		      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
		      //document.getElementById("dBoxHead").innerHTML = "FRIES"; 		      
		      pCP.modify = false;   	        
		      pCP.FootState = 10;     	      	
			   pSetFooter(); 		                  
		   }
		   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
		      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
		      document.getElementById("dBoxHead").innerHTML = pCP.dSelected;		      
		      pCP.modify = false;   	        
		      pCP.FootState = 10;     	      	
			   pSetFooter();  		   	
		   }
		   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
		      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
		      document.getElementById("dBoxHead").innerHTML = "WINGS"; 		      
		      pCP.modify = false;   	        
		      pCP.FootState = 10;     	      	
			   pSetFooter();         	
		   	
		   }      
		   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
		      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
		      document.getElementById("dBoxHead").innerHTML = "NUGGETS"; 		      
		      pCP.modify = false;   	        
		      pCP.FootState = 10;     	      	
			   pSetFooter();        	
		   	
		   }	
		   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
		      document.getElementById("" + pCP.mTotal[pCP.Mcnt2]).style.display = "block";
		      document.getElementById("dBoxHead").innerHTML = "FRIES"; 		      
		      pCP.modify = false;   	        
		      pCP.FootState = 10;     	      	
			   pSetFooter();  	
		   }     	
           	
      }
      else {
	      document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";      
	      document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];
	      
	      if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
	         pCP.FootState = 7;     	      	
		      pSetFooter();	   	
	      }      	      
	      else if(pCP.stInner == 2) {
	      	tmpTitle = document.getElementById("dBoxHead").innerHTML;
	      	
	         if (tmpTitle == "NUGGETS" || tmpTitle == "WINGS") {
	               document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner+1]).style.display = "block";
	         }       	
	         pCP.FootState = 4;     	      	
		      pSetFooter();
		   }
		   else {
			 if(pCP.RgIceState){ pIce("bt1"); }
			 if(pCP.LgIceState) { pIce("bt2"); }
	         pCP.FootState = 4;     	      	
		     pSetFooter();	   	
		   }       
	  }
	}
	else if (DisTitle == "MULTIORDER"){
		pCP.MultiOrder = pValue;
		alert(pCP.MultiOrder);
		
		pClear();
      document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";      
      document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];
       
      pCP.FootState = 2;     	      	
	   pSetFooter();       				
   }
   else {
   	if (pValue.length < 10) {
   		alert("Not enough numbers entered");
   	}	
   	else {			
		   pCP.TxtNum = pValue;
		   alert(pCP.TxtNum);
		   pClear();
         document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";      
         document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];

         pCP.FootState = 2;     	      	
	      pSetFooter();        
	  }	
	}   
 
} 

function pMore() {
	pClear();
   document.getElementById("pSelector14").style.display = "block";
   pCP.FootState = 9;     	      	
	pSetFooter(); 
}

function pCarRender(id) {
	
	pClear();
	if (pCP.CarSel == 0) {
     document.getElementById("dBoxHead").innerHTML = "Car Type: ";     		
     document.getElementById("pSelector15").style.display = "block";
     pCP.CarSel++;
   }
   else if (pCP.CarSel == 1) {		  
     document.getElementById("pSelector16").style.display = "block";
     pCP.CarType = document.getElementById("" + id).innerHTML;
     document.getElementById("dBoxHead").innerHTML += pCP.CarType;     
     pCP.CarSel++;
   }
   else {    	
      pCP.CarType += " " + document.getElementById("" + id).innerHTML;  	
      document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";      
      document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];
      
      pCP.FootState = 2;     	      	
	   pSetFooter();        
      pCP.CarSel = 0; 
      alert(pCP.CarType);  	
   }     	  	
} 

function pMultiOrder() {
	
	pClear();
   document.getElementById("pSelector13").style.display = "block";
   document.getElementById("dBoxHead").innerHTML = "MULTIORDER";
   document.getElementById('pDisplay').innerHTML = 0;
   //document.getElementById("dBoxFoot").innerHTML = "<button class='btn1' onclick='pStoreKeyPadAns()'>OK</button>";
   pCP.FootState = 8;     	      	
	pSetFooter();    	
}

function pTxtRender() {
	pClear();
   document.getElementById("pSelector13").style.display = "block";
   document.getElementById("dBoxHead").innerHTML = "PHONE NUMBER";
   document.getElementById('pDisplay').innerHTML = 0;
   //document.getElementById("dBoxFoot").innerHTML = "<button class='btn1' onclick='pStoreKeyPadAns()'>OK</button>";
   pCP.FootState = 8;     	      	
	pSetFooter();    	
}

function pIce(id) {
	IceSel1 = document.getElementById("pSelector17");
	IceSel2 = document.getElementById("pSelector18");
	
	//pClear();
	if (id == "bt1") {
	   if (IceSel1.style.display == "" || IceSel1.style.display =="none") {	  
        document.getElementById("pSelector17").style.display = "block";
        pCP.RgIceState = true;
      }  
      else {
        document.getElementById("pSelector17").style.display = "none";
        pCP.RgIceState = false;        
      }  
   }
   else {
	   if (IceSel2.style.display == "" || IceSel2.style.display =="none") {	  
        document.getElementById("pSelector18").style.display = "block";
        pCP.LgIceState = true;        
      }  
      else {
        document.getElementById("pSelector18").style.display = "none";
        pCP.LgIceState = false;              	
      }  
  	
   }  
	
}

function pModifyItem(id) {
	var pSel= document.getElementsByClassName("pSel");
	var pSel3= document.getElementsByClassName("pSel3");
	var ptext;
	
	switch(id) {
		
   // Bascially saying cut the sandwhich in half	   
	   case "dBoxHead":
         
         if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
	         ptext = document.getElementById("dBoxHead").innerHTML;	   
		      if ((ptext.search("1/2 ")) == -1) {
	            document.getElementById("dBoxHead").innerHTML = "1/2 " + 
	            pCP.TitleAry1[pCP.stInner];
		      }
		      else {
	         // removing the cut sandwhich in half	
		      	document.getElementById("dBoxHead").innerHTML = pCP.TitleAry1[pCP.stInner];
		      }
		   }   	  	      		   
	      break;
	   
   // Adding Ex to the selected food item	   
	   case "pS74":

         ptext = pSel[pCP.StoredId].innerHTML;
                 
	      if ((ptext.search("Lt ")) == 0) {
	      	ptext = ptext.replace("Lt ", "Ex ");
            pSel[pCP.StoredId].innerHTML = ptext;           	      		      	 
	      }
	      else if ((ptext.search("Ex ")) == -1) {
	      	pSel[pCP.StoredId].innerHTML = "Ex " +
	      	pSel[pCP.StoredId].innerHTML;	      	            	      		      		      	
	      }
	      else {
	         ptext = ptext.replace("Ex ", "");
            pSel[pCP.StoredId].innerHTML = ptext;         	
	      }         	   
	      break;

   // Adding Lt to the selected food item	      
	   case "pS75":

         ptext = pSel[pCP.StoredId].innerHTML;
	      if ((ptext.search("Ex ")) == 0) {
	      	ptext = ptext.replace("Ex ", "Lt ");
            pSel[pCP.StoredId].innerHTML = ptext;            	      		      	
	      } 	                  
	      else if ((ptext.search("Lt ")) == -1) {
	      	pSel[pCP.StoredId].innerHTML = "Lt " +
	      	pSel[pCP.StoredId].innerHTML;	      		      	 
	      }
	      else {
	         ptext = ptext.replace("Lt ", "");
            pSel[pCP.StoredId].innerHTML = ptext;	         	
	      } 	   
	      break;
	      
	   default: break;         	
	}
	
	if (id !== "dBoxHead") {
     document.getElementById("" + pCP.ScrAry1[pCP.stOutr][pCP.stInner]).style.display = "block";
     document.getElementById("pSelector19").style.display = "none";
     if (pCP.ScrAry1[pCP.stOutr][pCP.stInner] == "pSelector3") {
       pCP.FootState = 7;     	      	
	    pSetFooter();
	  }
	  else {
	  	 //alert(pCP.stInner);
       pCP.FootState = 4;     	      	
	    pSetFooter();	  	
	  }        
   }  		
}

function pDelete() {
   
   if(pCP.mTotal[pCP.Mcnt2] == "pSelector20") {
	  pDelMstrList();
      pDelSndWch();
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector10") {
	  pDelMstrList();
	  pDelDrinks(); 		   	
   }
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector6") {
	  pDelMstrList();
	  pDelWings();
   }      
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector7") {
	  pDelMstrList(); 
	  pDelNuggest();    	
   	
   }	
   else if (pCP.mTotal[pCP.Mcnt2] == "pSelector8") {
      pDelMstrList();
      pDelFries();
   }

   if(pCP.mTotal[pCP.Mcnt2] !== "") {
      pCP.Mcnt2=0;
      pClear();
      pViewOrder();
   }
   else {
      pCP.Mcnt2=0;
      pCP.stInner = 2;
      pBack();	   
   }   

}

function pDelFries(){
	pCP.fList1[0] = 0;
	pCP.fList1[1] = 0;
}

function pDelWings(){
	
	for(var i=0; i<pCP.wList1.length; i++){
	   pCP.wList1[i][0] = 0;
	   pCP.wList1[i][1] = "";
	}
}

function pDelNuggest(){

	for(var i=0; i<pCP.nList1.length; i++){
	   pCP.nList1[i][0] = 0;
	   pCP.nList1[i][1] = "";
	}	
}

function pDelDrinks(){

    var pElm;
    	
	if(pCP.dSelected == "") {
	   alert("Select a drink first");
	}
	else {
	   for(var i=0; i<pCP.dList1.length; i++){
		  if(pCP.dSelected == pCP.dList1[i][0]){
			  pElm = i;
		  }
	   }
	   
	   pCP.dList1[pElm][1] = 0;
	   pCP.dList1[pElm][2] = 0;
	
	   for(var i=3; i<5; i++) {
	      for(var x=0; x<3; x++){
		      pCP.dList1[pElm][i][x] = 0;
	      }
	   }
	}
}

function pDelMstrList(){
   var delAry1 = new Array(pCP.mTotal.length);
   var strHld = pCP.mTotal[pCP.Mcnt2];
   var myCnt = 0;	
	
   for(var i=0; i<delAry1.length; i++) {
	  delAry1[i] = "";
   }
   
   pCP.mTotal[pCP.Mcnt2] = "";
   myCnt =0;  
   
   for(var i=0; i<delAry1.length; i++) {
	  if ( pCP.mTotal[pCP.Mcnt2] !== ""){
	    delAry1[myCnt] = pCP.mTotal[i];
	    myCnt++;
	  }  
   }
   
   for(var i=0; i<delAry1.length; i++) {
	  if(delAry1[i] !== "") {
	     pCP.mTotal[i] = delAry1[i];
	  }
	  else {break;}
   }
   
   myCnt = 0;
   for(var i=0; i<pCP.mTotal.length; i++) {
	  if(pCP.mTotal[i] !== "") {
	     myCnt++;
	  }
   }	
}

function pDelSndWch(){
	
      var myCnt2 = 0;
      var pProceed = true;
         
      var delAry2 = new Array(pCP.sList1.length);
      for(var i=0; i<delAry2.length; i++) {
	     delAry2[i] = new Array();
	     for(var j=0; j<10; j++) {
		    delAry2[i][j] = "";
	     }
	  }
    	   
      for (var i=0; i<9; i++) {
	   for(var j=0; j<11; j++) {
	     if(pCP.sList1[i][j] !== ""){
           delAry2[i][j] = pCP.sList1[i][j];
           pCP.sList1[i][j] = "";
         //alert(delAry2[i][j]);
         }
         else{break;}  
       }
      } 
    
      for (var i=0; i<delAry2.length; i++) {
	   //alert("pCP.Mcnt2: " +pCP.Mcnt2 + " and myCnt2: " + myCnt2);
	    if(i !== pCP.Mcnt2) {	
	      for(var j=0; j<delAry2[i].length; j++) {
	         pCP.sList1[myCnt2][j] = delAry2[i][j]; 
	      }
	      myCnt2++;       
        }
      } 	
}

function pSetFooter() {
	var pFootSet = ["<button class='btn1' onclick='SelWin();'>Select</button>",            //   0
	                "<button class='btn1' onclick='pOrderIt()'>Order It</button>",         //   1
	                '<button class="btn1" onclick="pBack()">Back</button>',                //   2
                    '<button class="btn1" onclick="pMore()">More</button>',	               //   3           
	                '<button class="btn1" onclick="ShowExtra()">More</button>',            //   4	                
	                '<button class="btn1" onclick="ShowExtra()">Close</button>',           //   5 
                    "<button class='btn1' onclick='pStoreKeyPadAns()'>OK</button>",        //   6
	                "<button class='btn1' onclick='pBack2()'>Back</button>",               //   7
	                "<button class='btn1' onclick='pViewOrder()'>View Order</button>",      //  8
	                "<button class='btn1' onclick='pMstrModify()'>Modify</button>",         //  9
	                "<button class='btn1' onclick='pPlaceOrder()'>Place Order</button>",   //  10
	                "<button class='btn1' onclick='pUpdate()'>Update</button>",            //  11
	                "<button class='btn1' onclick='pDelete()'>Del</button>",               //  12	                 	                                	                	                
	               ];	
	               
   switch(pCP.FootState) {
  	  
  	  case 1:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[0];  	  
  	     break;
  	     
  	  case 2:
  	     document.getElementById("dBoxFoot").innerHTML = pFootSet[8] + pFootSet[3] + pFootSet[2];
  	     break;
  	     
  	  case 3:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[2];  	     
  	     break;
  	     
  	  case 4:
  	     document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[2];  	  
  	     break;
  	     
  	  case 5:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[4] + pFootSet[2];  	  
  	     break;
  	     
  	  case 6:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[5] + pFootSet[2];  	  
  	     break;
  	     
  	  case 7:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[1] + pFootSet[4] + pFootSet[2];  	  
  	     break;
  	     
  	  case 8:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[6];  	  
  	     break;
  	     
  	  case 9:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[7];  	  
  	     break;  	       	       	       	     
  	  
  	  case 10:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11];  	    	  
  	     break;
  	     
  	  case 11:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[10] + pFootSet[9] + pFootSet[12] + pFootSet[7];  	  
  	     break;

  	  case 12:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11] + pFootSet[4];  	  
  	     break;

  	  case 13:
        document.getElementById("dBoxFoot").innerHTML = pFootSet[11] + pFootSet[5];  	  
  	     break;  	       	        
  	  default: break;  	       	       	       	       	     
  }

}

 
/* When the window loads, call the setup function */
window.addEventListener("load", PhoneSetup, false);