cpFV = {
   fryUID: [],
   drkUID: [],
   nugUID: [],
   wngUDI: [],
   fdTimer: 0   
};

function fvSetup() {
// Start time function will begin the state machine
// to load data to the page

   cpFV.fryUID = [0,0,0,0,0,0,0,0];
   cpFV,drkUID = [0,0,0,0,0,0,0,0];
   cpFV.nugUID = [0,0,0];
   cpFV.wngUID = [0,0,0];
   
 	fvStartTime(); 	  	      
}

function fvMstrLoop() {
// fetching the data from the database
   getCPFry();
   getCPDrink();
   getCPNugget();
   getCPWing();    	
}

function fvStartTime() {
	cpFV.fdTimer = setInterval(fvMstrLoop, 3000);
}

function fvStopTime() {
	clearInterval(cpFV.fdTimer);
}

function fvFetchData(Selector) {
	
/* This function is used to fetch the data from the
   database to be stored in the cpInfo object. This
   will be used in other functions */
   	
   var cpFry = document.getElementsByClassName("ftype");	
   var cpDrink = document.getElementsByClassName("dtype");
   var cpNugget = document.getElementsByClassName("ntype");
   var cpWing = document.getElementsByClassName("wtype");      
   var cpButtons = document.getElementsByClassName("btnClass");     	  
   var cpWindow = document.getElementsByClassName("wintype");
   
      
   var DrinkAry1 = [8, 9, 10, 11, 12, 13, 14, 15];   
   var NugAry1 = [16, 17, 18];    
   var WngAry1 = [19, 20, 21];
    

   //var cpUrl="http://localhost:8500?" + Selector;
   var cpUrl;
   var i=0;
      
   switch(Selector) {
   	case 3:
        cpUrl="JSON/Wings.json";
        break;
      case 4:
        cpUrl="JSON/Nuggets.json";
        break;
      case 5:
        cpUrl="JSON/Fries.json";
        break;
      case 6:
        cpUrl="JSON/Drinks.json";
        break;
      default: break;       
   }
      
   var xmlhttp= new XMLHttpRequest();
   xmlhttp.open("GET", cpUrl, true);
   xmlhttp.send();   
   
   var CPrtnData;
   var CPtmp1;

   //cpWindow[WngAry1[j]].innerHTML 
      
   xmlhttp.onreadystatechange=function(){   
     if (xmlhttp.readyState==4 && xmlhttp.status==200) {
     	
     	  switch(Selector) {
     	  	
/************************
          Wings
 ************************/
     	  	 case 3:
     	  	      	  	 
             CPrtnData = JSON.parse(xmlhttp.responseText);
         
             for (var k=0; k<WngAry1.length; k++) {
                cpWindow[WngAry1[k]].innerHTML = "";
                cpWing[k].innerHTML = "";             	
             }
                 
             for (var x in CPrtnData) {
             
             // Storing the uid in global object             	
             	 cpFV.wngUID[i] = CPrtnData[x].uid;
             	         	   
        	       cpWindow[WngAry1[i]].innerHTML = "Tck " + CPrtnData[x].uid +
        	                                       "<br>" + CPrtnData[x].window;

        	       if (CPrtnData[x].SepBag == "YES") {
        	          cpWindow[WngAry1[i]].innerHTML += "<br> Sep Bag";
        	       }	                        
        	                                   	 
        	       CPtmp1 = CPrtnData[i].wtype.split("#");
        	  	
        	  	    for (var j=1; j<CPtmp1.length; j++) {       	  	  	  	  	
        	  	       cpWing[i].innerHTML += CPtmp1[j] + "<br>";        	  	  	  	   
        	  	    }
        	  	
                cpWindow[WngAry1[i]].style.display = "block";
                cpWing[i].style.display = "block";
                cpButtons[WngAry1[i]].style.display = "block";
                   	  	
        	       i++;  
             }    	  	 
     	       break;

/************************     	  	 
          Nuggets
 ************************/               	  	 
     	  	 case 4:     	  	 
     	  	 
             CPrtnData = JSON.parse(xmlhttp.responseText);
             
             for (var k=0; k<NugAry1.length; k++) {
                cpWindow[NugAry1[k]].innerHTML = "";
                cpNugget[k].innerHTML = "";             	
             }             
             
             for (var x in CPrtnData) {

             // Storing the uid in global object             	 
             	 cpFV.nugUID[i] = CPrtnData[x].uid;        	   

        	       cpWindow[NugAry1[i]].innerHTML = "Tck " + CPrtnData[x].uid +
        	                                       "<br>" + CPrtnData[x].window;

        	       if (CPrtnData[x].SepBag == "YES") {
        	          cpWindow[NugAry1[i]].innerHTML += "<br> Sep Bag";
        	       }	                        
        	                                   	 
        	       CPtmp1 = CPrtnData[i].ntype.split("#");
        	  	
        	  	    for (var j=1; j<CPtmp1.length; j++) {       	  	  	  	  	
        	  	       cpNugget[i].innerHTML += CPtmp1[j] + "<br>";        	  	  	  	   
        	  	    }
        	  	
                cpWindow[NugAry1[i]].style.display = "block";
                cpNugget[i].style.display = "block";
                cpButtons[NugAry1[i]].style.display = "block";
                   	  	
        	       i++;  
             }
             break;

/************************     	       
        Fries     	    
 ************************/           
     	  	 case 5:
     	  	      	  	    
             CPrtnData = JSON.parse(xmlhttp.responseText);
             
             for (var k=0; k<8; k++) {
                cpWindow[k].innerHTML = "";
                cpFry[k].innerHTML = "";             	
             }
             
             
             for (var x in CPrtnData) {

             // Storing the uid in global object 
             	 cpFV.fryUID[i] = CPrtnData[x].uid; 
             	         	   
        	       cpWindow[i].innerHTML = "Tck " + CPrtnData[x].uid +
        	                               "<br>" + CPrtnData[x].window;

        	       if (CPrtnData[x].SepBag == "YES") {
        	          cpWindow[i].innerHTML += "<br> Sep Bag";
        	       }	                        
        	                                   	 
        	       CPtmp1 = CPrtnData[i].ftype.split("#");
        	  	
        	  	    for (var j=1; j<CPtmp1.length; j++) {       	  	  	  	  	
        	  	       cpFry[i].innerHTML += CPtmp1[j] + "<br>";        	  	  	  	   
        	  	    }
        	  	
                cpWindow[i].style.display = "block";
                cpFry[i].style.display = "block";
                cpButtons[i].style.display = "block";
                   	  	
        	       i++;  
             }
                             	  	 
     	       break;
     	       
/************************     	       
          Drink
 ************************/               	       
     	  	 case 6:
     	  	 
             CPrtnData = JSON.parse(xmlhttp.responseText);

             for (var k=0; k<DrinkAry1.length; k++) {
                cpWindow[DrinkAry1[k]].innerHTML = "";
                cpDrink[k].innerHTML = "";             	
             }          
             
             for (var x in CPrtnData) {
             	
             // Storing the uid in global object 
             	 cpFV.drkUID[i] = CPrtnData[x].uid;                           	
        	   
        	       cpWindow[DrinkAry1[i]].innerHTML = "Tck " + CPrtnData[x].uid +
        	                               "<br>" + CPrtnData[x].window;

        	       if (CPrtnData[x].SepBag == "YES") {
        	          cpWindow[DrinkAry1[i]].innerHTML += "<br> Sep Bag";
        	       }	                        
        	                                   	 
        	       CPtmp1 = CPrtnData[i].dtype.split("#");
        	  	
        	  	    for (var j=1; j<CPtmp1.length; j++) {       	  	  	  	  	
        	  	       cpDrink[i].innerHTML += CPtmp1[j] + "<br>";        	  	  	  	   
        	  	    }
        	  	
                cpWindow[DrinkAry1[i]].style.display = "block";
                cpDrink[i].style.display = "block";
                cpButtons[DrinkAry1[i]].style.display = "block";
                   	  	
        	       i++;  
             }    	  	      	  	 
     	       break;
     	       
     	    default: break;
     	 }        	          
     	       
     }   // end if(xmlhttp.readyState==4)     
  }   	// end  xmlhttp.onreadystatechange     
  	
}

function getCPFry() {
	   
   fvFetchData(5);
}

function getCPDrink() {
	
   fvFetchData(6); 
}

function getCPNugget() {
	                  
   fvFetchData(4);
}

function getCPWing() {
	
   fvFetchData(3);	                        
}

function fvRemoveIt(id, mtype, elm) {
   var cpFry = document.getElementsByClassName("ftype");   	      
   var cpButtons = document.getElementsByClassName("btnClass");     	  
   var cpWindow = document.getElementsByClassName("wintype");
    
    var cpTmp;    
    var cpResults;
    var cpStr;
    var cpSendData; 
    
    var cpURL="http://"+window.location.hostname+":8500/cporder.com/?";   
    var xmlhttp= new XMLHttpRequest();
    
    fvStopTime();
               
    for (var i=0; i<cpButtons.length; i++) {
    	 if (cpButtons[i].id == id) {
          	 	
            switch(parseInt(mtype)) {
            	case 1:
             	  cpStr = cpFV.fryUID[elm];
             	  break;
             	
             	case 2:
             	  cpStr = cpFV.drkUID[elm];
             	  break;
             	               	
             	case 3:
             	  cpStr = cpFV.nugUID[elm];
             	  break;
             	
             	case 4:
             	  cpStr = cpFV.wngUID[elm];
             	  break;             	  
             	     	  
             	default: break;               	  
            }   
         
   /*************************
            Fries
    *************************/    
			   if (i<8) {

			    	cpSendData = "2&fcomplete&"+ cpStr;               
               xmlhttp.open("POST", cpURL, true);
               xmlhttp.send(cpSendData);
                             
               getCPFry();               		      	            		      
			    }

   /*************************
            Drinks
    *************************/			    
			    else if (i>7 && i<16) { 			        	 		    
			    
			    	cpSendData = "2&dcomplete&"+ cpStr;
               xmlhttp.open("POST", cpURL, true);
               xmlhttp.send(cpSendData);	
               
               getCPDrink();               		    	
			    }
			    
   /*************************
            Nuggets
    *************************/			    
			    else if (i>15 && i<18) {
			    	 	    
			    	cpSendData = "2&ncomplete&"+ cpStr;
               xmlhttp.open("POST", cpURL, true);
               xmlhttp.send(cpSendData);
               
               getCPNugget();               			    	
			    }
			    
   /*************************
            Wings
    *************************/			    
			    else {
			    	cpSendData = "2&wcomplete&"+ cpStr;
               xmlhttp.open("POST", cpURL, true);
               xmlhttp.send(cpSendData);
               
               getCPWing();               			    	
			    }    	                            	 	
    	 }
    }
       
    fvStartTime();     	
}


/* When the window loads, call the setup function */
window.addEventListener("load", fvSetup, false);
window.addEventListener("unload", fvStopTime, false);