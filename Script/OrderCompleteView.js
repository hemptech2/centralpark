// Defining a user's object   
   cpOrdCmpl = {
     rtnStr: "",	
     winPk: "",	
     uid: "",
     btype: "",
     wtype: "",
     ntype: "",
     ftype: "",
     dtype: "",
     samecar: "",
     phone: "",     
     MultiOrder: "",
     CarType: "",
     DisCnt: 0,
     cvTimer: 0,
     SepBag: "",
     DataSame: ["", "", "", "", "", ""]        
   };	

function OC_Setup() {	
	OC_ClearData();
}


function OC_ClearData() {
	document.getElementById("cvCustID").innerHTML = "";
	document.getElementById("cvCarDesc").innerHTML = "";
	document.getElementById("cvOrderItems").innerHTML = "";	

   cpOrdCmpl.uid = "";
  	cpOrdCmpl.winPk = "";
  	cpOrdCmpl.btype = "none";	
  	cpOrdCmpl.wtype = "none";
  	cpOrdCmpl.ntype = "none";
  	cpOrdCmpl.ftype = "none";
  	cpOrdCmpl.dtype = "none";	        	   	        	   	        	   	        	   
   cpOrdCmpl.samecar = "NO";
   cpOrdCmpl.phone = "none";     
   cpOrdCmpl.CarType = "none";
   cpOrdCmpl.DisCnt = 0;
   cpOrdCmpl.SepBag = "NO";	
   	
	
	OC_FetchData();
	//setTimeout(ShowData, 75);
	//OC_ShowData();
	OC_StartTime();
}

function OC_StartTime() {
	cpOrdCmpl.cvTimer = setInterval(OC_ShowData, 1000);
}

function OC_StopTime() {
	clearInterval(cpOrdCmpl.cvTimer);
}

function OC_FetchData() {
	
   var cpUrl="http://"+window.location.hostname+"/cporder.com/JSON/OrdCmpl.json"; 
	//var cpUrl="http://localhost:8500?7";
	//cpUrl="JSON/OrdCmpl.json";
   var xmlhttp= new XMLHttpRequest();
   xmlhttp.open("GET", cpUrl, true);
   xmlhttp.send();   
   
   var CPrtnData;
   var CPtmp1;
   var CPtmp2;
      
   xmlhttp.onreadystatechange=function(){   
     if (xmlhttp.readyState==4 && xmlhttp.status==200) {
     	
        CPrtnData = JSON.parse(xmlhttp.responseText);

	     cpOrdCmpl.uid = CPrtnData[0].uid
	     OC_ShowData();
	     
/*	     
	     cpOrdCmpl.winPk = CPrtnData[0].window;
	     cpOrdCmpl.btype = CPrtnData[0].btype;	
	     cpOrdCmpl.wtype = CPrtnData[0].wtype;
	     cpOrdCmpl.ntype = CPrtnData[0].ntype;
	     cpOrdCmpl.ftype = CPrtnData[0].ftype;
	     cpOrdCmpl.dtype = CPrtnData[0].dtype;   	   	        	   	        	   	        	   
	     cpOrdCmpl.samecar = CPrtnData[0].samecar;
	     cpOrdCmpl.phone = CPrtnData[0].phone;
	     cpOrdCmpl.CarType = CPrtnData[0].cartype;
	     cpOrdCmpl.DisCnt = CPrtnData[0].mOrdCnt;
	     cpOrdCmpl.SepBag = CPrtnData[0].SepBag;        
*/   	  
     }     // end if(xmlhttp.readyState)
     
  }        // end xmlhttp.onreadystatechange
}

function OC_ShowData() {
        
   var CPtmp3;

/*   	
	//alert("got here");
	if (cpOrdCmpl.samecar == "YES") {
	  	document.getElementById("cvCustID").innerHTML = cpOrdCmpl.winPk + "<br> Mutiple Order: Yes <br>" +
	  	                                                "Ticket:  " + cpOrdCmpl.uid +
	  	                                                "<br> Order:  " + cpOrdCmpl.DisCnt; 
	}
	else {
  	   document.getElementById("cvCustID").innerHTML = cpOrdCmpl.winPk + "<br> Ticket:  " + cpOrdCmpl.uid;
	}
	
	document.getElementById("cvCarDesc").innerHTML = "type/local <br>" + cpOrdCmpl.CarType;

   if (cpOrdCmpl.btype == "none" && cpOrdCmpl.wtype == "none" &&
       cpOrdCmpl.ntype == "none" && cpOrdCmpl.ftype == "none" &&
       cpOrdCmpl.dtype == "none") {
       	document.getElementById("cvOrderItems").innerHTML = "";
   }
       	   
/***************************************************   
   Getting the sandwhiches info to be displayed
 **************************************************     
   if (cpOrdCmpl.btype !== "none") {
   	//alert(cpOrdCmpl.DataSame);
   	if (cpOrdCmpl.DataSame[0] !== "YES") {
	      CPtmp3 = cpOrdCmpl.btype.split("#");   
	   
	      for (var i=1; i<CPtmp3.length; i++) {
		      document.getElementById("cvOrderItems").innerHTML += CPtmp3[i] + "<br/>";   
		   }
		   cpOrdCmpl.DataSame[0] = "YES";
		 }
		 //else { cpOrdCmpl.DataSame = "YES"; }  
	}
	
/***************************************************   
   Getting the wings info to be displayed
 **************************************************
   if (cpOrdCmpl.wtype !== "none") {
      CPtmp3 = cpOrdCmpl.wtype.split("#");   
      
      //alert(cpOrdCmpl.DataSame[1]);
   	if (cpOrdCmpl.DataSame[1] !== "YES") {
	      //CPtmp3 = cpOrdCmpl.btype.split("#");   
	   
	      for (var i=1; i<CPtmp3.length; i++) {
		      document.getElementById("cvOrderItems").innerHTML += CPtmp3[i] + "<br/>";   
		   }
		   cpOrdCmpl.DataSame[1] = "YES";
		 }
	} 

/***************************************************   
   Getting the nuggest info to be displayed
 **************************************************
   if (cpOrdCmpl.ntype !== "none") {
      CPtmp3 = cpOrdCmpl.ntype.split("#");   
   
   	if (cpOrdCmpl.DataSame[2] !== "YES") {
	      //CPtmp3 = cpOrdCmpl.btype.split("#");   
	   
	      for (var i=1; i<CPtmp3.length; i++) {
		      document.getElementById("cvOrderItems").innerHTML += CPtmp3[i] + "<br/>";   
		   }
		   cpOrdCmpl.DataSame[2] = "YES";
		 }
	}
	
/***************************************************   
   Getting the fries info to be displayed
 **************************************************
   if (cpOrdCmpl.ftype !== "none") {
   	//alert(cpOrdCmpl.ftype);
      CPtmp3 = cpOrdCmpl.ftype.split("#");   

   	if (cpOrdCmpl.DataSame[3] !== "YES") {
	      //CPtmp3 = cpOrdCmpl.btype.split("#");   
	   	      
	      for (var i=1; i<CPtmp3.length; i++) {
		      document.getElementById("cvOrderItems").innerHTML += CPtmp3[i] + "<br/>";   
		   }
		   cpOrdCmpl.DataSame[3] = "YES";
		 }
	}
	
/***************************************************   
   Getting the drink info to be displayed
 **************************************************
   if (cpOrdCmpl.dtype !== "none") {
      CPtmp3 = cpOrdCmpl.dtype.split("#");   
   
   	if (cpOrdCmpl.DataSame[4] !== "YES") {
	      //CPtmp3 = cpOrdCmpl.btype.split("#");   
	   
	      for (var i=1; i<CPtmp3.length; i++) {
		      document.getElementById("cvOrderItems").innerHTML += CPtmp3[i] + "<br/>";   
		   }
		   cpOrdCmpl.DataSame[4] = "YES";
		 }
	}
*/	document.getElementById("cvImage").src = "PNG/tck" + cpOrdCmpl.uid + ".png";  		 	  
 	
}

function OC_RemoveIt() {

    var cpSendData;
    var fItems = document.getElementById("cvOrderItems").innerHTML 
    
    //var cpURL="http://localhost:8500?";
    var cpURL="http://"+window.location.hostname+":8500/cporder.com/?";   
    var xmlhttp= new XMLHttpRequest();
    
    OC_StopTime();
    //alert(cpOrdCmpl.uid);
    if (cpOrdCmpl.uid !== "") {
	   cpSendData = "2&ordcomplete&"+ cpOrdCmpl.uid;               
      xmlhttp.open("POST", cpURL, true);
      xmlhttp.send(cpSendData); 
    }
    
    for (var i=0; i<6; i++) {
      cpOrdCmpl.DataSame[i]= "";
    }  
    
    OC_ClearData();    
    OC_StartTime();
                 	
}

function ReceiptView() {
	document.location.href = "receiptCanvas.html";
}

/* When the window loads, call the setup function */
window.addEventListener("load", OC_Setup, false);
window.addEventListener("unload", OC_StopTime, false);