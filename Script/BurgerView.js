cpBV = {
    prevRead: "",
    CurRead: ""
};

function BVSetup() {
	      
// Clearing out everything and setting to default position
   bvClearData();
 	  	  	    
   bvTimer = setInterval(getCPdata, 2000);  
}

function bvRemoveIt(id) {
   	
    var cpButtons = document.getElementsByClassName("btnClass");
    var cpWindow = document.getElementsByClassName("wintype");
    var cpSndWhch = document.getElementsByClassName("btype");
    
    var cpTmp;    
    var cpResults;
    var cpStr;
    var cpSendData; 
    
    BVStopTime();
       
    for (var i=0; i<cpButtons.length; i++) {
    	 if (cpButtons[i].id == id) {

    	 	cpTmp= cpWindow[i].innerHTML;
    	 	cpResults= cpTmp.search("Tck");
    	 	
    	 	if(cpResults !== -1){
            cpStr= cpTmp.replace(/Tck|<br>|A SIDE|B SIDE|Sep Bag/g, "");
            cpSendData = "2&bcomplete&"+ cpStr;                	 	
    	 	}
    	 }
    }
    
    var cpURL="http://"+window.location.hostname+":8500/cporder.com/?";
   
    var xmlhttp= new XMLHttpRequest();
    xmlhttp.open("POST", cpURL, true);
    xmlhttp.send(cpSendData);
    
    //setup(); 
    //bvClearData();    
    BVStartTime();     	
}

function BVStartTime() {
	bvTimer = setInterval(getCPData, 2000);
}

function BVStopTime() {
	clearInterval(bvTimer);
}

function bvClearData() {
	
   var cpSndWhch = document.getElementsByClassName("btype");
   var cpButtons = document.getElementsByClassName("btnClass");     	  
   var cpWindow = document.getElementsByClassName("wintype");
      
// Clearing out everything and setting to default position
   for (var i=0; i<cpButtons.length; i++) {
      cpWindow[i].innerHTML="";
      cpSndWhch[i].innerHTML="";
      
//      cpWindow[i].style.display= "none";
//      cpSndWhch[i].style.display= "none";               	
      cpButtons[i].style.display= "none";      
   }
   
// fetching the data from the database
   getCPdata();   	
}

function getCPdata() {
	
   var cpSndWhch = document.getElementsByClassName("btype");
   var cpButtons = document.getElementsByClassName("btnClass");     	  
   var cpWindow = document.getElementsByClassName("wintype");	

   //var cpUrl="http://localhost:8500?2";
   var cpUrl="JSON/SndWh.json";
   //var cpUrl="http://"+window.location.hostname+"/cporder.com/JSON/SndWh.json";   
   var xmlhttp= new XMLHttpRequest();
   xmlhttp.open("GET", cpUrl, true);
   xmlhttp.send();
      
   xmlhttp.onreadystatechange=function(){   
     if (xmlhttp.readyState==4 && xmlhttp.status==200) {     	  
     	       	  
        var CPrtnData = JSON.parse(xmlhttp.responseText);
        var i = 0;
        var bvTest = false;

//{"uid" : 4, "window" : "A SIDE", "btype" : "#1 1/2 GRILL TOM MAYO LET ", "SepBag" : "NO"},        
        for (var x in CPrtnData) {

        	  cpBV.CurRead += "" + CPrtnData[x].uid + CPrtnData[x].window + 
        	                  CPrtnData[x].btype + CPrtnData[x].SepBag;        	                              	                     
        }
        
        if (cpBV.prevRead == "") {
           cpBV.prevRead = cpBV.CurRead;       	                      
        }
        else {
           if (cpBV.prevRead == cpBV.CurRead) {
           	  bvTest = true;
           }
           else {
           	  cpBV.prevRead = cpBV.CurRead;
           }	
        }         
        
        if (!bvTest) {
        	
           bvClearData();        	        
           for (var x in CPrtnData) {
        	   
        	      cpWindow[i].innerHTML = "Tck " + CPrtnData[x].uid +
        	                              "<br/>" + CPrtnData[x].window;

        	      if (CPrtnData[x].SepBag == "YES") {
        	         cpWindow[i].innerHTML += "<br/> Sep Bag";
        	      }	                        
        	                                   	 
        	      CPtmp3 = CPrtnData[i].btype.split("#");
        	  	
        	  	   for (var j=1; j<CPtmp3.length; j++) {       	  	  	  	  	
        	  	      cpSndWhch[i].innerHTML += CPtmp3[j] + "<br/>";        	  	  	  	   
        	  	   }
        	  	
               //cpWindow[i].style.display = "block";
               //cpSndWhch[i].style.display = "block";
               cpButtons[i].style.display = "block";
                   	  	
        	      i++;  
           }
        
        }           // end if(!bvTest)                
     }              // end if(xmlhttp.readyState==4)
    
   }   	           // end  xmlhttp.onreadystatechange		
}

/* When the window loads, call the setup function */
window.addEventListener("load", BVSetup, false);
window.addEventListener("unload", BVStopTime, false);